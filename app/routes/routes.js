var User = require("../models/user");
var History = require("../models/history");
var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");
var routes = require("require-dir")();

module.exports = function(router) {
    
    router.route('/authenticate')
        .post(function(req, res) { 
            User.findOne({
               email: new RegExp('^' + req.body.email + '$', 'i'),
               deleted: false
            }, {'password':1, 'name': 1, 'email': 1, 'active': 1}, function(err, user) {
                if (err)
                    return res.status(500).send(err);
                    
                if (!user) {
                    History.create({timestamp: new Date(), activity: 'Login not successful - ' + req.body.email}, function(err, history) {
                        res.status(400).json({message: 'User Not Found'});
                    });
                } else if (user) {
                    if (user.active) {
                        bcrypt.compare(req.body.password, user.password, function(err, response) {
                            if (err)
                                return res.status(500).send(err);

                            if (response) {
                                var token = jwt.sign(user, process.env.ITDOCS_SECRET, {
                                    expiresIn: 86400
                                });
                                
                                var cleanUser = user.toObject();
                                delete cleanUser['password'];

                                History.create({user: user._id, timestamp: new Date(), activity: 'Login Successful'}, function(err, history) {
                                    res.status(200).json({message: 'Authentication successful', token: token, user: cleanUser});
                                });
                            } else {
                                History.create({user: user._id, timestamp: new Date(), activity: 'Login not successful, password incorrect'}, function(err, history) {
                                    return res.status(400).json({message: 'Password Incorrect'});
                                });
                            }
                        });
                    } else {
                        History.create({user: user._id, timestamp: new Date(), activity: 'Login attempted by inactive user'}, function(err, history) {
                            return res.status(400).json({message: 'Inactive User'});
                        });
                    }
                }
            });
        });
        
    // middleware to use for all requests
    router.use(function(req, res, next) {
        // do logging

        var token = req.params.token || req.headers.authorization;
        if (token) {
                
                var bearer = token.split(" ");
                var bearerToken = bearer[1];
                
                jwt.verify(bearerToken, process.env.ITDOCS_SECRET, function(err, decoded) {
                    if (err)
                        return res.status(403).json({message: 'Failed to authenticate token'});
                    
                    req.decoded = decoded;

                    next(); // make sure we go to the next routes and don't stop here
                });
        } else {
            res.status(403).send({
                message: 'No token provided'
            });
        };
    });
    
    // Loops through DIR and includes routes
    Object.keys(routes).forEach(function(routeName) {
        require('./' + routeName)(router);
    });
}