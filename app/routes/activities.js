var Activity = require('../models/activity');

module.exports = function(router) {

    router.route('/activities')
        .get(function(req, res) {
            var query = {};
            query['deleted'] = false;

            if (req.query.type) {
                query['type'] = req.query.type;
            }

            Activity.find(query)
             .exec(function(err, activity) {
                if (err)
                    return res.status(500).send(err);
                    
                res.status(200).json(activity);
            });
        })
        
        .post(function(req, res) {
            var activity = new Activity();

            activity.details = req.body.details;
            activity.type = req.body.type;
            
            activity.save(function(err) {
                if (err)
                    return res.status(500).send(err);

                var cleanActivity = activity.toObject();
                delete cleanActivity['deleted'];

                res.status(200).json(cleanActivity);
            });
        })
    ;
    
    router.route('/activities/:id')
        .get(function(req, res) {
            Activity.findById(req.params.id)
                .exec(function(err, activity) {
                    if (err)
                        return res.status(500).send(err);
                        
                    res.status(200).json(activity);
                });
        })
        
        .put(function(req, res) {
            Activity.findById(req.params.id, function(err, activity) {
                if (err)
                    return res.status(500).send(err);
                
                activity.details= req.body.details;
                activity.type = req.body.type;
                    
                activity.save(function(err) {
                    if(err)
                        return res.status(500).send(err);
                        
                    res.status(200).json(activity);
                });
            });
        })
        
        .delete(function(req, res) {
            Activity.findById(req.params.id, function(err, activity) {
                activity.deleted = true;
                activity.save(function(err) {
                    if (err)
                        return res.status(500).send(err);

                    res.status(200).json({message: "Activity Deleted"});
                })
            });
        })
    ;
       
}