var User = require('../models/user');
var History = require('../models/history');
var bcrypt = require('bcryptjs');
var Q = require('q');

function hashPassword(password) {
    var deferred = Q.defer();

    if (password) {
        bcrypt.hash(password, parseInt(process.env.ITDOCS_SALT), function(err, hash) {
            if (err) {
                deferred.reject(err);
            }

            deferred.resolve(hash);
        });
    } else {
        deferred.resolve(false);
    }

    return deferred.promise;
}

module.exports = function(router) {

	router.route('/users')
        .get(function(req, res) {
            User.find({deleted: false}, function(err, users) {
                if (err)
                    return res.status(500).send(err);

                res.status(200).json(users);
            });
        })
        
        .post(function(req, res) {
            var user = new User();
            user.name = req.body.name;
            user.email = req.body.email;
            user.active = req.body.active;

            User.findOne({email: new RegExp('^' + user.email + '$', 'i'), deleted: false}, function(err, data){
                if (err)
                    return res.status(500).send(err);
                
                if (data) {
                    res.status(409).json({code: 100, message: 'Email Already Exists'});
                } else {
                    hashPassword(req.body.password).then(function(response) {
                        user.password = response;

                        user.save(function(err) {
                            if (err)
                                return res.status(500).send(err);
                            
                            // Change use to an object, so I can remove the password, so it isn't sent back with the JSON object.
                            var newUser = user.toObject();
                            delete newUser['password'];
                            delete newUser['deleted'];
                            
                            res.status(200).json(newUser);
                        });
                    }, function(error) {
                        return res.status(500).send(err);
                    });
                }
            });
        })
    ;
    
    router.route('/users/:id')
        .get(function(req, res) {
            User.findById(req.params.id)
                .exec(function(err, user) {
                    if (err)
                        return res.status(500).send(err);
                        
                    res.status(200).json(user);
                });
        })
        
        .put(function(req, res) {
            // console.log(req.params);
            User.findById(req.params.id, function(err, user) {
                if (err)
                    return res.status(500).send(err);
                    
                /* Add your field updates here */
                user.name = req.body.name;
                user.email = req.body.email;
                user.active = req.body.active;

                User.findOne({email: new RegExp('^' + user.email + '$', 'i'), deleted: false}, function(err, response){
                    if (err)
                        return res.status(500).send(err);
                    
                    if (response && response._id != req.params.id) {
                        res.status(409).json({code: 100, message: 'Email Already Exists'});
                    } else {
                        hashPassword(req.body.password).then(function(response) {
                            if (response) {
                                user.password = response;
                            }

                            user.save(function(err) {
                                if(err)
                                    return res.status(500).send(err);
                                    
                                var updatedUser = user.toObject();
                                delete updatedUser['password'];
                                
                                res.status(200).json(updatedUser);
                            });
                        }, function(error) {
                            return res.status(500).send(err);
                        });

                    }
                });
            });
        })
        
        .delete(function(req, res) {
            User.findById(req.params.id, function(err, user) {
                if (user.admin) {
                    res.status(403).json({message: 'Can not delete admin'});
                } else {
                    user.deleted = true;
                    user.save(function(err) {
                        if (err)
                            return res.status(500).send(err);

                        res.status(200).json({message: 'User Deleted'});
                    })
                }
            });
        })
    ;

    router.route('/users/changepassword')
        .post(function(req, res) {
            var currentPassword, newPassword, repeatNewPassword;
            
            if (req.body.currentpassword === undefined) {
                currentPassword = '';
            } else {
                currentPassword = req.body.currentpassword;    
            }
            
            if (req.body.newpassword === undefined || req.body.newpassword === '') {
                return res.status(400).json({code: 103, message: 'New password is required'});
            } else {
                newPassword = req.body.newpassword;
            }

            if (req.body.repeatpassword === undefined || req.body.repeatpassword === '') {
                return res.status(400).json({code: 104, message: 'Repeat password is required'});
            } else {
                repeatNewPassword = req.body.repeatpassword;
            }            

            if (newPassword === repeatNewPassword) {

                User.findById(req.decoded._doc._id, {'password':1, 'name': 1, 'email': 1, 'active': 1}, function(err, user) {
                    if (err)
                        return res.status(500).send(err);

                    if (user) {
                        bcrypt.compare(currentPassword, user.password, function(err, response) {
                            if (err)
                                return res.status(500).send(err);

                            if (response) {
                                hashPassword(newPassword).then(function(response) {
                                    user.password = response;

                                    user.save(function(err) {
                                        if (err)
                                            return res.status(500).send(err);
                                        
                                        // Change use to an object, so I can remove the password, so it isn't sent back with the JSON object.
                                        var newUser = user.toObject();
                                        delete newUser['password'];
                                        delete newUser['deleted'];
                                        
                                        res.status(200).json({message: 'Password has been changed'});
                                    });
                                }, function(error) {
                                    return res.status(500).send(err);
                                });
                            } else {
                                History.create({user: user._id, timestamp: new Date(), activity: 'Change password error: current password incorrect'}, function(err, history) {
                                    return res.status(400).send({code: 102, message: "Current Password Incorrect"});
                                });
                            }
                        });
                    } else {
                        History.create({timestamp: new Date(), activity: 'Change Password: User does not exist'}, function(err, history) {
                            return res.status(400).send({code: 100, message: "User does not exist"});
                        });
                    }
                });
            } else {
                return res.status(400).send({code: 101, message: "New passwords do not match"});
            }
        })
    ;
}