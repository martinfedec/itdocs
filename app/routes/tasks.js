var Task = require('../models/task');
var User = require('../models/user');
var Activity = require('../models/activity');
var Q = require('q');

module.exports = function(router) {

	router.route('/tasks')
        .get(function(req, res) {
            // Default Query
            var recordLimit = 20;

            if (req.query.top) {
                recordLimit = parseInt(req.query.top);
            }

            // PAGING
            var page = 0;
            if (req.query.page) {
                page = Math.max(0, req.query.page - 1);
            }

            var query = {
                deleted: false,
                completed: false
            };

            var sort = {
                createdat: 'desc'
            };

            if (req.query.q) {
                if (req.query.q == 'completed') {
                    query.completed = true;
                }
            }

            if (req.query.created) {
                var author = req.query.created.replace(/'/g, "");
                query['createdby_docs.name']= { $eq: author };
            }

            if (req.query.assignedto) {
                if (req.query.assignedto == 'unassigned') {
                     query.assignedto = null;
                } else {
                    var assignedto = req.query.assignedto.replace(/'/g, "");
                    query['assignedto_docs.name']= { $eq: assignedto };
                }
            }

            if (req.query.client) {
                var client = req.query.client.replace(/'/g, "");
                query['client_docs.name']= { $eq: client };
            }

            if (req.query.asset) {
                var asset = req.query.asset.replace(/'/g, "");
                query['asset_docs.name']= { $eq: asset };
            }

            if(req.query.sort) {
                sort.createdat = req.query.sort;
            }

            Task
                .aggregate([
                    {
                        $lookup: {
                            from: "users",
                            localField: "createdby",
                            foreignField: "_id",
                            as: "createdby_docs"
                        }
                    },
                    {
                            $lookup: {
                            from: "users",
                            localField: "assignedto",
                            foreignField: "_id",
                            as: "assignedto_docs"
                        }
                    },
                    {
                            $lookup: {
                            from: "clients",
                            localField: "client",
                            foreignField: "_id",
                            as: "client_docs"
                        }
                    },
                    {
                            $lookup: {
                            from: "assets",
                            localField: "asset",
                            foreignField: "_id",
                            as: "asset_docs"
                        }
                    },
                    {
                        $match: query
                    },
                    {
                        $project: {
                            _id: 1,
                            title: 1,
                            description: 1,
                            completed: 1,
                            completedat: 1,
                            client: 1,
                            asset: 1,
                            assignedto: 1,
                            createdby: 1, 
                            createdat: 1,
                            checklists: 1,
                            active: 1,
                            lastupdatedat: 1,
                            lastupdatedby: 1,
                            activities: 1
                        }
                    }
                ])
                .sort(sort)
                .skip(recordLimit * page)
                .limit(recordLimit)
                .then(function(tasks) {
                // .cursor({})
                // .exec(function(err, tasks) {
                    // if (err)
                            // return res.status(500).send(err);
                        
                        Task.populate(tasks, [{path: 'assignedto', select: '_id name'}, {path: 'createdby', select: '_id name'}, {path: 'client', select: '_id name'}, {path: 'asset', select: '_id name'}, {path: 'lastupdatedby', select: '_id name'}, {path: 'checklists.items.completedby', select: '_id name'}, {path: 'activities', match: { deleted: false }, select: '_id details deleted'}], function(err, popTasks) {
                            var totalItems, completedItems;
                            for (var task = 0; task < tasks.length; task++) {
                                totalItems = 0;
                                completedItems = 0;
                                
                                for (var i = 0; i < tasks[task].checklists.length; i++) {
                                    for (var item = 0; item < tasks[task].checklists[i].items.length; item++) {
                                        totalItems += 1;

                                        if (tasks[task].checklists[i].items[item].completed) {
                                            completedItems += 1;
                                        }
                                    }
                                }

                                tasks[task].totalItems = totalItems;
                                tasks[task].completedItems = completedItems;
                            }
                            res.status(200).json(tasks);    
                        })
                        
                });
            ;
        })
        
        .post(function(req, res) {

            var task = new Task();
            task.title = req.body.title;
            task.description = req.body.description;
            task.client = req.body.client;
            task.asset = req.body.asset;
            task.assignedto = req.body.assignedto;
            task.createdby = req.decoded._doc._id;
            task.checklists = req.body.checklists;
            task.lastupdatedby = req.decoded._doc._id;
            
            if (req.body.completed != undefined) {
                task.completed = req.body.completed;
            }

            task.save(function(err) {
                if (err)
                    return res.status(500).send(err);

                Task.populate(task, [{path: 'assignedto', select: '_id name'}, {path: 'createdby', select: '_id name'}, {path: 'client', select: '_id name'}, {path: 'asset', select: '_id name'}, {path: 'lastupdatedby', select: '_id name'}, {path: 'checklists.items.completedby', select: '_id name'}, {path: 'activities', match: { deleted: false }, select: '_id details deleted'}], function(err, task) {
                    if (err)
                        return res.status(500).send(err);

                    var cleanTask = task.toObject();
                    delete cleanTask['deleted'];
                    res.status(200).json(cleanTask);
                });
            });
        })
    ;

    router.route('/tasks/count')
        .get(function(req, res) {
            var query = {
                deleted: false,
                completed: false
            };

            if (req.query.q) {
                if (req.query.q == 'completed') {
                    query.completed = true;
                }
            }

            if (req.query.created) {
                var author = req.query.created.replace(/'/g, "");
                query['createdby_docs.name']= { $eq: author };
            }

            if (req.query.assignedto) {
                if (req.query.assignedto == 'unassigned') {
                     query.assignedto = null;
                } else {
                    var assignedto = req.query.assignedto.replace(/'/g, "");
                    query['assignedto_docs.name']= { $eq: assignedto };
                }
            }

            if (req.query.client) {
                var client = req.query.client.replace(/'/g, "");
                query['client_docs.name']= { $eq: client };
            }

            if (req.query.asset) {
                var asset = req.query.asset.replace(/'/g, "");
                query['asset_docs.name']= { $eq: asset };
            }

            Task
                .aggregate([
                    {
                        $lookup: {
                            from: "users",
                            localField: "createdby",
                            foreignField: "_id",
                            as: "createdby_docs"
                        }
                    },
                    {
                            $lookup: {
                            from: "users",
                            localField: "assignedto",
                            foreignField: "_id",
                            as: "assignedto_docs"
                        }
                    },
                    {
                            $lookup: {
                            from: "clients",
                            localField: "client",
                            foreignField: "_id",
                            as: "client_docs"
                        }
                    },
                    {
                            $lookup: {
                            from: "assets",
                            localField: "asset",
                            foreignField: "_id",
                            as: "asset_docs"
                        }
                    },
                    {
                        $match: query
                    },
                    {
                        $project: {
                            _id: 1,
                            title: 1,
                            description: 1,
                            completed: 1,
                            completedat: 1,
                            client: 1,
                            asset: 1,
                            assignedto: 1,
                            createdby: 1, 
                            createdat: 1,
                            checklists: 1,
                            active: 1,
                            lastupdatedat: 1,
                            lastupdatedby: 1,
                            activities: 1
                        }
                    }
                ]).then(function(tasks) {
                    res.status(200).json({count: tasks.length});
                });
                // .exec(function(err, tasks) {

                //     if (err)
                //             return res.status(500).send(err);
                        
                //     res.status(200).json({count: tasks.length});                       
                // })
            ;
        })
    ;
    
    router.route('/tasks/:id')
        .get(function(req, res) {
            Task.findById(req.params.id)
                .populate([{path: 'assignedto', select: '_id name'}, {path: 'createdby', select: '_id name'}, {path: 'client', select: '_id name'}, {path: 'asset', select: '_id name'}, {path: 'lastupdatedby', select: '_id name'}, {path: 'checklists.items.completedby', select: '_id name'}, {path: 'activities', match: {deleted: false}, select: '_id details type'}])
                .exec(function(err, task) {
                    if (err)
                        return res.status(500).send(err);
                        
                    res.status(200).json(task);
                });
        })
        
        .put(function(req, res) {
            Task.findById(req.params.id, function(err, task) {
                if (err)
                    return res.status(500).send(err);
                    
                /* Add your field updates here */
                task.title = req.body.title;
                task.description = req.body.description;
                task.completed = req.body.completed;
                task.client = req.body.client;
                task.asset = req.body.asset;
                task.assignedto = req.body.assignedto;
                task.active = req.body.active;
                task.checklists = req.body.checklists;
                task.lastupdatedby = req.decoded._doc._id;
                    
                task.save(function(err) {
                    if(err)
                        return res.status(500).send(err);
                        
                    Task.populate(task, [{path: 'assignedto', select: '_id name'}, {path: 'createdby', select: '_id name'}, {path: 'client', select: '_id name'}, {path: 'asset', select: '_id name'}, {path: 'lastupdatedby', select: '_id name'}, {path: 'checklists.items.completedby', select: '_id name'}], function(err, task) {
                        if (err)
                            return res.status(500).send(err);

                        res.status(200).json(task);    
                    });
                });
            });
        })
        
        .delete(function(req, res) {
            Task.findById(req.params.id, function(err, task) {
                task.deleted = true;
                task.lastupdatedby = req.decoded._doc._id;

                task.save(function(err) {
                    if (err)
                        return res.status(500).send(err);

                    res.status(200).json({message: "Task Deleted"});
                })
            });
        })
    ;

    router.route('/tasks/:task_id/comments')
        .get(function(req, res) {
            Task.findOne({ _id: req.params.task_id, deleted: false })
                .populate({path: 'activities', match: {type: 'COMMENT', deleted: false}, select: '_id details type createdat createdby lastupdatedby lastupdatedat'})
                .exec(function(err, task) {
                    if (err)
                        return res.status(500).send(err);
                
                    Task.populate(task.activities, [{path: 'createdby', select: 'name'}, {path: 'lastupdatedby', select: 'name'}], function(err, activities) {

                        res.status(200).json(activities);
                    });                    
                });
        })

        .post(function(req, res) {
            var comment = new Activity();

            comment.details = req.body.details;
            comment.type = 'COMMENT';
            comment.createdby = req.decoded._doc._id;
            comment.lastupdatedby = req.decoded._doc._id;

            comment.save(function(err) {
                if (err)
                    return res.status(500).send(err);

                Task.findOne({ _id: req.params.task_id, deleted: false })
                    .exec(function(err, task) {
                        if (err)
                            return res.status(500).send(err);

                        task.activities.push(comment)

                        task.save(function(err) {
                            if (err)
                                return res.status(500).send(err);

                            var cleanComment = comment.toObject();
                            delete cleanComment['deleted'];

                            res.status(200).json(cleanComment);
                        });
                        
                    });
            });
        })
    ;

    router.route('/tasks/:task_id/comments/:id')
        .put(function(req, res) {
            Activity.findById(req.params.id, function(err, activity) {
                activity.details = req.body.details;
                activity.lastupdatedby = req.decoded._doc._id;

                activity.save(function(err) {
                    if(err)
                        return res.status(500).send(err);

                    res.status(200).json(activity);
                });
            });
        })

        .delete(function(req, res) {
            Activity.findById(req.params.id, function(err, activity) {
                activity.deleted = true;
                activity.lastupdatedby = req.decoded._doc._id;

                activity.save(function(err) {
                    if(err)
                        return res.status(500).send(err);

                    res.status(200).json({message: "Comment Deleted"});
                });
            });
        })
    ;
}