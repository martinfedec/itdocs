var TemplateType = require('../models/templateType');

module.exports = function(router) {

	router.route('/templatetypes')
        .get(function(req, res) {
            TemplateType.find({deleted: false}, function(err, templatetype) {
                if (err)
                    return res.status(500).send(err);
                    
                res.status(200).json(templatetype);
            });
        })
        
        .post(function(req, res) {
            var templatetype = new TemplateType();

            templatetype.description = req.body.description;
            templatetype.icon = req.body.icon;
            
            TemplateType.findOne({description: new RegExp('^' + templatetype.description + '$', 'i'), deleted: false}, function(err, response){
                if (err)
                    return res.status(500).send(err);
                
                if (response) {
                    res.status(409).json({message: 'TemplateType Already Exists'});
                } else {
                    templatetype.save(function(err) {
                        if (err)
                            return res.status(500).send(err);

                        var cleanTemplateType = templatetype.toObject();
                        delete cleanTemplateType['deleted'];

                        res.status(200).json(cleanTemplateType);
                    });
                }
            });
        })
    ;
    
    router.route('/templatetypes/:id')
        .get(function(req, res) {
            TemplateType.findById(req.params.id)
                .exec(function(err, templatetype) {
                    if (err)
                        return res.status(500).send(err);
                        
                    res.status(200).json(templatetype);
                });
        })
        
        .put(function(req, res) {
            TemplateType.findById(req.params.id, function(err, templatetype) {
                if (err)
                    return res.status(500).send(err);
                
                templatetype.description = req.body.description;
                templatetype.icon = req.body.icon;
                // templatetype.active = req.body.active;
                    
                templatetype.save(function(err) {
                    if(err)
                        return res.status(500).send(err);
                        
                    res.status(200).json(templatetype);
                });
            });
        })
        
        .delete(function(req, res) {
            TemplateType.findById(req.params.id, function(err, templatetype) {
                templatetype.deleted = true;
                templatetype.save(function(err) {
                    if (err)
                        return res.status(500).send(err);

                    res.status(200).json({message: "TemplateType Deleted"});
                })
            });
        })
    ;
       
}