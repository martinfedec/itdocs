var _ = require("lodash");
var Client = require('../models/client');
var Asset = require('../models/asset');
var Q = require("q")

function parseScript(template) {
    // Function that will replace %variable_name% with the actual content defined in the fields
    // ie) field name: company_name, value will replace all instances of %company_name%

    var deferred = Q.defer();

    var convertedScript = template.script;

    if (convertedScript) {
        if (template.fields) {
            template.fields.forEach(function(field) {
                var placeholder = "%" + field.name.toLowerCase() + "%";
                // convertedScript = convertedScript.replace(new RegExp(placeholder, 'g'), "'" + field.value + "'");
                convertedScript = convertedScript.replace(new RegExp(placeholder, 'gi'), field.value);
            });

            deferred.resolve(convertedScript);    
        } else {
            // This will never get called since we always add 1 field on the template.
            // Need to make it so the front end doesn't save a blank field, once it does then we can just pass the 
            // script back.
            deferred.resolve(template.script);
        }
    } else {
        deferred.resolve();
    }

    return deferred.promise;
};

module.exports = function(router) {

	router.route('/clients')
        .get(function(req, res) {
            Client.find({deleted: false})
                .populate([{path: 'createdby', select: '_id name'}, {path: 'lastupdatedby', select: '_id name'}])
                .exec(function(err, items) {
                    if (err)
                        return res.status(500).send(err);
                        
                    res.status(200).json(items);
                });
        })
        
        .post(function(req, res) {
            var client = new Client();
           
            client.name = req.body.name;
            client.website = req.body.website;
            client.businesshours = req.body.businesshours;
            client.description = req.body.description;
            client.primarylocation = req.body.primarylocation;
            client.lastupdatedby = req.decoded._doc._id;
            client.createdby = req.decoded._doc._id;
           	
            Client.findOne({name: new RegExp('^' + client.name + '$', 'i'), deleted: false}, function(err, data){
                if (err)
                    return res.status(500).send(err);
                
                if (data) {
                    res.status(409).json({code: 100, message: 'Client Already Exists'});
                } else {
                    client.save(function(err) {
                        if (err)
                            return res.status(500).send(err);

                        Client.populate(client, [{path: 'createdby', select: '_id name'}, {path: 'lastupdatedby', select: '_id name'}], function(err, client) {
                            if (err)
                                return res.status(500).send(err);

                            var cleanClient = client.toObject();
                            delete cleanClient['deleted'];

                            res.status(200).json(cleanClient);    
                        });
                        
                    });
                }
            });
        })
    ;
    
    router.route('/clients/:id')
        .get(function(req, res) {
            Client.findById(req.params.id)
                .populate([{path: 'assets', match: { deleted: false } }, {path: 'createdby', select: '_id name'}, {path: 'lastupdatedby', select: '_id name'}])
                .exec(function(err, client) {
                    if (err)
                        return res.status(500).send(err);
                        
                    res.status(200).json(client);
                });
        })
        
        .put(function(req, res) {
            Client.findOne({name: new RegExp('^' + req.body.name + '$','i'), deleted: false}, function(err, data) {
                if (err)
                    return res.status(500).send(err);

                if (data && data._id != req.params.id) {
                    res.status(409).json({code: 100, message: 'Client Already Exists'});
                } else {
                    Client.findById(req.params.id, function(err, client) {
                        if (err)
                            return res.status(500).send(err);
                            
                        client.name = req.body.name;
                        client.website = req.body.website;
                        client.businesshours = req.body.businesshours;
                        client.description = req.body.description;
                        client.primarylocation = req.body.primarylocation;
                        client.active = req.body.active;
                        client.lastupdatedby = req.decoded._doc._id;

                        client.save(function(err) {
                            if(err)
                                return res.status(500).send(err);
                            
                            Client.populate(client, [{path: 'createdby', select: '_id name'}, {path: 'lastupdatedby', select: '_id name'}], function(err, client) {
                                if (err)
                                    return res.status(500).send(err);

                                res.status(200).json(client);
                            });
                            
                        });
                    });
                }
            });
        })
        
        .delete(function(req, res) {
            Client.findById(req.params.id, function(err, client) {
                client.deleted = true;
                client.lastupdatedby = req.decoded._doc._id;
                client.save(function(err) {
                    if (err)
                        return res.status(500).send(err);

                    res.status(200).json({message: "Client Deleted"});
                })
            });
        })
    ;

    router.route('/clients/:client_id/locations')
        .get(function(req, res) {
            Client.findById(req.params.client_id, function(err, client) {
                if (err)
                    return res.status(500).send(err);

                res.status(200).json(client.locations);
            });
        })

        .post(function(req, res) {
            var newLocation = {
                name: req.body.name,
                address1: req.body.address1,
                address2: req.body.address2,
                city: req.body.city,
                province: req.body.province,
                country: req.body.country,
                postalcode: req.body.postalcode,
                phonenumber: req.body.phonenumber,
                faxnumber: req.body.faxnumber,
                email: req.body.email,
                active: req.body.active
            };
            
            Client.findById(req.params.client_id, function(err, client) {
                if (err)
                    return res.status(500).send(err);

                if (_.findIndex(client.locations, {name: newLocation.name}) == -1) {
                    var newLength = client.locations.push(newLocation);
                    
                    client.lastupdatedby = req.decoded._doc._id;
                    client.save(function(err) {
                         if (err)
                             return res.status(500).send(err);
                         
                        res.status(200).json(client.locations[newLength - 1]);
                    });
                } else {
                    res.status(409).json({code: 100, message: 'Location Already Exists'});
                }
            });
        })
    ;

    router.route('/clients/:client_id/locations/:id')
        .get(function(req, res) {
            Client.findById(req.params.client_id, function(err, client) {
                if (err)
                    return res.send(500).send(err);

                res.status(200).json(client.locations.id(req.params.id));
            });
        })

        .put(function(req, res) {
            Client.findById(req.params.client_id, function(err, client) {
                if (err)
                    return res.send(500).send(err);

                var checkLocation = _.findIndex(client.locations, {name: req.body.name});
    
                if (checkLocation != -1 && client.locations[checkLocation]._id != req.params.id) {
                    res.status(409).json({code: 100, message: 'Location Already Exists'});
                } else {
                    var location = client.locations.id(req.params.id);
                    location.name = req.body.name;
                    location.address1 = req.body.address1;
                    location.address2 = req.body.address2;
                    location.city = req.body.city;
                    location.province = req.body.province;
                    location.country = req.body.country;
                    location.postalcode = req.body.postalcode;
                    location.phonenumber = req.body.phonenumber,
                    location.faxnumber = req.body.faxnumber,
                    location.email = req.body.email,
                    location.active = req.body.active;

                    client.lastupdatedby = req.decoded._doc._id;
                    client.save(function(err) {
                        if (err)
                            return res.send(500).send(err);

                        res.status(200).json(location);
                    });
                }
            });
        })

        .delete(function(req, res) {
            Client.findById(req.params.client_id, function(err, client) {
                if (err)
                    return res.send(500).send(err);

                if (client.primarylocation === req.params.id) {
                    res.status(409).json("LOCATION_ASSIGNED_EXISTS");
                } else {
                    client.lastupdatedby = req.decoded._doc._id;
                    client.locations.id(req.params.id).remove();
                    client.save(function(err) {
                        if (err)
                            return res.send(500).send(err);

                        res.status(200).json(client);
                    });
                } 
            });
        })
    ;

    router.route('/clients/:client_id/assets')
        .get(function(req, res) {

            // Default Query
            var recordLimit = 20;

            if (req.query.top) {
                recordLimit = parseInt(req.query.top);
            }

            // PAGING
            var page = 0;
            if (req.query.page) {
                page = Math.max(0, req.query.page - 1);
            }

            Asset.find({client: req.params.client_id, deleted: false})
                .exec(function(err, totalAssets) {
                    if (err)
                        return res.status(500).send(err);

                    Asset.find({client: req.params.client_id, deleted: false})
                        .populate([{path: 'types.template', select: '_id name description templateType'}, {path: 'createdby', select: '_id name'}, {path: 'lastupdatedby', select: '_id name'}])
                        .skip(recordLimit * page)
                        .limit(recordLimit)
                        .exec(function(err, assets) {
                            if (err)
                                return res.status(500).send(err);

                            var result = {};
                            result.total = totalAssets.length;
                            result.data = assets;
                            result.page = page + 1;
                            result.top = recordLimit;
                            return res.status(200).json(result);
                        });
                });

            
        })

        .post(function(req, res) {
            Client.findById(req.params.client_id, function(err, client) {
                if (err)
                    return res.status(500).send(err);

                var asset = new Asset();
                asset.name = req.body.name;
                asset.types = req.body.types;
                asset.client = req.params.client_id;
                asset.custom = req.body.custom;
                asset.createdby = req.decoded._doc._id;
                asset.lastupdatedby = req.decoded._doc._id;
                
                asset.save(function(err) {
                    if (err)
                        return res.status(500).send(err);

                    client.assets.push(asset);

                    client.save(function(err) {
                        if (err)
                            return res.status(500).send(err);

                        Asset.populate(asset, [{path: 'createdby', select: '_id name'}, {path: 'lastupdatedby', select: '_id name'}], function(err, asset) {
                            if (err)
                                return res.status(500).send(err);

                            res.status(200).json(asset);
                        });
                    });
                });
            });
        })
    ;

    router.route('/clients/:client_id/assets/:id/copy')
        .post(function(req, res) {
            Client.findById(req.params.client_id, function(err, client) {
                if (err)
                    return res.status(500).send(err);
                
                Asset.findById(req.params.id)

                    // .populate()
                    .exec(function(err, asset) {
                        if (err)
                            return res.status(500).send(err);

                        var copiedAsset = new Asset();
                        copiedAsset.name = 'copy of ' + asset.name;
                        copiedAsset.types = asset.types;
                        copiedAsset.client = asset.client;
                        copiedAsset.custom = asset.custom;
                        copiedAsset.history = asset.history;
                        copiedAsset.createdby = req.decoded._doc._id;
                        copiedAsset.lastupdatedby = req.decoded._doc._id;

                        copiedAsset.save(function(error) {
                            if (err)
                                return res.status(500).send(err);

                            client.assets.push(copiedAsset);

                            client.save(function(err) {
                                if (err)
                                    return res.status(500).send(err);

                                Asset.populate(copiedAsset, [{path: 'createdby', select: '_id name'}, {path: 'lastupdatedby', select: '_id name'}], function(err, asset) {
                                    if (err)
                                        return res.status(500).send(err);

                                    res.status(200).json(asset);
                                });
                            });
                        });
                    });
            });
        })
    ;

    router.route('/clients/:client_id/assets/:id')
        .get(function(req, res) {
            Asset.findById(req.params.id)
                .populate([{path: 'types.template', select: '_id name description templateType'}, {path: 'createdby', select: '_id name'}, {path: 'lastupdatedby', select: '_id name'}])
                .exec(function(err, asset) {
                    if (err)
                        return res.status(500).send(err);

                    res.status(200).json(asset);
                });
        })

        .put(function(req, res) {
            Asset.findById(req.params.id)
                .exec(function(err, asset) {

                    if (err)
                        return res.status(500).send(err);

                    asset.name = req.body.name;
                    asset.types = req.body.types;
                    asset.custom = req.body.custom;
                    asset.lastupdatedby = req.decoded._doc._id;

                    asset.save(function(err) {
                        if (err)
                            return res.status(500).send(err);

                        Asset.populate(asset, [{path: 'types.template', select: '_id name description templateType'}, {path: 'createdby', select: '_id name'}, {path: 'lastupdatedby', select: '_id name'}], function(err, asset) {
                            if (err)
                                return res.status(500).send(err);

                            res.status(200).json(asset);    
                        })
                    });
                });
        })

        .delete(function(req, res) {
            Asset.findById(req.params.id, function(err, asset) {
                asset.deleted = true;
                asset.lastupdatedby = req.decoded._doc._id;

                asset.save(function(err) {
                    if (err)
                        return res.status(500).send(err);

                    res.status(200).json({message: "Asset Deleted"});
                })
            });
        })
    ;

    router.route('/clients/:client_id/assets/:asset_id/script/:id')
        .get(function(req, res) {
            Asset.findById(req.params.asset_id, {_id: 1, types: 1})
                .exec(function(err, asset) {
                    if (err)
                        return res.status(500).send(err);

                    var template = asset.types.id(req.params.id);

                    parseScript(template).then(function(response) {
                        res.status(200).json({script: response});
                    });
                });
        })
    ;
       
}