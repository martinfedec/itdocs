var Template = require('../models/template');

module.exports = function(router) {

	router.route('/templates')
        .get(function(req, res) {
            var query = {};
            query['deleted'] = false;

            if (req.query.type) {
                query['templateType'] = req.query.type;
            }

            Template.find(query)
                .populate('templateType')
                .exec(function(err, templates) {
                    if (err)
                        return res.status(500).send(err);
                    
                    res.status(200).json(templates);
                });
        })
        
        .post(function(req, res) {
            var template = new Template();
           
           	template.name = req.body.name;
            template.description = req.body.description;
            template.templateType = req.body.templateType;
            template.fields = req.body.fields;
            template.script = req.body.script;
            
            Template.findOne({name: new RegExp('^' + template.name + '$', 'i'), deleted: false}, function(err, response){
                if (err)
                    return res.status(500).send(err);
                
                if (response) {
                    res.status(409).json({code: 100, message: 'Template Already Exists'});
                } else {
                    template.save(function(err) {
                        if (err)
                            return res.status(500).send(err);

                        var cleanTemplate = template.toObject();
                        delete cleanTemplate['deleted'];

                        res.status(200).json(cleanTemplate);
                    });
                }
            });
        })
    ;
    
    router.route('/templates/:id')
        .get(function(req, res) {
            Template.findById(req.params.id)
                .populate('templateType')
                .exec(function(err, template) {
                    if (err)
                        return res.status(500).send(err);
                        
                    res.status(200).json(template);
                });
        })
        
        .put(function(req, res) {
            Template.findOne({name: new RegExp('^' + req.body.name + '$', 'i'), deleted: false}, function(err, data) {
                if (err)
                    return res.status(500).send(err);

                if (data && data._id != req.params.id) {
                    res.status(409).json({code: 100, message: 'Template Name Already Exists'});
                } else {
                    Template.findById(req.params.id, function(err, template) {
                        if (err)
                            return res.status(500).send(err);
                        
                        template.name = req.body.name;
                        template.description = req.body.description;
                        template.templateType = req.body.templateType;
                        template.fields = req.body.fields;
                        template.active = req.body.active;
                        template.script = req.body.script;
                            
                        template.save(function(err) {
                            if(err)
                                return res.status(500).send(err);
                                
                            res.status(200).json(template);
                        });
                    });
                }
            });
        })
        
        .delete(function(req, res) {
            Template.findById(req.params.id, function(err, template) {
                template.deleted = true;
                template.save(function(err) {
                    if (err)
                        return res.status(500).send(err);

                    res.status(200).json({message: "Template Deleted"});
                })
            });
        })
    ;
       
}