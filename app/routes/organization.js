var _ = require("lodash");
var Organization = require('../models/organization');

module.exports = function(router) {

	router.route('/organization')
        .get(function(req, res) {
            Organization.findOne({})
            .populate('admin')
            .exec(function(err, organization) {
                if (err)
                    return res.status(500).send(err);
                    
                res.status(200).json(organization);
            });
        })
        
        .put(function(req, res) {
            Organization.findOne({}, function(err, organization) {
                if (err)
                    return res.status(500).send(err);
                    
                organization.name = req.body.name;
                organization.shortname = req.body.shortname;
                organization.website = req.body.website;
                organization.description = req.body.description;
                organization.logo = req.body.logo;
                organization.primarylocation = req.body.primarylocation;
                organization.admin = req.body.admin;

                organization.save(function(err) {
                    if(err)
                        return res.status(500).send(err);
                        
                    res.status(200).json(organization);
                });
            });
        })
    ;

    router.route('/organization/locations')
        .get(function(req, res) {
            Organization.findOne({}, function(err, organization) {
                if (err)
                    return res.status(500).send(err);

                res.status(200).json(organization.locations);
            });
        })

        .post(function(req, res) {
            var newLocation = {
                name: req.body.name,
                address1: req.body.address1,
                address2: req.body.address2,
                city: req.body.city,
                province: req.body.province,
                country: req.body.country,
                postalcode: req.body.postalcode,
                phonenumber: req.body.phonenumber,
                faxnumber: req.body.faxnumber,
                email: req.body.email,
                active: req.body.active
            };
            
            Organization.findOne({}, function(err, organization) {
                if (err)
                    return res.status(500).send(err);

                if (_.findIndex(organization.locations, {name: newLocation.name}) == -1) {
                    var newLength = organization.locations.push(newLocation);
               
                    organization.save(function(err) {
                         if (err)
                             return res.status(500).send(err);
                         
                        res.status(200).json(organization.locations[newLength - 1]);
                    });
                } else {
                    res.status(409).json({code: 100, message: 'Location Already Exists'});
                }
                
            });
        })
    ;

    router.route('/organization/locations/:id')
        .get(function(req, res) {
            Organization.findOne({}, function(err, organization) {
                if (err)
                    return res.send(500).send(err);

                res.status(200).json(organization.locations.id(req.params.id));
            });
        })

        .put(function(req, res) {
            Organization.findOne({}, function(err, organization) {
                if (err)
                    return res.send(500).send(err);

                var checkLocation = _.findIndex(organization.locations, {name: req.body.name});
    
                if (checkLocation != -1 && organization.locations[checkLocation]._id != req.params.id) {
                    res.status(409).json({code: 100, message: 'Location Already Exists'});
                } else {
                    var location = organization.locations.id(req.params.id);
                    location.name = req.body.name;
                    location.address1 = req.body.address1;
                    location.address2 = req.body.address2;
                    location.city = req.body.city;
                    location.province = req.body.province;
                    location.country = req.body.country;
                    location.postalcode = req.body.postalcode;
                    location.phonenumber = req.body.phonenumber,
                    location.faxnumber = req.body.faxnumber,
                    location.email = req.body.email,
                    location.active = req.body.active;

                    organization.save(function(err) {
                        if (err)
                            return res.send(500).send(err);

                        res.status(200).json(location);
                    });
                }
            });
        })

        .delete(function(req, res) {
            Organization.findOne({}, function(err, organization) {
                if (err)
                    return res.send(500).send(err);

                if (organization.primarylocation === req.params.id) {
                    res.status(409).json("LOCATION_ASSIGNED_EXISTS");
                } else {
                    organization.locations.id(req.params.id).remove();
                    organization.save(function(err) {
                        if (err)
                            return res.send(500).send(err);

                        res.status(200).json(organization);
                    });
                } 
            });
        })
    ;
       
}