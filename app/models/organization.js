var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var OrganizationSchema = new Schema({
    name: String,
    shortname: String,
    website: String,
    description: String,
    logo: String,
    primarylocation: String,
    admin: { type: Schema.Types.ObjectId, ref: 'User'},
    locations: [{
        name: String,
        address1: String,
        address2: String,
        city: String,
        province: String,
        country: String,
        postalcode: String,
        phonenumber: String,
        faxnumber: String,
        email: String,
        active: { type: Boolean, default: true },
        deleted: { type: Boolean, select: false, default: false }
    }]
});

module.exports = mongoose.model('Organization', OrganizationSchema);