var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var TemplateTypeSchema = new Schema({
	description: String,
	icon: String,
    active: { type: Boolean, default: true },
    deleted: { type: Boolean, select: false, default: false }
});

module.exports = mongoose.model("TemplateType", TemplateTypeSchema);