var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var TaskSchema = new Schema({
	title: String,
	description: String,
	completed: { type: Boolean, default: false },
	completedat: Date,
	client: { type: Schema.Types.ObjectId, ref: 'Client' },
	asset: { type: Schema.Types.ObjectId, ref: 'Asset' },
	assignedto: { type: Schema.Types.ObjectId, ref: 'User' },
	createdby: { type: Schema.Types.ObjectId, ref: 'User' },
	createdat: Date,
	checklists: [{ name: String, 
		items: [{ description: String, completed: Boolean, completedat: Date, completedby: { type: Schema.Types.ObjectId, ref: 'User' } }]
	}],
    active: { type: Boolean, default: true },
    deleted: { type: Boolean, select: false, default: false },
    lastupdatedat: Date,
    lastupdatedby: { type: Schema.Types.ObjectId, ref: 'User' },
    activities: [{ type: Schema.Types.ObjectId, ref: 'Activity' }]
});

TaskSchema.pre("save", function(next) {
	now = new Date();
	
	if (!this.createdat) {
		this.createdat = now;
	}

	if (this.completed && (this.completedat == undefined || this.completedat == null)) {
		this.completedat = now;
	}

	if (!this.completed && (this.completedat != undefined || this.completedat != null)) {
		this.completedat = null;
	}

	this.lastupdatedat = now;

	// Loop through checklist items and check for completed items and set as complete
	for (var index = 0; index < this.checklists.length; index++) {
		for (var i = 0; i < this.checklists[index].items.length; i++) {
			if (this.checklists[index].items[i].completed && (this.checklists[index].items[i].completedat == undefined || this.checklists[index].items[i].completedat == null)) {
				this.checklists[index].items[i].completedat = now;
				this.checklists[index].items[i].completedby = this.lastupdatedby;
			}

			if (!this.checklists[index].items[i].completed && (this.checklists[index].items[i].completedat != undefined || this.checklists[index].items[i].completedat != null)) {
				this.checklists[index].items[i].completedby = null;
				this.checklists[index].items[i].completedat = null;
			}

		}	
	}

	next();
});

module.exports = mongoose.model("Task", TaskSchema);