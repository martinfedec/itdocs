var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var ActivitySchema = new Schema({
	details: String,
	lastupdatedat: Date,
    lastupdatedby: { type: Schema.Types.ObjectId, ref: 'User' },
    createdby: { type: Schema.Types.ObjectId, ref: 'User' },
	createdat: Date,
	type: String, // COMMENT, LABEL, 
	deleted: { type: Boolean, select: false, default: false }
});

ActivitySchema.pre("save", function(next) {
	
	now = new Date();
	
	if (!this.createdat) {
		this.createdat = now;
	}

	this.lastupdatedat = now;
	
	next();
});


module.exports = mongoose.model("Activity", ActivitySchema);