var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var HistorySchema = new Schema({
    user: { type: Schema.Types.Object, ref: 'User' },
    timestamp: Date,
    activity: String,
    // type: String // COMMENT, UPDATE, ASSIGNED, LABEL?
});

module.exports = mongoose.model("History", HistorySchema);