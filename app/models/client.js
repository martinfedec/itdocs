var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var ClientSchema = new Schema({
    name: String,
    website: String,
    businesshours: String,
    description: String,
    assets: [{ type: Schema.Types.ObjectId, ref: 'Asset' }],
    primarylocation: String,
    locations: [{
        name: String,
        address1: String,
        address2: String,
        city: String,
        province: String,
        country: String,
        postalcode: String,
        phonenumber: String,
        faxnumber: String,
        email: String,
        active: { type: Boolean, default: true },
        deleted: { type: Boolean, select: false, default: false }
    }],
    active: { type: Boolean, default: true },
    deleted: { type: Boolean, select: false, default: false },
    lastupdatedat: Date,
    lastupdatedby: { type: Schema.Types.ObjectId, ref: 'User' },
    createdby: { type: Schema.Types.ObjectId, ref: 'User' },
    createdat: Date
});

ClientSchema.pre("save", function(next) {
    now = new Date();
    
    if (!this.createdat) {
        this.createdat = now;
    }

    this.lastupdatedat = now;
    
    next();
});

module.exports = mongoose.model("Client", ClientSchema);