var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var TemplateSchema = new Schema({
	name: String,
	description: String,
	fields: [{ name: String }],
	script: String,
	templateType: { type: Schema.Types.ObjectId, ref: 'TemplateType' },
    active: { type: Boolean, default: true },
    deleted: { type: Boolean, select: false, default: false }
});

module.exports = mongoose.model("Template", TemplateSchema);