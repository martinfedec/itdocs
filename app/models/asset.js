var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var AssetSchema = new Schema({
	name: String,
	types: [
		{ 
			template: { type: Schema.Types.ObjectId, ref: 'Template' },
			fields: [{
				name: String,
				// type: String, // Will add later, this will be for when I define more than just a text
				value: String
			}],
			script: String
		}
	],
	client: { type: Schema.Types.ObjectId, ref: 'Client' },
    active: { type: Boolean, default: true },
    deleted: { type: Boolean, select: false, default: false },
    custom: {
    	fields: [{
    		name: String,
			// type: String, // Will add later, this will be for when I define more than just a text
			value: String
    	}]
    },
    lastupdatedat: Date,
    lastupdatedby: { type: Schema.Types.ObjectId, ref: 'User' },
    createdby: { type: Schema.Types.ObjectId, ref: 'User' },
	createdat: Date,
	history: [
		{ type: Schema.Types.ObjectId, ref: 'History' }
	]
});

AssetSchema.pre("save", function(next) {
	
	now = new Date();
	
	if (!this.createdat) {
		this.createdat = now;
	}

	this.lastupdatedat = now;
	
	next();
});

module.exports = mongoose.model("Asset", AssetSchema);