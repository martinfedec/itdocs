[![build status](https://gitlab.com/martin.fedec/itdocs/badges/0.6/build.svg)](https://gitlab.com/martin.fedec/itdocs/commits/0.6)

IT Docs... will add more to this later.

Define EVN variables

ITDOCS_SECRET

ITDOCS_SALT

ITDOCS_URI

ITDOCS_API_URL

ITDOCS_TEST_URI - Used only for Gitlab CI testing at the moment. 

Deploy to heroku: git push heroku master
