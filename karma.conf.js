// Karma configuration
// Generated on Sat Jan 28 2017 10:39:06 GMT-0800 (Pacific Standard Time)

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],


    // list of files / patterns to load in the browser
    files: [
        './client/bower_components/angular/angular.js',
        './client/bower_components/angular-ui-router/release/angular-ui-router.js',
        './node_modules/angular-mocks/angular-mocks.js',
        './client/bower_components/AngularJS-Toaster/toaster.js',
        './client/bower_components/angular-bootstrap/ui-bootstrap.js',      
        './client/bower_components/angular-ui-codemirror/ui-codemirror.js',
        './client/bower_components/angular-animate/angular-animate.js',
        './client/bower_components/moment/moment.js',
        
        './client/bower_components/angular-moment/angular-moment.js',
        './client/testing/common.js',

        // Load Services: naming convention <model>Service.js
        './client/*/*Service.js',
        './client/*/*/*Service.js',
        './client/js/services.js',

        // Load Controllers: naming convention <model>Controller.js
        './client/*/*Controller.js',
        './client/*/*/*Controller.js',
        './client/js/controllers.js',

        './client/js/app.js',

        // Load Service Specs: naming convention <model>Service.spec.js
        './client/*/*Service.spec.js',
        './client/*/*/*Service.spec.js',
        './client/js/services.spec.js',

        // Load Controller Specs: naming convention <model>Controller.spec.js
        './client/*/*Controller.spec.js',
        './client/*/*/*Controller.spec.js',
        './client/js/controllers.spec.js'
        
    ],


    // list of files to exclude
    exclude: [
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['spec'],


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['PhantomJS'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity
  })
}
