var mongoose        = require("mongoose");
var bcrypt = require("bcryptjs");

var path = require("path");

var db = require("./config/db");

mongoose.connect(db, function(err, res) {
	if (err)
		console.log(err);
});

console.log("Starting to seed db");

// ### Create Default User Admin Account
console.log("Creating Default Admin Account");

var User = require('./app/models/user');
var name = "Administrator";
var email = "admin@itdocs.com";
var password = "password";

var userDetails;

var hash = bcrypt.hashSync(password, parseInt(process.env.ITDOCS_SALT));
User.create({name: name, email: email, password: hash, admin: true});

console.log("Creating Organization....");
var Organization = require('./app/models/organization');

Organization.create({
	name: 'Default Organization'
});

console.log("Done Creating Organization");


// ### Create Default Template Types
console.log("Creating Template Types....");
var TemplateType = require('./app/models/templateType');

TemplateType.create({description: "Asset", icon: "fa-laptop"});
TemplateType.create({description: "Script", icon: "fa-code"});
TemplateType.create({description: "Document", icon: "fa-file-o"});
TemplateType.create({description: "Client", icon: "fa-database"});

console.log("Finished creating Template Types...");

// ## NEW DB DEFAULTS FOR VERSION 0.4 ###

// ### EVERYTHING BEFORE VERSION 0.4 ###

// ## NEW DB DEFAULTS FOR VERSION 0.5 ###

// ### EVERYTHING BEFORE VERSION 0.5 ###

// ## NEW DB DEFAULTS FOR VERSION 0.6 ### 

// ### EVERYTHING BEFORE VERSION 0.6 ###

mongoose.connection.close();