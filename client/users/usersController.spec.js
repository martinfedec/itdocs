describe('Users Controllers', function() {
	var $controller, UsersFactory, $q, $httpBackend, API_URL, toaster, userList;
	var createdDate = new Date();

	beforeEach(module(function($provide) {
		$provide.constant("API_URL", "http://localhost:443/api");
	}));

	userList = [
	{
		_id: 1,
		name: 'User 1',
		email: 'user1@test.com',
		active: true,
		created: createdDate
	},
	{
		_id: 2,
		name: 'User 2',
		email: 'user2@test.com',
		active: true,
		created: createdDate
	}];

	beforeEach(angular.mock.module('ui.router'));
	beforeEach(angular.mock.module('toaster'));
	
	beforeEach(angular.mock.module('itdocs.services.users'));
	beforeEach(angular.mock.module('itdocs.controllers.users'));


	beforeEach(inject(function(_$controller_, _users_, _API_URL_, _$q_, _$httpBackend_, _toaster_) {
		$controller = _$controller_;
		UsersFactory = _users_;
		$q = _$q_;
		$httpBackend = _$httpBackend_;
		API_URL = _API_URL_;
		toaster = _toaster_;
	}));

	describe('usersCtrl', function() {
		var usersCtrl;

		beforeEach(function() {
			usersCtrl = $controller('usersCtrl', {});
		});

		it('should be defined', function() {
			expect(usersCtrl).toBeDefined();
		});
	});

	describe('userListCtrl', function() {
		var userListCtrl;

		beforeEach(function() {
			spyOn(UsersFactory, 'get').and.callThrough();
			spyOn(UsersFactory, 'remove').and.callThrough();
			spyOn(toaster, 'pop').and.callThrough();

			userListCtrl = $controller('userListCtrl', {users: UsersFactory, toaster: toaster});
		});

		it('should be defined', function() {
			expect(userListCtrl).toBeDefined();
		});

		it('should get all users for the initial view and set deleteActive to 0', function() {
			$httpBackend.whenGET(API_URL + '/users').respond(200, $q.when(userList));
			$httpBackend.flush();
			
			expect(userListCtrl.users).toEqual(userList);
			expect(userListCtrl.deleteActive).toEqual(0);
		});

		it('should delete a specified user and toggle deleteActive and reload user list', function() {
			var removedList = [{
				_id: 2,
				name: 'User 2',
				email: 'user2@test.com',
				active: true,
				created: createdDate
			}]

			$httpBackend.whenDELETE(API_URL + '/users/' + 1).respond(200, $q.when({message: 'User Deleted'}));
			
			userListCtrl.deleteUser(1);
			expect(userListCtrl.deleteActive).toEqual(1);
			
			$httpBackend.whenGET(API_URL + '/users').respond(200, $q.when(removedList));

			$httpBackend.flush();
			
			expect(toaster.pop).toHaveBeenCalled();
			expect(UsersFactory.remove).toHaveBeenCalledWith(1);
			expect(userListCtrl.deleteActive).toEqual(0);
			expect(userListCtrl.users.length).toEqual(1);
		});
	});

	describe('userEditCtrl', function() {
		var userEditCtrl, $stateParams, $state;

		beforeEach(inject(function(_$stateParams_, _$state_) {
			$stateParams = _$stateParams_;
			$state = _$state_;
		}));

		beforeEach(function() {
			spyOn(toaster, 'pop').and.callThrough();
			spyOn($state, 'go');

			userEditCtrl = $controller('userEditCtrl', {users: UsersFactory, toaster: toaster});
		});

		it('should be defined', function() {
			expect(userEditCtrl).toBeDefined();
		});

		describe('Add new user', function() {
			beforeEach(function() {
				spyOn(UsersFactory, 'add').and.callThrough();
			});

			it('should add a new user', function() {
				var user = {
					name: 'User 1',
					email: 'user1@user.com',
					password: 'password123',
					active: true
				};

				var response = {
					_id: 1,
					name: 'User 1',
					email: 'user1@user.com',
					active: true
				}

				expect(userEditCtrl.mode).toEqual('Add New');
				expect(userEditCtrl.user).toEqual({active: true});
				expect(userEditCtrl.emailExists).toEqual(false);

				$httpBackend.whenPOST(API_URL + '/users', user).respond(200, $q.when(response));
				userEditCtrl.user = user;
				userEditCtrl.saveUser();
				$httpBackend.flush();

				expect(toaster.pop).toHaveBeenCalled();
				expect($state.go).toHaveBeenCalledWith('app.users.list');
			});

			it('should try to add a new user with duplicate email', function() {
				var user = {
					name: 'User 1',
					email: 'user2@user.com',
					password: 'password123',
					active: true
				};

				var response = {
					code: 100,
					message: 'Email Already Exists'
				}

				expect(userEditCtrl.mode).toEqual('Add New');
				expect(userEditCtrl.user).toEqual({active: true});
				expect(userEditCtrl.emailExists).toEqual(false);

				$httpBackend.whenPOST(API_URL + '/users', user).respond(409, response);
				userEditCtrl.user = user;
				userEditCtrl.saveUser();
				$httpBackend.flush();
				
				expect(userEditCtrl.emailExists).toEqual(true);
			});
		});
		

		describe('Edit existing user', function() {
			beforeEach(inject(function(_$stateParams_) {
				$stateParams = _$stateParams_;
				$stateParams.id = 1;
				userEditCtrl = $controller('userEditCtrl', {users: UsersFactory, toaster: toaster, $stateParams: $stateParams});
			}));

			beforeEach(function() {
				spyOn(UsersFactory, 'getById').and.callThrough();
				spyOn(UsersFactory, 'save').and.callThrough();
			});

			it('should edit a user', function() {
				var user = userList[0];

				var changedUser = {
					_id: 1,
					name: 'User 99',
					email: 'user99@user.com',
					active: true
				}
			

				expect(userEditCtrl.mode).toEqual('Edit');
				expect(userEditCtrl.emailExists).toEqual(false);
				$httpBackend.whenGET(API_URL + '/users/1').respond(200, $q.when(user));
				$httpBackend.flush();
				expect(userEditCtrl.user).toEqual(user);

				$httpBackend.whenPUT(API_URL + '/users/' + 1, changedUser).respond(200, $q.when(changedUser));
				userEditCtrl.user = changedUser;
				userEditCtrl.saveUser();
				$httpBackend.flush();

				expect(toaster.pop).toHaveBeenCalled();
				expect($state.go).toHaveBeenCalledWith('app.users.list');
			});

			it('should try to edit a user with duplicate email', function() {
				var user = userList[0];

				var response = {
					code: 100,
					message: 'Email Already Exists'
				}
				
				expect(userEditCtrl.emailExists).toEqual(false);
				
				$httpBackend.whenGET(API_URL + '/users/1').respond(200, $q.when(user));
				$httpBackend.flush();

				$httpBackend.whenPUT(API_URL + '/users/' + 1, user).respond(409, response);
				userEditCtrl.user = user;
				userEditCtrl.saveUser();
				$httpBackend.flush();
				
				expect(userEditCtrl.emailExists).toEqual(true);
			});
		});
	});
});