'use strict';

angular
	.module('itdocs.controllers.users', [])
	
	.controller('usersCtrl', function() {

	})

	.controller('userListCtrl', function(users, toaster) {
		var ul = this;
		ul.deleteActive = 0;

		function loadUsers() {
			users.get().then(function(users) {
				ul.users = users;
			});	
		};
		
		loadUsers();

		ul.deleteUser = function(id) {
			ul.deleteActive = id;

			users.remove(id).then(function(response) {
				ul.deleteActive = 0;
				toaster.pop({
					type: 'error',
					body: 'User has been removed',
					showCloseButton: true,
					timeout: 3000,
					bodyOutputType: 'trustedHtml'
				});
				loadUsers();
			});
		};
	})

	.controller('userEditCtrl', function(users, $stateParams, $state, toaster) {
		var ue = this;

		ue.user = {};
		ue.emailExists = false;

		if ($stateParams.id) {
			ue.mode = 'Edit';

			users.getById($stateParams.id).then(function(user) {
				ue.user = user;
			})

		} else {
			ue.mode = 'Add New';
			ue.user.active = true;
		}

		ue.saveUser = function() {
			if (ue.mode === 'Edit') {
				users.save(ue.user).then(function(response) {
					toaster.pop({
						type: 'success',
						body: 'User has been saved',
						showCloseButton: true,
						timeout: 3000
					});
					$state.go('app.users.list');
				}, function(error) {
					switch (error.data.code) {
						case 100:
							ue.emailExists = true;
							// content = "Role Name already in exists"
							break;
						default:
							// content = error;
					};
				});
			} else {
				users.add(ue.user).then(function(response) {
					toaster.pop({
						type: 'success',
						body: 'User has been added',
						showCloseButton: true,
						timeout: 3000
					});
					$state.go('app.users.list');
				}, function(error) {
					switch (error.data.code) {
						case 100:
							ue.emailExists = true;
							// content = "Role Name already in exists"
							break;
						default:
							// content = error;
					};
				});
			}
		};
	})

;