'use strict';

angular.module('itdocs.services.users', [])

.factory('users', function($http, API_URL){
    return {
        get: function() {
            return $http.get(API_URL + '/users').then(function(response) {
        		return response.data;
        	});
        },
        getById: function(id) {
            return $http.get(API_URL + '/users/' + id).then(function(response) {
        		return response.data;
        	});  
        },
        add: function(user) {
        	return $http.post(API_URL + '/users', user).then(function(response) {
        		return response.data;
        	});
        },
        save: function(user) {
			return $http.put(API_URL + '/users/' + user._id, user).then(function(response) {
        		return response.data;
        	});
        },
        remove: function(id) {
            return $http.delete(API_URL + '/users/' + id).then(function(response) {
                return response.data;
            });
        }
    };
})

;