describe('Users Factory', function() {
	var users, API_URL, $httpBackend, $q;

	var createdDate = new Date();

	beforeEach(module(function($provide) {
		$provide.constant("API_URL", "http://localhost:443/api");
	}));

	var userList = [
	{
		_id: 1,
		name: 'User 1',
		email: 'user1@test.com',
		active: true,
		created: createdDate
	},
	{
		_id: 2,
		name: 'User 2',
		email: 'user2@test.com',
		active: true,
		created: createdDate
	}];

	beforeEach(angular.mock.module('itdocs.services.users'));

	beforeEach(inject(function(_users_,_$q_, _$httpBackend_, _API_URL_) {
		users = _users_;
		$q = _$q_;
		$httpBackend = _$httpBackend_;
		API_URL = _API_URL_;
	}));

	it('should exist', function() {
		expect(users).toBeDefined();
	});

	describe('.get()', function() {
		beforeEach(function() {
			result = {};
			spyOn(users, "get").and.callThrough();
		});


		it('should exist', function() {
			expect(users.get).toBeDefined();
		});

		it('should return a list of users', function() {
			$httpBackend.whenGET(API_URL + '/users').respond(200, $q.when(userList));

			expect(users.get).not.toHaveBeenCalled();
			expect(result).toEqual({});

			users.get().then(function(res) {
				result = res;
			});

			$httpBackend.flush();

			expect(users.get).toHaveBeenCalled();
			expect(result.length).toBe(2);
			expect(result[0]._id).toEqual(1);
			expect(result[0].name).toEqual('User 1');
			expect(result[0].email).toEqual('user1@test.com');
			expect(result[0].active).toEqual(true);
			expect(result[0].created).toEqual(createdDate);
		});
	});

	describe('.getById()', function() {
		beforeEach(function() {
			result = {};
			spyOn(users, "getById").and.callThrough();
		});


		it('should exist', function() {
			expect(users.getById).toBeDefined();
		});

		it('should return a user when called with a valid id', function() {
			$httpBackend.whenGET(API_URL + '/users/' + 1).respond(200, $q.when(userList[0]));

			expect(users.getById).not.toHaveBeenCalled();
			expect(result).toEqual({});

			users.getById(1).then(function(res) {
				result = res;
			});

			$httpBackend.flush();

			expect(users.getById).toHaveBeenCalledWith(1);
			expect(result._id).toEqual(1);
			expect(result.name).toEqual('User 1');
			expect(result.email).toEqual('user1@test.com');
			expect(result.active).toEqual(true);
			expect(result.created).toEqual(createdDate);
		});
	});

	describe('.add()', function() {
		beforeEach(function() {
			result = {};
			spyOn(users, "add").and.callThrough();
		});

		it('should exist', function() {
			expect(users.add).toBeDefined();
		});

		it('should add a new user and return the added users data', function() {
			var created = new Date();
			var newUser = {
				name: 'User 3',
				email: 'user3@test.com',
				active: true,
				created: created
			};

			var newUserResponse = {
				_id: 3,
				name: 'User 3',
				email: 'user3@test.com',
				active: true,
				created: created
			};

			$httpBackend.whenPOST(API_URL + '/users', newUser).respond(200, $q.when(newUserResponse));

			expect(users.add).not.toHaveBeenCalled();
			expect(result).toEqual({});

			users.add(newUser).then(function(res) {
				result = res;
			});

			$httpBackend.flush();

			expect(users.add).toHaveBeenCalledWith(newUser);
			expect(result._id).toEqual(3);
			expect(result.name).toEqual(newUser.name);
			expect(result.email).toEqual(newUser.email);
			expect(result.active).toEqual(true);
			expect(result.created).toEqual(newUser.created);
		});
	});

	describe('.save()', function() {
		beforeEach(function() {
			result = {};
			spyOn(users, "save").and.callThrough();
		});

		it('should exist', function() {
			expect(users.save).toBeDefined();
		});

		it('should save a user and return the users data', function() {
			var editUser = userList[1];

			editUser.name = 'User Two';
			editUser.email = 'usertwo@test.com';

			var editUserResponse = editUser;

			$httpBackend.whenPUT(API_URL + '/users/' + editUser._id, editUser).respond(200, $q.when(editUserResponse));

			expect(users.save).not.toHaveBeenCalled();
			expect(result).toEqual({});

			users.save(editUser).then(function(res) {
				result = res;
			});

			$httpBackend.flush();

			expect(users.save).toHaveBeenCalledWith(editUser);
			expect(result._id).toEqual(2);
			expect(result._id).toEqual(editUser._id);
			expect(result.name).toEqual(editUser.name);
			expect(result.email).toEqual(editUser.email);
			expect(result.active).toEqual(true);
			expect(result.created).toEqual(editUser.created);
		});
	});

	describe('.remove()', function() {
		beforeEach(function() {
			result = {};
			spyOn(users, "remove").and.callThrough();
		});

		it('should exist', function() {
			expect(users.remove).toBeDefined();
		});

		it('should remove a user based on id', function() {
			var response = {message: 'User Deleted'};

			$httpBackend.whenDELETE(API_URL + '/users/' + 2).respond(200, $q.when(response));

			expect(users.remove).not.toHaveBeenCalled();
			expect(result).toEqual({});

			users.remove(2).then(function(res) {
				result = res;
			});

			$httpBackend.flush();

			expect(users.remove).toHaveBeenCalledWith(2);
			expect(result.message).toEqual('User Deleted');
		});
	});
});