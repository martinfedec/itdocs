describe('Dashboard Controller', function() {
	var $controller, API_URL;

	beforeEach(module(function($provide) {
		$provide.constant("API_URL", "http://localhost:443/api");
	}));

	beforeEach(angular.mock.module('ui.router'));
	
	// beforeEach(angular.mock.module('itdocs.services'));
	beforeEach(angular.mock.module('itdocs.controllers.dashboard'));

	beforeEach(inject(function(_$controller_, _API_URL_) {
		$controller = _$controller_;
		API_URL = _API_URL_;
	}));

	describe('dashboardCtrl', function() {
		var dashboardCtrl;

		beforeEach(function() {
			dashboardCtrl = $controller('dashboardCtrl');
		});

		it('should be defined', function() {
			expect(dashboardCtrl).toBeDefined();
		});

		it('should get default temp content', function() {
			expect(dashboardCtrl.DashContent).toEqual("Dashboard Controller... here. If ?client passed in, it will show dashboard specific to that client, if not supplied, will show general info about all clents.");
		});
	});
});