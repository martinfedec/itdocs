'use strict';

angular.module('itdocs.controllers.dashboard', [])

.controller('dashboardCtrl', function() {
	var db = this;
	db.DashContent = "Dashboard Controller... here. If ?client passed in, it will show dashboard specific to that client, if not supplied, will show general info about all clents."

	db.todos = [
		{ _id: 1, description: 'Buy Milk', order: 1, completed: false },
		{ _id: 2, description: 'Go to shop and find some products. ', order: 3, completed: false },
		{ _id: 3, description: 'Send documents to Mike', order: 5, completed: false },
		{ _id: 4, description: 'Go to the doctor dr Smith  42 mins', order: 4, completed: false },
		{ _id: 5, description: 'Get fork serviced', order: 2, completed: false }
	];

	db.checkCompleted = function(index) {

	};

	db.addTodo = function() {
		var todo = {
			description: db.newTodo,
			completed: false
		}
		db.todos.push(todo);
		db.newTodo = '';
	};
})

;