'use strict';

angular.module('itdocs', [
    'ui.router',                    // Routing
    'ui.bootstrap',	             // Ui Bootstrap
	'oitozero.ngSweetAlert',
	'ngclipboard',
	'itdocs.services',
	'itdocs.controllers',
	'ui.sortable' // NOT SURE WHY THIS WORKS HERE BUT MAKES KARMA TESTS FILE WHEN ADDED TO CONTROLLERS.JS
]);