angular.module('itdocs.controllers', [
	'itdocs.controllers.login',
	'itdocs.controllers.dashboard',
	'itdocs.controllers.clients',
	'itdocs.controllers.templates',
	'itdocs.controllers.users',
	'itdocs.controllers.changePassword',
	'itdocs.controllers.organization',
	'itdocs.controllers.tasks',
	'toaster',
	'ngAnimate',
	'angularMoment'
])

.controller('mainCtrl', function(auth, $state) {
	var mc = this;
	mc.userName = auth.getDetails().name;

	mc.logout = function() {
		auth.logout();
		$state.go('login');
	};
})

;