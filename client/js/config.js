'use strict';

angular.module('itdocs')

    .config(function($httpProvider) {
        $httpProvider.interceptors.push('authInterceptor');
    })

    .run(function($rootScope, $state, $window) {
        // Used for loading the default view from a parent state.
        // ie) go to /settings and it goes to /settings/general
        $rootScope.$on('$stateChangeSuccess', function(event, toState){
            var aac;
            if(aac = toState && toState.params && toState.params.autoActivateChild){
                $state.go(aac);
            }
        });

        $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
            var requireLogin = toState.data.requireLogin;
            
            if (requireLogin && typeof $window.sessionStorage.token === 'undefined') {
                event.preventDefault();
                $state.go('login', {return: toState.name});
            }
        });
    })

    .config(function($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/dashboard');

        // $ocLazyLoadProvider.config({
        //     // Set to true if you want to see what and when is dynamically loaded
        //     debug: false
        // });

        $stateProvider

            .state('login', {
                url: '/login?return',
                templateUrl: 'login/login.html',
                controller: 'loginCtrl as lg',
                data: {
                    pageTitle: 'Login',
                    requireLogin: false,
                    specialClass: 'gray-bg'
                }
            })

            .state('app', {
                abstract: true,
                url: '',
                templateUrl: 'views/common/main.html',
                controller: 'mainCtrl as mc',
                params: {
                    // Load nested view general by default.
                    autoActivateChild: 'app.dashboard'
                },
                data: {
                    requireLogin: true
                }
            })

            .state('app.dashboard', {
                url: '/dashboard',
                templateUrl: 'dashboard/dashboard.html',
                data: { pageTitle: 'Dashboard' },
                controller: 'dashboardCtrl as db'
            })

            .state('app.clients', {
                abstract: true,
                url: '/clients',
                templateUrl: 'clients/clients.html',
                controller: 'clientCtrl as c'
            })

            .state('app.clients.list', {
                url: '',
                templateUrl: 'clients/clientList.html',
                data: { pageTitle: 'Client Listing' },
                controller: 'clientListCtrl as cl'
            })

            .state('app.clients.new', {
                url: '/new',
                templateUrl: 'clients/clientEdit.html',
                data: { pageTitle: 'New Client' },
                params: {
                    // Load nested view general by default.
                    autoActivateChild: 'app.clients.new.basic'
                },
            })

            .state('app.clients.new.basic', {
                url: '/basic',
                templateUrl: 'clients/clientEditBasic.html',
                data: { pageTitle: 'New Client' },
                controller: 'clientEditCtrl as ce'
            })

            .state('app.clients.edit', {
                url: '/:id/edit',
                templateUrl: 'clients/clientEdit.html',
                data: { pageTitle: 'Edit Client' },
                controller: 'clientEditListCtrl as cel',
                params: {
                    // Load nested view general by default.
                    autoActivateChild: 'app.clients.edit.basic'
                }
            })

            .state('app.clients.edit.basic', {
                url: '/basic',
                templateUrl: 'clients/clientEditBasic.html',
                data: { pageTitle: 'Edit Client' },
                controller: 'clientEditCtrl as ce'
            })

            .state('app.clients.edit.locations', {
                url: '/locations',
                templateUrl: 'clients/client.locations.html',
                data: { pageTitle: 'Add Client Locations' },
                params: {
                    // Load nested view general by default.
                    autoActivateChild: 'app.clients.edit.locations.list'
                }
            })

            .state('app.clients.edit.locations.list', {
                url: '',
                templateUrl: 'clients/client.locations.list.html',
                data: { pageTitle: 'Your Organization Locations' },
                controller: 'clientLocationCtrl as clo'
            })

            .state('app.clients.edit.locations.new', {
                url: '/new',
                templateUrl: 'clients/client.locations.edit.html',
                data: { pageTitle: 'Add Client Location' },
                controller: 'clientLocationEditCtrl as cle'
            })

            .state('app.clients.edit.locations.edit', {
                url: '/:location_id',
                templateUrl: 'clients/client.locations.edit.html',
                data: { pageTitle: 'Edit Client Location' },
                controller: 'clientLocationEditCtrl as cle'
            })

            .state('app.clients.detail', {
                url: '/:id',
                templateUrl: 'clients/clientDetail.html',
                data: { pageTitle: 'Client Detail' },
                // controller: 'clientDetailCtrl as cd',
                params: {
                    // Load nested view general by default.
                    autoActivateChild: 'app.clients.detail.dashboard'
                }
            })

            .state('app.clients.detail.dashboard', {
                url: '',
                templateUrl: 'clients/client.detail.dashboard.html',
                data: { pageTitle: 'Client Detail' },
                controller: 'clientDetailCtrl as cd'
            })

            .state('app.clients.detail.asset', {
                abstract: true,
                url: '/assets',
                templateUrl: 'clients/assets/assets.html',
                data: { pageTitle: 'Assets' }
            })

            .state('app.clients.detail.asset.list', {
                abstract: true,
                url: '',
                templateUrl: 'clients/assets/assetsList.html',
                data: { pageTitle: 'Asset Listing' },
                params: {
                    // Load nested view general by default.
                    autoActivateChild: 'app.clients.detail.asset.list.details'
                },
                controller: 'assetListCtrl as al'
            })

            .state('app.clients.detail.asset.list.details', {
                url: '?q&?page',
                templateUrl: 'clients/assets/assetList.details.html',
                controller: 'assetListDetailsCtrl as alc'
            })

            .state('app.clients.detail.asset.dashboard', {
                url: '/:asset_id/dashboard',
                templateUrl: 'clients/assets/assetDashboard.html',
                data: { pageTitle: 'Asset Dashboard' },
                controller: 'assetDashboardCtrl as ad'
            })

            .state('app.clients.detail.asset.new', {
                url: '/new',
                templateUrl: 'clients/assets/assets.edit.html',
                data: { pageTitle: 'New Asset' },
                controller: 'assetEditCtrl as ae'
            })

            .state('app.clients.detail.asset.edit', {
                url: '/:asset_id/edit',
                templateUrl: 'clients/assets/assets.edit.html',
                data: { pageTitle: 'Edit Asset' },
                controller: 'assetEditCtrl as ae'
            })

            .state('app.templates', {
                abstract: true,
                url: '/templates',
                templateUrl: 'templates/templates.html',
                controller: 'templatesCtrl as t'
            })

            .state('app.templates.list', {
                abstract: true,
                url: '',
                templateUrl: 'templates/templateLayout.html',
                data: { pageTitle: 'Template Listings' },
            })

            .state('app.templates.list.details', {
                url: '?type',
                templateUrl: 'templates/templateList.html',
                data: { pageTitle: 'Template Listings' },
                controller: 'templatesListCtrl as tl'
            })

            .state('app.templates.new', {
                url: '/new?type',
                templateUrl: 'templates/templateEdit.html',
                data: { pageTitle: 'New Template' },
                controller: 'templateEditCtrl as te'
            })

            .state('app.templates.edit', {
                url: '/:id/edit?type',
                templateUrl: 'templates/templateEdit.html',
                data: { pageTitle: 'Edit Template' },
                controller: 'templateEditCtrl as te'
            })

            .state('app.users', {
                abstract: true,
                url: '/users',
                templateUrl: 'users/users.html',
                data: { pageTitle: 'System Users'},
                controller: 'usersCtrl as uc'
            })

            .state('app.users.list', {
                url: '',
                templateUrl: 'users/userList.html',
                // data: { pageTitle: 'System Users'},
                controller: 'userListCtrl as ul'
            })

            .state('app.users.new', {
                url: '/new',
                templateUrl: 'users/userEdit.html',
                data: { pageTitle: 'New User' },
                controller: 'userEditCtrl as ue'
            })

            .state('app.users.edit', {
                url: '/:id',
                templateUrl: 'users/userEdit.html',
                data: { pageTitle: 'Edit User' },
                controller: 'userEditCtrl as ue'
            })

            .state('app.changepassword', {
                url: '/changepassword',
                templateUrl: 'changepassword/changePassword.html',
                data: { pageTitle: 'Change your password' },
                controller: 'changePasswordCtrl as cp'
            })

            .state('app.settings', {
                abstract: true,
                url: '/settings',
                templateUrl: 'settings/settings.html'
            })

            .state('app.settings.organization', {
                url: '/settings/organization',
                templateUrl: 'settings/organization/organization.html',
                data: { pageTitle: 'Your Organization' },
                params: {
                    // Load nested view general by default.
                    autoActivateChild: 'app.settings.organization.basic'
                }
            })

            .state('app.settings.organization.basic', {
                url: '',
                templateUrl: 'settings/organization/organization.basic.html',
                data: { pageTitle: 'Your Organization Details' },
                controller: 'organizationCtrl as o'
            })

            .state('app.settings.organization.locations', {
                url: '/locations',
                templateUrl: 'settings/organization/organization.locations.html',
                data: { pageTitle: 'Your Organization Locations' },
                params: {
                    // Load nested view general by default.
                    autoActivateChild: 'app.settings.organization.locations.list'
                }
            })

            .state('app.settings.organization.locations.list', {
                url: '',
                templateUrl: 'settings/organization/organization.locations.list.html',
                data: { pageTitle: 'Your Organization Locations' },
                controller: 'organizationLocationCtrl as ol'
            })

            .state('app.settings.organization.locations.new', {
                url: '/new',
                templateUrl: 'settings/organization/organization.locations.edit.html',
                data: { pageTitle: 'Add Organization Location' },
                controller: 'organizationLocationEditCtrl as ole'
            })

            .state('app.settings.organization.locations.edit', {
                url: '/:id',
                templateUrl: 'settings/organization/organization.locations.edit.html',
                data: { pageTitle: 'Edit Organization Location' },
                controller: 'organizationLocationEditCtrl as ole'
            })

            .state('app.tasks', {
                abstract: true,
                url: '/tasks',
                templateUrl: 'tasks/tasks.html',
                data: { pageTitle: 'Tasks'},
                controller: 'tasksCtrl as tc'
            })

            .state('app.tasks.list', {
                abstract: true,
                url: '',
                templateUrl: 'tasks/taskList.html',
                // data: { pageTitle: 'System Users'},
                params: {
                    // Load nested view general by default.
                    autoActivateChild: 'app.tasks.list.details'
                },
                controller: 'taskListCtrl as tl'
            })

            .state('app.tasks.list.details', {
                url: '?q&?created&?assignedto&?client&?asset&?sort&?page',
                templateUrl: 'tasks/taskList.details.html',
                // data: { pageTitle: 'System Users'},
                controller: 'taskListDetailsCtrl as td'
            })

            .state('app.tasks.new', {
                url: '/new',
                templateUrl: 'tasks/taskEdit.html',
                data: { pageTitle: 'New Task' },
                controller: 'taskEditCtrl as te'
            })

            .state('app.tasks.edit', {
                url: '/:id',
                templateUrl: 'tasks/taskEdit.html',
                data: { pageTitle: 'Edit Task' },
                controller: 'taskEditCtrl as te'
            })
        ;
    })

    .run(function($rootScope, $state) {
        $rootScope.$state = $state;
    })

;