'use strict';

angular
	.module('itdocs.controllers.changePassword', [])

	.controller('changePasswordCtrl', function(auth, toaster, $state) {
		var cp = this;
		cp.user = {};
		cp.passwordIncorrect = false;
		cp.passwordNotMatch = false;

		cp.changePassword = function() {
			if (cp.user.newpassword !== cp.user.repeatpassword) {
				cp.passwordIncorrect = false;
				cp.passwordNotMatch = true;
			} else {
				auth.changePassword(cp.user.currentpassword, cp.user.newpassword, cp.user.repeatpassword).then(function(response) {
					toaster.pop({
						type: 'success',
						body: 'Your password has been successfully changed',
						showCloseButton: true,
						timeout: 3000,
						bodyOutputType: 'trustedHtml'
					});
					$state.go('app.dashboard');
				}, function(error) {
					switch (error.data.code) {
						case 101:
							cp.passwordIncorrect = false;
							cp.passwordNotMatch = true;
							break;
						case 102:
							cp.passwordIncorrect = true;
							cp.passwordNotMatch = false;
							break;
						default:
						};
				});
			}
		}
	})

;

