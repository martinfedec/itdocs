describe('Users Controllers', function() {
	var $controller, $q, $httpBackend, API_URL, toaster, auth, $state;

	beforeEach(module(function($provide) {
		$provide.constant("API_URL", "http://localhost:443/api");
	}));

	beforeEach(angular.mock.module('ui.router'));
	beforeEach(angular.mock.module('toaster'));

	beforeEach(angular.mock.module('itdocs.services'));
	beforeEach(angular.mock.module('itdocs.controllers.changePassword'));

	beforeEach(inject(function(_$controller_, _auth_, _API_URL_, _$q_, _$httpBackend_, _toaster_, _$state_) {
		$controller = _$controller_;
		AuthFactory = _auth_;
		$q = _$q_;
		$httpBackend = _$httpBackend_;
		API_URL = _API_URL_;
		toaster = _toaster_;
		$state = _$state_;
	}));

	describe('changePasswordCtrl', function() {
		var changePasswordCtrl;

		beforeEach(function() {
			spyOn($state, 'go');
			spyOn(toaster, 'pop').and.callThrough();
			spyOn(AuthFactory, 'changePassword').and.callThrough();

			changePasswordCtrl = $controller('changePasswordCtrl', {});
		});

		it('should be defined', function() {
			expect(changePasswordCtrl).toBeDefined();
		});

		it('should change the currently logged in users password', function() {
			var response = {message: 'Password has been changed'};
			changePasswordCtrl.user = {
				currentpassword: '12345',
				newpassword: '123',
				repeatpassword: '123'
			};

			expect(changePasswordCtrl.passwordIncorrect).toEqual(false);
			expect(changePasswordCtrl.passwordNotMatch).toEqual(false);
			expect(changePasswordCtrl.changePassword).toBeDefined();

			$httpBackend.whenPOST(API_URL + '/users/changepassword', changePasswordCtrl.user).respond(200, $q.when(response));
			changePasswordCtrl.changePassword();
			$httpBackend.flush();

			expect(toaster.pop).toHaveBeenCalled();
			expect($state.go).toHaveBeenCalledWith('app.dashboard');
		});

		it('should error when new password and repeat do not match check from UI', function() {
			changePasswordCtrl.user = {
				currentpassword: '12345',
				newpassword: '132',
				repeatpassword: '123'
			};

			expect(changePasswordCtrl.passwordIncorrect).toEqual(false);
			expect(changePasswordCtrl.passwordNotMatch).toEqual(false);
			expect(changePasswordCtrl.changePassword).toBeDefined();

			changePasswordCtrl.changePassword();
			expect(changePasswordCtrl.passwordIncorrect).toEqual(false);
			expect(changePasswordCtrl.passwordNotMatch).toEqual(true);
		});

		it('should error when current password is incorrect', function() {
			var response = {code: 102, message: "Current Password Incorrect"};
			changePasswordCtrl.user = {
				currentpassword: '12345',
				newpassword: '123',
				repeatpassword: '123'
			};

			expect(changePasswordCtrl.passwordIncorrect).toEqual(false);
			expect(changePasswordCtrl.passwordNotMatch).toEqual(false);
			expect(changePasswordCtrl.changePassword).toBeDefined();

			$httpBackend.whenPOST(API_URL + '/users/changepassword', changePasswordCtrl.user).respond(400, response);
			changePasswordCtrl.changePassword();
			$httpBackend.flush();

			changePasswordCtrl.changePassword();
			expect(changePasswordCtrl.passwordIncorrect).toEqual(true);
			expect(changePasswordCtrl.passwordNotMatch).toEqual(false);
		});

		// it('should error when no current password supplied', function() {

		// });

		// it('should error when no new password supplied', function() {

		// });

		// it('should error when no repeat password supplied', function() {
			
		// });
	});
});
