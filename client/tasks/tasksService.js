'use strict';

angular.module('itdocs.services.tasks', [])

.factory('tasks', function($http, API_URL){
    return {
        get: function(q) {
            var query = '?';

            if (q.q) {
                query += 'q=completed'
            }

            if (q.page) {
                if (query != '?') {
                    query += '&';
                }

                query += 'page=' + q.page;
            }

            if (q.created) {
                if (query != '?') {
                    query += '&';
                }
                query += 'created=' + q.created;
            }

            if (q.assignedto) {
                if (query != '?') {
                    query += '&';
                }
                query += 'assignedto=' + q.assignedto;
            }

            if (q.client) {
                if (query != '?') {
                    query += '&';
                }
                query += 'client=' + q.client;
            }

            if (q.asset) {
                if (query != '?') {
                    query += '&';
                }
                query += 'asset=' + q.asset;
            }

            if (q.sort) {
                if (query != '?') {
                    query += '&';
                }
                query += 'sort=' + q.sort;
            }

            return $http.get(API_URL + '/tasks' + query).then(function(response) {
        		return response.data;
        	});
        },
        getById: function(id) {
            return $http.get(API_URL + '/tasks/' + id).then(function(response) {
        		return response.data;
        	});  
        },
        add: function(task) {
        	return $http.post(API_URL + '/tasks', task).then(function(response) {
        		return response.data;
        	});
        },
        save: function(task) {
			return $http.put(API_URL + '/tasks/' + task._id, task).then(function(response) {
        		return response.data;
        	});
        },
        remove: function(id) {
            return $http.delete(API_URL + '/tasks/' + id).then(function(response) {
                return response.data;
            });
        },
        getCount: function(q) {
            var query = '?';

            if (q) {
                if (q.q) {
                    query += 'q=completed'
                }

                // if (q.page) {
                //     if (query != '?') {
                //         query += '&';
                //     }

                //     query += 'page=' + q.page;
                // }

                if (q.created) {
                    if (query != '?') {
                        query += '&';
                    }
                    query += 'created=' + q.created;
                }

                if (q.assignedto) {
                    if (query != '?') {
                        query += '&';
                    }
                    query += 'assignedto=' + q.assignedto;
                }

                if (q.client) {
                    if (query != '?') {
                        query += '&';
                    }
                    query += 'client=' + q.client;
                }

                if (q.asset) {
                    if (query != '?') {
                        query += '&';
                    }
                    query += 'asset=' + q.asset;
                }

                // if (q.sort) {
                //     if (query != '?') {
                //         query += '&';
                //     }
                //     query += 'sort=' + q.sort;
                // }
            }
            
            return $http.get(API_URL + '/tasks/count' + query).then(function(response) {
                return response.data;
            });
        },
        getComments: function(task_id) {
            return $http.get(API_URL + '/tasks/' + task_id + '/comments').then(function(response) {
                return response.data;
            });
        },
        addComment: function(task_id, comment) {
            return $http.post(API_URL + '/tasks/' + task_id + '/comments', comment).then(function(response) {
                return response.data;
            });
        },
        saveComment: function(task_id, comment) {
            return $http.put(API_URL + '/tasks/' + task_id + '/comments/' + comment._id, comment).then(function(response) {
                return response.data;
            });
        },
        removeComment: function(task_id, id) {
             return $http.delete(API_URL + '/tasks/' + task_id + '/comments/' + id).then(function(response) {
                return response.data;
            });
        }
    };
})

;