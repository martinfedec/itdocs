describe('Tasks Factory', function() {
	var tasks, API_URL, $httpBackend, $q;

	var tasksList = [
		{_id: 1, title: 'New Task 1', description: 'Setup This', completed: true},
		{_id: 2, title: 'New Task 2', description: 'Setup That', completed: false}
	]

	beforeEach(module(function($provide) {
		$provide.constant("API_URL", "http://localhost:443/api");
	}));

	beforeEach(angular.mock.module('itdocs.services.tasks'));

	beforeEach(inject(function(_tasks_,_$q_, _$httpBackend_, _API_URL_) {
		tasks = _tasks_;
		$q = _$q_;
		$httpBackend = _$httpBackend_;
		API_URL = _API_URL_;
	}));

	it('should be defined', function() {
		expect(tasks).toBeDefined();
	});

	describe('.get(q)', function() {
		beforeEach(function() {
			result = {};
			spyOn(tasks, "get").and.callThrough();
		});

		it('should exist', function() {
			expect(tasks.get).toBeDefined();
		});

		it('should return a list of all tasks, query not supplied', function() {
			$httpBackend.whenGET(API_URL + '/tasks?').respond(200, $q.when(tasksList));

			expect(tasks.get).not.toHaveBeenCalled();
			expect(result).toEqual({});

			tasks.get({}).then(function(res) {
				result = res;
			});

			$httpBackend.flush();

			expect(tasks.get).toHaveBeenCalledWith({});
			expect(result.length).toBe(2);
			expect(result[0]._id).toEqual(1);
			expect(result[0].title).toEqual('New Task 1');
			expect(result[0].description).toEqual('Setup This');
			expect(result[0].completed).toEqual(true);
		});

		it('should return a list of all completed tasks query', function() {
			$httpBackend.whenGET(API_URL + '/tasks?q=completed').respond(200, $q.when(tasksList));

			expect(tasks.get).not.toHaveBeenCalled();
			expect(result).toEqual({});

			tasks.get({q: 'completed'}).then(function(res) {
				result = res;
			});

			$httpBackend.flush();

			expect(tasks.get).toHaveBeenCalledWith({q: 'completed'});
			expect(result.length).toBe(2);
			expect(result[0]._id).toEqual(1);
			expect(result[0].title).toEqual('New Task 1');
			expect(result[0].description).toEqual('Setup This');
			expect(result[0].completed).toEqual(true);
		});

		it('should return a list of all tasks created by query', function() {
			$httpBackend.whenGET(API_URL + '/tasks?created=12345').respond(200, $q.when(tasksList));

			expect(tasks.get).not.toHaveBeenCalled();
			expect(result).toEqual({});

			tasks.get({created: '12345'}).then(function(res) {
				result = res;
			});

			$httpBackend.flush();

			expect(tasks.get).toHaveBeenCalledWith({created: '12345'});
			expect(result.length).toBe(2);
			expect(result[0]._id).toEqual(1);
			expect(result[0].title).toEqual('New Task 1');
			expect(result[0].description).toEqual('Setup This');
			expect(result[0].completed).toEqual(true);
		});

		it('should return a list of all tasks assignedto by query', function() {
			$httpBackend.whenGET(API_URL + '/tasks?assignedto=12345').respond(200, $q.when(tasksList));

			expect(tasks.get).not.toHaveBeenCalled();
			expect(result).toEqual({});

			tasks.get({assignedto: '12345'}).then(function(res) {
				result = res;
			});

			$httpBackend.flush();

			expect(tasks.get).toHaveBeenCalledWith({assignedto: '12345'});
			expect(result.length).toBe(2);
			expect(result[0]._id).toEqual(1);
			expect(result[0].title).toEqual('New Task 1');
			expect(result[0].description).toEqual('Setup This');
			expect(result[0].completed).toEqual(true);
		});

		it('should return a list of all tasks client by query', function() {
			$httpBackend.whenGET(API_URL + '/tasks?client=12345').respond(200, $q.when(tasksList));

			expect(tasks.get).not.toHaveBeenCalled();
			expect(result).toEqual({});

			tasks.get({client: '12345'}).then(function(res) {
				result = res;
			});

			$httpBackend.flush();

			expect(tasks.get).toHaveBeenCalledWith({client: '12345'});
			expect(result.length).toBe(2);
			expect(result[0]._id).toEqual(1);
			expect(result[0].title).toEqual('New Task 1');
			expect(result[0].description).toEqual('Setup This');
			expect(result[0].completed).toEqual(true);
		});

		it('should return a list of all tasks asset by query', function() {
			$httpBackend.whenGET(API_URL + '/tasks?asset=12345').respond(200, $q.when(tasksList));

			expect(tasks.get).not.toHaveBeenCalled();
			expect(result).toEqual({});

			tasks.get({asset: '12345'}).then(function(res) {
				result = res;
			});

			$httpBackend.flush();

			expect(tasks.get).toHaveBeenCalledWith({asset: '12345'});
			expect(result.length).toBe(2);
			expect(result[0]._id).toEqual(1);
			expect(result[0].title).toEqual('New Task 1');
			expect(result[0].description).toEqual('Setup This');
			expect(result[0].completed).toEqual(true);
		});

		it('should return a list of all tasks sort query', function() {
			$httpBackend.whenGET(API_URL + '/tasks?sort=asc').respond(200, $q.when(tasksList));

			expect(tasks.get).not.toHaveBeenCalled();
			expect(result).toEqual({});

			tasks.get({sort: 'asc'}).then(function(res) {
				result = res;
			});

			$httpBackend.flush();

			expect(tasks.get).toHaveBeenCalledWith({sort: 'asc'});
			expect(result.length).toBe(2);
			expect(result[0]._id).toEqual(1);
			expect(result[0].title).toEqual('New Task 1');
			expect(result[0].description).toEqual('Setup This');
			expect(result[0].completed).toEqual(true);
		});

		it('should return a list with created, assigned and completed in desc order', function() {
			$httpBackend.whenGET(API_URL + '/tasks?q=completed&created=12345&assignedto=54321&sort=desc').respond(200, $q.when(tasksList));

			expect(tasks.get).not.toHaveBeenCalled();
			expect(result).toEqual({});

			tasks.get({q: 'completed', created: '12345', assignedto: '54321', sort: 'desc'}).then(function(res) {
				result = res;
			});

			$httpBackend.flush();

			expect(tasks.get).toHaveBeenCalledWith({q: 'completed', created: '12345', assignedto: '54321', sort: 'desc'});
			expect(result.length).toBe(2);
			expect(result[0]._id).toEqual(1);
			expect(result[0].title).toEqual('New Task 1');
			expect(result[0].description).toEqual('Setup This');
			expect(result[0].completed).toEqual(true);
		});

		it('should return a list of tasks with paging', function() {
			$httpBackend.whenGET(API_URL + '/tasks?page=1').respond(200, $q.when(tasksList));

			expect(tasks.get).not.toHaveBeenCalled();
			expect(result).toEqual({});

			tasks.get({page: 1}).then(function(res) {
				result = res;
			});

			$httpBackend.flush();

			expect(tasks.get).toHaveBeenCalledWith({page: 1});
			expect(result.length).toBe(2);
			expect(result[0]._id).toEqual(1);
			expect(result[0].title).toEqual('New Task 1');
			expect(result[0].description).toEqual('Setup This');
			expect(result[0].completed).toEqual(true);
		});
	});

	describe('.getById()', function() {
		beforeEach(function() {
			result = {};
			spyOn(tasks, "getById").and.callThrough();
		});

		it('should exist', function() {
			expect(tasks.getById).toBeDefined();
		});

		it('should return a task when called with a valid id', function() {
			$httpBackend.whenGET(API_URL + '/tasks/' + 1).respond(200, $q.when(tasksList[0]));

			expect(tasks.getById).not.toHaveBeenCalled();
			expect(result).toEqual({});

			tasks.getById(1).then(function(res) {
				result = res;
			});

			$httpBackend.flush();

			expect(tasks.getById).toHaveBeenCalledWith(1);
			expect(result._id).toEqual(1);
			expect(result.title).toEqual('New Task 1');
			expect(result.description).toEqual('Setup This');
			expect(result.completed).toEqual(true);
		});
	});

	describe('.add()', function() {
		beforeEach(function() {
			result = {};
			spyOn(tasks, "add").and.callThrough();
		});

		it('should exist', function() {
			expect(tasks.add).toBeDefined();
		});

		it('should add a new task and return the added task data', function() {
			var created = new Date();
			var newTask = {
				title: 'New Task 3',
				description: '',
				completed: true
			};

			var newTaskResponse = {
				_id: 3,
				title: 'New Task 3',
				description: '',
				completed: true
			};

			$httpBackend.whenPOST(API_URL + '/tasks', newTask).respond(200, $q.when(newTaskResponse));

			expect(tasks.add).not.toHaveBeenCalled();
			expect(result).toEqual({});

			tasks.add(newTask).then(function(res) {
				result = res;
			});

			$httpBackend.flush();

			expect(tasks.add).toHaveBeenCalledWith(newTask);
			expect(result._id).toEqual(3);
			expect(result.title).toEqual(newTask.title);
			expect(result.description).toEqual(newTask.description);
			expect(result.completed).toEqual(newTask.completed);
		});
	});

	describe('.save()', function() {
		beforeEach(function() {
			result = {};
			spyOn(tasks, "save").and.callThrough();
		});

		it('should exist', function() {
			expect(tasks.save).toBeDefined();
		});

		it('should save a task and return the task data', function() {
			var editTask = tasksList[1];

			editTask.title = 'Edit Task 2';
			editTask.description = 'Doing Some Stuff';
			editTask.completed = false;

			var editTaskResponse = editTask;

			$httpBackend.whenPUT(API_URL + '/tasks/' + editTask._id, editTask).respond(200, $q.when(editTaskResponse));

			expect(tasks.save).not.toHaveBeenCalled();
			expect(result).toEqual({});

			tasks.save(editTask).then(function(res) {
				result = res;
			});

			$httpBackend.flush();

			expect(tasks.save).toHaveBeenCalledWith(editTask);
			expect(result._id).toEqual(2);
			expect(result._id).toEqual(editTask._id);
			expect(result.title).toEqual(editTask.title);
			expect(result.description).toEqual(editTask.description);
			expect(result.completed).toEqual(editTask.completed);
		});
	});

	describe('.remove()', function() {
		beforeEach(function() {
			result = {};
			spyOn(tasks, "remove").and.callThrough();
		});

		it('should exist', function() {
			expect(tasks.remove).toBeDefined();
		});

		it('should remove a task based on id', function() {
			var response = {message: 'Task Deleted'};

			$httpBackend.whenDELETE(API_URL + '/tasks/' + 2).respond(200, $q.when(response));

			expect(tasks.remove).not.toHaveBeenCalled();
			expect(result).toEqual({});

			tasks.remove(2).then(function(res) {
				result = res;
			});

			$httpBackend.flush();

			expect(tasks.remove).toHaveBeenCalledWith(2);
			expect(result.message).toEqual('Task Deleted');
		});
	});

	describe('.getCount()', function() {
		beforeEach(function() {
			result = {};
			spyOn(tasks, "getCount").and.callThrough();

			// var tasksList = [];

			// for (i = 1; i < 25; i++) {
			// 	tasksList.push({_id: i, title: 'Task Item ' + i, completed: false});
   //          };
		});

		it('should exist', function() {
			expect(tasks.getCount).toBeDefined();
		});

		it('should return count of 5 with no filters', function() {
			$httpBackend.whenGET(API_URL + '/tasks/count?').respond(200, $q.when({count: 5}));

			expect(tasks.getCount).not.toHaveBeenCalled();
			expect(result).toEqual({});

			tasks.getCount().then(function(res) {
				result = res;
			});

			$httpBackend.flush();

			expect(tasks.getCount).toHaveBeenCalled();
			expect(result.count).toEqual(5);
		});

		it('should return count of 3 with completed filter', function() {
			$httpBackend.whenGET(API_URL + '/tasks/count?q=completed').respond(200, $q.when({count: 3}));

			expect(tasks.getCount).not.toHaveBeenCalled();
			expect(result).toEqual({});

			tasks.getCount({q: 'completed'}).then(function(res) {
				result = res;
			});

			$httpBackend.flush();

			expect(tasks.getCount).toHaveBeenCalledWith({q: 'completed'});
			expect(result.count).toEqual(3);
		});

		it('should return count of 2 with created filter', function() {
			$httpBackend.whenGET(API_URL + '/tasks/count?created=Test User').respond(200, $q.when({count: 2}));

			expect(tasks.getCount).not.toHaveBeenCalled();
			expect(result).toEqual({});

			tasks.getCount({created: 'Test User'}).then(function(res) {
				result = res;
			});

			$httpBackend.flush();

			expect(tasks.getCount).toHaveBeenCalledWith({created: 'Test User'});
			expect(result.count).toEqual(2);
		});

		it('should return count of 1 with assignedto filter', function() {
			$httpBackend.whenGET(API_URL + '/tasks/count?assignedto=Test User 2').respond(200, $q.when({count: 1}));

			expect(tasks.getCount).not.toHaveBeenCalled();
			expect(result).toEqual({});

			tasks.getCount({assignedto: 'Test User 2'}).then(function(res) {
				result = res;
			});

			$httpBackend.flush();

			expect(tasks.getCount).toHaveBeenCalledWith({assignedto: 'Test User 2'});
			expect(result.count).toEqual(1);
		});

		it('should return count of 4 with client filter', function() {
			$httpBackend.whenGET(API_URL + '/tasks/count?client=Client 1').respond(200, $q.when({count: 4}));

			expect(tasks.getCount).not.toHaveBeenCalled();
			expect(result).toEqual({});

			tasks.getCount({client: 'Client 1'}).then(function(res) {
				result = res;
			});

			$httpBackend.flush();

			expect(tasks.getCount).toHaveBeenCalledWith({client: 'Client 1'});
			expect(result.count).toEqual(4);
		});

		it('should return count of 6 with client filter', function() {
			$httpBackend.whenGET(API_URL + '/tasks/count?asset=Asset 1').respond(200, $q.when({count: 6}));

			expect(tasks.getCount).not.toHaveBeenCalled();
			expect(result).toEqual({});

			tasks.getCount({asset: 'Asset 1'}).then(function(res) {
				result = res;
			});

			$httpBackend.flush();

			expect(tasks.getCount).toHaveBeenCalledWith({asset: 'Asset 1'});
			expect(result.count).toEqual(6);
		});
	});

	describe('comments', function() {
		tasksList[0].comments = [
			{_id: 1, comment: 'Comment 1'},
			{_id: 2, comment: 'Comment 2'},
			{_id: 3, comment: 'Comment 3'}
		];

		tasksList[1].comments = '';

		describe('.getComments()', function() {
			beforeEach(function() {
				result = {};
				spyOn(tasks, "getComments").and.callThrough();
			});

			it('should exist', function() {
				expect(tasks.getComments).toBeDefined();
			});

			it('should return all the tasks', function() {
				$httpBackend.whenGET(API_URL + '/tasks/' + 1 + '/comments').respond(200, $q.when(tasksList[0].comments));

				expect(tasks.getComments).not.toHaveBeenCalled();
				expect(result).toEqual({});

				tasks.getComments(1).then(function(res) {
					result = res;
				});

				$httpBackend.flush();

				expect(tasks.getComments).toHaveBeenCalled();
				expect(result).toEqual(tasksList[0].comments);
				expect(result[0]._id).toEqual(1);
				expect(result[0].comment).toEqual('Comment 1');
			});
		});

		describe('.addComment()', function() {
			beforeEach(function() {
				result = {};
				spyOn(tasks, "addComment").and.callThrough();
			});

			it('should exist', function() {
				expect(tasks.addComment).toBeDefined();
			});

			it('should return add a comment', function() {
				var newComment = {
					comment: 'New Comment'
				};

				var addedComment = {
					_id: 4,
					comment: 'New Comment'
				};

				$httpBackend.whenPOST(API_URL + '/tasks/' + 1 + '/comments', newComment).respond(200, $q.when(addedComment));

				expect(tasks.addComment).not.toHaveBeenCalled();
				expect(result).toEqual({});

				tasks.addComment(1, newComment).then(function(res) {
					result = res;
				});

				$httpBackend.flush();

				expect(tasks.addComment).toHaveBeenCalled();
				expect(result).toEqual(addedComment);
				expect(result._id).toEqual(4);
				expect(result.comment).toEqual('New Comment');
			});
		});

		describe('.editComment()', function() {
			beforeEach(function() {
				result = {};
				spyOn(tasks, "saveComment").and.callThrough();
			});

			it('should exist', function() {
				expect(tasks.saveComment).toBeDefined();
			});

			it('should save a comment and return the comment', function() {
				var editComment = tasksList[0].comments[1];

				editComment.comment = 'Change Comment';

				var response = editComment;

				$httpBackend.whenPUT(API_URL + '/tasks/' + 1 + '/comments/' + 2, editComment).respond(200, $q.when(response));

				expect(tasks.saveComment).not.toHaveBeenCalled();
				expect(result).toEqual({});

				tasks.saveComment(1, editComment).then(function(res) {
					result = res;
				});

				$httpBackend.flush();

				expect(tasks.saveComment).toHaveBeenCalled();
				expect(result).toEqual(editComment);
				expect(result._id).toEqual(2);
				expect(result.comment).toEqual('Change Comment');
			});
		});

		describe('.removeComment()', function() {
			beforeEach(function() {
				result = {};
				spyOn(tasks, "removeComment").and.callThrough();
			});

			it('should exist', function() {
				expect(tasks.removeComment).toBeDefined();
			});

			it('should remove a comment based on id', function() {
				var response = {message: 'Comment Deleted'};

				$httpBackend.whenDELETE(API_URL + '/tasks/' + 1 + '/comments/' + 2).respond(200, $q.when(response));

				expect(tasks.removeComment).not.toHaveBeenCalled();
				expect(result).toEqual({});

				tasks.removeComment(1, 2).then(function(res) {
					result = res;
				});

				$httpBackend.flush();

				expect(tasks.removeComment).toHaveBeenCalledWith(1, 2);
				expect(result.message).toEqual('Comment Deleted');
			});
		});

	});
});