describe('Tasks Controller', function() {
	var $controller, API_URL, TasksFactory, UsersFactory, ClientsFactory, $q, $httpBackend, toaster
	var taskList, userList, clientList, assetList;

	taskList = [
		{_id: 1, title: 'New Task 1', description: 'Do Something', completed: false},
		{_id: 2, title: 'New Task 2', description: 'Not that thing', completed: true},
		{_id: 3, title: 'New Task 3', description: 'OK maybe that one', completed: false}
	];

	var userList = [
		{_id: 1, name: 'User 1'},
		{_id: 2, name: 'User 2'},
		{_id: 3, name: 'User 3'}
	];

	var clientList = [
		{_id: 1, name: 'Client 1'},
		{_id: 2, name: 'Client 2'},
		{_id: 3, name: 'Client 3'}
	];

	var assetList = [
		{_id: 1, name: 'Asset 1', client: 1},
		{_id: 2, name: 'Asset 2', client: 2},
		{_id: 3, name: 'Asset 3', client: 3}
	];

	var commentList = [
		{_id: 1, details: 'Comment 1', type: 'COMMENT'},
		{_id: 2, details: 'Comment 2', type: 'COMMENT'},
		{_id: 3, details: 'Comment 3', type: 'COMMENT'}
	];

	beforeEach(module(function($provide) {
		$provide.constant("API_URL", "http://localhost:443/api");
	}));

	beforeEach(angular.mock.module('ui.router'));
	beforeEach(angular.mock.module('toaster'));
	beforeEach(angular.mock.module('angularMoment'));	
	beforeEach(angular.mock.module('itdocs.services'));
	beforeEach(angular.mock.module('itdocs.services.tasks'));
	beforeEach(angular.mock.module('itdocs.services.users'));
	beforeEach(angular.mock.module('itdocs.services.clients'));
	beforeEach(angular.mock.module('itdocs.controllers.tasks'));

	beforeEach(function() {
		inject(function(_$controller_, _API_URL_, _$q_, _$httpBackend_, _toaster_, _tasks_, _users_, _clients_) {
			$controller = _$controller_;
			API_URL = _API_URL_;
			$q = _$q_;
			$httpBackend = _$httpBackend_;
			toaster = _toaster_;
			TasksFactory = _tasks_;
			UsersFactory = _users_;
			ClientsFactory = _clients_;
		});

		spyOn(TasksFactory, 'get').and.callThrough();
		spyOn(TasksFactory, 'remove').and.callThrough();
		spyOn(toaster, 'pop');
	});
		

	describe('tasksCtrl', function() {
		var tasksCtrl;

		beforeEach(function() {
			tasksCtrl = $controller('tasksCtrl');
		});

		it('should be defined', function() {
			expect(tasksCtrl).toBeDefined();
		});
	});

	describe('taskListCtrl', function() {
		var taskListCtrl;

		beforeEach(function() {
			inject(function(_$state_) {
				$state = _$state_;
			})

			spyOn(UsersFactory, 'get').and.callThrough();
			spyOn(ClientsFactory, 'get').and.callThrough();
			spyOn($state, 'reload');
			spyOn($state, 'go');

			taskListCtrl = $controller('taskListCtrl');
		});

		it('should be defined', function() {
			expect(taskListCtrl).toBeDefined();
		});

		it('should initially setup the screen defaults, no query selected', function() {
			expect(taskListCtrl.created).toEqual({open: false, selected: undefined});
			expect(taskListCtrl.assignedto).toEqual({open: false, selected: undefined});
			expect(taskListCtrl.client).toEqual({open: false, selected: undefined});
			expect(taskListCtrl.asset).toEqual({open: false, selected: undefined});
			expect(taskListCtrl.sort).toEqual({open: false, selected: 'desc'});

			$httpBackend.whenGET(API_URL + '/users').respond(200, $q.when(userList));
			$httpBackend.whenGET(API_URL + '/clients').respond(200, $q.when(clientList));
			$httpBackend.flush();

			expect(UsersFactory.get).toHaveBeenCalledWith();
			expect(ClientsFactory.get).toHaveBeenCalledWith();
		});

		describe('filter by createdby', function() {
			beforeEach(function() {
				$state.params.created = 1;
				taskListCtrl = $controller('taskListCtrl');
			});

			it('should filter by createdby', function() {
				expect(taskListCtrl.created).toEqual({open: false, selected: 1});
				expect(taskListCtrl.assignedto).toEqual({open: false, selected: undefined});
				expect(taskListCtrl.client).toEqual({open: false, selected: undefined});
				expect(taskListCtrl.asset).toEqual({open: false, selected: undefined});
				expect(taskListCtrl.sort).toEqual({open: false, selected: 'desc'});
			});
		});
		
		describe('filter by assignedto', function() {
			beforeEach(function() {
				inject(function(_$state_) {
					$state = _$state_;
				})

				$state.params.assignedto = 2;
				taskListCtrl = $controller('taskListCtrl');
			});

			it('should filter by assignedto', function() {
				expect(taskListCtrl.created).toEqual({open: false, selected: undefined});
				expect(taskListCtrl.assignedto).toEqual({open: false, selected: 2});
				expect(taskListCtrl.client).toEqual({open: false, selected: undefined});
				expect(taskListCtrl.asset).toEqual({open: false, selected: undefined});
				expect(taskListCtrl.sort).toEqual({open: false, selected: 'desc'});
			});
		});

		describe('filter by client', function() {
			beforeEach(function() {
				inject(function(_$state_) {
					$state = _$state_;
				})

				$state.params.client = 3;
				taskListCtrl = $controller('taskListCtrl');
			});

			it('should filter by client', function() {
				expect(taskListCtrl.created).toEqual({open: false, selected: undefined});
				expect(taskListCtrl.assignedto).toEqual({open: false, selected: undefined});
				expect(taskListCtrl.client).toEqual({open: false, selected: 3});
				expect(taskListCtrl.asset).toEqual({open: false, selected: undefined});
				expect(taskListCtrl.sort).toEqual({open: false, selected: 'desc'});
			});
		});

		describe('filter by asset', function() {
			beforeEach(function() {
				inject(function(_$state_) {
					$state = _$state_;
				})

				$state.params.asset = 4;
				taskListCtrl = $controller('taskListCtrl');
			});

			it('should filter by asset', function() {
				expect(taskListCtrl.created).toEqual({open: false, selected: undefined});
				expect(taskListCtrl.assignedto).toEqual({open: false, selected: undefined});
				expect(taskListCtrl.client).toEqual({open: false, selected: undefined});
				expect(taskListCtrl.asset).toEqual({open: false, selected: 4});
				expect(taskListCtrl.sort).toEqual({open: false, selected: 'desc'});
			});
		});

		describe('sort the list', function() {
			beforeEach(function() {
				inject(function(_$state_) {
					$state = _$state_;
				})

				$state.params.sort = 'desc';
				taskListCtrl = $controller('taskListCtrl');
			});

			it('should filter by asset', function() {
				expect(taskListCtrl.created).toEqual({open: false, selected: undefined});
				expect(taskListCtrl.assignedto).toEqual({open: false, selected: undefined});
				expect(taskListCtrl.client).toEqual({open: false, selected: undefined});
				expect(taskListCtrl.asset).toEqual({open: false, selected: undefined});
				expect(taskListCtrl.sort).toEqual({open: false, selected: 'desc'});
			});
		});

		describe('addTask()', function() {
			beforeEach(function() {
				spyOn(TasksFactory, 'add').and.callThrough();
			});

			it('Should add a new quick task', function() {
				var newTask = {
					title: 'Quick Add Task',
					checklists: []
				};

				$httpBackend.whenGET(API_URL + '/users').respond(200, $q.when(userList));
				$httpBackend.whenGET(API_URL + '/clients').respond(200, $q.when(clientList));
				$httpBackend.whenPOST(API_URL + '/tasks', newTask).respond(200, $q.when({_id: 1, title: 'Quick Add Task', completed: false, checklists: []}));

				taskListCtrl.newTask = 'Quick Add Task';
				taskListCtrl.addTask();
				$httpBackend.flush();
				
				expect(TasksFactory.add).toHaveBeenCalledWith({title: 'Quick Add Task', checklists: []});
				expect($state.reload).toHaveBeenCalledWith('app.tasks.list.details');
				expect(taskListCtrl.newTask).toEqual('');
			});

			it('Should go to new task page', function() {
				taskListCtrl.addTask();
				expect($state.go).toHaveBeenCalledWith('app.tasks.new');
			});
		});
		
		describe('createdFilterChange()', function() {
			it('Select a created by user', function() {
				taskListCtrl.createdFilterChange(1);
				expect(taskListCtrl.created.selected).toEqual(1);
				expect(taskListCtrl.created.open).toEqual(false);
				expect($state.go).toHaveBeenCalledWith('app.tasks.list.details', { created: 1 });
				expect(taskListCtrl.createdSearch).toEqual('');
			});

			it('Deselect a created by user', function() {
				taskListCtrl.createdFilterChange();
				expect(taskListCtrl.created.selected).toEqual(undefined);
				expect(taskListCtrl.created.open).toEqual(false);
				expect($state.go).toHaveBeenCalledWith('app.tasks.list.details', { created: '' });
				expect(taskListCtrl.createdSearch).toEqual('');
			});
		});

		describe('createdFilterChange()', function() {
			it('Select a assignedto user', function() {
				taskListCtrl.assignedtoFilterChange(1);
				expect(taskListCtrl.assignedto.selected).toEqual(1);
				expect(taskListCtrl.assignedto.open).toEqual(false);
				expect($state.go).toHaveBeenCalledWith('app.tasks.list.details', { assignedto: 1 });
				expect(taskListCtrl.assignedtoSearch).toEqual('');
			});

			it('Deselect a assignedto user', function() {
				taskListCtrl.assignedtoFilterChange();
				expect(taskListCtrl.assignedto.selected).toEqual(undefined);
				expect(taskListCtrl.assignedto.open).toEqual(false);
				expect($state.go).toHaveBeenCalledWith('app.tasks.list.details', { assignedto: '' });
				expect(taskListCtrl.assignedtoSearch).toEqual('');
			});
		});

		describe('clientFilterChange()', function() {
			it('Select a client', function() {
				taskListCtrl.clientFilterChange({name: 1});
				expect(taskListCtrl.client.selected).toEqual(1);
				expect(taskListCtrl.client.open).toEqual(false);
				expect($state.go).toHaveBeenCalledWith('app.tasks.list.details', { client: 1, asset: '' });
				expect(taskListCtrl.clientSearch).toEqual('');
			});

			it('Deselect a client', function() {
				taskListCtrl.clientFilterChange();
				expect(taskListCtrl.client.selected).toEqual(undefined);
				expect(taskListCtrl.client.open).toEqual(false);
				expect($state.go).toHaveBeenCalledWith('app.tasks.list.details', { client: '', asset: ''  });
				expect(taskListCtrl.clientSearch).toEqual('');
			});
		});

		describe('assetFilterChange()', function() {
			it('Select a asset', function() {
				taskListCtrl.assetFilterChange(1);
				expect(taskListCtrl.asset.selected).toEqual(1);
				expect(taskListCtrl.asset.open).toEqual(false);
				expect($state.go).toHaveBeenCalledWith('app.tasks.list.details', { asset: 1 });
				expect(taskListCtrl.assetSearch).toEqual('');
			});

			it('Deselect a asset', function() {
				taskListCtrl.assetFilterChange();
				expect(taskListCtrl.asset.selected).toEqual(undefined);
				expect(taskListCtrl.asset.open).toEqual(false);
				expect($state.go).toHaveBeenCalledWith('app.tasks.list.details', { asset: '' });
				expect(taskListCtrl.assetSearch).toEqual('');
			});
		});

		describe('sortFilterChange()', function() {
			it('Sort by newest', function() {
				taskListCtrl.sortFilterChange('desc');
				expect(taskListCtrl.sort.selected).toEqual('desc');
				expect(taskListCtrl.sort.open).toEqual(false);
				expect($state.go).toHaveBeenCalledWith('app.tasks.list.details', { sort: 'desc' });
			});

			it('sort by oldest', function() {
				taskListCtrl.sortFilterChange();
				expect(taskListCtrl.client.selected).toEqual(undefined);
				expect(taskListCtrl.client.open).toEqual(false);
				expect($state.go).toHaveBeenCalledWith('app.tasks.list.details', { sort: '' });
			});
		});
	});

	describe('taskListDetailsCtrl', function() {
		var taskListDetailsCtrl, countList;

		openList = {
			open: 1
		};

		completedList = {
			completed: 4
		};

		beforeEach(function() {
			inject(function(_$state_, _$stateParams_) {
				$state = _$state_;
				$stateParams = _$stateParams_;
			});

			spyOn($state, 'go');
			spyOn(TasksFactory, 'getCount').and.callThrough();
			spyOn(TasksFactory, 'save').and.callThrough();

			taskListDetailsCtrl = $controller('taskListDetailsCtrl');
		});

		it('should be difined', function() {
			expect(taskListDetailsCtrl).toBeDefined();
		});

		it('should load the screen defaults, no query passed', function() {
			$httpBackend.whenGET(API_URL + '/tasks?').respond(200, $q.when(taskList));
			$httpBackend.whenGET(API_URL + '/tasks/count?').respond(200, $q.when(openList));
			$httpBackend.whenGET(API_URL + '/tasks/count?q=completed').respond(200, $q.when(completedList));

			$httpBackend.flush();

			expect(taskListDetailsCtrl.tasks).toEqual(taskList);
			expect(TasksFactory.get).toHaveBeenCalledWith($stateParams);
			expect(TasksFactory.getCount).toHaveBeenCalled();
			expect(TasksFactory.getCount).toHaveBeenCalledWith({q: 'completed'});
			expect(taskListDetailsCtrl.taskFilter).toEqual('is:open');
		});

		it('should clear any applied filters', function() {
			taskListDetailsCtrl.clearFilter();
			expect($state.go).toHaveBeenCalledWith('app.tasks.list.details', {q: '', created: '', assignedto: '', client: '', asset: '', sort: '', page: ''})
		});

		it('should mark an item complete', function() {
			var task = taskList[0];
			task.completed = true;

			$httpBackend.whenGET(API_URL + '/tasks?').respond(200, $q.when(taskList));
			$httpBackend.whenPUT(API_URL + '/tasks/' + taskList[0]._id, task).respond(200, $q.when(task));
			$httpBackend.whenGET(API_URL + '/tasks/count?').respond(200, $q.when(openList));
			$httpBackend.whenGET(API_URL + '/tasks/count?q=completed').respond(200, $q.when(completedList));

			taskListDetailsCtrl.checkCompleted(task);
			$httpBackend.flush();

			expect(toaster.pop).toHaveBeenCalledWith({
				type: 'success',
				body: 'Task has marked as complete',
				showCloseButton: true,
				timeout: 3000
			});

			expect(TasksFactory.get).toHaveBeenCalledWith($stateParams);
			expect(TasksFactory.getCount).toHaveBeenCalled();
			expect(TasksFactory.getCount).toHaveBeenCalledWith({q: 'completed'});
			expect(TasksFactory.save).toHaveBeenCalledWith(task);
		});

		describe('Passing createdby to details screen', function() {
			beforeEach(function() {
				$stateParams.created = 'User 1';
				taskListDetailsCtrl = $controller('taskListDetailsCtrl');
			});

			it('Should query based on created by', function() {
				$httpBackend.whenGET(API_URL + '/tasks?').respond(200, $q.when(taskList));
				$httpBackend.whenGET(API_URL + '/tasks?created=User 1').respond(200, $q.when(taskList));
				$httpBackend.whenGET(API_URL + '/tasks/count?').respond(200, $q.when(openList));
				$httpBackend.whenGET(API_URL + '/tasks/count?created=User 1').respond(200, $q.when(openList));
				$httpBackend.whenGET(API_URL + '/tasks/count?q=completed&created=User 1').respond(200, $q.when(openList));
				$httpBackend.whenGET(API_URL + '/tasks/count?q=completed').respond(200, $q.when(completedList));

				$httpBackend.flush();

				expect(taskListDetailsCtrl.tasks).toEqual(taskList);
				expect(TasksFactory.get).toHaveBeenCalledWith($stateParams);
				expect(TasksFactory.getCount).toHaveBeenCalled();
				expect(TasksFactory.getCount).toHaveBeenCalledWith({q: 'completed', created: 'User 1'});
				expect(taskListDetailsCtrl.taskFilter).toEqual('is:open createdby:User 1');
			});
		});

		describe('Passing assigned to to details screen', function() {
			beforeEach(function() {
				$stateParams.assignedto = 'User 1';
				taskListDetailsCtrl = $controller('taskListDetailsCtrl');
			});

			it('Should query based on assignedto', function() {
				$httpBackend.whenGET(API_URL + '/tasks?').respond(200, $q.when(taskList));
				$httpBackend.whenGET(API_URL + '/tasks?assignedto=User 1').respond(200, $q.when(taskList));
				$httpBackend.whenGET(API_URL + '/tasks/count?').respond(200, $q.when(openList));
				$httpBackend.whenGET(API_URL + '/tasks/count?assignedto=User 1').respond(200, $q.when(openList));
				$httpBackend.whenGET(API_URL + '/tasks/count?q=completed&assignedto=User 1').respond(200, $q.when(openList));
				$httpBackend.whenGET(API_URL + '/tasks/count?q=completed').respond(200, $q.when(completedList));

				$httpBackend.flush();

				expect(taskListDetailsCtrl.tasks).toEqual(taskList);
				expect(TasksFactory.get).toHaveBeenCalledWith($stateParams);
				expect(TasksFactory.getCount).toHaveBeenCalled();
				expect(TasksFactory.getCount).toHaveBeenCalledWith({q: 'completed', assignedto: 'User 1'});
				expect(taskListDetailsCtrl.taskFilter).toEqual('is:open assignedto:User 1');
			});
		});

		describe('Passing client to to details screen', function() {
			beforeEach(function() {
				$stateParams.client = 'Client 1';
				taskListDetailsCtrl = $controller('taskListDetailsCtrl');
			});

			it('Should query based on client', function() {
				$httpBackend.whenGET(API_URL + '/tasks?').respond(200, $q.when(taskList));
				$httpBackend.whenGET(API_URL + '/tasks?client=Client 1').respond(200, $q.when(taskList));
				$httpBackend.whenGET(API_URL + '/tasks/count?').respond(200, $q.when(openList));
				$httpBackend.whenGET(API_URL + '/tasks/count?client=Client 1').respond(200, $q.when(openList));
				$httpBackend.whenGET(API_URL + '/tasks/count?q=completed&client=Client 1').respond(200, $q.when(openList));	
				$httpBackend.whenGET(API_URL + '/tasks/count?q=completed').respond(200, $q.when(completedList));

				$httpBackend.flush();

				expect(taskListDetailsCtrl.tasks).toEqual(taskList);
				expect(TasksFactory.get).toHaveBeenCalledWith($stateParams);
				expect(TasksFactory.getCount).toHaveBeenCalled();
				expect(TasksFactory.getCount).toHaveBeenCalledWith({q: 'completed', client: 'Client 1'});
				expect(taskListDetailsCtrl.taskFilter).toEqual('is:open client:Client 1');
			});
		});

		describe('Passing asset to to details screen', function() {
			beforeEach(function() {
				$stateParams.asset = 'Asset 1';
				taskListDetailsCtrl = $controller('taskListDetailsCtrl');
			});

			it('Should query based on asset', function() {
				$httpBackend.whenGET(API_URL + '/tasks?').respond(200, $q.when(taskList));
				$httpBackend.whenGET(API_URL + '/tasks?asset=Asset 1').respond(200, $q.when(taskList));
				$httpBackend.whenGET(API_URL + '/tasks/count?').respond(200, $q.when(openList));
				$httpBackend.whenGET(API_URL + '/tasks/count?asset=Asset 1').respond(200, $q.when(openList));
				$httpBackend.whenGET(API_URL + '/tasks/count?q=completed&asset=Asset 1').respond(200, $q.when(openList));	
				$httpBackend.whenGET(API_URL + '/tasks/count?q=completed').respond(200, $q.when(completedList));

				$httpBackend.flush();

				expect(taskListDetailsCtrl.tasks).toEqual(taskList);
				expect(TasksFactory.get).toHaveBeenCalledWith($stateParams);
				expect(TasksFactory.getCount).toHaveBeenCalled();
				expect(TasksFactory.getCount).toHaveBeenCalledWith({q: 'completed', asset: 'Asset 1'});
				expect(taskListDetailsCtrl.taskFilter).toEqual('is:open asset:Asset 1');
			});
		});

		describe('Passing sort to to details screen', function() {
			beforeEach(function() {
				$stateParams.sort = 'asc';
				taskListDetailsCtrl = $controller('taskListDetailsCtrl');
			});

			it('Should query based on sort', function() {
				$httpBackend.whenGET(API_URL + '/tasks?').respond(200, $q.when(taskList));
				$httpBackend.whenGET(API_URL + '/tasks?sort=asc').respond(200, $q.when(taskList));
				$httpBackend.whenGET(API_URL + '/tasks/count?').respond(200, $q.when(openList));
				$httpBackend.whenGET(API_URL + '/tasks/count?sort=asc').respond(200, $q.when(openList));
				$httpBackend.whenGET(API_URL + '/tasks/count?q=completed&sort=asc').respond(200, $q.when(openList));
				$httpBackend.whenGET(API_URL + '/tasks/count?q=completed').respond(200, $q.when(completedList));

				$httpBackend.flush();

				expect(taskListDetailsCtrl.tasks).toEqual(taskList);
				expect(TasksFactory.get).toHaveBeenCalledWith($stateParams);
				expect(TasksFactory.getCount).toHaveBeenCalled();
				expect(TasksFactory.getCount).toHaveBeenCalledWith({q: 'completed', sort: 'asc'});
				expect(taskListDetailsCtrl.taskFilter).toEqual('is:open sort:asc');
			});
		});

		describe('Verify Counts', function() {
			beforeEach(function() {

			});

			it('Should have a not completed count of 5 and completed items count of 3', function() {
				$httpBackend.whenGET(API_URL + '/tasks?').respond(200, $q.when(taskList));
				$httpBackend.whenGET(API_URL + '/tasks/count?').respond(200, $q.when({count: 5}));
				$httpBackend.whenGET(API_URL + '/tasks/count?q=completed').respond(200, $q.when({count: 3}));

				$httpBackend.flush();

				expect(taskListDetailsCtrl.open).toEqual(5);
				expect(taskListDetailsCtrl.completed).toEqual(3);
			});
		});

		describe('Paging', function() {
			beforeEach(function() {
			});

			it('should have 5 pages and default to page 1', function() {
				$httpBackend.whenGET(API_URL + '/tasks?').respond(200, $q.when(taskList));
				$httpBackend.whenGET(API_URL + '/tasks/count?').respond(200, $q.when({count: 100}));
				$httpBackend.whenGET(API_URL + '/tasks/count?q=completed').respond(200, $q.when({count: 3}));
				$httpBackend.flush();
				
				expect(taskListDetailsCtrl.totalItems).toEqual(100);
				expect(taskListDetailsCtrl.currentPage).toEqual(undefined);
			});

			it('should have 3 pages and go to page 2', function() {
				$stateParams.page = 2;
				$httpBackend.whenGET(API_URL + '/tasks?').respond(200, $q.when(taskList));
				$httpBackend.whenGET(API_URL + '/tasks/count?').respond(200, $q.when({count: 60}));
				$httpBackend.whenGET(API_URL + '/tasks/count?q=completed').respond(200, $q.when({count: 3}));
				$httpBackend.flush();
				
				expect(taskListDetailsCtrl.totalItems).toEqual(60);
				expect(taskListDetailsCtrl.currentPage).toEqual(2);
			});

			it('changePage should change to the next page', function() {
				taskListDetailsCtrl.currentPage = 1;
				taskListDetailsCtrl.changePage();
				expect($state.go).toHaveBeenCalledWith('.', {page: 1}, {notify: false});
				expect($stateParams.page).toEqual(1);
				expect(taskListDetailsCtrl.currentPage).toEqual(1);
			});


		});
	});

	describe('taskEditCtrl', function() {
		var taskEditCtrl, $state, $stateParams, AuthFactory;

		beforeEach(inject(function(_$state_, _$stateParams_, _auth_) {
			$stateParams = _$stateParams_;
			$state = _$state_;
			AuthFactory = _auth_;
		}));

		beforeEach(function() {
			spyOn($state, 'go');
			spyOn(UsersFactory, 'get').and.callThrough();
			spyOn(ClientsFactory, 'get').and.callThrough();
			spyOn(AuthFactory, 'getDetails').and.callThrough();

			taskEditCtrl = $controller('taskEditCtrl');

			taskEditCtrl.taskForm = {
				$setDirty: function() {}
			};

			spyOn(taskEditCtrl.taskForm, '$setDirty');
		});

		it('should be defined', function() {
			expect(taskEditCtrl).toBeDefined();
		});

		describe('Add new task', function() {
			var newTask = {
				title: 'New Task 1',
				description: 'Do Some Stuff',
				completed: false
			};

			var resTask = {
				_id: 4,
				title: 'New Task 1',
				description: 'Do Some Stuff',
				completed: false,
				createddat: new Date(),
				active: true
			};

			var assetList = [
				{ _id: 1, name: 'Asset 1', client: 1 }
			];

			beforeEach(function() {
				spyOn(TasksFactory, 'add').and.callThrough();
				spyOn(ClientsFactory, 'getAssets').and.callThrough();
			});

			it('should setup screen defaults', function() {
				expect(taskEditCtrl.task).toEqual({checklists: []});
				expect(taskEditCtrl.view).toEqual({});
				expect(taskEditCtrl.checklistName).toEqual('Checklist');
				expect(taskEditCtrl.taskProgress).toEqual([]);

				expect(UsersFactory.get).toHaveBeenCalledWith();
				$httpBackend.whenGET(API_URL + '/users').respond(200, $q.when(userList));
				
				expect(ClientsFactory.get).toHaveBeenCalledWith();
				$httpBackend.whenGET(API_URL + '/clients').respond(200, $q.when(clientList));

				expect(taskEditCtrl.mode).toEqual('Add New');
				expect(AuthFactory.getDetails).toHaveBeenCalled();

				expect(taskEditCtrl.sortableOptions).toEqual({
					handle: '.ckItemHandle',
					connectWith: '.todo-list'
				});

				$httpBackend.flush();
			});

			it('should add a new task', function() {
				$httpBackend.whenGET(API_URL + '/users').respond(200, $q.when(userList));
				$httpBackend.whenGET(API_URL + '/clients').respond(200, $q.when(clientList));
				$httpBackend.whenPOST(API_URL + '/tasks', newTask).respond(200, $q.when(resTask))
				
				taskEditCtrl.task = newTask;
				taskEditCtrl.saveTask();

				$httpBackend.flush();

				expect(toaster.pop).toHaveBeenCalledWith({
					type: 'success',
					body: 'Task has been added',
					showCloseButton: true,
					timeout: 3000
				})
				expect($state.go).toHaveBeenCalledWith('app.tasks.list.details');
			});

			it('should assign a user, no user previously selected', function() {
				taskEditCtrl.assignedToChange({_id: 1, name: 'Test User'});

				expect(taskEditCtrl.task.assignedto).toEqual(1);
				expect(taskEditCtrl.view.assignedto).toEqual('Test User');
				expect(taskEditCtrl.assignedto.selected).toEqual(1);

				expect(taskEditCtrl.taskForm.$setDirty).toHaveBeenCalled();
				expect(taskEditCtrl.assignedto.open).toEqual(false);
			});

			it('should assign a new user, previous user selected', function() {
				taskEditCtrl.task.assignedto = 1;
				taskEditCtrl.view.assignedto = 1;
				taskEditCtrl.assignedto.selected = 1;

				taskEditCtrl.assignedToChange({_id: 2, name: 'Test User 2'});

				expect(taskEditCtrl.task.assignedto).toEqual(2);
				expect(taskEditCtrl.view.assignedto).toEqual('Test User 2');
				expect(taskEditCtrl.assignedto.selected).toEqual(2);

				expect(taskEditCtrl.taskForm.$setDirty).toHaveBeenCalled();
				expect(taskEditCtrl.assignedto.open).toEqual(false);
			});

			it ('should remove the currently assigned user', function() {
				taskEditCtrl.task.assignedto = 1;
				taskEditCtrl.view.assignedto = 1;
				taskEditCtrl.assignedto.selected = 1;

				taskEditCtrl.assignedToChange({_id: 1, name: 'Test User 1'});

				expect(taskEditCtrl.task.assignedto).toEqual(null);
				expect(taskEditCtrl.view.assignedto).toEqual('');
				expect(taskEditCtrl.assignedto.selected).toEqual('');

				expect(taskEditCtrl.taskForm.$setDirty).toHaveBeenCalled();
				expect(taskEditCtrl.assignedto.open).toEqual(false);
			});

			it('should assign a client', function() {
				$httpBackend.whenGET(API_URL + '/users').respond(200, $q.when(userList));
				$httpBackend.whenGET(API_URL + '/clients').respond(200, $q.when(clientList));

				taskEditCtrl.clientChange({_id: 1, name: 'Client 1'});
				expect(taskEditCtrl.task.client).toEqual(1);
				expect(taskEditCtrl.view.client).toEqual('Client 1');
				expect(taskEditCtrl.client.selected).toEqual(1);

				$httpBackend.whenGET(API_URL + '/clients/' + 1 + '/assets?top=10').respond(200, $q.when({total: 1, data: assetList}));
				$httpBackend.flush();

				expect(ClientsFactory.getAssets).toHaveBeenCalledWith(1, {top: 10});
				expect(taskEditCtrl.assets).toEqual(assetList);
				expect(taskEditCtrl.taskForm.$setDirty).toHaveBeenCalled();
				expect(taskEditCtrl.client.open).toEqual(false);
			});

			it('should remove the currently assigned client', function() {
				taskEditCtrl.task.client = 1;
				taskEditCtrl.view.client = 'Client 1';
				taskEditCtrl.client.selected = 1;

				taskEditCtrl.clientChange({_id: 1, name: 'Client 1'});

				expect(taskEditCtrl.task.asset).toEqual(null);
				expect(taskEditCtrl.view.asset).toEqual('');
				expect(taskEditCtrl.asset.selected).toEqual('');
				expect(taskEditCtrl.assets).toEqual([]);

				expect(taskEditCtrl.task.client).toEqual(null);
				expect(taskEditCtrl.view.client).toEqual('');
				expect(taskEditCtrl.client.selected).toEqual('');

				expect(taskEditCtrl.taskForm.$setDirty).toHaveBeenCalled();
				expect(taskEditCtrl.client.open).toEqual(false);
			});

			it('should select an asset', function() {
				taskEditCtrl.assetChange(assetList[0]);

				expect(taskEditCtrl.task.asset).toEqual(assetList[0]._id);
				expect(taskEditCtrl.view.asset).toEqual(assetList[0].name);
				expect(taskEditCtrl.asset.selected).toEqual(assetList[0]._id);

				expect(taskEditCtrl.taskForm.$setDirty).toHaveBeenCalled();
				expect(taskEditCtrl.asset.open).toEqual(false);
			});

			it('should remove a selected an asset', function() {
				taskEditCtrl.task.asset = assetList;
				taskEditCtrl.view.asset = assetList[0]._id;
				taskEditCtrl.asset.selected = assetList[0]._id;

				taskEditCtrl.assetChange(assetList[0]);

				expect(taskEditCtrl.task.asset).toEqual(null);
				expect(taskEditCtrl.view.asset).toEqual('');
				expect(taskEditCtrl.asset.selected).toEqual('');

				expect(taskEditCtrl.taskForm.$setDirty).toHaveBeenCalled();
				expect(taskEditCtrl.asset.open).toEqual(false);
			});

			it('should add a new checklist', function() {
				taskEditCtrl.addChecklist();

				expect(taskEditCtrl.task.checklists.length).toEqual(1);
				expect(taskEditCtrl.task.checklists).toEqual([{name: 'Checklist', items: [{description: '', completed: false}]}]);

				expect(taskEditCtrl.taskProgress.length).toEqual(1);
				expect(taskEditCtrl.taskProgress).toEqual([{total: 1, complete: 0, toggle: false}]);

				expect(taskEditCtrl.checklist.open).toEqual(false);
				expect(taskEditCtrl.checklistName).toEqual('Checklist');
				expect(taskEditCtrl.taskForm.$setDirty).toHaveBeenCalled();
			});

			it('should add a checklist item', function() {
				taskEditCtrl.addChecklist();

				taskEditCtrl.addChecklistItem(0);

				expect(taskEditCtrl.taskProgress[0].total).toEqual(2);
				expect(taskEditCtrl.task.checklists[0].items.length).toEqual(2);
				expect(taskEditCtrl.task.checklists[0].items[1]).toEqual({description: '', completed: false});
			});

			it('should set a checklist item as complete', function() {
				taskEditCtrl.addChecklist();
				taskEditCtrl.checkComplete({description: 'Do Stuff', completed: true}, 0);

				expect(taskEditCtrl.taskProgress[0].complete).toEqual(1);
			});

			it('should set a checklist item as not completed', function() {
				taskEditCtrl.addChecklist();
				taskEditCtrl.checkComplete({description: 'Do Stuff', completed: true}, 0);

				taskEditCtrl.checkComplete({description: 'Do Stuff', completed: false}, 0);
				expect(taskEditCtrl.taskProgress[0].complete).toEqual(0);
			});

			it('should remove a checklist completely', function() {
				taskEditCtrl.addChecklist();
				expect(taskEditCtrl.task.checklists.length).toEqual(1);

				taskEditCtrl.removeChecklist(0);
				expect(taskEditCtrl.task.checklists.length).toEqual(0);
			});

			it('should remove a uncompleted checklist item', function() {
				taskEditCtrl.addChecklist();

				taskEditCtrl.removeChecklistItem(0, 0, {description: 'Item 1', completed: false});

				expect(taskEditCtrl.task.checklists[0].items.length).toEqual(0);
				expect(taskEditCtrl.taskProgress[0].complete).toEqual(0);
				expect(taskEditCtrl.taskProgress[0].total).toEqual(0);
				expect(taskEditCtrl.taskForm.$setDirty).toHaveBeenCalled();
			});

			it('should remove a completed checklist item', function() {
				taskEditCtrl.addChecklist();
				taskEditCtrl.checkComplete({description: 'Item 1', completed: true}, 0);

				taskEditCtrl.removeChecklistItem(0, 0, {description: 'Item 1', completed: true});

				expect(taskEditCtrl.task.checklists[0].items.length).toEqual(0);
				expect(taskEditCtrl.taskProgress[0].complete).toEqual(0);
				expect(taskEditCtrl.taskProgress[0].total).toEqual(0);
				expect(taskEditCtrl.taskForm.$setDirty).toHaveBeenCalled();
			});

			it('should convert a completed checklist item to a task', function() {
				taskEditCtrl.task = {
					_id: 1,
					title: 'Item 1',
					client: { _id: 1 },
					asset: { _id: 2 },
					assignedto: 3,
					checklists: []
				};

				$httpBackend.whenGET(API_URL + '/users').respond(200, $q.when(userList));
				$httpBackend.whenGET(API_URL + '/clients').respond(200, $q.when(clientList));

				taskEditCtrl.addChecklist();
				taskEditCtrl.checkComplete({description: 'Item 1', completed: true}, 0);

				$httpBackend.whenPOST(API_URL + '/tasks', { title: 'Item 1', client: 1, asset: 2, assignedto: 3 }).respond(200, $q.when({_id: 1, title: 'Item 1', client: 1, asset: 2, assignedto: 3}));

				taskEditCtrl.convertToTask(0, 0, { description: 'Item 1', client: 1, asset: 2, assignedto: 3, completed: true });

				$httpBackend.flush();

				expect(TasksFactory.add).toHaveBeenCalledWith({
					title: 'Item 1',
					client: 1,
					asset: 2,
					assignedto: 3
				});

				expect(taskEditCtrl.taskProgress[0].complete).toEqual(0);
				expect(taskEditCtrl.taskProgress[0].total).toEqual(0);
				expect(taskEditCtrl.task.checklists[0].items.length).toEqual(0);
				expect(taskEditCtrl.taskForm.$setDirty).toHaveBeenCalled();
			});

			it('should convert a uncompleted checklist item to a task', function() {
				taskEditCtrl.task = {
					_id: 1,
					title: 'Item 1',
					client: { _id: 1 },
					asset: { _id: 2 },
					assignedto: 3,
					checklists: []
				};

				$httpBackend.whenGET(API_URL + '/users').respond(200, $q.when(userList));
				$httpBackend.whenGET(API_URL + '/clients').respond(200, $q.when(clientList));

				taskEditCtrl.addChecklist();

				$httpBackend.whenPOST(API_URL + '/tasks', { title: 'Item 1', client: 1, asset: 2, assignedto: 3 }).respond(200, $q.when({_id: 1, title: 'Item 1', client: 1, asset: 2, assignedto: 3}));
				
				taskEditCtrl.convertToTask(0, 0, { description: 'Item 1', client: 1, asset: 2, assignedto: 3, completed: false });

				$httpBackend.flush();

				expect(TasksFactory.add).toHaveBeenCalledWith({
					title: 'Item 1',
					client: 1,
					asset: 2,
					assignedto: 3
				});

				expect(taskEditCtrl.taskProgress[0].complete).toEqual(0);
				expect(taskEditCtrl.taskProgress[0].total).toEqual(0);
				expect(taskEditCtrl.task.checklists[0].items.length).toEqual(0);
				expect(taskEditCtrl.taskForm.$setDirty).toHaveBeenCalled();
			});

			it('should mark the task completed', function() {
				taskEditCtrl.completedClick();
				expect(taskEditCtrl.task.completed).toEqual(true);
				expect(taskEditCtrl.taskForm.$setDirty).toHaveBeenCalled();
			});

			it('should mark the task not complete', function() {
				taskEditCtrl.task.completed = true;

				taskEditCtrl.completedClick();
				expect(taskEditCtrl.task.completed).toEqual(false);
				expect(taskEditCtrl.taskForm.$setDirty).toHaveBeenCalled();
			});

			it('should toggle completed checklist items', function() {
				taskEditCtrl.addChecklist();
				taskEditCtrl.addChecklistItem(0);

				taskEditCtrl.toggleCompleted(0);
				expect(taskEditCtrl.taskProgress[0].toggle).toEqual(true);
			});
		});

		describe('Edit a task', function() {
			var assetList = [
				{ _id: 1, name: 'Asset 1', client: 1 }
			];

			var editTask = {
				_id: 1,
				title: 'New Task 1',
				description: 'Do Some Stuff',
				completed: false,
				assignedto: {
					_id: 1,
					name: 'Test User'
				},
				client: {
					_id: 1,
					name: 'Client 1'
				},
				asset: {
					_id: 1, 
					name: 'Asset 1', 
					client: 1
				},
				checklists: [
					{ name: 'Checklist 1', items: [
						{ description: 'Item 1', completed: false },
						{ description: 'Item 2', completed: true },
						{ description: 'Item 3', completed: false },
					]},
					{ name: 'Checklist 2', items: [
						{ description: 'Item 4', completed: true },
						{ description: 'Item 5', completed: true }
					]}
				],
				createdat: new Date(),
				active: true
			};

			var editedTask = {
				_id: 1,
				title: 'Edit Task 1',
				description: 'Do Some Stuff Edited',
				completed: false,
				assignedto: {
					_id: 2,
					name: 'Test User 1'
				},
				client: {
					_id: 3,
					name: 'Client 2'
				},
				checklists: [
					{ name: 'Checklist 1', items: [
						{ description: 'Item 1', completed: false },
						{ description: 'Item 2', completed: true },
						{ description: 'Item 3', completed: false },
					]},
					{ name: 'Checklist 2', items: [
						{ description: 'Item 4', completed: true },
						{ description: 'Item 5', completed: true }
					]}
				],
				asset: {
					_id: 4, 
					name: 'Asset 4', 
					client: 3
				},
				createdat: new Date(),
				active: true
			};

			

			beforeEach(inject(function(_$stateParams_) {
				$stateParams = _$stateParams_;
				$stateParams.id = 1;

				taskEditCtrl = $controller('taskEditCtrl');
			}));

			beforeEach(function() {
				spyOn(TasksFactory, 'save').and.callThrough();
				spyOn(ClientsFactory, 'getAssets').and.callThrough();
				spyOn(TasksFactory, 'getComments').and.callThrough();
			});

			it('should setup screen defaults', function() {
				expect(taskEditCtrl.view).toEqual({});
				expect(taskEditCtrl.checklistName).toEqual('Checklist');
				expect(taskEditCtrl.taskProgress).toEqual([]);

				$httpBackend.whenGET(API_URL + '/users').respond(200, $q.when(userList));
				$httpBackend.whenGET(API_URL + '/clients').respond(200, $q.when(clientList));
				$httpBackend.whenGET(API_URL + '/tasks/' + $stateParams.id).respond(200, $q.when(editTask));
				$httpBackend.whenGET(API_URL + '/clients/' + 1 + '/assets?top=10').respond(200, $q.when({total: 1, data: assetList}));
				$httpBackend.whenGET(API_URL + '/tasks/' + $stateParams.id + '/comments').respond(200, $q.when(commentList));
				$httpBackend.flush();

				expect(UsersFactory.get).toHaveBeenCalled();
				expect(ClientsFactory.get).toHaveBeenCalled();
				expect(TasksFactory.getComments).toHaveBeenCalledWith($stateParams.id);
				expect(AuthFactory.getDetails).toHaveBeenCalled();

				expect(taskEditCtrl.mode).toEqual('Edit');
				expect(taskEditCtrl.task).toEqual(editTask);

				expect(taskEditCtrl.taskProgress.length).toEqual(2);
				expect(taskEditCtrl.taskProgress[0].total).toEqual(3);
				expect(taskEditCtrl.taskProgress[0].complete).toEqual(1);

				expect(taskEditCtrl.taskProgress[1].total).toEqual(2);
				expect(taskEditCtrl.taskProgress[1].complete).toEqual(2);

				expect(taskEditCtrl.assignedto.selected).toEqual(editTask.assignedto._id);
				expect(taskEditCtrl.view.assignedto).toEqual(editTask.assignedto.name);

				expect(taskEditCtrl.client.selected).toEqual(editTask.client._id);
				expect(taskEditCtrl.view.client).toEqual(editTask.client.name);

				expect(ClientsFactory.getAssets).toHaveBeenCalledWith(editTask.client._id, {top: 10});
				expect(taskEditCtrl.assets).toEqual(assetList);

				expect(taskEditCtrl.asset.selected).toEqual(editTask.asset._id);
				expect(taskEditCtrl.view.asset).toEqual(editTask.asset.name);

				// expect(TasksFactory.getById).toHaveBeenCalled();
			});

			it('should save the task', function() {
				$httpBackend.whenGET(API_URL + '/tasks/' + $stateParams.id).respond(200, $q.when(editTask));
				$httpBackend.whenGET(API_URL + '/users').respond(200, $q.when(userList));
				$httpBackend.whenGET(API_URL + '/clients').respond(200, $q.when(clientList));
				$httpBackend.whenPUT(API_URL + '/tasks/' + $stateParams.id, editedTask).respond(200, $q.when(editedTask))
				$httpBackend.whenGET(API_URL + '/clients/' + 1 + '/assets?top=10').respond(200, $q.when(assetList));
				$httpBackend.whenGET(API_URL + '/tasks/' + $stateParams.id + '/comments').respond(200, $q.when(commentList));
				taskEditCtrl.task = editedTask;
				taskEditCtrl.saveTask();

				$httpBackend.flush();

				expect(toaster.pop).toHaveBeenCalledWith({
					type: 'success',
					body: 'Task has been saved',
					showCloseButton: true,
					timeout: 3000
				})

				expect($state.go).toHaveBeenCalledWith('app.tasks.list.details');
			});
		});

		describe('Comments', function() {
			var comments;

			beforeEach(inject(function(_$stateParams_) {
				$stateParams = _$stateParams_;
				$stateParams.id = 1;

				taskEditCtrl = $controller('taskEditCtrl');
			}));

			beforeEach(function() {
				spyOn(TasksFactory, 'addComment').and.callThrough();
				spyOn(TasksFactory, 'getComments').and.callThrough();

				comments = [];
				comments.push({ _id: 1, details: 'User Comment 1' });
			});

			it('should add a commment', function() {
				$httpBackend.whenGET(API_URL + '/tasks/' + $stateParams.id).respond(200, $q.when({_id: 1, name: 'Test'}));
				$httpBackend.whenGET(API_URL + '/users').respond(200, $q.when(userList));
				$httpBackend.whenGET(API_URL + '/clients').respond(200, $q.when(clientList));

				var commentList = comments;
				commentList.push({_id: 2, details: "Let's get this done", type: "COMMENT"});

				taskEditCtrl.newComment = "Let's get this done";

				$httpBackend.whenGET(API_URL + '/tasks/' + $stateParams.id + '/comments').respond(200, $q.when(comments));
				$httpBackend.whenPOST(API_URL + '/tasks/' + $stateParams.id + '/comments').respond(200, $q.when(commentList));

				taskEditCtrl.addComment();
				$httpBackend.flush();

				expect(TasksFactory.addComment).toHaveBeenCalledWith(1, { details: "Let's get this done" });
				expect(TasksFactory.getComments).toHaveBeenCalledWith(1);
				expect(taskEditCtrl.newComment).toEqual('');
			});

			it('should edit a comment', function() {
				expect(taskEditCtrl.commentEditing).toEqual('');
				expect(taskEditCtrl.commentEditingValue).toEqual('');

				taskEditCtrl.editComment(comments[0]);

				expect(taskEditCtrl.commentEditing).toEqual(1);
				expect(taskEditCtrl.commentEditingValue).toEqual("User Comment 1");
			});
			
			it('should save an edited comment', function() {
				$httpBackend.whenGET(API_URL + '/tasks/' + $stateParams.id).respond(200, $q.when({_id: 1, name: 'Test'}));
				$httpBackend.whenGET(API_URL + '/users').respond(200, $q.when(userList));
				$httpBackend.whenGET(API_URL + '/clients').respond(200, $q.when(clientList));
				$httpBackend.whenGET(API_URL + '/tasks/' + $stateParams.id + '/comments').respond(200, $q.when(comments));

				var response = { _id: 1, details: "Changing the comment", type: "COMMENT" };
				taskEditCtrl.editComment(comments[0]);
				expect(taskEditCtrl.commentEditing).toEqual(1);
				expect(taskEditCtrl.commentEditingValue).toEqual("User Comment 1");
				taskEditCtrl.commentEditingValue = "Changing the comment";

				$httpBackend.whenPUT(API_URL + '/tasks/' + $stateParams.id + '/comments/' + comments[0]._id).respond(200, $q.when(response));

				taskEditCtrl.saveComment(comments[0]);

				$httpBackend.flush();

				expect(comments[0].details).toEqual("Changing the comment");
				expect(toaster.pop).toHaveBeenCalledWith({
					type: 'success',
					body: 'Comment has been edited',
					showCloseButton: true,
					timeout: 3000
				});
				expect(taskEditCtrl.commentEditing).toEqual('');
				expect(taskEditCtrl.commentEditingValue).toEqual('');
				expect(TasksFactory.getComments).toHaveBeenCalledWith(1);
			});

			it('should cancel the editing a comment', function() {
				taskEditCtrl.editComment(comments[0]);
				expect(taskEditCtrl.commentEditing).toEqual(1);
				expect(taskEditCtrl.commentEditingValue).toEqual("User Comment 1");

				taskEditCtrl.cancelComment();

				expect(taskEditCtrl.commentEditing).toEqual('');
				expect(taskEditCtrl.commentEditingValue).toEqual('');
			});

			it('should delete a comment', function() {
				$httpBackend.whenGET(API_URL + '/tasks/' + $stateParams.id).respond(200, $q.when({_id: 1, name: 'Test'}));
				$httpBackend.whenGET(API_URL + '/users').respond(200, $q.when(userList));
				$httpBackend.whenGET(API_URL + '/clients').respond(200, $q.when(clientList));
				$httpBackend.whenGET(API_URL + '/tasks/' + $stateParams.id + '/comments').respond(200, $q.when(comments));
				$httpBackend.flush();
				
				$httpBackend.whenDELETE(API_URL + '/tasks/' + $stateParams.id + '/comments/' + 1).respond(200, $q.when({message: 'Comment Deleted'}));
				
				taskEditCtrl.removeComment(1);

				$httpBackend.flush();

				expect(toaster.pop).toHaveBeenCalledWith({
					type: 'success',
					body: 'Comment has been deleted',
					showCloseButton: true,
					timeout: 3000
				});

				expect(TasksFactory.getComments).toHaveBeenCalledWith(1);
			});

			it('should not be able to edit someone elses comment', function() {

			});

			it('should not be able to delete someone elses comment', function() {

			});
		});
	});
});