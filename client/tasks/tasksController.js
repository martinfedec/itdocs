'use strict';

angular
	.module('itdocs.controllers.tasks', [])
	
	.controller('tasksCtrl', function() {

	})

	.controller('taskListCtrl', function(tasks, toaster, users, clients, $state) {
		var tl = this;

		tl.created = {
			open: false,
			selected: undefined
		}

		tl.assignedto = {
			open: false,
			selected: undefined
		}

		tl.client = {
			open: false,
			selected: undefined
		}

		tl.asset = {
			open: false,
			selected: undefined
		}

		tl.sort = {
			open: false,
			selected: 'desc'
		}

		if ($state.params.created) {
			tl.created.selected = $state.params.created;
		}

		if ($state.params.assignedto) {
			tl.assignedto.selected = $state.params.assignedto;
		}

		if ($state.params.client) {
			tl.client.selected = $state.params.client;
		}

		if ($state.params.asset) {
			tl.asset.selected = $state.params.asset;
		}

		if ($state.params.sort) {
			tl.sort.selected = $state.params.sort;
		}

		users.get().then(function(users) {
			tl.users = users;
		});

		clients.get().then(function(clientRes) {
			tl.clients = clientRes;

			if (tl.client.selected) {
				var obj = tl.clients.filter(function ( obj ) {
				    return obj.name === tl.client.selected;
				})[0];

				clients.getAssets(obj._id).then(function(assets) {
					tl.assets = assets.data;
				});
			}
		});

		tl.addTask = function() {
			var task = {
				title: tl.newTask,
				checklists: []
			}

			if (tl.newTask) {
				tasks.add(task).then(function(response) {
					$state.reload('app.tasks.list.details');
					tl.newTask = '';
				}, function(error) {
					console.log(error);
					// switch (error.data.code) {
					// 	default:
					// 	// content = error;
					// };
				});	
			} else {
				$state.go('app.tasks.new');	
			}
		};

		tl.createdFilterChange = function(filterId) {
			var createdFilter = {};
			if (filterId) {
				createdFilter.created = filterId;
				tl.created.selected = filterId;
			} else {
				createdFilter.created = '';
				tl.created.selected = undefined;
			}

			tl.created.open = false;
			$state.go('app.tasks.list.details', createdFilter);
			tl.createdSearch = '';
		};

		tl.assignedtoFilterChange = function(filterId) {
			var createdFilter = {};
			if (filterId) {
				createdFilter.assignedto = filterId;
				tl.assignedto.selected = filterId;
			} else {
				createdFilter.assignedto = '';
				tl.assignedto.selected = undefined;
			}

			tl.assignedto.open = false;
			$state.go('app.tasks.list.details', createdFilter);
			tl.assignedtoSearch = '';
		};

		tl.clientFilterChange = function(filter) {
			var createdFilter = {};
			if (filter) {
				createdFilter.client = filter.name;
				tl.client.selected = filter.name;

				clients.getAssets(filter._id, {top: 10}).then(function(assets) {
					tl.assets = assets.data;
				});

				createdFilter.asset = '';
				tl.asset.selected = undefined;
			} else {
				createdFilter.client = '';
				tl.client.selected = undefined;

				createdFilter.asset = '';
				tl.asset.selected = undefined;

				tl.assets = [];
			}

			tl.client.open = false;
			$state.go('app.tasks.list.details', createdFilter);
			tl.clientSearch = '';
		};

		tl.assetFilterChange = function(filterId) {
			var createdFilter = {};
			if (filterId) {
				createdFilter.asset = filterId;
				tl.asset.selected = filterId;
			} else {
				createdFilter.asset = '';
				tl.asset.selected = undefined;
			}

			tl.asset.open = false;
			$state.go('app.tasks.list.details', createdFilter);
			tl.assetSearch = '';
		};

		tl.sortFilterChange = function(filterId) {
			var createdFilter = {};
			if (filterId) {
				createdFilter.sort = filterId;
				tl.sort.selected = filterId;
			} else {
				createdFilter.sort = '';
				tl.sort.selected = 'desc';
			}

			tl.sort.open = false;
			$state.go('app.tasks.list.details', createdFilter);
		};
	})

	.controller('taskListDetailsCtrl', function(tasks, $state, $stateParams, toaster) {
		var td = this;

		td.tasks = [];

		td.totalItems = 0;

		td.itemsPerPage = 20;
		// td.maxSize = 5;

		function loadTasks() {
			tasks.get($stateParams).then(function(res) {
				td.tasks = res;

				tasks.getCount($stateParams).then(function(response) {
					td.totalItems = response.count;
				});
				// Need to set here so it set's it properly.
				if ($stateParams.page) {
					td.currentPage = $stateParams.page;
				}
			});
		};

		function loadCounts() {
			var completedItems = angular.copy($stateParams);
			completedItems.q = '';
			tasks.getCount(completedItems).then(function(response) {
				td.open = response.count;
				// td.totalItems += response.count;
			});

			// var completedItems = angular.copy($stateParams);
			completedItems.q = 'completed'
			tasks.getCount(completedItems).then(function(response) {
				td.completed = response.count;
				// td.totalItems += response.count;
			});
		};

		loadTasks();
		loadCounts();

		var statusFilter = 'is:open';

		if ($stateParams.q) {
			statusFilter = 'is:completed';
		}

		var createdFilter = '';
		if ($stateParams.created) {
			createdFilter = ' createdby:' + $stateParams.created;
		}

		var assignedtoFilter = '';
		if ($stateParams.assignedto) {
			assignedtoFilter = ' assignedto:' + $stateParams.assignedto;
		}

		var clientFilter = '';
		if ($stateParams.client) {
			clientFilter = ' client:' + $stateParams.client;
		}

		var assetFilter = '';
		if ($stateParams.asset) {
			assetFilter = ' asset:' + $stateParams.asset;
		}

		var sortFilter = '';
		if ($stateParams.sort) {
			sortFilter = ' sort:' + $stateParams.sort;
		}

		td.taskFilter = statusFilter + createdFilter + assignedtoFilter + clientFilter + assetFilter + sortFilter;

		td.checkCompleted = function(task) {
			tasks.save(task).then(function(response) {
				toaster.pop({
					type: 'success',
					body: 'Task has marked as complete',
					showCloseButton: true,
					timeout: 3000
				});
				loadTasks();
				loadCounts();
			});
		};

		td.clearFilter = function() {
			$state.go('app.tasks.list.details', {q: '', created: '', assignedto: '', client: '', asset: '', sort: '', page: ''});
		};

		td.submitFilter = function() {
			/* Need to make it so it can accept is:open is:completed, right now you either need to set it to blank
			   or completed for it to work... need to make query handling a bit different.
			 */
			// $state.go('app.tasks.list.details', {q: td.taskFilter});
		};

		td.changePage = function() {
			$state.go('.', {page: td.currentPage}, {notify: false});
			$stateParams.page = td.currentPage;
			loadTasks();
		};
	})

	.controller('taskEditCtrl', function(tasks, users, clients, toaster, $state, $stateParams, auth, moment) {
		var te = this;
		
		te.task = {};
		te.view = {};
		te.checklistName = 'Checklist';
		te.taskProgress = [];
		te.assignedto = { selected: null };
		te.asset = { selected: null };
		te.client = { selected: null };
		te.checklist = { open: false };

		users.get().then(function(users) {
			te.users = users;
		});

		clients.get().then(function(clients) {
			te.clients = clients;
		});

		te.currentUser = auth.getDetails();

		function getComments() {
			tasks.getComments($stateParams.id).then(function(comments) {
				te.comments = comments;
			});
		};

		te.sortableOptions = {
			handle: '.ckItemHandle',
			connectWith: '.todo-list'
		};

		if ($stateParams.id) {
			te.mode = 'Edit';

			tasks.getById($stateParams.id).then(function(task) {
				te.task = task;
				getComments();

				var index;

				angular.forEach(te.task.checklists, function(value, key) {
					te.taskProgress.push({
						total: value.items.length,
						complete: 0
					});
					index = key;
					angular.forEach(value.items, function(value, key) {
						if (value.completed) {
							te.taskProgress[index].complete += 1;
						}
					});
				});

				if (task.assignedto) {
					te.assignedto.selected = task.assignedto._id;
					te.view.assignedto = task.assignedto.name;
				}
				if (task.client) {
					te.client.selected = task.client._id;
					te.view.client = task.client.name;

					clients.getAssets(task.client._id, {top: 10}).then(function(assets) {
						te.assets = assets.data;
					});
				}
				if (task.asset) {
					te.asset.selected = task.asset._id;
					te.view.asset = task.asset.name;
				}
			});
		} else {
			te.mode = 'Add New';
			te.task = {
				checklists: []
			};
		}

		te.assignedToChange = function(user) {
			if (te.assignedto.selected == user._id) {
				te.task.assignedto = null;
				te.view.assignedto = '';
				te.assignedto.selected = '';
			} else {
				te.task.assignedto = user._id;
				te.view.assignedto = user.name;
				te.assignedto.selected = user._id;
			}

			te.taskForm.$setDirty();
			te.assignedto.open = false;
		};

		te.clientChange = function(client) {
			// Clear Asset as Well
			te.task.asset = null;
			te.view.asset = '';
			te.asset.selected = '';
			te.assets = [];

			if (te.client.selected == client._id) {
				te.task.client = null;
				te.view.client = '';
				te.client.selected = '';
			} else {
				te.task.client = client._id;
				te.view.client = client.name;
				te.client.selected = client._id;

				clients.getAssets(client._id, {top: 10}).then(function(assets) {
					te.assets = assets.data;
				});
			}

			te.taskForm.$setDirty();
			te.client.open = false;
		};

		te.assetChange = function(asset) {
			if (te.asset.selected == asset._id) {
				te.task.asset = null;
				te.view.asset = '';
				te.asset.selected = '';
			} else {
				te.task.asset = asset._id;
				te.view.asset = asset.name;
				te.asset.selected = asset._id;
			}

			te.taskForm.$setDirty();
			te.asset.open = false;
		};

		te.addChecklist = function() {
			te.task.checklists.push({
				name: te.checklistName,
				items: [{
					description: '',
					completed: false
				}]
			});

			te.taskProgress.push({
				total: 1,
				complete: 0,
				toggle: false
			});

			te.checklist.open = false;
			te.checklistName = 'Checklist';
			te.taskForm.$setDirty();
		};

		te.addChecklistItem = function(index) {
			te.taskProgress[index].total += 1;
			te.task.checklists[index].items.push({description: '', completed: false});
		};

		te.checkComplete = function(item, index) {
			if (item.completed) {
				te.taskProgress[index].complete += 1;
			} else {
				te.taskProgress[index].complete -= 1;
			}
		};

		te.removeChecklist = function(index) {
			te.task.checklists.splice(index, 1);
			te.taskProgress.splice(index, 1);
			te.taskForm.$setDirty();
		};

		te.removeChecklistItem = function(parentIndex, index, item) {
			te.task.checklists[parentIndex].items.splice(index, 1);
			if (item.completed) {
				te.taskProgress[parentIndex].complete -= 1;
			}

			te.taskProgress[parentIndex].total -= 1;
			te.taskForm.$setDirty();
		};

		te.convertToTask = function(parentIndex, index, item) {
			tasks.add({
				title: item.description,
				client: te.task.client._id,
				asset: te.task.asset._id,
				assignedto: te.task.assignedto
			}).then(function(res) {
				if (item.completed) {
					te.taskProgress[parentIndex].complete -= 1;
				}
				te.taskProgress[parentIndex].total -= 1;
				te.task.checklists[parentIndex].items.splice(index, 1);	
			});
			te.taskForm.$setDirty();
		};

		te.completedClick = function() {
			te.task.completed = !te.task.completed;
			te.taskForm.$setDirty();
		};

		te.toggleCompleted = function(index) {
			te.taskProgress[index].toggle = !te.taskProgress[index].toggle;
		};

		te.saveTask = function() {
			if (te.mode === 'Edit') {
				tasks.save(te.task).then(function(response) {
					toaster.pop({
						type: 'success',
						body: 'Task has been saved',
						showCloseButton: true,
						timeout: 3000
					});
					$state.go('app.tasks.list.details');
				}, function(error) {
					switch (error.data.code) {
						default:
						// content = error;
					};
				});
			} else {
				tasks.add(te.task).then(function(response) {
					toaster.pop({
						type: 'success',
						body: 'Task has been added',
						showCloseButton: true,
						timeout: 3000
					});
					$state.go('app.tasks.list.details');
				}, function(error) {
					switch (error.data.code) {
						default:
							// content = error;
					};
				});
			}
		};

		te.addComment = function() {
			tasks.addComment($stateParams.id, {details: te.newComment}).then(function(comment) {
				getComments();
				te.newComment = '';
			});
		};

		te.removeComment = function(id) {
			tasks.removeComment($stateParams.id, id).then(function(result) {
				toaster.pop({
					type: 'success',
					body: 'Comment has been deleted',
					showCloseButton: true,
					timeout: 3000
				});
				getComments();
			});
		};

		te.commentEditing = '';
		te.commentEditingValue = '';

		te.editComment = function(comment) {
			te.commentEditing = comment._id;
			te.commentEditingValue = comment.details;
		};

		te.saveComment = function(comment) {
			comment.details = te.commentEditingValue;
			tasks.saveComment($stateParams.id, comment).then(function(comment) {
				toaster.pop({
					type: 'success',
					body: 'Comment has been edited',
					showCloseButton: true,
					timeout: 3000
				});
				te.commentEditing = '';
				te.commentEditingValue = '';
				getComments();
			});
		};

		te.cancelComment = function() {
			te.commentEditingValue = '';
			te.commentEditing = '';
		};
	})

;