describe('Login Controller', function() {
	var $controller, API_URL, AuthFactory;

	beforeEach(module(function($provide) {
		$provide.constant("API_URL", "http://localhost:443/api");
	}));

	beforeEach(angular.mock.module('ui.router'));
	
	beforeEach(angular.mock.module('itdocs.services'));
	beforeEach(angular.mock.module('itdocs.controllers.login'));

	beforeEach(inject(function(_$controller_, _API_URL_, _auth_) {
		$controller = _$controller_;
		API_URL = _API_URL_;
		AuthFactory = _auth_;
	}));

	describe('loginCtrl', function() {
		var loginCtrl;

		beforeEach(function() {
			loginCtrl = $controller('loginCtrl');
		});

		it('should be defined', function() {
			expect(loginCtrl).toBeDefined();
		});
	});
});