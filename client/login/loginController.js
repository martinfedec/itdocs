'use strict';

angular
	.module('itdocs.controllers.login', [])
	.controller('loginCtrl', function(auth, $state, $stateParams) {
		var lg = this;
		lg.isProcessing = false;
		lg.errorCaught = false;

		lg.signInClick = function() {
			lg.errorCaught = false;
			lg.isProcessing = true;
			auth.authenticate(lg.user.email, lg.user.password).then(function(response) {
				var gotoState = 'app.dashboard';

				if ($stateParams.return) {
					gotoState = $stateParams.return;
				}
				$state.go(gotoState);
			}, function(error) {
				switch (error.status) {
					case 400:
						lg.errorCaught = true;
						break;
					default:
				}
				lg.isProcessing = false;
			});
		}
	})

;