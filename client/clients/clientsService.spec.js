describe('Clients Factory', function() {
	var clients, API_URL, $httpBackend, $q;

	beforeEach(module(function($provide) {
		$provide.constant("API_URL", "http://localhost:443/api");
	}));

	var clientList = [
	{
		_id: 1,
		name: 'Client 1',
		phonenumber: '111-222-3333',
		email: 'user1@test.com',
		website: 'www.client.com',
		businesshours: 'M-F 9-5',
		description: 'Client 1 description',
		active: true,
		assets: [
			{
				_id: 1,
				name: 'HV01',
				types: [{
					template: 1,
					fields: [
						{name: 'Field 1', value: 'Stuff in here'},
						{name: 'Field 2', value: 'More stuff in here'},
						{name: 'Field 3', value: 'Stuff in here 3'}
					]
				}],
				client: 1
			}
		]
	},
	{
		_id: 2,
		name: 'Client 2',
		phonenumber: '444-333-2222',
		email: 'user2@test.com',
		website: 'www.client2.com',
		businesshours: 'S-S 12-12',
		description: 'Client 2 description',
		active: true,
		assets: [
			{
				_id: 2,
				name: 'HV02',
				types: [{
					template: 1,
					fields: [
						{name: 'Field 4', value: 'Stuff in here 4'},
						{name: 'Field 5', value: 'More stuff in here 5'},
						{name: 'Field 6', value: 'Stuff in here 6'}
					]
				}],
				client: 2
			}
		]
	}];

	beforeEach(angular.mock.module('itdocs.services.clients'));

	beforeEach(inject(function(_clients_,_$q_, _$httpBackend_, _API_URL_) {
		clients = _clients_;
		$q = _$q_;
		$httpBackend = _$httpBackend_;
		API_URL = _API_URL_;
	}));

	it('should exist', function() {
		expect(clients).toBeDefined();
	});

	describe('.get()', function() {
		beforeEach(function() {
			result = {};
			spyOn(clients, "get").and.callThrough();
		});


		it('should exist', function() {
			expect(clients.get).toBeDefined();
		});

		it('should return a list of clients', function() {
			$httpBackend.whenGET(API_URL + '/clients').respond(200, $q.when(clientList));

			expect(clients.get).not.toHaveBeenCalled();
			expect(result).toEqual({});

			clients.get().then(function(res) {
				result = res;
			});

			$httpBackend.flush();

			expect(clients.get).toHaveBeenCalled();
			expect(result.length).toBe(2);
			expect(result[0]._id).toEqual(1);
			expect(result[0].name).toEqual('Client 1');
			expect(result[0].phonenumber).toEqual('111-222-3333');
			expect(result[0].email).toEqual('user1@test.com');
			expect(result[0].website).toEqual('www.client.com');
			expect(result[0].businesshours).toEqual('M-F 9-5');
			expect(result[0].description).toEqual('Client 1 description');
			expect(result[0].active).toEqual(true);
		});
	});

	describe('.getById()', function() {
		beforeEach(function() {
			result = {};
			spyOn(clients, "getById").and.callThrough();
		});


		it('should exist', function() {
			expect(clients.getById).toBeDefined();
		});

		it('should return a clients when called with a valid id', function() {
			$httpBackend.whenGET(API_URL + '/clients/' + 1).respond(200, $q.when(clientList[0]));

			expect(clients.getById).not.toHaveBeenCalled();
			expect(result).toEqual({});

			clients.getById(1).then(function(res) {
				result = res;
			});

			$httpBackend.flush();

			expect(result._id).toEqual(1);
			expect(result.name).toEqual('Client 1');
			expect(result.phonenumber).toEqual('111-222-3333');
			expect(result.email).toEqual('user1@test.com');
			expect(result.website).toEqual('www.client.com');
			expect(result.businesshours).toEqual('M-F 9-5');
			expect(result.description).toEqual('Client 1 description');
			expect(result.active).toEqual(true);
		});
	});

	describe('.add()', function() {
		beforeEach(function() {
			result = {};
			spyOn(clients, "add").and.callThrough();
		});

		it('should exist', function() {
			expect(clients.add).toBeDefined();
		});

		it('should add a new client and return the added clients data', function() {
			var newClient = {
				name: 'Client 4',
				phonenumber: '555-555-5555',
				email: 'user5@test.com',
				website: 'www.client5.com',
				businesshours: 'M-F 12-12',
				description: 'Client 5 description',
				active: true
			};

			var response = {
				_id: 4,
				name: 'Client 4',
				phonenumber: '555-555-5555',
				email: 'user5@test.com',
				website: 'www.client5.com',
				businesshours: 'M-F 12-12',
				description: 'Client 5 description',
				active: true
			};

			$httpBackend.whenPOST(API_URL + '/clients', newClient).respond(200, $q.when(response));

			expect(clients.add).not.toHaveBeenCalled();
			expect(result).toEqual({});

			clients.add(newClient).then(function(res) {
				result = res;
			});

			$httpBackend.flush();

			expect(clients.add).toHaveBeenCalledWith(newClient);
			expect(result._id).toEqual(4);
			expect(result.name).toEqual(newClient.name);
			expect(result.phonenumber).toEqual(newClient.phonenumber);
			expect(result.email).toEqual(newClient.email);
			expect(result.website).toEqual(newClient.website);
			expect(result.businesshours).toEqual(newClient.businesshours);
			expect(result.description).toEqual(newClient.description);
			expect(result.active).toEqual(newClient.active);
		});
	});

	describe('.save()', function() {
		beforeEach(function() {
			result = {};
			spyOn(clients, "save").and.callThrough();
		});

		it('should exist', function() {
			expect(clients.save).toBeDefined();
		});

		it('should save a client and return the clients data', function() {
			var editClient = clientList[1];

			editClient.name = 'Client 33';
			editClient.email = 'client33@email.com';
			editClient.phonenumber = '666-666-6666',
			editClient.website = 'www.client33.com',
			editClient.businesshours = 'Sunday Only',
			editClient.description = 'very good little business',
			editClient.active = false;

			var response = editClient;

			$httpBackend.whenPUT(API_URL + '/clients/' + editClient._id, editClient).respond(200, $q.when(response));

			expect(clients.save).not.toHaveBeenCalled();
			expect(result).toEqual({});

			clients.save(editClient).then(function(res) {
				result = res;
			});

			$httpBackend.flush();

			expect(clients.save).toHaveBeenCalledWith(editClient);
			expect(result._id).toEqual(2);
			expect(result.name).toEqual(editClient.name);
			expect(result.phonenumber).toEqual(editClient.phonenumber);
			expect(result.email).toEqual(editClient.email);
			expect(result.website).toEqual(editClient.website);
			expect(result.businesshours).toEqual(editClient.businesshours);
			expect(result.description).toEqual(editClient.description);
			expect(result.active).toEqual(editClient.active);
		});
	});

	describe('.remove()', function() {
		beforeEach(function() {
			result = {};
			spyOn(clients, "remove").and.callThrough();
		});

		it('should exist', function() {
			expect(clients.remove).toBeDefined();
		});

		it('should remove a user based on id', function() {
			var response = {message: 'Client Deleted'};

			$httpBackend.whenDELETE(API_URL + '/clients/' + 2).respond(200, $q.when(response));

			expect(clients.remove).not.toHaveBeenCalled();
			expect(result).toEqual({});

			clients.remove(2).then(function(res) {
				result = res;
			});

			$httpBackend.flush();

			expect(clients.remove).toHaveBeenCalledWith(2);
			expect(result.message).toEqual('Client Deleted');
		});
	});

	describe('Assets', function() {

		describe('.getAssets()', function() {
			beforeEach(function() {
				result = {};
				spyOn(clients, "getAssets").and.callThrough();
			});

			it('should exist', function() {
				expect(clients.getAssets).toBeDefined();
			});

			it('should return a list of individual client assets without a page', function() {
				$httpBackend.whenGET(API_URL + '/clients/' + 1 + '/assets').respond(200, $q.when(clientList[0].assets));

				expect(clients.getAssets).not.toHaveBeenCalled();
				expect(result).toEqual({});

				clients.getAssets(1).then(function(res) {
					result = res;
				});

				$httpBackend.flush();

				expect(clients.getAssets).toHaveBeenCalledWith(1);
				expect(result.length).toBe(1);
				expect(result).toEqual(clientList[0].assets);
			});

			it('should return page 1', function() {
				$httpBackend.whenGET(API_URL + '/clients/' + 1 + '/assets?page=1').respond(200, $q.when({total: 1, page: 1, top: 20, data: clientList[0].assets}));

				clients.getAssets(1,{page: 1}).then(function(res) {
					result = res;
				});

				$httpBackend.flush();

				expect(clients.getAssets).toHaveBeenCalledWith(1, {page: 1});
				expect(result.total).toEqual(1);
				expect(result.page).toEqual(1);
				expect(result.top).toEqual(20);
				expect(result.data).toEqual(clientList[0].assets);
			});

			it('should return 5 items', function() {
				$httpBackend.whenGET(API_URL + '/clients/' + 1 + '/assets?top=5').respond(200, $q.when({total: 1, page: 1, top: 5, data: clientList[0].assets}));

				clients.getAssets(1,{top: 5}).then(function(res) {
					result = res;
				});

				$httpBackend.flush();

				expect(clients.getAssets).toHaveBeenCalledWith(1, {top: 5});
				expect(result.total).toEqual(1);
				expect(result.page).toEqual(1);
				expect(result.top).toEqual(5);
				expect(result.data).toEqual(clientList[0].assets);
			});

			it('should return 3 items and page 2', function() {
				$httpBackend.whenGET(API_URL + '/clients/' + 1 + '/assets?page=2&top=3').respond(200, $q.when({total: 1, page: 2, top: 3, data: clientList[0].assets}));

				clients.getAssets(1,{page: 2, top: 3}).then(function(res) {
					result = res;
				});

				$httpBackend.flush();

				expect(clients.getAssets).toHaveBeenCalledWith(1, {page: 2, top: 3});
				expect(result.total).toEqual(1);
				expect(result.page).toEqual(2);
				expect(result.top).toEqual(3);
				expect(result.data).toEqual(clientList[0].assets);
			});
		});

		describe('.getAsset()', function() {
			beforeEach(function() {
				result = {};
				spyOn(clients, "getAsset").and.callThrough();
			});

			it('should exist', function() {
				expect(clients.getAsset).toBeDefined();
			});

			it('should return a clieng asset when called with a valid id', function() {
				$httpBackend.whenGET(API_URL + '/clients/' + 1 + '/assets/' + 1).respond(200, $q.when(clientList[0].assets[0]));

				expect(clients.getAsset).not.toHaveBeenCalled();
				expect(result).toEqual({});

				clients.getAsset(1, 1).then(function(res) {
					result = res;
				});

				$httpBackend.flush();

				expect(result).toEqual(clientList[0].assets[0]);
			});
		});

		describe('.addAsset()', function() {
			beforeEach(function() {
				result = {};
				spyOn(clients, "addAsset").and.callThrough();
			});

			it('should exist', function() {
				expect(clients.addAsset).toBeDefined();
			});

			it('should add a new asset and return the asset details', function() {
				var newAsset = {
					name: 'HV01',
					types: [{
						template: 1,
						fields: [
							{name: 'Field 1', value: 'Stuff in here'},
							{name: 'Field 2', value: 'More stuff in here'},
							{name: 'Field 3', value: 'Stuff in here 3'}
						]
					}],
					client: 1
				};

				var response = {
					_id: 4,
					name: 'HV01',
					types: [{
						template: 1,
						fields: [
							{name: 'Field 1', value: 'Stuff in here'},
							{name: 'Field 2', value: 'More stuff in here'},
							{name: 'Field 3', value: 'Stuff in here 3'}
						]
					}],
					client: 1	
				};

				$httpBackend.whenPOST(API_URL + '/clients/' + 1 + '/assets', newAsset).respond(200, $q.when(response));

				expect(clients.addAsset).not.toHaveBeenCalled();
				expect(result).toEqual({});

				clients.addAsset(1, newAsset).then(function(res) {
					result = res;
				});

				$httpBackend.flush();

				expect(clients.addAsset).toHaveBeenCalledWith(1, newAsset);
				expect(result._id).toEqual(4);
				expect(result.name).toEqual(newAsset.name);
				expect(result.types).toEqual(newAsset.types);
				expect(result.client).toEqual(newAsset.client);
				expect(result.active).toEqual(newAsset.active);
			});
		});

		describe('.saveAsset()', function() {
			beforeEach(function() {
				result = {};
				spyOn(clients, "saveAsset").and.callThrough();
			});

			it('should exist', function() {
				expect(clients.saveAsset).toBeDefined();
			});

			it('should save a client asset and return the asset data', function() {
				var editAsset = clientList[0].assets[0];

				editAsset.name = 'HV-NEW';
				editAsset.client = 1;
				editAsset.types = [
					{
						template: 1,
						fields: [
							{name: 'Field 11', value: 'Stuff in here 11'},
							{name: 'Field 22', value: 'More stuff in here 22'},
							{name: 'Field 33', value: 'Stuff in here 3 33'},
							{name: 'Field 44', value: 'Stuff in here 44'}
						]
					},
					{
						template: 2,
						fields: [
							{name: '1', value: '1'},
							{name: '2', value: '2'},
							{name: '3', value: '3'},
							{name: '4', value: '4'}
						]
					}
					];

				var response = editAsset;

				$httpBackend.whenPUT(API_URL + '/clients/' + 1 + '/assets/' + 1, editAsset).respond(200, $q.when(response));

				expect(clients.saveAsset).not.toHaveBeenCalled();
				expect(result).toEqual({});

				clients.saveAsset(1, editAsset).then(function(res) {
					result = res;
				});

				$httpBackend.flush();

				expect(clients.saveAsset).toHaveBeenCalledWith(1, editAsset);
				expect(result).toEqual(editAsset);
			});
		});

		describe('.removeAsset()', function() {
			beforeEach(function() {
				result = {};
				spyOn(clients, "removeAsset").and.callThrough();
			});

			it('should exist', function() {
				expect(clients.removeAsset).toBeDefined();
			});

			it('should remove an asset based on id', function() {
				var response = {message: 'Asset Deleted'};

				$httpBackend.whenDELETE(API_URL + '/clients/' + 2 + '/assets/' + 1).respond(200, $q.when(response));

				expect(clients.removeAsset).not.toHaveBeenCalled();
				expect(result).toEqual({});

				clients.removeAsset(2, 1).then(function(res) {
					result = res;
				});

				$httpBackend.flush();

				expect(clients.removeAsset).toHaveBeenCalledWith(2, 1);
				expect(result.message).toEqual('Asset Deleted');
			});
		});

		describe('.getScript()', function() {
			beforeEach(function() {
				result = {};
				spyOn(clients, "getScript").and.callThrough();
			});

			it('should exist', function() {
				expect(clients.getScript).toBeDefined();
			});

			it('should get the script of an asset based on type id selected', function() {
				var response = {script: 'script will be here'};

				$httpBackend.whenGET(API_URL + '/clients/' + 2 + '/assets/' + 1 + '/script/' + 3).respond(200, $q.when(response));

				expect(clients.getScript).not.toHaveBeenCalled();
				expect(result).toEqual({});

				clients.getScript(2, 1, 3).then(function(res) {
					result = res;
				});

				$httpBackend.flush();

				expect(clients.getScript).toHaveBeenCalledWith(2, 1, 3);
				expect(result.script).toEqual('script will be here');
			});
		});

		describe('.copyAsset()', function() {
			beforeEach(function() {
				result = {};
				spyOn(clients, "copyAsset").and.callThrough();
			});

			it('should exist', function() {
				expect(clients.copyAsset).toBeDefined();
			});

			it('should copy an asset and duplicate it', function() {
				var response = {
					_id: 4,
					name: 'HV01',
					types: [{
						template: 1,
						fields: [
							{name: 'Field 1', value: 'Stuff in here'},
							{name: 'Field 2', value: 'More stuff in here'},
							{name: 'Field 3', value: 'Stuff in here 3'}
						]
					}],
					client: 1
				};

				$httpBackend.whenPOST(API_URL + '/clients/' + 1 + '/assets/' + 4 + '/copy').respond(200, $q.when(response));

				expect(clients.copyAsset).not.toHaveBeenCalled();
				expect(result).toEqual({});

				clients.copyAsset(1, 4).then(function(res) {
					result = res;
				});

				$httpBackend.flush();

				expect(clients.copyAsset).toHaveBeenCalledWith(1, 4);
				expect(result).toEqual(response);
			});
		});
	});

	describe('Locations', function() {
		var locationList = [
			{_id: 1, name: 'Nelson'},
			{_id: 2, name: 'Edmonton'}
		];

		describe('.getLocations()', function() {
			beforeEach(function() {
				result = {};
				spyOn(clients, "getLocations").and.callThrough();
			});

			it('should exist', function() {
				expect(clients.getLocations).toBeDefined();
			});

			it('should return all the locations', function() {
				$httpBackend.whenGET(API_URL + '/clients/' + 1 + '/locations').respond(200, $q.when(locationList));

				expect(clients.getLocations).not.toHaveBeenCalled();
				expect(result).toEqual({});

				clients.getLocations(1).then(function(res) {
					result = res;
				});

				$httpBackend.flush();

				expect(clients.getLocations).toHaveBeenCalled();
				expect(result).toEqual(locationList);
				expect(result[0]._id).toEqual(1);
				expect(result[0].name).toEqual('Nelson');
			});
		});

		describe('.getLocation()', function() {
			beforeEach(function() {
				result = {};
				spyOn(clients, "getLocation").and.callThrough();
			});

			it('should exist', function() {
				expect(clients.getLocation).toBeDefined();
			});

			it('should return an individual location', function() {
				$httpBackend.whenGET(API_URL + '/clients/' + 1 + '/locations/' + 1).respond(200, $q.when(locationList[0]));

				expect(clients.getLocation).not.toHaveBeenCalled();
				expect(result).toEqual({});

				clients.getLocation(1, 1).then(function(res) {
					result = res;
				});

				$httpBackend.flush();

				expect(clients.getLocation).toHaveBeenCalled();
				expect(result).toEqual(locationList[0]);
				expect(result._id).toEqual(1);
				expect(result.name).toEqual('Nelson');
			});
		});

		describe('.add()', function() {
			beforeEach(function() {
				result = {};
				spyOn(clients, "addLocation").and.callThrough();
			});

			it('should exist', function() {
				expect(clients.addLocation).toBeDefined();
			});

			it('should return add a location', function() {
				var newLocation = {
					name: 'Vancouver'
				};

				var addedLocation = {
					_id: 3,
					name: 'Vancouver'
				};

				$httpBackend.whenPOST(API_URL + '/clients/' + 1 + '/locations', newLocation).respond(200, $q.when(addedLocation));

				expect(clients.addLocation).not.toHaveBeenCalled();
				expect(result).toEqual({});

				clients.addLocation(1, newLocation).then(function(res) {
					result = res;
				});

				$httpBackend.flush();

				expect(clients.addLocation).toHaveBeenCalled();
				expect(result).toEqual(addedLocation);
				expect(result._id).toEqual(3);
				expect(result.name).toEqual('Vancouver');
			});
		});

		describe('.save()', function() {
			beforeEach(function() {
				result = {};
				spyOn(clients, "saveLocation").and.callThrough();
			});

			it('should exist', function() {
				expect(clients.saveLocation).toBeDefined();
			});

			it('should save a location and return the details', function() {
				var editLocation = locationList[0];

				editLocation.name = 'New Location';

				var response = editLocation;

				$httpBackend.whenPUT(API_URL + '/clients/' + 1 + '/locations/' + 1, editLocation).respond(200, $q.when(response));

				expect(clients.saveLocation).not.toHaveBeenCalled();
				expect(result).toEqual({});

				clients.saveLocation(1, editLocation).then(function(res) {
					result = res;
				});

				$httpBackend.flush();

				expect(clients.saveLocation).toHaveBeenCalled();
				expect(result).toEqual(editLocation);
				expect(result._id).toEqual(1);
				expect(result.name).toEqual('New Location');
			});
		});

		describe('.remove()', function() {
			beforeEach(function() {
				result = {};
				spyOn(clients, "removeLocation").and.callThrough();
			});

			it('should exist', function() {
				expect(clients.removeLocation).toBeDefined();
			});

			it('should remove a location based on id', function() {
				var response = {message: 'Location Deleted'};

				$httpBackend.whenDELETE(API_URL + '/clients/' + 1 + '/locations/' + 2).respond(200, $q.when(response));

				expect(clients.removeLocation).not.toHaveBeenCalled();
				expect(result).toEqual({});

				clients.removeLocation(1, 2).then(function(res) {
					result = res;
				});

				$httpBackend.flush();

				expect(clients.removeLocation).toHaveBeenCalledWith(1, 2);
				expect(result.message).toEqual('Location Deleted');
			});
		});		
	});
});