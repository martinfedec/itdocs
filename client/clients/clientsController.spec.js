describe('Clients Controller', function() {
	var $controller, API_URL, $q, $httpBackend, $scope, $rootScope, ClientsFactory, toaster;

	clientList = [
	{
		_id: 1,
		name: 'Client 1',
		email: 'client1@test.com',
		active: true
	},
	{
		_id: 2,
		name: 'Client 2',
		email: 'client2@test.com',
		active: true
	}];

	beforeEach(module(function($provide) {
		$provide.constant("API_URL", "http://localhost:443/api");
	}));

	beforeEach(angular.mock.module('ui.router'));
	beforeEach(angular.mock.module('toaster'));
	beforeEach(angular.mock.module('itdocs.services.clients'));
	beforeEach(angular.mock.module('itdocs.controllers.clients'));

	beforeEach(inject(function(_$controller_, _API_URL_, _$q_, _$httpBackend_, _$rootScope_, _clients_, _toaster_) {
		$controller = _$controller_;
		API_URL = _API_URL_;
		$q = _$q_;
		$httpBackend = _$httpBackend_;
		$rootScope = _$rootScope_;
		$scope = _$rootScope_.$new();
		ClientsFactory = _clients_;
		toaster = _toaster_;
	}));

	describe('clientCtrl', function() {
		var clientCtrl;

		beforeEach(function() {
			clientCtrl = $controller('clientCtrl');
		});

		it('should be defined', function() {
			expect(clientCtrl).toBeDefined();
		});

		it('should set the title bar variables', function() {
			expect(clientCtrl.parentObj.parentTitle).toEqual('');
		});
	});

	describe('clientListCtrl', function() {
		var clientListCtrl;

		beforeEach(function() {
			spyOn(ClientsFactory, 'get').and.callThrough();
			spyOn(ClientsFactory, 'remove').and.callThrough();
			spyOn(toaster, 'pop').and.callThrough();

			$rootScope.parentObj = {};
			$rootScope.parentObj.parentTitle = '';
			$scope.c = $rootScope.$new();
			clientListCtrl = $controller('clientListCtrl', {$scope: $scope});
		});

		it('should get all users for the initial view and set deleteActive to 0', function() {
			$httpBackend.whenGET(API_URL + '/clients').respond(200, $q.when(clientList));
			$httpBackend.flush();
			
			expect($scope.c.parentObj.parentTitle).toEqual('Select a Client');
			expect(clientListCtrl.clients).toEqual(clientList);
			expect(clientListCtrl.deleteActive).toEqual(0);
		});

		it('should delete a specified client and toggle deleteActive and reload client list', function() {
			var removedList = [{
				_id: 2,
				name: 'Client 2',
				email: 'client2@test.com',
				active: true
			}]

			$httpBackend.whenDELETE(API_URL + '/clients/' + 1).respond(200, $q.when({message: 'Client Deleted'}));
			
			clientListCtrl.deleteClient(1);
			expect(clientListCtrl.deleteActive).toEqual(1);
			
			$httpBackend.whenGET(API_URL + '/clients').respond(200, $q.when(removedList));

			$httpBackend.flush();
			
			expect(toaster.pop).toHaveBeenCalled();
			expect(ClientsFactory.remove).toHaveBeenCalledWith(1);
			expect(clientListCtrl.deleteActive).toEqual(0);
			expect(clientListCtrl.clients.length).toEqual(1);
		});
	});

	describe('clientDetailCtrl', function() {
		var clientDetailCtrl, $stateParams;

		beforeEach(inject(function(_$stateParams_) {
			$stateParams = _$stateParams_;
			$stateParams.id = 1;
		}));

		beforeEach(function() {
			spyOn(ClientsFactory, 'getById').and.callThrough();

			$rootScope.parentObj = {};
			$rootScope.parentObj.parentTitle = '';
			$scope.c = $rootScope.$new();

			clientDetailCtrl = $controller('clientDetailCtrl', {$scope: $scope});
		});

		it('should be defined', function() {
			expect(clientDetailCtrl).toBeDefined();
		});

		it('should load the specified client', function() {
			var client = clientList[0];

			$httpBackend.whenGET(API_URL + '/clients/' + $stateParams.id).respond(200, client);
			$httpBackend.flush();
			expect(clientDetailCtrl.client).toEqual(client);
			expect($scope.c.parentObj.parentTitle).toEqual(client.name + ' Dashboard');
		});
	});

	describe('clientEditListCtrl', function() {
		var clientEditListCtrl, $stateParams, $state;

		beforeEach(inject(function(_$stateParams_, _$state_) {
			$stateParams = _$stateParams_;
			$state = _$state_;
		}));

		beforeEach(function() {
			clientEditListCtrl = $controller('clientEditListCtrl');
		});

		it('should be defined', function() {
			expect(clientEditListCtrl).toBeDefined();
		});

		describe('Add new client', function() {
			beforeEach(function() {
				spyOn(ClientsFactory, 'add').and.callThrough();
			});

			it('sub menus should be disabled', function() {
				expect(clientEditListCtrl.clientEdit).toBeDefined();
				expect(clientEditListCtrl.clientEdit).toEqual(false);
			});
		});

		describe('Edit existing client', function() {
			beforeEach(inject(function(_$stateParams_) {
				$stateParams = _$stateParams_;
				$stateParams.id = 1;
				clientEditListCtrl = $controller('clientEditListCtrl');
			}));

			beforeEach(function() {
				spyOn(clientEditListCtrl, 'navigate').and.callThrough();
				spyOn($state, 'go');
			});

			it('sub menus should be enabled', function() {
				expect(clientEditListCtrl.clientEdit).toBeDefined();
				expect(clientEditListCtrl.clientEdit).toEqual(true);
			});

			it('should navigate to the locations state', function() {
				// clientEditListCtrl.navigate('locations');
				// expect(clientEditListCtrl.navigate).toHaveBeenCalledWith('locations');
				// expect($state.go).toHaveBeenCalledWith('^.locations');
			});
		});		
	});

	describe('clientEditCtrl', function() {
		var clientEditCtrl, $stateParams, $state;

		beforeEach(inject(function(_$stateParams_, _$state_) {
			$stateParams = _$stateParams_;
			$state = _$state_;
		}));

		beforeEach(function() {
			spyOn(toaster, 'pop').and.callThrough();
			spyOn($state, 'go');

			$rootScope.parentObj = {};
			$rootScope.parentObj.parentTitle = '';
			$scope.c = $rootScope.$new();

			clientEditCtrl = $controller('clientEditCtrl', {$scope: $scope, clients: ClientsFactory, toaster: toaster});
		});

		it('should be defined', function() {
			expect(clientEditCtrl).toBeDefined();

		});

		describe('Add new client', function() {
			beforeEach(function() {
				spyOn(ClientsFactory, 'add').and.callThrough();
			});

			it('should add a new client', function() {
				var client = {
					name: 'Client 1',
					email: 'user1@user.com',
					active: true
				};

				var response = {
					_id: 1,
					name: 'client 2',
					email: 'user1@user.com',
					active: true
				}

				expect($scope.c.parentObj.parentTitle).toEqual('Add New Client');
				expect(clientEditCtrl.client).toEqual({});
				expect(clientEditCtrl.nameExists).toEqual(false);
				expect(clientEditCtrl.isSaving).toEqual(false);

				$httpBackend.whenPOST(API_URL + '/clients', client).respond(200, $q.when(response));
				clientEditCtrl.client = client;
				clientEditCtrl.submitClick();
				expect(clientEditCtrl.isSaving).toEqual(true);
				$httpBackend.flush();

				expect(clientEditCtrl.isSaving).toEqual(false);
				expect(toaster.pop).toHaveBeenCalled();
				expect($state.go).toHaveBeenCalledWith('app.clients.edit', {id: response._id});
			});

			it('should try to add a new client with duplicate name', function() {
				var client = {
					name: 'Client 1',
					email: 'client1@user.com',
					active: true
				};

				var response = {
					code: 100,
					message: 'Name Already Exists'
				}

				expect(clientEditCtrl.nameExists).toEqual(false);

				$httpBackend.whenPOST(API_URL + '/clients', client).respond(409, response);
				clientEditCtrl.client = client;
				clientEditCtrl.submitClick();
				$httpBackend.flush();
				
				expect(clientEditCtrl.nameExists).toEqual(true);
			});
		});
		

		describe('Edit existing client', function() {
			beforeEach(inject(function(_$stateParams_) {
				$stateParams = _$stateParams_;
				$stateParams.id = 1;
				clientEditCtrl = $controller('clientEditCtrl', {$scope: $scope, clients: ClientsFactory, toaster: toaster, $stateParams: $stateParams});
			}));

			beforeEach(function() {
				spyOn(ClientsFactory, 'getById').and.callThrough();
				spyOn(ClientsFactory, 'save').and.callThrough();

				clientEditCtrl.clientForm = {
					$setPristine: function() {}
				}

				spyOn(clientEditCtrl.clientForm, '$setPristine');
			});

			it('should edit a client', function() {
				var client = clientList[0];

				var changedClient = {
					_id: 1,
					name: 'Client 99',
					email: 'client99@user.com',
					active: true
				}
				
				expect(clientEditCtrl.nameExists).toEqual(false);
				expect(clientEditCtrl.isSaving).toEqual(false);
				$httpBackend.whenGET(API_URL + '/clients/' + 1).respond(200, $q.when(client));
				$httpBackend.flush();
				
				expect($scope.c.parentObj.parentTitle).toEqual('Editing ' + client.name);
				expect(clientEditCtrl.client).toEqual(client);
				
				$httpBackend.whenPUT(API_URL + '/clients/' + 1, changedClient).respond(200, $q.when(changedClient));
				clientEditCtrl.client = changedClient;
				clientEditCtrl.submitClick();
				expect(clientEditCtrl.isSaving).toEqual(true);
				$httpBackend.flush();

				expect(clientEditCtrl.isSaving).toEqual(false);
				expect(toaster.pop).toHaveBeenCalled();
				expect(clientEditCtrl.clientForm.$setPristine).toHaveBeenCalled();
			});

			it('should try to edit a client with duplicate email', function() {
				var client = clientList[0];

				var response = {
					code: 100,
					message: 'Name Already Exists'
				}

				expect(clientEditCtrl.nameExists).toEqual(false);
				
				$httpBackend.whenGET(API_URL + '/clients/' + 1).respond(200, $q.when(client));
				$httpBackend.flush();

				$httpBackend.whenPUT(API_URL + '/clients/' + 1, client).respond(409, response);
				clientEditCtrl.client = client;
				clientEditCtrl.submitClick();
				$httpBackend.flush();
				
				expect(clientEditCtrl.nameExists).toEqual(true);
			});
		});
	});

	describe('clientLocationCtrl', function() {
		var clientLocationCtrl;
		var locationList = [
			{ _id: 1, name: 'Nelson' },
			{ _id: 2, name: 'Vancouver' }
		];

		beforeEach(inject(function(_$stateParams_) {
			$stateParams = _$stateParams_;
			$stateParams.id = 1;
			clientLocationCtrl = $controller('clientLocationCtrl');
		}));

		beforeEach(function() {
			spyOn(ClientsFactory, 'getLocations').and.callThrough();
			spyOn(ClientsFactory, 'removeLocation').and.callThrough();
			spyOn(toaster, 'pop');
		});

		it('should be defined', function() {
			expect(clientLocationCtrl).toBeDefined();
		});

		it('should get a listing of all client locations', function() {
			$httpBackend.whenGET(API_URL + '/clients/' + $stateParams.id + '/locations').respond(200, $q.when(locationList));
			$httpBackend.flush();

			expect(clientLocationCtrl.locations).toEqual(locationList);
			expect(clientLocationCtrl.deleteActive).toEqual(0);
		});

		it('should delete a specified location and toggle deleteActive and reload location list', function() {
			var removedList = [{
				_id: 2,
				name: 'Vancouver'
			}]

			$httpBackend.whenDELETE(API_URL + '/clients/' + $stateParams.id + '/locations/' + 1).respond(200, $q.when({message: 'Location Deleted'}));
			
			clientLocationCtrl.deleteLocation($stateParams.id, 1);
			expect(clientLocationCtrl.deleteActive).toEqual(1);
			
			$httpBackend.whenGET(API_URL + '/clients/' + $stateParams.id + '/locations').respond(200, $q.when(removedList));

			$httpBackend.flush();
			
			expect(toaster.pop).toHaveBeenCalled();
			expect(ClientsFactory.removeLocation).toHaveBeenCalledWith($stateParams.id, 1);
			expect(clientLocationCtrl.deleteActive).toEqual(0);
			expect(clientLocationCtrl.locations.length).toEqual(1);
		});

		it('should try to delete the primary location but it is uanble', function() {
			var locationList = [{
				_id: 2,
				name: 'Vancouver'
			}]

			var response ='LOCATION_ASSIGNED_EXISTS';

			expect(clientLocationCtrl.deleteActive).toEqual(0);
			$httpBackend.whenGET(API_URL + '/clients/' + $stateParams.id + '/locations').respond(200, $q.when(locationList));
			$httpBackend.whenDELETE(API_URL + '/clients/' + $stateParams.id + '/locations/' + 1).respond(409, response);

			clientLocationCtrl.deleteLocation($stateParams.id, 1);
			
			$httpBackend.flush();

			expect(toaster.pop).toHaveBeenCalled();
			expect(clientLocationCtrl.deleteActive).toEqual(0);
		});
	});

	describe('clientLocationEditCtrl', function() {
		var clientLocationEditCtrl, $stateParams, $state;

		beforeEach(inject(function(_$stateParams_, _$state_) {
			$stateParams = _$stateParams_;
			$state = _$state_;

			$stateParams.id = 1;
		}));

		beforeEach(function() {
			spyOn(toaster, 'pop').and.callThrough();
			spyOn($state, 'go');

			clientLocationEditCtrl = $controller('clientLocationEditCtrl');
		});

		it('should be defined', function() {
			expect(clientLocationEditCtrl).toBeDefined();
		});

		describe('Add new location', function() {
			beforeEach(function() {
				spyOn(ClientsFactory, 'addLocation').and.callThrough();
			});

			it('should add a new location', function() {
				var location = {
					name: 'Nelson',
				};

				var response = {
					_id: 1,
					name: 'Nelson',
					active: true
				}

				expect(clientLocationEditCtrl.mode).toEqual('Add New');
				expect(clientLocationEditCtrl.location).toEqual({active: true});
				expect(clientLocationEditCtrl.nameExists).toEqual(false);

				$httpBackend.whenPOST(API_URL + '/clients/' + $stateParams.id + '/locations', location).respond(200, $q.when(response));
				clientLocationEditCtrl.location = location;
				clientLocationEditCtrl.saveLocation();
				$httpBackend.flush();

				expect(toaster.pop).toHaveBeenCalled();
				expect($state.go).toHaveBeenCalledWith('app.clients.edit.locations');
			});

			it('should try to add a new location with duplicate name', function() {
				var location = {
					name: 'Nelson'
				};

				var response = {
					code: 100,
					message: 'Name Already Exists'
				}

				expect(clientLocationEditCtrl.mode).toEqual('Add New');
				expect(clientLocationEditCtrl.location).toEqual({active: true});
				expect(clientLocationEditCtrl.nameExists).toEqual(false);

				$httpBackend.whenPOST(API_URL + '/clients/' + $stateParams.id + '/locations', location).respond(409, response);
				clientLocationEditCtrl.location = location;
				clientLocationEditCtrl.saveLocation();
				$httpBackend.flush();
				
				expect(clientLocationEditCtrl.nameExists).toEqual(true);
			});
		});
		

		describe('Edit existing location', function() {
			var locationList = [
				{ _id: 1, name: 'Nelson' },
				{ _id: 2, name: 'Vancouver' }
			];

			beforeEach(function() {
				spyOn(ClientsFactory, 'getLocation').and.callThrough();
				spyOn(ClientsFactory, 'saveLocation').and.callThrough();

				$stateParams.location_id = 2;
				
				clientLocationEditCtrl = $controller('clientLocationEditCtrl');
			});

			it('should edit a location', function() {
				var location = locationList[0];

				var changed = {
					_id: 1,
					name: 'Location 99'
				}

				expect(clientLocationEditCtrl.mode).toEqual('Edit');
				expect(clientLocationEditCtrl.nameExists).toEqual(false);
				$httpBackend.whenGET(API_URL + '/clients/' + $stateParams.id + '/locations/' + $stateParams.location_id).respond(200, $q.when(location));
				$httpBackend.flush();
				expect(clientLocationEditCtrl.location).toEqual(location);

				$httpBackend.whenPUT(API_URL + '/clients/' + $stateParams.id + '/locations/' + 1, changed).respond(200, $q.when(changed));
				clientLocationEditCtrl.location = changed;
				clientLocationEditCtrl.saveLocation();
				$httpBackend.flush();

				expect(toaster.pop).toHaveBeenCalled();
				expect($state.go).toHaveBeenCalledWith('app.clients.edit.locations');
			});

			it('should try to edit a location with duplicate name', function() {
				var location = locationList[0];

				var response = {
					code: 100,
					message: 'Name Already Exists'
				}
				
				expect(clientLocationEditCtrl.nameExists).toEqual(false);
				
				$httpBackend.whenGET(API_URL + '/clients/' + $stateParams.id + '/locations/' + $stateParams.location_id).respond(200, $q.when(location));
				$httpBackend.flush();

				$httpBackend.whenPUT(API_URL + '/clients/' + $stateParams.id + '/locations/' + 1, location).respond(409, response);
				clientLocationEditCtrl.location = location;
				clientLocationEditCtrl.saveLocation();
				$httpBackend.flush();
				
				expect(clientLocationEditCtrl.nameExists).toEqual(true);
			});
		});
	});
});