describe('Assets Controller', function() {
	var $controller, API_URL, $q, $httpBackend, ClientsFactory, toaster;

	AssetList = [
	{
		_id: 1,
		name: 'Server'
	},
	{
		_id: 2,
		name: 'Script'
	}];

	beforeEach(module(function($provide) {
		$provide.constant("API_URL", "http://localhost:443/api");
	}));

	beforeEach(angular.mock.module('ui.router'));
	beforeEach(angular.mock.module('ui.bootstrap'));
	beforeEach(angular.mock.module('toaster'));
	beforeEach(angular.mock.module('itdocs.services.clients'));
	beforeEach(angular.mock.module('itdocs.services.templates'));
	beforeEach(angular.mock.module('itdocs.services.templateTypes'));
	beforeEach(angular.mock.module('itdocs.controllers.clients.assets'));

	beforeEach(inject(function(_$controller_, _API_URL_, _$q_, _$httpBackend_, _clients_, _toaster_) {
		$controller = _$controller_;
		API_URL = _API_URL_;
		$q = _$q_;
		$httpBackend = _$httpBackend_;
		ClientsFactory = _clients_;
		toaster = _toaster_;
	}));

	describe('assetDashboardCtrl', function() {
		var assetDashboardCtrl, uibModal;

		beforeEach(inject(function(_$stateParams_, _$uibModal_, _$state_) {
			$uibModal = _$uibModal_;
			$state = _$state_;
			$stateParams = _$stateParams_;
			$stateParams.id = 1;
			$stateParams.asset_id = 1;
		}));

		beforeEach(function() {
			spyOn(ClientsFactory, 'getAsset').and.callThrough();
			spyOn($uibModal, 'open').and.callThrough();
			spyOn($state, 'go');
			assetDashboardCtrl = $controller('assetDashboardCtrl');
		});

		it('should be defined', function() {
			expect(assetDashboardCtrl).toBeDefined();
		});

		it('should load the asset requested', function() {
			$httpBackend.whenGET(API_URL + '/clients/' + 1 + '/assets/' + 1).respond(200, $q.when(AssetList[0]));
			$httpBackend.flush();
			
			expect(assetDashboardCtrl.asset).toEqual(AssetList[0]);
		});

		it('should open a modal with the script for the asset', function() {
			expect(assetDashboardCtrl.viewScript).toBeDefined();

			assetDashboardCtrl.viewScript(3);

			expect($uibModal.open).toHaveBeenCalled();

			// Below fails for some reason even though it is the exact same.
			// expect($uibModal.open).toHaveBeenCalledWith({
	  //           templateUrl: 'clients/assets/scriptModal.html',
	  //           controller: 'assetScriptModalCtrl as asm',
	  //           size: 'lg',
	  //           resolve: {
	  //           	clientId: function() {
	  //           		return $stateParams.id;
	  //           	},
	  //           	assetId: function() {
	  //           		return $stateParams.asset_id;
	  //           	},
	  //           	scriptId: function() {
	  //           		return script_id;
	  //           	}
	  //           }
	  //       });
		});

		it('should copy the current asset and create a new one', function() {
			expect(assetDashboardCtrl.copyAsset).toBeDefined();
			
			var clientAsset = {
				name: 'Asset 1',
				types: [
					{ 
						template: '123456',
						fields: [{
							name: 'Test Field 1',
							value: 'Stuff in here.'
						}],
						script: 'Script will go here'
					}
				],
				client: 'Client 1',
			    active: true,
			    custom: {
			    	fields: [{
			    		name: 'Custom Field 1',
						value: 'Some Data'
			    	}]
			    },
			    lastupdatedat: 'Sept 1, 2017',
			    lastupdatedby: 'Some Dude',
			    createdby: 'Some Dude',
				createdat: 'Aug 29, 2017',
				history: 'history goes here.',
				_id: 1
			};

			assetDashboardCtrl.asset = clientAsset;

			$httpBackend.whenGET(API_URL + '/clients/' + 1 + '/assets/' + 1).respond(200, $q.when(AssetList[0]));
			$httpBackend.whenPOST(API_URL + '/clients/' + 1 + '/assets/' + 1 + '/copy').respond(200, $q.when(clientAsset));
			
			assetDashboardCtrl.copyAsset();
			
			$httpBackend.flush();

			expect($state.go).toHaveBeenCalledWith('app.clients.detail.asset.edit', {id: 'Client 1', asset_id: 1});
		});
	});

	describe('assetListCtrl', function() {
		var assetListCtrl;

		beforeEach(inject(function(_$state_) {
			$state = _$state_;
		}))

		beforeEach(function() {
			spyOn($state, 'go');

			assetListCtrl = $controller('assetListCtrl');
		});

		it('should be defined', function() {
			expect(assetListCtrl).toBeDefined();
		});

		it('should navigate to new asset screen', function() {
			expect(assetListCtrl.addAsset).toBeDefined();

			assetListCtrl.addAsset();

			expect($state.go).toHaveBeenCalledWith('app.clients.detail.asset.new');
		});
	});

	describe('assetListDetailsCtrl', function() {
		var assetListDetailsCtrl;

		beforeEach(inject(function(_$state_, _$stateParams_) {
			$state = _$state_;
			$stateParams = _$stateParams_;
			$stateParams.id = 1;
		}))

		beforeEach(function() {
			spyOn($state, 'go');
		 	spyOn(ClientsFactory, 'getAssets').and.callThrough();
		 	spyOn(ClientsFactory, 'removeAsset').and.callThrough();
		 	spyOn(toaster, 'pop').and.callThrough();

			assetListDetailsCtrl = $controller('assetListDetailsCtrl');
		});

		it('should be defined', function() {
			expect(assetListDetailsCtrl).toBeDefined();
		});

		it('should get all assets for the client and set deleteActive to 0', function() {
			$httpBackend.whenGET(API_URL + '/clients/' + 1 + '/assets').respond(200, $q.when({total: 2, data: AssetList}));
			$httpBackend.flush();
			
			expect(assetListDetailsCtrl.assets).toEqual(AssetList);
			expect(assetListDetailsCtrl.totalItems).toEqual(2);
			expect(assetListDetailsCtrl.deleteActive).toEqual(0);
		});

		it('should delete a specified asset and toggle deleteActive and reload asset list', function() {
			var removedList = [{
				_id: 2,
				name: 'Script'
			}]

			$httpBackend.whenDELETE(API_URL + '/clients/' + 1 + '/assets/' + 1).respond(200, $q.when({message: 'Asset Deleted'}));
			
			assetListDetailsCtrl.deleteAsset(1);
			expect(assetListDetailsCtrl.deleteActive).toEqual(1);
			
			$httpBackend.whenGET(API_URL + '/clients/' + 1 + '/assets').respond(200, $q.when({total: 1, data: removedList}));

			$httpBackend.flush();
			
			expect(toaster.pop).toHaveBeenCalled();
			expect(ClientsFactory.removeAsset).toHaveBeenCalledWith(1, 1);
			expect(assetListDetailsCtrl.deleteActive).toEqual(0);
			expect(assetListDetailsCtrl.assets.length).toEqual(1);
			expect(assetListDetailsCtrl.totalItems).toEqual(1);
		});

		it('should copy the asset and create a new one and reload the list', function() {
			expect(assetListDetailsCtrl.copyAsset).toBeDefined();
			
			var response = AssetList;
			response.push(AssetList[0]);

			$httpBackend.whenGET(API_URL + '/clients/' + 1 + '/assets').respond(200, $q.when({total: 3, data: response}));
			$httpBackend.whenPOST(API_URL + '/clients/' + 1 + '/assets/' + 1 + '/copy').respond(200, $q.when(AssetList[0]));
			
			assetListDetailsCtrl.copyAsset(1);
			
			$httpBackend.flush();

			expect(toaster.pop).toHaveBeenCalledWith({
				type: 'success',
				body: 'Asset has been copied <b>' + AssetList[0].name + '</b>',
				showCloseButton: true,
				timeout: 3000,
				bodyOutputType: 'trustedHtml'
			});

			expect(assetListDetailsCtrl.totalItems).toEqual(3);
			expect(assetListDetailsCtrl.assets[2]).toEqual(AssetList[0]);
		});

		it('should change the page', function() {
			assetListDetailsCtrl.currentPage = 1;
			expect(assetListDetailsCtrl.changePage).toBeDefined();

			assetListDetailsCtrl.changePage();
			expect($state.go).toHaveBeenCalledWith('.', {page: 1}, {notify: false});
			expect($stateParams.page).toEqual(1);
			expect(assetListDetailsCtrl.currentPage).toEqual(1);
		});
	});

	describe('assetEditCtrl', function() {
		var assetEditCtrl, $stateParams, $state;

		beforeEach(inject(function(_$stateParams_, _$state_) {
			$stateParams = _$stateParams_;
			$state = _$state_;

			$stateParams.id = 1;
		}));

		beforeEach(function() {
			spyOn(toaster, 'pop').and.callThrough();
			spyOn($state, 'go');

			assetEditCtrl = $controller('assetEditCtrl');
		});

		it('should be defined', function() {
			expect(assetEditCtrl).toBeDefined();
		});

		it('should add a new template', function() {
			var selectedTemplate = {
				_id: 1,
				name: 'Server',
				fields: [
					{ name: 'IP Address' },
					{ name: 'Gateway' }
				]
			};

			assetEditCtrl.selectedTemplate = selectedTemplate;

			assetEditCtrl.addTemplate();
			
			expect(assetEditCtrl.asset.types.length).toEqual(1);
			expect(assetEditCtrl.asset.types[0].template._id).toEqual(1);
			expect(assetEditCtrl.asset.types[0].template.fields).toEqual(selectedTemplate.fields);

			expect(assetEditCtrl.templateType).toEqual('');
			expect(assetEditCtrl.selectedTemplate).toEqual('');
		});

		it('should remove a template', function() {
			assetEditCtrl.asset = {
				_id: 1,
				name: 'HV01'
			};

			assetEditCtrl.asset.types = [];

			assetEditCtrl.asset.types.push({
				template: 1,
				fields: [
					{ _id: 1, name: 'IP Address' },
					{ _id: 2, name: 'Gateway' }
				]
			});

			assetEditCtrl.asset.types.push({
				template: 2,
				fields: [
					{ _id: 3, name: 'Field 1' },
					{ _id: 4, name: 'Field 2' },
					{ _id: 5, name: 'Field 3' }
				]
			});

			expect(assetEditCtrl.asset.types.length).toEqual(2);

			assetEditCtrl.removeTemplate(1);

			expect(assetEditCtrl.asset.types.length).toEqual(1);
			expect(assetEditCtrl.asset.types[0].template).toEqual(1);
			expect(assetEditCtrl.asset.types[0].fields.length).toEqual(2);
			expect(assetEditCtrl.asset.types[0].fields[0]._id).toEqual(1);
			expect(assetEditCtrl.asset.types[0].fields[0].name).toEqual('IP Address');
			expect(assetEditCtrl.asset.types[0].fields[1]._id).toEqual(2);
			expect(assetEditCtrl.asset.types[0].fields[1].name).toEqual('Gateway');			
		});

		it('should add a new custom field', function() {
			assetEditCtrl.asset = {
				_id: 1,
				name: 'New Asset',
				custom: {
					fields: []
				}
			};

			expect(assetEditCtrl.addCustomField).toBeDefined();
			assetEditCtrl.addCustomField();

			expect(assetEditCtrl.asset.custom.fields[0]).toEqual({name: '', value: ''});
		});

		it('should remove a custom field', function() {
			assetEditCtrl.asset = {
				_id: 1,
				name: 'New Asset',
				custom: {
					fields: [
						{_id: 1, name: 'Field 1', value: 'Data 1'},
						{_id: 2, name: 'Field 2', value: 'Data 2'}
					]
				}
			};

			expect(assetEditCtrl.removeCustomField).toBeDefined();
			assetEditCtrl.removeCustomField(0);

			expect(assetEditCtrl.asset.custom.fields.length).toEqual(1);
			expect(assetEditCtrl.asset.custom.fields[0]).toEqual({_id: 2, name: 'Field 2', value: 'Data 2'});
		});

		describe('Add new asset', function() {
			var response = {
				_id: 1,
				name: 'HV01'
			};

			beforeEach(function() {
				spyOn(ClientsFactory, 'addAsset').and.callThrough();
			});

			it('should add a new asset', function() {
				var asset = {
					name: 'HV01'
				};	

				expect(assetEditCtrl.mode).toEqual('Add New');
				expect(assetEditCtrl.asset).toEqual({types: []});

				$httpBackend.whenPOST(API_URL + '/clients/' + 1 + '/assets', asset).respond(200, $q.when(response));
				$httpBackend.whenGET(API_URL + '/templatetypes').respond(200, $q.when({_id: 1, name: 'Asset'}));
				$httpBackend.whenGET(API_URL + '/templates').respond(200, $q.when({_id: 1, name: 'Server'}));

				assetEditCtrl.asset = asset;
				assetEditCtrl.saveAsset();
				$httpBackend.flush();

				expect(toaster.pop).toHaveBeenCalled();
				expect($state.go).toHaveBeenCalledWith('app.clients.detail.asset.list.details');
			});
		});

		describe('Edit existing asset', function() {
			beforeEach(inject(function(_$stateParams_) {
				$stateParams = _$stateParams_;
				$stateParams.asset_id = 1;

				assetEditCtrl = $controller('assetEditCtrl');
			}));

			beforeEach(function() {
				spyOn(ClientsFactory, 'getAsset').and.callThrough();
				spyOn(ClientsFactory, 'saveAsset').and.callThrough();
			});

			it('should edit an asset', function() {
				var asset = {
					_id: 1,
					name: 'HV01',
					types: [
						{
							_id: 2,
							template: 1,
							fields: [
								{ _id: 3, name: 'Field 1', value: 'Test' },
								{ _id: 4, name: 'Field 2', value: 'Test 2' },
								{ _id: 5, name: 'Field 3', value: 'Test 3' }
							] 
						}
					],
					client: 1
				};

				var changedAsset = {
					_id: 1,
					name: 'HV01-01',
					types: [
						{
							_id: 2,
							template: 2,
							fields: [
								{ _id: 3, name: 'Field 1', value: 'Test 99' },
								{ _id: 4, name: 'Field 2', value: 'Test 100' },
								{ _id: 5, name: 'Field 3', value: 'Test 101' }
							] 
						}
					],
					client: 1
				};

				expect(assetEditCtrl.mode).toEqual('Edit');
				expect(assetEditCtrl.asset).toEqual({types: []});

				$httpBackend.whenGET(API_URL + '/templatetypes').respond(200, $q.when({_id: 1, name: 'Asset'}));
				$httpBackend.whenGET(API_URL + '/templates').respond(200, $q.when({_id: 1, name: 'Server'}));
				$httpBackend.whenGET(API_URL + '/clients/' + 1 + '/assets/' + 1).respond(200, $q.when(asset));
				$httpBackend.flush();
				expect(assetEditCtrl.asset).toEqual(asset);
				$httpBackend.whenPUT(API_URL + '/clients/' + 1 + '/assets/' + 1, changedAsset).respond(200, $q.when(changedAsset));

				assetEditCtrl.asset = changedAsset;
				assetEditCtrl.saveAsset();
				$httpBackend.flush();

				expect(toaster.pop).toHaveBeenCalled();
				expect($state.go).toHaveBeenCalledWith('app.clients.detail.asset.list.details');
			});
		});
	});

	describe('assetScriptModalCtrl', function() {
		var assetScriptModalCtrl, $uibModalInstance;

		beforeEach(inject(function() {
			$uibModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close', 'dismiss']);
			clientId = 1;
			assetId = 2;
			scriptId = 3;
		}));

		beforeEach(function() {
			spyOn(toaster, 'pop').and.callThrough();
			
			assetScriptModalCtrl = $controller('assetScriptModalCtrl', { clients: ClientsFactory, $uibModalInstance: $uibModalInstance, clientId: clientId, assetId: assetId, scriptId: scriptId });
		});

		it('should be defined', function() {
			expect(assetScriptModalCtrl).toBeDefined();
		});

		it('should load the modal with the supplied parameters', function() {
			$httpBackend.whenGET(API_URL + '/clients/' + clientId + '/assets/' + assetId + '/script/' + scriptId).respond(200, $q.when({script: 'Script here'}));
			
			expect(assetScriptModalCtrl.editorOptions).toEqual({
				lineNumbers: true,
		        matchBrackets: true,
		        styleActiveLine: true,
		        theme:"ambiance",
		        readOnly: true,
		        autoRefresh: true
			});

			$httpBackend.flush();
			expect(assetScriptModalCtrl.script).toEqual('Script here');
		});

		it('should close the modal when closed pressed', function() {
			assetScriptModalCtrl.cancel();

			expect($uibModalInstance.dismiss).toHaveBeenCalledWith('cancel');
		});

		it('should show a toaster when copy clicked', function() {
			assetScriptModalCtrl.onSuccess({action: 'copy'});

			expect(toaster.pop).toHaveBeenCalledWith({
				type: 'success',
				body: 'Script Copied to Clipboard',
				showCloseButton: true,
				timeout: 3000
			});
		});
	});
});