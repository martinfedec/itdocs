'use strict';

angular
	.module('itdocs.controllers.clients.assets', [])

	.controller('assetListCtrl', function($state) {
		var al = this;

		al.addAsset = function() {
			$state.go('app.clients.detail.asset.new');
		};
	})

	.controller('assetListDetailsCtrl', function(clients, $stateParams, $state, toaster) {
		var alc = this;
		alc.deleteActive = 0;

		alc.assets = [];
		alc.totalItems = 0;
		alc.itemsPerPage = 20;

		function loadAssets() {
			clients.getAssets($stateParams.id, {page: $stateParams.page}).then(function(assets) {
				alc.assets = assets.data;
				alc.totalItems = assets.total;

				// Need to set here so it set's it properly.
				if ($stateParams.page) {
					alc.currentPage = $stateParams.page;
				}
			});
		};
		
		loadAssets();

		alc.deleteAsset = function(id) {
			alc.deleteActive = id;
			clients.removeAsset($stateParams.id, id).then(function(response) {
				alc.deleteActive = 0;
				toaster.pop({
					type: 'error',
					body: 'Asset has been removed',
					showCloseButton: true,
					timeout: 3000,
					bodyOutputType: 'trustedHtml'
				});
				loadAssets();
			});
		};

		alc.copyAsset = function(id) {
			clients.copyAsset($stateParams.id, id).then(function(asset) {
				toaster.pop({
					type: 'success',
					body: 'Asset has been copied <b>' + asset.name + '</b>',
					showCloseButton: true,
					timeout: 3000,
					bodyOutputType: 'trustedHtml'
				});
				loadAssets();
			});
		};

		alc.changePage = function() {
			$state.go('.', {page: alc.currentPage}, {notify: false});
			$stateParams.page = alc.currentPage;
			loadAssets();
		};
	})

	.controller('assetDashboardCtrl', function(clients, templates, templateTypes, $state, $stateParams, $uibModal) {
		var ad = this;

		clients.getAsset($stateParams.id, $stateParams.asset_id).then(function(asset) {
			ad.asset = asset;
		});

		ad.viewScript = function(script_id) {

			var modalInstance = $uibModal.open({
	            templateUrl: 'clients/assets/scriptModal.html',
	            controller: 'assetScriptModalCtrl as asm',
	            size: 'lg',
	            resolve: {
	            	clientId: function() {
	            		return $stateParams.id;
	            	},
	            	assetId: function() {
	            		return $stateParams.asset_id;
	            	},
	            	scriptId: function() {
	            		return script_id;
	            	}
	            }
	        });

	        modalInstance.result.then(function () {
				// loadContactTypes();
			}, function() {
				// Need to put here or else you get console error "Possibly unhandled rejection: cancel"
			});
		};

		ad.copyAsset = function() {
			clients.copyAsset($stateParams.id, ad.asset._id).then(function(asset) {
				$state.go('app.clients.detail.asset.edit', { id: asset.client, asset_id: asset._id });
			});
		};
	})

	.controller('assetScriptModalCtrl', function(clients, $uibModalInstance, clientId, assetId, scriptId, toaster) {
		var asm = this;

		asm.editorOptions = {
			lineNumbers: true,
	        matchBrackets: true,
	        styleActiveLine: true,
	        theme:"ambiance",
	        readOnly: true,
	        autoRefresh: true
	    };

		clients.getScript(clientId, assetId, scriptId).then(function(script) {
			asm.script = script.script;
		});

		asm.cancel = function() {
			$uibModalInstance.dismiss('cancel');
		};

		asm.onSuccess = function(e) {
			toaster.pop({
				type: 'success',
				body: 'Script Copied to Clipboard',
				showCloseButton: true,
				timeout: 3000
			});
		};
	})

	.controller('assetEditCtrl', function(clients, templates, templateTypes, $state, $stateParams, toaster) {
		var ae = this;
		ae.asset = {};
		ae.asset.types = [];

		templateTypes.get().then(function(templateTypes) {
			ae.templateTypes = templateTypes;
		});
		
		templates.get().then(function(templates) {
			ae.templates = templates;
		});

		ae.editorOptions = {
			lineNumbers: true,
	        matchBrackets: true,
	        styleActiveLine: true,
	        theme:"ambiance"
	    };

		if ($stateParams.asset_id) {
			ae.mode = 'Edit';

			clients.getAsset($stateParams.id, $stateParams.asset_id).then(function(asset) {
				ae.asset = asset;
			})
		} else {
			ae.mode = 'Add New';
		}

		ae.saveAsset = function() {
			if (ae.mode === 'Edit') {
				clients.saveAsset($stateParams.id, ae.asset).then(function(response) {
					toaster.pop({
						type: 'success',
						body: 'Asset has been saved',
						showCloseButton: true,
						timeout: 3000
					});
					$state.go('app.clients.detail.asset.list.details');
				}, function(error) {
					switch (error.data.code) {
						case 100:
							ae.nameExists = true;
							// content = "Role Name already in exists"
							break;
						default:
							// content = error;
					};
				});
			} else {
				clients.addAsset($stateParams.id, ae.asset).then(function(response) {
					toaster.pop({
						type: 'success',
						body: 'Asset has been added',
						showCloseButton: true,
						timeout: 3000
					});
					$state.go('app.clients.detail.asset.list.details');
				}, function(error) {
					switch (error.data.code) {
						case 100:
							ae.nameExists = true;
							// content = "Role Name already in exists"
							break;
						default:
							// content = error;
					};
				});
			}
		};

		ae.addTemplate = function() {
			ae.asset.types.push({
				template: angular.copy(ae.selectedTemplate),
				fields: angular.copy(ae.selectedTemplate.fields),
				script: angular.copy(ae.selectedTemplate.script)
			});

			ae.templateType = '';
			ae.selectedTemplate = '';
		};

		ae.removeTemplate = function(index) {
			ae.asset.types.splice(index, 1);
		};

		ae.addCustomField = function() {
			ae.asset.custom.fields.push({name: '', value: ''});
		};

		ae.removeCustomField = function(index) {
			ae.asset.custom.fields.splice(index, 1);
		};
	})

	
;