'use strict';

angular
	.module('itdocs.controllers.clients', [
		'itdocs.controllers.clients.assets'
	])

	.controller('clientCtrl', function() {
		var vm = this;

		vm.parentObj = {};
		vm.parentObj.parentTitle = '';	
	})

	.controller('clientListCtrl', function($scope, clients, toaster) {
		var cl = this;
		cl.deleteActive = 0;

		$scope.c.parentObj.parentTitle = 'Select a Client';
		// $scope.c.ClientContent = 'Show a listing of all clients here... then they can select one to view.';

		function loadClients() {
			clients.get().then(function(clients) {
				cl.clients = clients;
			});
		};

		loadClients();

		cl.deleteClient = function(id) {
			cl.deleteActive = id;
			clients.remove(id).then(function(response) {
				cl.deleteActive = 0;
				toaster.pop({
					type: 'error',
					body: 'Client has been removed',
					showCloseButton: true,
					timeout: 3000,
					bodyOutputType: 'trustedHtml'
				});
				loadClients();
			});
		};
	})

	.controller('clientDetailCtrl', function($scope, $stateParams, clients) {
		var cd = this;

		// Client view details controller
		clients.getById($stateParams.id).then(function(client) {
			cd.client = client;
			$scope.c.parentObj.parentTitle = client.name + ' Dashboard';
		});
	})

	.controller('clientEditListCtrl', function($state, $stateParams) {
		var ce = this;
		ce.clientEdit = false;
		
		if ($stateParams.id) {
			ce.clientEdit = true;
		}
		
		// console.log($state.current.url);
		
		ce.navigate = function(stateRef) {
			// console.log($state.getCurrentPath());
			if ($state.current.url == ('/' + stateRef)) {
				// console.log('in here');
				$state.go('^.' + stateRef);
			}
		};
	})

	.controller('clientEditCtrl', function($scope, $state, $stateParams, clients, toaster) {
		var ce = this;
		// Client Edit and New Controller
		ce.isSaving = false;
		ce.client = {};
		ce.nameExists = false;

		if ($stateParams.id) {
			// Edit Client
			clients.getById($stateParams.id).then(function(client) {
				ce.client = client;
				$scope.c.parentObj.parentTitle = 'Editing ' + client.name;
			});
		} else {
			// Add Client
			$scope.c.parentObj.parentTitle = 'Add New Client';
		}

		ce.navigate = function(stateRef) {
			console.log('test');
			$state.go('.locations');
		};

		ce.submitClick = function() {
			ce.isSaving = true;
			if ($stateParams.id) {
				// Save Client
		        clients.save(ce.client).then(function(response) {
					ce.isSaving = false;
					ce.nameExists = false;
					toaster.pop({
						type: 'success',
						body: 'Client has been saved',
						showCloseButton: true,
						timeout: 3000
					});
					ce.clientForm.$setPristine();
				}, function(error) {
					switch (error.data.code) {
						case 100:
							ce.nameExists = true;
							ce.isSaving = false;
							// content = "Role Name already in exists"
							break;
						default:
							// content = error;
					};
				});
			} else {
				// Add Client
				clients.add(ce.client).then(function(response) {
					ce.isSaving = false;
					ce.nameExists = false;
					toaster.pop({
						type: 'success',
						body: 'Client has been added',
						showCloseButton: true,
						timeout: 3000
					});
					$state.go('app.clients.edit', {id: response._id});
				}, function(error) {
					switch (error.data.code) {
						case 100:
							ce.nameExists = true;
							ce.isSaving = false;
							// content = "Role Name already in exists"
							break;
						default:
							// content = error;
					};
				});
			}
		};
	})

	.controller('clientLocationCtrl', function($state, $stateParams, clients, toaster) {
		var clo = this;
		clo.deleteActive = 0;
		function loadLocations() {
			clients.getLocations($stateParams.id).then(function(locations) {
				clo.locations = locations;
			});
		};
			
		loadLocations();

		clo.deleteLocation = function(locationId) {
			clo.deleteActive = locationId;

			clients.removeLocation($stateParams.id, locationId).then(function(response) {
				clo.deleteActive = 0;
				toaster.pop({
					type: 'error',
					body: 'Location has been removed',
					showCloseButton: true,
					timeout: 3000,
					bodyOutputType: 'trustedHtml'
				});
				loadLocations();
			}, function(error) {
				clo.deleteActive = 0;
				switch(error.data) {
					case 'LOCATION_ASSIGNED_EXISTS':
						toaster.pop({
							type: 'error',
							body: 'Location is assigned as primary, can not delete',
							showCloseButton: true,
							timeout: 3000,
							bodyOutputType: 'trustedHtml'
						});
						break;
					default:
				};
			});
		};
	})

	.controller('clientLocationEditCtrl', function(clients, $stateParams, $state, toaster) {
		var cle = this;

		cle.location = {active: true};
		cle.nameExists = false;

		if ($stateParams.location_id) {
			cle.mode = 'Edit';

			clients.getLocation($stateParams.id, $stateParams.location_id).then(function(location) {
				cle.location = location;
			})

		} else {
			cle.mode = 'Add New';
		}

		cle.saveLocation = function() {
			if (cle.mode === 'Edit') {
				clients.saveLocation($stateParams.id, cle.location).then(function(response) {
					toaster.pop({
						type: 'success',
						body: 'Location has been saved',
						showCloseButton: true,
						timeout: 3000
					});
					$state.go('app.clients.edit.locations');
				}, function(error) {
					switch (error.data.code) {
						case 100:
							cle.nameExists = true;
							break;
						default:
							// content = error;
					};
				});
			} else {
				clients.addLocation($stateParams.id, cle.location).then(function(response) {
					toaster.pop({
						type: 'success',
						body: 'Location has been added',
						showCloseButton: true,
						timeout: 3000
					});
					$state.go('app.clients.edit.locations');
				}, function(error) {
					switch (error.data.code) {
						case 100:
							cle.nameExists = true;
							break;
						default:
							// content = error;
					};
				});
			}
		};
	})
;






