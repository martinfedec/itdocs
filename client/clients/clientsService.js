'use strict';

angular.module('itdocs.services.clients', [])

.factory('clients', function($http, API_URL){
    return {
        get: function() {
            return $http.get(API_URL + '/clients').then(function(response) {
        		return response.data;
        	});
        },
        getById: function(id) {
            return $http.get(API_URL + '/clients/' + id).then(function(response) {
        		return response.data;
        	});  
        },
        add: function(client) {
        	return $http.post(API_URL + '/clients', client).then(function(response) {
        		return response.data;
        	});
        },
        save: function(client) {
			return $http.put(API_URL + '/clients/' + client._id, client).then(function(response) {
        		return response.data;
        	});
        },
        remove: function(id) {
            return $http.delete(API_URL + '/clients/' + id).then(function(response) {
                return response.data;
            });
        },
        getAssets: function(id, q) {
            var query = '';
            
            if (q) {
                if (q.page) {

                    if (query == '') {
                        query += '?';
                    } else {
                        query += '&';
                    }

                    query += 'page=' + q.page;
                }

                if (q.top) {

                    if (query == '') {
                        query += '?';
                    } else {
                        query += '&';
                    }

                    query += 'top=' + q.top;
                }
            }

            return $http.get(API_URL + '/clients/' + id + '/assets' + query).then(function(response) {
                return response.data;
            });
        },
        getAsset: function(id, asset) {
            return $http.get(API_URL + '/clients/' + id + '/assets/' + asset).then(function(response) {
                return response.data;
            });
        },
        addAsset: function(id, asset) {
            return $http.post(API_URL + '/clients/' + id + '/assets', asset).then(function(response) {
                return response.data;
            });
        },
        saveAsset: function(id, asset) {
            return $http.put(API_URL + '/clients/' + id + '/assets/' + asset._id, asset).then(function(response) {
                return response.data;
            });
        },
        removeAsset: function(id, asset) {
            return $http.delete(API_URL + '/clients/' + id + '/assets/' + asset).then(function(response) {
                return response.data;
            });
        },
        copyAsset: function(id, asset) {
            return $http.post(API_URL + '/clients/' + id + '/assets/' + asset + '/copy').then(function(response) {
                return response.data;
            });
        },
        getLocations: function(id) {
            return $http.get(API_URL + '/clients/' + id + '/locations').then(function(response) {
                return response.data;
            });
        },
        getLocation: function(id, location) {
            return $http.get(API_URL + '/clients/' + id + '/locations/' + location).then(function(response) {
                return response.data;
            });
        },
        addLocation: function(id, location) {
            return $http.post(API_URL + '/clients/' + id + '/locations', location).then(function(response) {
                return response.data;
            });
        },
        saveLocation: function(id, location) {
            return $http.put(API_URL + '/clients/' + id + '/locations/' + location._id, location).then(function(response) {
                return response.data;
            });
        },
        removeLocation: function(id, location) {
            return $http.delete(API_URL + '/clients/' + id + '/locations/' + location).then(function(response) {
                return response.data;
            });
        },
        getScript: function(id, asset, script) {
            return $http.get(API_URL + '/clients/' + id + '/assets/' + asset + '/script/' + script).then(function(response) {
                return response.data;
            });
        }
    };
})

;