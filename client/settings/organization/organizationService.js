'use strict';

angular.module('itdocs.services.organization', [])

.factory('organization', function($http, API_URL){
    return {
        get: function() {
            return $http.get(API_URL + '/organization').then(function(response) {
        		return response.data;
        	});
        },
        save: function(client) {
			return $http.put(API_URL + '/organization', client).then(function(response) {
        		return response.data;
        	});
        },
        getLocations: function() {
            return $http.get(API_URL + '/organization/locations').then(function(response) {
                return response.data;
            });
        },
        getLocation: function(id) {
            return $http.get(API_URL + '/organization/locations/' + id).then(function(response) {
                return response.data;
            });
        },
        addLocation: function(location) {
            return $http.post(API_URL + '/organization/locations', location).then(function(response) {
                return response.data;
            });
        },
        saveLocation: function(location) {
            return $http.put(API_URL + '/organization/locations/' + location._id, location).then(function(response) {
                return response.data;
            });
        },
        removeLocation: function(id) {
            return $http.delete(API_URL + '/organization/locations/' + id).then(function(response) {
                return response.data;
            });
        }
    };
})

;