describe('Organization Factory', function() {
	var organization, API_URL, $httpBackend, $q;

	beforeEach(module(function($provide) {
		$provide.constant("API_URL", "http://localhost:443/api");
	}));

	var organizationDetails = {
		_id: 1,
		name: 'Organization 1'
	};

	beforeEach(angular.mock.module('itdocs.services.organization'));

	beforeEach(inject(function(_organization_,_$q_, _$httpBackend_, _API_URL_) {
		organization = _organization_;
		$q = _$q_;
		$httpBackend = _$httpBackend_;
		API_URL = _API_URL_;
	}));

	it('should exist', function() {
		expect(organization).toBeDefined();
	});

	describe('.get()', function() {
		beforeEach(function() {
			result = {};
			spyOn(organization, "get").and.callThrough();
		});

		it('should exist', function() {
			expect(organization.get).toBeDefined();
		});

		it('should return the organization', function() {
			$httpBackend.whenGET(API_URL + '/organization').respond(200, $q.when(organizationDetails));

			expect(organization.get).not.toHaveBeenCalled();
			expect(result).toEqual({});

			organization.get().then(function(res) {
				result = res;
			});

			$httpBackend.flush();

			expect(organization.get).toHaveBeenCalled();
			expect(result).toEqual(organizationDetails);
			expect(result._id).toEqual(1);
			expect(result.name).toEqual('Organization 1');
		});
	});

	describe('.save()', function() {
		beforeEach(function() {
			result = {};
			spyOn(organization, "save").and.callThrough();
		});

		it('should exist', function() {
			expect(organization.save).toBeDefined();
		});

		it('should save the organization detils', function() {
			var editOrg = organizationDetails;

			editOrg.name = 'Changed Me';

			$httpBackend.whenPUT(API_URL + '/organization', editOrg).respond(200, $q.when(editOrg));

			expect(organization.save).not.toHaveBeenCalled();
			expect(result).toEqual({});

			organization.save(editOrg).then(function(res) {
				result = res;
			});

			$httpBackend.flush();

			expect(organization.save).toHaveBeenCalledWith(editOrg);
			expect(result._id).toEqual(1);
			expect(result._id).toEqual(editOrg._id);
			expect(result.name).toEqual('Changed Me');
		});
	});

	describe('Locations', function() {
		var locationList = [
			{_id: 1, name: 'Nelson'},
			{_id: 2, name: 'Edmonton'}
		];

		describe('.getLocations()', function() {
			beforeEach(function() {
				result = {};
				spyOn(organization, "getLocations").and.callThrough();
			});

			it('should exist', function() {
				expect(organization.getLocations).toBeDefined();
			});

			it('should return all the locations', function() {
				$httpBackend.whenGET(API_URL + '/organization/locations').respond(200, $q.when(locationList));

				expect(organization.getLocations).not.toHaveBeenCalled();
				expect(result).toEqual({});

				organization.getLocations().then(function(res) {
					result = res;
				});

				$httpBackend.flush();

				expect(organization.getLocations).toHaveBeenCalled();
				expect(result).toEqual(locationList);
				expect(result[0]._id).toEqual(1);
				expect(result[0].name).toEqual('Nelson');
			});
		});

		describe('.getLocation()', function() {
			beforeEach(function() {
				result = {};
				spyOn(organization, "getLocation").and.callThrough();
			});

			it('should exist', function() {
				expect(organization.getLocation).toBeDefined();
			});

			it('should return an individual location', function() {
				$httpBackend.whenGET(API_URL + '/organization/locations/' + 1).respond(200, $q.when(locationList[0]));

				expect(organization.getLocation).not.toHaveBeenCalled();
				expect(result).toEqual({});

				organization.getLocation(1).then(function(res) {
					result = res;
				});

				$httpBackend.flush();

				expect(organization.getLocation).toHaveBeenCalled();
				expect(result).toEqual(locationList[0]);
				expect(result._id).toEqual(1);
				expect(result.name).toEqual('Nelson');
			});
		});

		describe('.add()', function() {
			beforeEach(function() {
				result = {};
				spyOn(organization, "addLocation").and.callThrough();
			});

			it('should exist', function() {
				expect(organization.addLocation).toBeDefined();
			});

			it('should return add a location', function() {
				var newLocation = {
					name: 'Vancouver'
				};

				var addedLocation = {
					_id: 3,
					name: 'Vancouver'
				};

				$httpBackend.whenPOST(API_URL + '/organization/locations', newLocation).respond(200, $q.when(addedLocation));

				expect(organization.addLocation).not.toHaveBeenCalled();
				expect(result).toEqual({});

				organization.addLocation(newLocation).then(function(res) {
					result = res;
				});

				$httpBackend.flush();

				expect(organization.addLocation).toHaveBeenCalled();
				expect(result).toEqual(addedLocation);
				expect(result._id).toEqual(3);
				expect(result.name).toEqual('Vancouver');
			});
		});

		describe('.save()', function() {
			beforeEach(function() {
				result = {};
				spyOn(organization, "saveLocation").and.callThrough();
			});

			it('should exist', function() {
				expect(organization.saveLocation).toBeDefined();
			});

			it('should save a location and return the details', function() {
				var editLocation = locationList[0];

				editLocation.name = 'New Location';

				var response = editLocation;

				$httpBackend.whenPUT(API_URL + '/organization/locations/' + 1, editLocation).respond(200, $q.when(response));

				expect(organization.saveLocation).not.toHaveBeenCalled();
				expect(result).toEqual({});

				organization.saveLocation(editLocation).then(function(res) {
					result = res;
				});

				$httpBackend.flush();

				expect(organization.saveLocation).toHaveBeenCalled();
				expect(result).toEqual(editLocation);
				expect(result._id).toEqual(1);
				expect(result.name).toEqual('New Location');
			});
		});

		describe('.remove()', function() {
			beforeEach(function() {
				result = {};
				spyOn(organization, "removeLocation").and.callThrough();
			});

			it('should exist', function() {
				expect(organization.removeLocation).toBeDefined();
			});

			it('should remove a location based on id', function() {
				var response = {message: 'Location Deleted'};

				$httpBackend.whenDELETE(API_URL + '/organization/locations/' + 2).respond(200, $q.when(response));

				expect(organization.removeLocation).not.toHaveBeenCalled();
				expect(result).toEqual({});

				organization.removeLocation(2).then(function(res) {
					result = res;
				});

				$httpBackend.flush();

				expect(organization.removeLocation).toHaveBeenCalledWith(2);
				expect(result.message).toEqual('Location Deleted');
			});
		});		
	});
});