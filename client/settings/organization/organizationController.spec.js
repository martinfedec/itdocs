describe('Organization Controller', function() {
	var $controller, API_URL, OrganizationFactory, $q, $httpBackend, toaster, users, userList;

	var organizationDetails = {
		_id: 1,
		name: 'Organization 1'
	};

	var userList = [
		{_id: 1, name: 'User 1', email: 'user@email.com'},
		{_id: 2, name: 'User 2', email: 'user2@email.com'},
		{_id: 3, name: 'User 3', email: 'user3@email.com'}
	];

	beforeEach(module(function($provide) {
		$provide.constant("API_URL", "http://localhost:443/api");
	}));

	beforeEach(angular.mock.module('ui.router'));
	beforeEach(angular.mock.module('toaster'));
	beforeEach(angular.mock.module('itdocs.services.organization'));
	beforeEach(angular.mock.module('itdocs.services.users'));
	beforeEach(angular.mock.module('itdocs.controllers.organization'));

	beforeEach(inject(function(_$controller_, _API_URL_, _organization_, _$q_, _$httpBackend_, _toaster_, _users_) {
		$controller = _$controller_;
		API_URL = _API_URL_;
		OrganizationFactory = _organization_;
		$q = _$q_;
		$httpBackend = _$httpBackend_;
		toaster = _toaster_;
		users = _users_;
	}));

	describe('organizationCtrl', function() {
		var organizationCtrl;

		beforeEach(function() {
			spyOn(OrganizationFactory, 'get').and.callThrough();
			spyOn(OrganizationFactory, 'save').and.callThrough();
			spyOn(toaster, 'pop');
			spyOn(users, 'get').and.callThrough();

			organizationCtrl = $controller('organizationCtrl');

			organizationCtrl.organizationForm = {
				$setPristine: function() {}
			}

			spyOn(organizationCtrl.organizationForm, '$setPristine');
		});

		it('should be defined', function() {
			expect(organizationCtrl).toBeDefined();
		});

		it('should load the organization details', function() {
			expect(organizationCtrl.organization).toBeDefined();

			$httpBackend.whenGET(API_URL + '/organization').respond(200, $q.when(organizationDetails));
			$httpBackend.whenGET(API_URL + '/users').respond(200, $q.when(userList));
			$httpBackend.flush();

			expect(organizationCtrl.users).toEqual(userList);
			expect(OrganizationFactory.get).toHaveBeenCalled();
			expect(organizationCtrl.organization).toEqual(organizationDetails);
		});

		it('should save the organization details', function() {
			var editedOrg = {
				_id: 1,
				name: 'Organization 1 Test Details'
			};
			$httpBackend.whenGET(API_URL + '/organization').respond(200, $q.when(organizationDetails));
			$httpBackend.whenGET(API_URL + '/users').respond(200, $q.when(userList));
			expect(organizationCtrl.isSaving).toEqual(false);

			$httpBackend.whenPUT(API_URL + '/organization').respond(200, $q.when(editedOrg));
			organizationCtrl.submitClick();
			expect(organizationCtrl.isSaving).toEqual(true);

			$httpBackend.flush();

			expect(organizationCtrl.organizationForm.$setPristine).toHaveBeenCalled();
			expect(organizationCtrl.isSaving).toEqual(false);
			expect(toaster.pop).toHaveBeenCalledWith({
				type: 'success',
				body: 'Organization has been saved',
				showCloseButton: true,
				timeout: 3000
			});
		});
	});

	describe('organizationLocationCtrl', function() {
		var organizationLocationCtrl;
		var locationList = [
			{ _id: 1, name: 'Nelson' },
			{ _id: 2, name: 'Vancouver' }
		];

		beforeEach(function() {
			spyOn(OrganizationFactory, 'getLocations').and.callThrough();
			spyOn(OrganizationFactory, 'removeLocation').and.callThrough();
			spyOn(toaster, 'pop');

			organizationLocationCtrl = $controller('organizationLocationCtrl');
		});

		it('should be defined', function() {
			expect(organizationLocationCtrl).toBeDefined();
		});

		it('should get a listing of all locations', function() {
			$httpBackend.whenGET(API_URL + '/organization/locations').respond(200, $q.when(locationList));
			$httpBackend.flush();

			expect(organizationLocationCtrl.locations).toEqual(locationList);
			expect(organizationLocationCtrl.deleteActive).toEqual(0);
		});

		it('should delete a specified location and toggle deleteActive and reload location list', function() {
			var removedList = [{
				_id: 2,
				name: 'Vancouver'
			}]

			$httpBackend.whenDELETE(API_URL + '/organization/locations/' + 1).respond(200, $q.when({message: 'Location Deleted'}));
			
			organizationLocationCtrl.deleteLocation(1);
			expect(organizationLocationCtrl.deleteActive).toEqual(1);
			
			$httpBackend.whenGET(API_URL + '/organization/locations').respond(200, $q.when(removedList));

			$httpBackend.flush();
			
			expect(toaster.pop).toHaveBeenCalled();
			expect(OrganizationFactory.removeLocation).toHaveBeenCalledWith(1);
			expect(organizationLocationCtrl.deleteActive).toEqual(0);
			expect(organizationLocationCtrl.locations.length).toEqual(1);
		});	
	});

	describe('organizationLocationEditCtrl', function() {
		var organizationLocationEditCtrl, $stateParams, $state;

		beforeEach(inject(function(_$stateParams_, _$state_) {
			$stateParams = _$stateParams_;
			$state = _$state_;
		}));

		beforeEach(function() {
			spyOn(toaster, 'pop').and.callThrough();
			spyOn($state, 'go');

			organizationLocationEditCtrl = $controller('organizationLocationEditCtrl');
		});

		it('should be defined', function() {
			expect(organizationLocationEditCtrl).toBeDefined();
		});

		describe('Add new location', function() {
			beforeEach(function() {
				spyOn(OrganizationFactory, 'addLocation').and.callThrough();
			});

			it('should add a new location', function() {
				var location = {
					name: 'Nelson',
				};

				var response = {
					_id: 1,
					name: 'Nelson',
					active: true
				}

				expect(organizationLocationEditCtrl.mode).toEqual('Add New');
				expect(organizationLocationEditCtrl.location).toEqual({active: true});
				expect(organizationLocationEditCtrl.nameExists).toEqual(false);

				$httpBackend.whenPOST(API_URL + '/organization/locations', location).respond(200, $q.when(response));
				organizationLocationEditCtrl.location = location;
				organizationLocationEditCtrl.saveLocation();
				$httpBackend.flush();

				expect(toaster.pop).toHaveBeenCalled();
				expect($state.go).toHaveBeenCalledWith('app.settings.organization.locations');
			});

			it('should try to add a new location with duplicate name', function() {
				var location = {
					name: 'Nelson'
				};

				var response = {
					code: 100,
					message: 'Name Already Exists'
				}

				expect(organizationLocationEditCtrl.mode).toEqual('Add New');
				expect(organizationLocationEditCtrl.location).toEqual({active: true});
				expect(organizationLocationEditCtrl.nameExists).toEqual(false);

				$httpBackend.whenPOST(API_URL + '/organization/locations', location).respond(409, response);
				organizationLocationEditCtrl.location = location;
				organizationLocationEditCtrl.saveLocation();
				$httpBackend.flush();
				
				expect(organizationLocationEditCtrl.nameExists).toEqual(true);
			});
		});
		

		describe('Edit existing location', function() {
			var locationList = [
				{ _id: 1, name: 'Nelson' },
				{ _id: 2, name: 'Vancouver' }
			];

			beforeEach(inject(function(_$stateParams_) {
				$stateParams = _$stateParams_;
				$stateParams.id = 1;
				organizationLocationEditCtrl = $controller('organizationLocationEditCtrl');
			}));

			beforeEach(function() {
				spyOn(OrganizationFactory, 'getLocation').and.callThrough();
				spyOn(OrganizationFactory, 'saveLocation').and.callThrough();
			});

			it('should edit a location', function() {
				var location = locationList[0];

				var changed = {
					_id: 1,
					name: 'Location 99'
				}
			
				expect(organizationLocationEditCtrl.mode).toEqual('Edit');
				expect(organizationLocationEditCtrl.nameExists).toEqual(false);
				$httpBackend.whenGET(API_URL + '/organization/locations/' + 1).respond(200, $q.when(location));
				$httpBackend.flush();
				expect(organizationLocationEditCtrl.location).toEqual(location);

				$httpBackend.whenPUT(API_URL + '/organization/locations/' + 1, changed).respond(200, $q.when(changed));
				organizationLocationEditCtrl.location = changed;
				organizationLocationEditCtrl.saveLocation();
				$httpBackend.flush();

				expect(toaster.pop).toHaveBeenCalled();
				expect($state.go).toHaveBeenCalledWith('app.settings.organization.locations');
			});

			it('should try to edit a location with duplicate name', function() {
				var location = locationList[0];

				var response = {
					code: 100,
					message: 'Name Already Exists'
				}
				
				expect(organizationLocationEditCtrl.nameExists).toEqual(false);
				
				$httpBackend.whenGET(API_URL + '/organization/locations/' + 1).respond(200, $q.when(location));
				$httpBackend.flush();

				$httpBackend.whenPUT(API_URL + '/organization/locations/' + 1, location).respond(409, response);
				organizationLocationEditCtrl.location = location;
				organizationLocationEditCtrl.saveLocation();
				$httpBackend.flush();
				
				expect(organizationLocationEditCtrl.nameExists).toEqual(true);
			});
		});
	});
});