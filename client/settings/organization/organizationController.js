'use strict';

angular
	.module('itdocs.controllers.organization', [])

	.controller('organizationCtrl', function($state, $stateParams, organization, toaster, users) {
		var o = this;
		o.organization = {};
		o.isSaving = false;

		organization.get().then(function(response) {
			o.organization = response;
		});

		users.get().then(function(response) {
			o.users = response;
		});

		o.submitClick = function() {
			o.isSaving = true;
			organization.save(o.organization).then(function(response) {
				o.isSaving = false;
				toaster.pop({
					type: 'success',
					body: 'Organization has been saved',
					showCloseButton: true,
					timeout: 3000
				});
				o.organizationForm.$setPristine();
			});
		}
	})

	.controller('organizationLocationCtrl', function($state, $stateParams, organization, toaster) {
		var ol = this;
		ol.deleteActive = 0;

		function loadLocations() {
			organization.getLocations().then(function(locations) {
				ol.locations = locations;
			});
		};
			
		loadLocations();

		ol.deleteLocation = function(id) {
			ol.deleteActive = id;

			organization.removeLocation(id).then(function(response) {
				ol.deleteActive = 0;
				toaster.pop({
					type: 'error',
					body: 'Location has been removed',
					showCloseButton: true,
					timeout: 3000,
					bodyOutputType: 'trustedHtml'
				});
				loadLocations();
			});
		};
	})

	.controller('organizationLocationEditCtrl', function(organization, $stateParams, $state, toaster) {
		var ole = this;

		ole.location = {active: true};
		ole.nameExists = false;

		if ($stateParams.id) {
			ole.mode = 'Edit';

			organization.getLocation($stateParams.id).then(function(location) {
				ole.location = location;
			})

		} else {
			ole.mode = 'Add New';
		}

		ole.saveLocation = function() {
			if (ole.mode === 'Edit') {
				organization.saveLocation(ole.location).then(function(response) {
					toaster.pop({
						type: 'success',
						body: 'Location has been saved',
						showCloseButton: true,
						timeout: 3000
					});
					$state.go('app.settings.organization.locations');
				}, function(error) {
					switch (error.data.code) {
						case 100:
							ole.nameExists = true;
							// content = "Role Name already in exists"
							break;
						default:
							// content = error;
					};
				});
			} else {
				organization.addLocation(ole.location).then(function(response) {
					toaster.pop({
						type: 'success',
						body: 'Location has been added',
						showCloseButton: true,
						timeout: 3000
					});
					$state.go('app.settings.organization.locations');
				}, function(error) {
					switch (error.data.code) {
						case 100:
							ole.nameExists = true;
							// content = "Role Name already in exists"
							break;
						default:
							// content = error;
					};
				});
			}
		};
	})
;