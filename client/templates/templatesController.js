'use strict';

angular
	.module('itdocs.controllers.templates', ['ui.codemirror'])
	.controller('templatesCtrl', function(templateTypes) {
		var t = this;

		templateTypes.get().then(function(templateTypes) {
			t.templateTypes = templateTypes;
		});
	})

	.controller('templatesListCtrl', function($stateParams, templates, templateTypes, toaster) {
		var tl = this;

		tl.deleteActive = 0;
		tl.templateType = '';

		function loadTemplates() {
			templates.get($stateParams.type).then(function(templates) {
				tl.templates = templates;
			});
		};
		
		if ($stateParams.type) {
			// Get template type
			templateTypes.getById($stateParams.type).then(function(templateType) {
				tl.templateType = templateType;
			});
		}

		loadTemplates();

		tl.toastUndo = function() {
			console.log('ooops.');
		};

		tl.deleteTemplate = function(id) {
			tl.deleteActive = id;

			templates.remove(id).then(function(response) {
				tl.deleteActive = 0;
				toaster.pop({
					type: 'error',
					body: 'Template has been removed',
					showCloseButton: true,
					timeout: 3000,
					bodyOutputType: 'trustedHtml'
				});
				loadTemplates();
			});
		};
	})
	.controller('templateEditCtrl', function($state, $stateParams, templates, templateTypes, toaster) {
		var te = this;
		te.nameExists = false;
		
		te.editorOptions = {
			lineNumbers: true,
	        matchBrackets: true,
	        styleActiveLine: true,
	        theme:"ambiance"
	    };
	    
		te.template = {};
		
		templateTypes.get().then(function(templateTypes) {
			te.templateTypes = templateTypes;
		});

		if ($stateParams.id) {
			te.mode = 'Edit';

			templates.getById($stateParams.id).then(function(template) {
				te.template = template;
			})

		} else {
			te.mode = 'Add New';

			if ($stateParams.type) {
				templateTypes.getById($stateParams.type).then(function(templateType) {
					te.template.templateType = templateType;
				});
			}

			te.template.fields = [
				{name: ''}
			];
		}

		te.addField = function() {
			te.template.fields.push({name: ''});
			te.templateForm.$setDirty();
		};

		te.removeField = function(index) {
			te.template.fields.splice(index, 1);
			te.templateForm.$setDirty();
		};

		te.saveTemplate = function() {
			if (te.mode === 'Edit') {
				templates.save(te.template).then(function(response) {
					toaster.pop({
						type: 'success',
						body: 'Template has been saved',
						showCloseButton: true,
						timeout: 3000
					});
					$state.go('app.templates.list.details', {type: $stateParams.type});
				}, function(error) {
					switch (error.data.code) {
						case 100:
							te.nameExists = true;
							// content = "Role Name already in exists"
							break;
						default:
							// content = error;
					};
				});
			} else {
				templates.add(te.template).then(function(response) {
					toaster.pop({
						type: 'success',
						body: 'Template has been added',
						showCloseButton: true,
						timeout: 3000
					});
					$state.go('app.templates.list.details', {type: $stateParams.type});
				}, function(error) {
					switch (error.data.code) {
						case 100:
							te.nameExists = true;
							// content = "Role Name already in exists"
							break;
						default:
							// content = error;
					};
				});
			}
		};
	})
;