'use strict';

angular.module('itdocs.services.templates', [])

.factory('templates', function($http, API_URL){
    return {
        get: function(type) {
            var filter = '';
            if (type) {
                filter = '?type=' + type;
            }

            return $http.get(API_URL + '/templates' + filter).then(function(response) {
        		return response.data;
        	});
        },
        getById: function(id) {
            return $http.get(API_URL + '/templates/' + id).then(function(response) {
        		return response.data;
        	});
        },
        add: function(template) {
        	return $http.post(API_URL + '/templates', template).then(function(response) {
        		return response.data;
        	});
        },
        save: function(template) {
			return $http.put(API_URL + '/templates/' + template._id, template).then(function(response) {
        		return response.data;
        	});
        },
        remove: function(id) {
            return $http.delete(API_URL + '/templates/' + id).then(function(response) {
                return response.data;
            });
        }
    };

})

;