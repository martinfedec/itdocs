'use strict';

angular.module('itdocs.services.templateTypes', [])

.factory('templateTypes', function($http, API_URL){
    return {
        get: function() {
            return $http.get(API_URL + '/templatetypes').then(function(response) {
        		return response.data;
        	});
        },
        getById: function(id) {
            return $http.get(API_URL + '/templatetypes/' + id).then(function(response) {
        		return response.data;
        	});
        }
    };

})

;