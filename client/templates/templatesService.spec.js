describe('Templates Factory', function() {
	var templates, API_URL, $httpBackend, $q;

	var templateList = [
		{_id: 1, name: 'Server', fields: [], script: '', templateType: 1, active: true},
		{_id: 2, name: 'New Client', fields: [], script: '', templateType: 2, active: true}
	]

	beforeEach(module(function($provide) {
		$provide.constant("API_URL", "http://localhost:443/api");
	}));

	beforeEach(angular.mock.module('itdocs.services.templates'));

	beforeEach(inject(function(_templates_,_$q_, _$httpBackend_, _API_URL_) {
		templates = _templates_;
		$q = _$q_;
		$httpBackend = _$httpBackend_;
		API_URL = _API_URL_;
	}));

	it('should be defined', function() {
		expect(templates).toBeDefined();
	});

	describe('.get(type)', function() {
		beforeEach(function() {
			result = {};
			spyOn(templates, "get").and.callThrough();
		});


		it('should exist', function() {
			expect(templates.get).toBeDefined();
		});

		it('should return a list of all templates, type not supplied', function() {
			$httpBackend.whenGET(API_URL + '/templates').respond(200, $q.when(templateList));

			expect(templates.get).not.toHaveBeenCalled();
			expect(result).toEqual({});

			templates.get().then(function(res) {
				result = res;
			});

			$httpBackend.flush();

			expect(templates.get).toHaveBeenCalled();
			expect(result.length).toBe(2);
			expect(result[0]._id).toEqual(1);
			expect(result[0].name).toEqual('Server');
			expect(result[0].templateType).toEqual(1);
			expect(result[0].active).toEqual(true);
		});
	});

	describe('.getById()', function() {
		beforeEach(function() {
			result = {};
			spyOn(templates, "getById").and.callThrough();
		});


		it('should exist', function() {
			expect(templates.getById).toBeDefined();
		});

		it('should return a template when called with a valid id', function() {
			$httpBackend.whenGET(API_URL + '/templates/' + 1).respond(200, $q.when(templateList[0]));

			expect(templates.getById).not.toHaveBeenCalled();
			expect(result).toEqual({});

			templates.getById(1).then(function(res) {
				result = res;
			});

			$httpBackend.flush();

			expect(templates.getById).toHaveBeenCalledWith(1);
			expect(result._id).toEqual(1);
			expect(result.name).toEqual('Server');
			expect(result.templateType).toEqual(1);
			expect(result.active).toEqual(true);
		});
	});

	describe('.add()', function() {
		beforeEach(function() {
			result = {};
			spyOn(templates, "add").and.callThrough();
		});

		it('should exist', function() {
			expect(templates.add).toBeDefined();
		});

		it('should add a new templates and return the added templates data', function() {
			var created = new Date();
			var newTemplate = {
				name: 'Client',
				templateType: 3,
				active: true
			};

			var newTemplateResponse = {
				_id: 3,
				name: 'Client',
				templateType: 3,
				active: true
			};

			$httpBackend.whenPOST(API_URL + '/templates', newTemplate).respond(200, $q.when(newTemplateResponse));

			expect(templates.add).not.toHaveBeenCalled();
			expect(result).toEqual({});

			templates.add(newTemplate).then(function(res) {
				result = res;
			});

			$httpBackend.flush();

			expect(templates.add).toHaveBeenCalledWith(newTemplate);
			expect(result._id).toEqual(3);
			expect(result.name).toEqual(newTemplate.name);
			expect(result.templateType).toEqual(newTemplate.templateType);
			expect(result.active).toEqual(newTemplate.active);
		});
	});

	describe('.save()', function() {
		beforeEach(function() {
			result = {};
			spyOn(templates, "save").and.callThrough();
		});

		it('should exist', function() {
			expect(templates.save).toBeDefined();
		});

		it('should save a template and return the templates data', function() {
			var editTemplate = templateList[1];

			editTemplate.name = 'Document Template';
			editTemplate.templateType = 1;
			editTemplate.active = false;

			var editTemplateResponse = editTemplate;

			$httpBackend.whenPUT(API_URL + '/templates/' + editTemplate._id, editTemplate).respond(200, $q.when(editTemplateResponse));

			expect(templates.save).not.toHaveBeenCalled();
			expect(result).toEqual({});

			templates.save(editTemplate).then(function(res) {
				result = res;
			});

			$httpBackend.flush();

			expect(templates.save).toHaveBeenCalledWith(editTemplate);
			expect(result._id).toEqual(2);
			expect(result._id).toEqual(editTemplate._id);
			expect(result.name).toEqual(editTemplate.name);
			expect(result.templateType).toEqual(editTemplate.templateType);
			expect(result.active).toEqual(false);
		});
	});

	describe('.remove()', function() {
		beforeEach(function() {
			result = {};
			spyOn(templates, "remove").and.callThrough();
		});

		it('should exist', function() {
			expect(templates.remove).toBeDefined();
		});

		it('should remove a template based on id', function() {
			var response = {message: 'Template Deleted'};

			$httpBackend.whenDELETE(API_URL + '/templates/' + 2).respond(200, $q.when(response));

			expect(templates.remove).not.toHaveBeenCalled();
			expect(result).toEqual({});

			templates.remove(2).then(function(res) {
				result = res;
			});

			$httpBackend.flush();

			expect(templates.remove).toHaveBeenCalledWith(2);
			expect(result.message).toEqual('Template Deleted');
		});
	});
});