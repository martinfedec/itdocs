describe('Template Types Factory', function() {
	var templateTypes, API_URL, $httpBackend, $q;

	var templateTypesList = [
		{_id: 1, description: 'Asset', icon: 'fa-asset', active: true},
		{_id: 2, description: 'Document', icon: 'fa-document', active: true},
		{_id: 3, description: 'Script', icon: 'fa-script', active: true}
	]

	beforeEach(module(function($provide) {
		$provide.constant("API_URL", "http://localhost:443/api");
	}));

	beforeEach(angular.mock.module('itdocs.services.templateTypes'));

	beforeEach(inject(function(_templateTypes_,_$q_, _$httpBackend_, _API_URL_) {
		templateTypes = _templateTypes_;
		$q = _$q_;
		$httpBackend = _$httpBackend_;
		API_URL = _API_URL_;
	}));

	it('should be defined', function() {
		expect(templateTypes).toBeDefined();
	});

	describe('.get()', function() {
		beforeEach(function() {
			result = {};
			spyOn(templateTypes, "get").and.callThrough();
		});


		it('should exist', function() {
			expect(templateTypes.get).toBeDefined();
		});

		it('should return a list of template types', function() {
			$httpBackend.whenGET(API_URL + '/templatetypes').respond(200, $q.when(templateTypesList));

			expect(templateTypes.get).not.toHaveBeenCalled();
			expect(result).toEqual({});

			templateTypes.get().then(function(res) {
				result = res;
			});

			$httpBackend.flush();

			expect(templateTypes.get).toHaveBeenCalled();
			expect(result.length).toBe(3);
			expect(result[0]._id).toEqual(1);
			expect(result[0].description).toEqual('Asset');
			expect(result[0].icon).toEqual('fa-asset');
			expect(result[0].active).toEqual(true);
		});
	});

	describe('.getById()', function() {
		beforeEach(function() {
			result = {};
			spyOn(templateTypes, "getById").and.callThrough();
		});


		it('should exist', function() {
			expect(templateTypes.getById).toBeDefined();
		});

		it('should return a template type when called with a valid id', function() {
			$httpBackend.whenGET(API_URL + '/templatetypes/' + 2).respond(200, $q.when(templateTypesList[1]));

			expect(templateTypes.getById).not.toHaveBeenCalled();
			expect(result).toEqual({});

			templateTypes.getById(2).then(function(res) {
				result = res;
			});

			$httpBackend.flush();

			expect(templateTypes.getById).toHaveBeenCalledWith(2);
			expect(result._id).toEqual(2);
			expect(result.description).toEqual('Document');
			expect(result.icon).toEqual('fa-document');
			expect(result.active).toEqual(true);
		});
	});
});