describe('Templates Controller', function() {
	var $controller, API_URL, TemplateTypesFactory, TemplatesFactory, $q, $httpBackend, toaster, templatesList, templateTypeList;

	templateTypeList = [
		{_id: 1, description: 'Asset'},
		{_id: 2, description: 'Document'},
		{_id: 3, description: 'Script'}
	];

	templatesList = [
		{_id: 1, name: 'Server', type: 1},
		{_id: 2, name: 'New Client Script', type: 3},
		{_id: 3, name: 'AD Server Document', type: 2}
	]

	beforeEach(module(function($provide) {
		$provide.constant("API_URL", "http://localhost:443/api");
	}));

	beforeEach(angular.mock.module('ui.router'));
	beforeEach(angular.mock.module('toaster'));
	beforeEach(angular.mock.module('itdocs.services.templates'));
	beforeEach(angular.mock.module('itdocs.services.templateTypes'));
	beforeEach(angular.mock.module('itdocs.controllers.templates'));

	beforeEach(function() {
		inject(function(_$controller_, _API_URL_, _$q_, _$httpBackend_, _toaster_, _templates_, _templateTypes_) {
			$controller = _$controller_;
			API_URL = _API_URL_;
			$q = _$q_;
			$httpBackend = _$httpBackend_;
			toaster = _toaster_;
			TemplatesFactory = _templates_;
			TemplateTypesFactory = _templateTypes_;
		});

		spyOn(TemplateTypesFactory, 'get').and.callThrough();
		spyOn(TemplateTypesFactory, 'getById').and.callThrough();
		spyOn(TemplatesFactory, 'get').and.callThrough();
		spyOn(TemplatesFactory, 'remove').and.callThrough();
		spyOn(toaster, 'pop');
	});
		

	describe('templatesCtrl', function() {
		var templatesCtrl;

		beforeEach(function() {
			templatesCtrl = $controller('templatesCtrl');
		});

		it('should be defined', function() {
			expect(templatesCtrl).toBeDefined();
		});

		it('should get a list of template types', function() {
			expect(templatesCtrl.templateTypes).toBeUndefined();

			$httpBackend.whenGET(API_URL + '/templatetypes').respond(200, $q.when(templateTypeList));
			$httpBackend.flush();

			expect(templatesCtrl.templateTypes).toBeDefined();
			expect(templatesCtrl.templateTypes).toEqual(templateTypeList);
		});
	});

	describe('templatesListCtrl', function() {
		var templatesListCtrl, $stateParams;

		beforeEach(function() {
			inject(function(_$stateParams_) {
				$stateParams = _$stateParams_;
			});

			templatesListCtrl = $controller('templatesListCtrl');
		});

		it('should be defined', function() {
			expect(templatesListCtrl).toBeDefined();
		});

		describe('No Type Specified', function() {
			it('should get all templates for the initial view and set deleteActive to 0, no type', function() {
				$httpBackend.whenGET(API_URL + '/templates').respond(200, $q.when(templatesList));
				$httpBackend.flush();
				
				expect($stateParams.type).toBeUndefined();
				expect(TemplatesFactory.get).toHaveBeenCalled();
				expect(templatesListCtrl.templates).toEqual(templatesList);
				expect(templatesListCtrl.deleteActive).toEqual(0);
				expect(templatesListCtrl.templateType).toEqual('');
				expect(TemplateTypesFactory.get).not.toHaveBeenCalled();
			});

			it('should delete a specified template and toggle deleteActive and reload template list', function() {
				var removedList = [
					{_id: 2, name: 'New Client Script', type: 3},
					{_id: 3, name: 'AD Server Document', type: 2}
				]

				$httpBackend.whenDELETE(API_URL + '/templates/' + 1).respond(200, $q.when({message: 'Template Deleted'}));
				
				templatesListCtrl.deleteTemplate(1);
				expect(templatesListCtrl.deleteActive).toEqual(1);
				
				$httpBackend.whenGET(API_URL + '/templates').respond(200, $q.when(removedList));

				$httpBackend.flush();
				
				expect(toaster.pop).toHaveBeenCalled();
				expect(TemplatesFactory.remove).toHaveBeenCalledWith(1);
				expect(templatesListCtrl.deleteActive).toEqual(0);
				expect(templatesListCtrl.templates.length).toEqual(2);
			});
		});

		describe('Type Specified', function() {
			beforeEach(function() {
				$stateParams.type = 1;
				templatesListCtrl = $controller('templatesListCtrl');
			});

			it('should get all templates for the initial view by type', function() {
				expect($stateParams.type).toBeDefined();

				$httpBackend.whenGET(API_URL + '/templatetypes/' + 1).respond(200, $q.when(templateTypeList[0]));
				// Not sure why this is running twice.
				$httpBackend.whenGET(API_URL + '/templates').respond(200, $q.when(templatesList));
				$httpBackend.whenGET(API_URL + '/templates?type=1').respond(200, $q.when(templatesList));
				$httpBackend.flush();

				expect(TemplatesFactory.get).toHaveBeenCalledWith($stateParams.type);
				expect(TemplateTypesFactory.getById).toHaveBeenCalledWith($stateParams.type);

				expect(templatesListCtrl.templates).toEqual(templatesList);
				expect(templatesListCtrl.deleteActive).toEqual(0);
				expect(templatesListCtrl.templateType).toEqual(templateTypeList[0]);
			});

			it('should delete a specified template and toggle deleteActive and reload template list', function() {
				// NEE TO WORK ON THIS ONE.... 
				expect($stateParams.type).toBeDefined();
				expect($stateParams.type).toEqual(1);

				var removedList = [
					{_id: 2, name: 'New Client Script', type: 3},
					{_id: 3, name: 'AD Server Document', type: 2}
				]

				$httpBackend.whenGET(API_URL + '/templates').respond(200, $q.when(removedList));
				$httpBackend.whenGET(API_URL + '/templatetypes/' + $stateParams.type).respond(200, $q.when(templateTypeList[0]));
				$httpBackend.whenGET(API_URL + '/templates?type=' + $stateParams.type).respond(200, $q.when(removedList));
				$httpBackend.whenDELETE(API_URL + '/templates/' + $stateParams.type).respond(200, $q.when({message: 'Template Deleted'}));
				
				templatesListCtrl.deleteTemplate(1);
				expect(templatesListCtrl.deleteActive).toEqual(1);

				$httpBackend.flush();
				
				expect(toaster.pop).toHaveBeenCalled();
				expect(TemplatesFactory.remove).toHaveBeenCalledWith(1);
				expect(templatesListCtrl.deleteActive).toEqual(0);
				expect(templatesListCtrl.templates.length).toEqual(2);
			});
		});
	});

	describe('templateEditCtrl', function() {
		var templateEditCtrl, $stateParams, $state;

		beforeEach(inject(function(_$stateParams_, _$state_) {
			$stateParams = _$stateParams_;
			$state = _$state_;
		}));

		beforeEach(function() {
			spyOn($state, 'go');
			
			templateEditCtrl = $controller('templateEditCtrl');

			templateEditCtrl.templateForm = {
				$setDirty: function() {}
			};

			spyOn(templateEditCtrl.templateForm, '$setDirty');
		});

		it('should be defined', function() {
			expect(templateEditCtrl).toBeDefined();
		});

		it('should add a new field', function() {
			templateEditCtrl.template = templatesList[0];
			templateEditCtrl.template.fields = [];

			templateEditCtrl.addField();
			
			expect(templateEditCtrl.templateForm.$setDirty).toHaveBeenCalled();
			expect(templateEditCtrl.template.fields.length).toEqual(1);
			expect(templateEditCtrl.template.fields[0].name).toEqual('');
		});

		it('should remove a field', function() {
			templateEditCtrl.template = templatesList[0];
			templateEditCtrl.template.fields = [];
			templateEditCtrl.template.fields.push({name: 'Server Name'});
			templateEditCtrl.template.fields.push({name: 'IP Address'});
			templateEditCtrl.template.fields.push({name: 'Gateway'});
			
			templateEditCtrl.removeField(1);

			expect(templateEditCtrl.templateForm.$setDirty).toHaveBeenCalled();
			expect(templateEditCtrl.template.fields.length).toEqual(2);
			expect(templateEditCtrl.template.fields[0].name).toEqual('Server Name');
			expect(templateEditCtrl.template.fields[1].name).toEqual('Gateway');
		});

		describe('Add new template', function() {
			beforeEach(function() {
				spyOn(TemplatesFactory, 'add').and.callThrough();
			});

			it('should add a new template', function() {
				var template = {
					name: 'Server',
					active: true
				};

				var response = {
					_id: 1,
					name: 'Server',
					active: true
				}

				expect(templateEditCtrl.editorOptions).toEqual({
					lineNumbers: true,
			        matchBrackets: true,
			        styleActiveLine: true,
			        theme:"ambiance"
				});

				expect(templateEditCtrl.nameExists).toEqual(false);
				expect(TemplateTypesFactory.get).toHaveBeenCalled();

				expect(templateEditCtrl.mode).toEqual('Add New');
				expect(templateEditCtrl.template).toEqual({fields: [{name: ''}]});
				
				$httpBackend.whenGET(API_URL + '/templatetypes').respond(200, $q.when(templateTypeList));
				$httpBackend.whenPOST(API_URL + '/templates', template).respond(200, $q.when(template));
				templateEditCtrl.template = template;
				templateEditCtrl.saveTemplate();
				$httpBackend.flush();

				expect(toaster.pop).toHaveBeenCalledWith({
					type: 'success',
					body: 'Template has been added',
					showCloseButton: true,
					timeout: 3000
				});
				expect($state.go).toHaveBeenCalledWith('app.templates.list.details', {type: $stateParams.type});
			});

			it('should try to add a new template with duplicate name', function() {
				var template = {
					name: 'Server',
					active: true
				};

				var response = {
					code: 100,
					message: 'Template Name Already Exists'
				}

				expect(templateEditCtrl.mode).toEqual('Add New');
				expect(templateEditCtrl.template).toEqual({fields: [{name: ''}]});
				expect(templateEditCtrl.nameExists).toEqual(false);

				$httpBackend.whenGET(API_URL + '/templatetypes').respond(200, $q.when(templateTypeList));
				$httpBackend.whenPOST(API_URL + '/templates', template).respond(409, response);
				templateEditCtrl.template = template;
				templateEditCtrl.saveTemplate();
				$httpBackend.flush();
				
				expect(templateEditCtrl.nameExists).toEqual(true);
			});

			it('should add a new field to the template', function() {
				expect(templateEditCtrl.template).toEqual({fields: [ {name: ''} ]});
				templateEditCtrl.addField();
				expect(templateEditCtrl.templateForm.$setDirty).toHaveBeenCalled();
				expect(templateEditCtrl.template).toEqual({fields: [ {name: ''}, {name: ''} ]});
			});

			describe('Supplying $stateParams.type', function() {
				beforeEach(function() {
					$stateParams.type = 1;

					templateEditCtrl = $controller('templateEditCtrl');
				});

				it('should select template type', function() {
					var templateType = templateTypeList[0];

					expect($stateParams.type).toEqual(1);

					$httpBackend.whenGET(API_URL + '/templatetypes').respond(200, $q.when(templateTypeList));
					$httpBackend.whenGET(API_URL + '/templatetypes/' + $stateParams.type).respond(200, $q.when(templateType));
					$httpBackend.flush();

					expect(templateEditCtrl.template.templateType).toEqual(templateType);
				});
			});
		});
		
		describe('Edit existing template', function() {
			beforeEach(inject(function(_$stateParams_) {
				$stateParams = _$stateParams_;
				$stateParams.id = 1;
				templateEditCtrl = $controller('templateEditCtrl');
			}));

			beforeEach(function() {
				spyOn(TemplatesFactory, 'getById').and.callThrough();
				spyOn(TemplatesFactory, 'save').and.callThrough();

				templateEditCtrl.templateForm = {
					$valid: true,
					$dirty: false,
					$setDirty: function() {}
				};

				spyOn(templateEditCtrl.templateForm, '$setDirty');
			});

			it('should edit a template', function() {
				var template = templatesList[0];

				var changed = {
					_id: 1,
					name: 'Template 99',
					active: true
				}
			
				expect(templateEditCtrl.editorOptions).toEqual({
					lineNumbers: true,
			        matchBrackets: true,
			        styleActiveLine: true,
			        theme:"ambiance"
				});

				expect(templateEditCtrl.nameExists).toEqual(false);
				expect(TemplateTypesFactory.get).toHaveBeenCalled();

				expect(templateEditCtrl.mode).toEqual('Edit');
				
				$httpBackend.whenGET(API_URL + '/templatetypes').respond(200, $q.when(templateTypeList));
				$httpBackend.whenGET(API_URL + '/templates/' + $stateParams.id).respond(200, $q.when(template));
				$httpBackend.flush();
				expect(templateEditCtrl.template).toEqual(template);

				$httpBackend.whenPUT(API_URL + '/templates/' + 1, changed).respond(200, $q.when(changed));
				templateEditCtrl.template = changed;
				templateEditCtrl.saveTemplate();
				$httpBackend.flush();

				expect(toaster.pop).toHaveBeenCalled();
				expect($state.go).toHaveBeenCalledWith('app.templates.list.details', { type: $stateParams.type });
			});

			it('should try to edit a template with duplicate name', function() {
				var template = templatesList[0];

				var response = {
					code: 100,
					message: 'Template Name Already Exists'
				}
				
				expect(templateEditCtrl.nameExists).toEqual(false);

				$httpBackend.whenGET(API_URL + '/templatetypes').respond(200, $q.when(templateTypeList));
				$httpBackend.whenGET(API_URL + '/templates/' + $stateParams.id).respond(200, $q.when(template));

				$httpBackend.whenPUT(API_URL + '/templates/' + $stateParams.id, template).respond(409, response);
				
				templateEditCtrl.template = template;
				templateEditCtrl.saveTemplate();
				
				$httpBackend.flush();
				
				expect(templateEditCtrl.nameExists).toEqual(true);
			});

			it('should add a new field to the template', function() {
				var template = templatesList[0];
				template.fields = [
					{name: 'Field1'},
					{name: 'Field2'}
				];

				var addedField = angular.copy(template);
				addedField.fields.push({name: ''});

				$httpBackend.whenGET(API_URL + '/templatetypes').respond(200, $q.when(templateTypeList));
				$httpBackend.whenGET(API_URL + '/templates/' + $stateParams.id).respond(200, $q.when(template));
				$httpBackend.flush();
				expect(templateEditCtrl.template).toEqual(template);

				templateEditCtrl.addField();
				expect(templateEditCtrl.templateForm.$setDirty).toHaveBeenCalled();
				expect(templateEditCtrl.template).toEqual(addedField);
			});
		});
	});
});