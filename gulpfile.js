'use strict';

var gulp = require('gulp');
var gulpNgConfig = require('gulp-ng-config');

var configureSetup  = {
  createModule: false,
  constants: {
  	API_URL: process.env.ITDOCS_API_URL || 'http://localhost:443/api'
  }
};

gulp.task('config', function() {
  gulp.src('api.json')
      .pipe(gulpNgConfig('itdocs', configureSetup))
      .pipe(gulp.dest('client/js')); 
});