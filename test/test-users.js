process.env.NODE_ENV = "test";

var chai = require("chai");
var chaiHttp = require("chai-http");

// Start JWT Requirements
var bcrypt = require("bcryptjs");
var authUser;
// End JWT Requirements

var User = require("../app/models/user");

var server = require("../server");

var should = chai.should();

var addedUser;

chai.use(chaiHttp);

describe("User", function() {
    
    User.collection.drop();
    
    beforeEach(function(done) {
        bcrypt.hash("password123", parseInt(process.env.ITDOCS_SALT), function(err, hash) {
            User.create({name: "Jane Samson", email: "jsamson@hotmail.com", password: hash, admin: true}, function(err, user){
                
                // Authenticate User, that way every test can use.
                chai.request(server)
                    .post("/api/authenticate")
                    .send({email: "jsamson@hotmail.com", password: "password123"})
                    .end(function(err, res) {
                        res.should.have.status(200);
                        res.should.be.a("object");
                        res.body.should.have.property("message");
                        res.body.message.should.equal("Authentication successful");
                        authUser = res.body;
                        
                        addedUser = res.body.user;
                        done();
                    });
            });
        });
    });
    
    afterEach(function(done) {
        User.collection.drop();
        done();
    });

    it("Should add a new User", function(done) {
        var newUser = {name: "New User 1", email: "dentist@gmail.com", password: "Password123456", active: true};

        chai.request(server)
            .post("/api/users")
            // Add next link only when JWT Used
            .set("authorization", 'Bearer ' + authUser.token)
            .send(newUser)
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.be.json;
                res.should.be.a("object");
                res.body.should.have.property("_id");
                res.body.should.have.property("name");
                res.body.name.should.equal("New User 1");
                res.body.should.have.property("email");
                res.body.email.should.equal("dentist@gmail.com");
                res.body.should.have.property("active");
                res.body.active.should.equal(true);
                res.body.should.have.property("admin");
                res.body.admin.should.equal(false);
                res.body.should.not.have.property("deleted");
                done();              
            });
    });

    it("Should try to add a user that already exists", function(done) {        
        chai.request(server)
            .post("/api/users")
            .set('authorization', 'Bearer ' + authUser.token)
            .send({email: "jsamson@hotmail.com"})
            .end(function(err, res) {
                res.should.have.status(409);
                
                chai.request(server)
                    .get("/api/users")
                    .set('authorization', 'Bearer ' + authUser.token)
                    .end(function(err, res) {
                        res.should.have.status(200);
                        res.should.be.json;
                        res.body.should.be.a("array");
                        res.body.length.should.equal(1);
                        done();
                    })
            });
    });
    
    it("Should get a listing of all Users", function(done) {
        chai.request(server)
            .get("/api/users")
            // Add next link only when JWT Used
            .set("authorization", 'Bearer ' + authUser.token)
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a("array");
                res.body.length.should.equal(1);
                res.body[0].should.have.property("_id");
                res.body[0].should.have.property("name");
                res.body[0].name.should.equal("Jane Samson");
                res.body[0].should.have.property("email");
                res.body[0].email.should.equal("jsamson@hotmail.com");
                res.body[0].should.have.property("active");
                res.body[0].active.should.equal(true);
                res.body[0].should.not.have.property("password");
                res.body[0].should.have.property("admin");
                res.body[0].admin.should.equal(true);
                res.body[0].should.not.have.property("deleted");
                done();
            });
    });
    
    it("Should get a single User", function(done) {
        chai.request(server)
            .get("/api/users/" + addedUser._id)
            .set("authorization", 'Bearer ' + authUser.token)
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.json;
                res.should.be.a("object");
                res.body.should.have.property("_id");
                res.body._id.should.equal(addedUser._id.toString());
                res.body.should.have.property("name");
                res.body.name.should.equal("Jane Samson");
                res.body.should.have.property("email");
                res.body.email.should.equal("jsamson@hotmail.com");
                res.body.should.have.property("active");
                res.body.active.should.equal(true);
                res.body.should.have.property("admin");
                res.body.admin.should.equal(true);
                res.body.should.not.have.property("password");
                res.body.should.not.have.property("deleted");
                done();
            });
    });
    
    it("Should update a single user without password", function(done) {
        chai.request(server)
            .put("/api/users/" + addedUser._id)
            .set("authorization", 'Bearer ' + authUser.token)
            // INSERT FIELDS TO UPDATE {description: test, active: false}
            .send({name: "Jan Sam", email: "jsam@gmail.com", active: false})
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.json;
                res.should.be.a("object");
                res.body._id.should.equal(addedUser._id.toString());
                res.body.should.have.property("name");
                res.body.name.should.equal("Jan Sam");
                res.body.should.have.property("email");
                res.body.email.should.equal("jsam@gmail.com");
                res.body.should.have.property("active");
                res.body.active.should.equal(false);
                res.body.should.have.property("admin");
                res.body.admin.should.equal(true);
                res.body.should.not.have.property("password");
                res.body.should.not.have.property("deleted");
                done();
            });
    });

    it("Should try to update a single user with an existing email", function(done) {
        User.create({email: 'blah@blah.com'}, function(err, user){
            chai.request(server)
                .put("/api/users/" + addedUser._id)
                .set('authorization', 'Bearer ' + authUser.token)
                .send({email: 'blah@blah.com'})
                .end(function(err, res) {
                    res.should.have.status(409);
                    res.should.json;
                    
                    done();
                });
        })
    });

    it("Should update a user and their password", function(done) {
        chai.request(server)
            .put("/api/users/" + addedUser._id)
            .set("authorization", 'Bearer ' + authUser.token)
            // INSERT FIELDS TO UPDATE {description: test, active: false}
            .send({name: "Jan Sam", email: "jsam@gmail.com", active: false, password: "change me", active: true})
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.json;
                res.should.be.a("object");
                res.body._id.should.equal(addedUser._id.toString());
                res.body.should.have.property("name");
                res.body.name.should.equal("Jan Sam");
                res.body.should.have.property("email");
                res.body.email.should.equal("jsam@gmail.com");
                res.body.should.have.property("active");
                res.body.active.should.equal(true);
                res.body.should.have.property("admin");
                res.body.admin.should.equal(true);
                res.body.should.not.have.property("password");
                res.body.should.not.have.property("deleted");

                // Try to login with new password to ensure it was changed.
                chai.request(server)
                    .post("/api/authenticate")
                    .send({email: "jsam@gmail.com", password: "change me"})
                    .end(function(err, res) {
                        res.should.have.status(200);
                        res.should.be.a("object");
                        res.body.should.have.property("message");
                        res.body.message.should.equal("Authentication successful");
                        res.body.should.have.property("user");
                        res.body.user.should.have.property("_id");
                        res.body.user._id.should.equal(addedUser._id.toString());
                        res.body.user.should.have.property("name");
                        res.body.user.name.should.equal("Jan Sam");
                        res.body.user.should.have.property("email");
                        res.body.user.email.should.equal("jsam@gmail.com");
                        done();
                    });
            });
    });

    it("Should delete a user", function(done) {
        User.create({name: "Test User", email: "test@test.com", password: "password123"}, function(err, user) {
            chai.request(server)
            .delete("/api/users/" + user._id)
            .set("authorization", 'Bearer ' + authUser.token)
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.json;
                res.should.be.a("object");
                
                chai.request(server)
                    .get("/api/users")
                    .set("authorization", 'Bearer ' + authUser.token)
                    .end(function(err, res) {
                        res.should.have.status(200);
                        res.should.json;
                        res.body.should.be.a("array");
                        res.body.length.should.equal(1);
                        res.body[0].should.have.property("_id");
                        res.body[0].should.have.property("name");
                        res.body[0].name.should.equal("Jane Samson");
                        res.body[0].should.have.property("email");
                        res.body[0].email.should.equal("jsamson@hotmail.com");
                        res.body[0].should.have.property("active");
                        res.body[0].active.should.equal(true);
                        res.body[0].should.not.have.property("password");
                        res.body[0].should.have.property("admin");
                        res.body[0].admin.should.equal(true);
                        res.body[0].should.not.have.property("deleted");
                        done();        
                    })
            });
        });
        
    });

    it("Should try and delete a user but they are set as for the organization", function(done) {
        chai.request(server)
            .delete("/api/users/" + addedUser._id)
            .set("authorization", 'Bearer ' + authUser.token)
            .end(function(err, res) {
                res.should.have.status(403);
                res.should.json;
                res.should.be.a("object");
                res.body.should.have.property("message");
                res.body.message.should.equal("Can not delete admin");
                
                chai.request(server)
                    .get("/api/users")
                    .set("authorization", 'Bearer ' + authUser.token)
                    .end(function(err, res) {
                        res.should.have.status(200);
                        res.should.json;
                        res.body.should.be.a("array");
                        res.body.length.should.equal(1);
                        res.body[0].should.have.property("_id");
                        res.body[0].should.have.property("name");
                        res.body[0].name.should.equal("Jane Samson");
                        res.body[0].should.have.property("email");
                        res.body[0].email.should.equal("jsamson@hotmail.com");
                        res.body[0].should.have.property("active");
                        res.body[0].active.should.equal(true);
                        res.body[0].should.not.have.property("password");
                        res.body[0].should.have.property("admin");
                        res.body[0].admin.should.equal(true);
                        res.body[0].should.not.have.property("deleted");
                        done();        
                    })
            });
    });
    
    it("Should change a users own password", function(done) {
        chai.request(server)
            .post('/api/users/changepassword')
            .set('authorization', 'Bearer ' + authUser.token)
            .send({currentpassword: 'password123', newpassword: '12345', repeatpassword: '12345' })
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.json;
                res.should.be.a("object");
                res.body.should.have.property('message');
                res.body.message.should.equal('Password has been changed');
                done();
            });
    });

    it("Should not change a users own password, current password wrong", function(done) {
        chai.request(server)
            .post('/api/users/changepassword')
            .set('authorization', 'Bearer ' + authUser.token)
            .send({currentpassword: 'wrongpassword', newpassword: '12345', repeatpassword: '12345' })
            .end(function(err, res) {
                res.should.have.status(400);
                res.should.json;
                res.should.be.a("object");
                res.body.should.have.property('message');
                res.body.message.should.equal('Current Password Incorrect');
                res.body.should.have.property('code');
                res.body.code.should.equal(102);
                done();
            });
    });

    it("Should not change a users own password, new password and repeat don't match", function(done) {
        chai.request(server)
            .post('/api/users/changepassword')
            .set('authorization', 'Bearer ' + authUser.token)
            .send({currentpassword: 'password123', newpassword: '12345', repeatpassword: '54321' })
            .end(function(err, res) {
                res.should.have.status(400);
                res.should.json;
                res.should.be.a("object");
                res.body.should.have.property('message');
                res.body.message.should.equal('New passwords do not match');
                res.body.should.have.property('code');
                res.body.code.should.equal(101);
                done();
            });
    });

    it("Should error when current password is not supplied", function(done) {
        chai.request(server)
            .post('/api/users/changepassword')
            .set('authorization', 'Bearer ' + authUser.token)
            .send({newpassword: '12345', repeatpassword: '12345' })
            .end(function(err, res) {
                res.should.have.status(400);
                res.should.json;
                res.should.be.a("object");
                res.body.should.have.property('message');
                res.body.message.should.equal('Current Password Incorrect');
                res.body.should.have.property('code');
                res.body.code.should.equal(102);
                done();
            });
    });

    it("Should error when new password is not supplied", function(done) {
        chai.request(server)
            .post('/api/users/changepassword')
            .set('authorization', 'Bearer ' + authUser.token)
            .send({currentpassword: 'password123', repeatpassword: '12345' })
            .end(function(err, res) {
                res.should.have.status(400);
                res.should.json;
                res.should.be.a("object");
                res.body.should.have.property('message');
                res.body.message.should.equal('New password is required');
                res.body.should.have.property('code');
                res.body.code.should.equal(103);
                done();
            });
    });

    it("Should error when repeat password is not supplied", function(done) {
        chai.request(server)
            .post('/api/users/changepassword')
            .set('authorization', 'Bearer ' + authUser.token)
            .send({currentpassword: 'password123', newpassword: '12345'})
            .end(function(err, res) {
                res.should.have.status(400);
                res.should.json;
                res.should.be.a("object");
                res.body.should.have.property('message');
                res.body.message.should.equal('Repeat password is required');
                res.body.should.have.property('code');
                res.body.code.should.equal(104);
                done();
            });
    });
});