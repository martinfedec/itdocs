process.env.NODE_ENV = "test";

var chai = require("chai");
var chaiHttp = require("chai-http");

// Start JWT Requirements
var bcrypt = require("bcryptjs");
var User = require("../app/models/user");
var authUser;
// End JWT Requirements

var TemplateType = require("../app/models/templateType");

var server = require("../server");

var should = chai.should();

var addedTemplateType;

chai.use(chaiHttp);

describe("TemplateType", function() {
    
    User.collection.drop();
    TemplateType.collection.drop();
    
    beforeEach(function(done) {
        bcrypt.hash("password123", 10, function(err, hash) {
            User.create({name: "Jane Samson", email: "jsamson@hotmail.com", password: hash}, function(err, user){
                // Authenticate User, that way every test can use.
                chai.request(server)
                    .post("/api/authenticate")
                    .send({email: "jsamson@hotmail.com", password: "password123"})
                    .end(function(err, res) {
                        res.should.have.status(200);
                        res.should.be.a("object");
                        res.body.should.have.property("message");
                        res.body.message.should.equal("Authentication successful");
                        authUser = res.body;
                        
                        // ADD FIELDS FOR THE MODEL {FIELD_NAME: FIELD_VALUE}
                        TemplateType.create({description: "Asset", icon: "fa-laptop"}, function(err, templatetype) {
                            addedTemplateType = templatetype;
                            done();
                        });        
                    });
            });
        });
    });
    
    afterEach(function(done) {
        User.collection.drop();
        TemplateType.collection.drop();
        done();
    });
    
    it("Should add a new TemplateType", function(done) {
        // ADD FIELDS FOR THE MODEL TO ADD {FIELD_NAME: FIELD_VALUE}
        var newTemplateType = {description: "Client", icon: "fa-user"};

        chai.request(server)
            .post("/api/templatetypes")
            .set("authorization", 'Bearer ' + authUser.token)
            .send(newTemplateType)
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.be.json;
                res.should.be.a("object");
                res.body.should.have.property("_id");
                res.body.should.have.property("description");
                res.body.description.should.equal("Client");
                res.body.should.have.property("icon");
                res.body.icon.should.equal("fa-user");
                res.body.should.have.property("active");
                res.body.active.should.equal(true);
                res.body.should.not.have.property("deleted");
                done();              
            });
    });
    
    it("Should get a listing of all TemplateType", function(done) {
        chai.request(server)
            .get("/api/templatetypes")
            .set("authorization", 'Bearer ' + authUser.token)
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a("array");
                res.body.length.should.equal(1);
                res.body[0].should.have.property("_id");
                res.body[0].should.have.property("description");
                res.body[0].description.should.equal("Asset");
                res.body[0].should.have.property("icon");
                res.body[0].icon.should.equal("fa-laptop");
                res.body[0].should.have.property("active");
                res.body[0].active.should.equal(true);
                res.body[0].should.not.have.property("deleted");
                done();
            });
    });
    
    it("Should get a single TemplateType", function(done) {
        chai.request(server)
            .get("/api/templatetypes/" + addedTemplateType._id)
            .set("authorization", 'Bearer ' + authUser.token)
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.json;
                res.should.be.a("object");
                res.body.should.have.property("_id");
                res.body._id.should.equal(addedTemplateType._id.toString());
                res.body.should.have.property("description");
                res.body.description.should.equal("Asset");
                res.body.should.have.property("icon");
                res.body.icon.should.equal("fa-laptop");
                res.body.should.have.property("active");
                res.body.active.should.equal(true);
                res.body.should.not.have.property("deleted");
                done();
            });
    });
    
    it("Should update a single TemplateType", function(done) {
        chai.request(server)
            .put("/api/templatetypes/" + addedTemplateType._id)
            .send({description: "Document", icon: "fa-file"})
            .set("authorization", 'Bearer ' + authUser.token)
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.json;
                res.should.be.a("object");
                res.body.should.have.property("description");
                res.body.description.should.equal("Document");
                res.body.should.have.property("icon");
                res.body.icon.should.equal("fa-file");
                res.body.should.not.have.property("deleted");
                done();
            });
    });

    it("Should delete a TemplateType", function(done) {
        chai.request(server)
            .delete("/api/templatetypes/" + addedTemplateType._id)
            .set("authorization", 'Bearer ' + authUser.token)
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.json;
                res.should.be.a("object");
                
                chai.request(server)
                    .get("/api/templatetypes")
                    .set("authorization", 'Bearer ' + authUser.token)
                    .end(function(err, res) {
                        res.should.have.status(200);
                        res.should.json;
                        res.body.should.be.a("array");
                        res.body.length.should.equal(0);
                        done();        
                    })
            });
    });
    
});