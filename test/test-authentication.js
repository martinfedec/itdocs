process.env.NODE_ENV = "test";

var chai = require("chai");
var chaiHttp = require("chai-http");

// Start JWT Requirements
var bcrypt = require("bcryptjs");
var authUser;
// End JWT Requirements

var User = require("../app/models/user");

var server = require("../server");

var should = chai.should();

var addedUser;

chai.use(chaiHttp);

describe("User", function() {
    
    User.collection.drop();
    
    beforeEach(function(done) {
        bcrypt.hash("password123", 10, function(err, hash) {
            User.create({name: "Jane Samson", email: "jsamson@hotmail.com", password: hash}, function(err, user){
                done();
            });
        });
    });
    
    afterEach(function(done) {
        User.collection.drop();
        done();
    });

    it("Should authenticate user successfully", function(done) {
        chai.request(server)
            .post("/api/authenticate")
            .send({email: "jsamson@hotmail.com", password: "password123"})
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.be.a("object");
                res.body.should.have.property("message");
                res.body.message.should.equal("Authentication successful");
                res.body.should.have.property("token");
                res.body.should.have.property("user");
                res.body.user.should.have.property("_id");
                res.body.user.should.have.property("name");
                res.body.user.name.should.equal("Jane Samson");
                res.body.user.should.have.property("email");
                res.body.user.email.should.equal("jsamson@hotmail.com");
                res.body.user.should.have.property("active");
                res.body.user.active.should.equal(true);
                res.body.user.should.not.have.property("password");
                done();
            });
    });

    it("Should try to authenticate a user but fails with incorrect password", function(done) {
        chai.request(server)
            .post("/api/authenticate")
            .send({email: "jsamson@hotmail.com", password: "Password123"})
            .end(function(err, res) {
                res.should.have.status(400);
                res.should.be.a("object");
                res.body.should.have.property("message");
                res.body.message.should.equal("Password Incorrect");
                res.body.should.not.have.property("token");
                res.body.should.not.have.property("user");
                done();
            });
    });

    it("Should ty to authenticate a user but fails because email does not exists", function(done) {
        chai.request(server)
            .post("/api/authenticate")
            .send({email: "jsamson", password: "Password123"})
            .end(function(err, res) {
                res.should.have.status(400);
                res.should.be.a("object");
                res.body.should.have.property("message");
                res.body.message.should.equal("User Not Found");
                res.body.should.not.have.property("token");
                res.body.should.not.have.property("user");
                done();
            });
    });

    it("Should try to authenticate a user but fails because the user is inactive", function(done) {
        User.findOne({email: "jsamson@hotmail.com"}, function(err, user) {
            user.active = false;
            user.save(function(err) {
                chai.request(server)
                    .post("/api/authenticate")
                    .send({email: "jsamson@hotmail.com", password: "Password123"})
                    .end(function(err, res) {
                        res.should.have.status(400);
                        res.should.be.a("object");
                        res.body.should.have.property("message");
                        res.body.message.should.equal("Inactive User");
                        res.body.should.not.have.property("token");
                        res.body.should.not.have.property("user");
                        done();
                    });
            });
        });
    });
});