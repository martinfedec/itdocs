process.env.NODE_ENV = "test";

var chai = require("chai");
var chaiHttp = require("chai-http");

// Start JWT Requirements
var bcrypt = require("bcryptjs");
var User = require("../app/models/user");
var authUser;
// End JWT Requirements

var TemplateType = require("../app/models/templateType");
var Template = require("../app/models/template");

var server = require("../server");

var should = chai.should();

var addedTemplate;
var addedTemplateType;

chai.use(chaiHttp);

describe("Template", function() {
    
    User.collection.drop();
    TemplateType.collection.drop();
    Template.collection.drop();
    
    beforeEach(function(done) {

        bcrypt.hash("password123", 10, function(err, hash) {
            User.create({name: "Jane Samson", email: "jsamson@hotmail.com", password: hash}, function(err, user){
                // Authenticate User, that way every test can use.
                chai.request(server)
                    .post("/api/authenticate")
                    .send({email: "jsamson@hotmail.com", password: "password123"})
                    .end(function(err, res) {
                        res.should.have.status(200);
                        res.should.be.a("object");
                        res.body.should.have.property("message");
                        res.body.message.should.equal("Authentication successful");
                        authUser = res.body;

                        TemplateType.create({description: "Asset"}, function(err, templateType) {
                            addedTemplateType = templateType;  

                            Template.create({
                                name: "Server",
                                description: "Base Server Template",
                                templateType: addedTemplateType._id,
                                fields: [{name: "Server Name"}, {name: "IP Address"}, {name: "Subnet"}],
                                script: "Test Script Goes Here"
                            }, function(err, template) {
                                addedTemplate = template;
                                done();
                            });
                        });
                    });
            });
        });
    });
    
    afterEach(function(done) {
        User.collection.drop();
        TemplateType.collection.drop();
        Template.collection.drop();
        done();
    });
    
    it("Should add a new Template", function(done) {
        // ADD FIELDS FOR THE MODEL TO ADD {FIELD_NAME: FIELD_VALUE}
        var newTemplate = {
            name: "Document",
            description: "Base Documentation Template",
            templateType: addedTemplateType._id,
            fields: [{name: "Audience"}, {name: "content"}, {name: "meeting by"}],
            script: "Test Script Goes Here"
        };

        chai.request(server)
            .post("/api/templates")
            .set("authorization", 'Bearer ' + authUser.token)
            .send(newTemplate)
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.be.json;
                res.should.be.a("object");
                res.body.should.have.property("_id");
                res.body.should.have.property("name");
                res.body.name.should.equal("Document");
                res.body.should.have.property("description");
                res.body.description.should.equal("Base Documentation Template");
                res.body.should.have.property("templateType");
                res.body.templateType.should.equal(addedTemplateType._id.toString());
                res.body.should.have.property("fields");
                res.body.fields.should.be.a("array");
                res.body.fields[0].should.have.property("name");
                res.body.fields[0].name.should.equal("Audience");
                res.body.fields[1].should.have.property("name");
                res.body.fields[1].name.should.equal("content");
                res.body.fields[2].should.have.property("name");
                res.body.fields[2].name.should.equal("meeting by");
                res.body.should.have.property("script");
                res.body.script.should.equal("Test Script Goes Here");
                res.body.should.not.have.property("deleted");
                done();              
            });
    });

    it("Should try to add a template that already exists", function(done) {        
        chai.request(server)
            .post("/api/templates")
            .set('authorization', 'Bearer ' + authUser.token)
            .send({name: "Server"})
            .end(function(err, res) {
                res.should.have.status(409);
                
                chai.request(server)
                    .get("/api/templates")
                    .set('authorization', 'Bearer ' + authUser.token)
                    .end(function(err, res) {
                        res.should.have.status(200);
                        res.should.be.json;
                        res.body.should.be.a("array");
                        res.body.length.should.equal(1);
                        done();
                    })
            });
    });

    it("Should get a listing of all Template", function(done) {
        chai.request(server)
            .get("/api/templates")
            .set("authorization", 'Bearer ' + authUser.token)
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a("array");
                res.body.length.should.equal(1);
                res.body[0].should.have.property("_id");
                res.body[0]._id.should.equal(addedTemplate._id.toString());
                res.body[0].should.have.property("name");
                res.body[0].name.should.equal("Server");
                res.body[0].should.have.property("description");
                res.body[0].description.should.equal("Base Server Template");
                res.body[0].should.have.property("templateType");
                res.body[0].templateType.should.be.a("object");
                res.body[0].templateType.should.have.property("_id");
                res.body[0].templateType._id.should.equal(addedTemplateType._id.toString());
                res.body[0].templateType.should.have.property("description");
                res.body[0].templateType.description.should.equal(addedTemplateType.description);
                res.body[0].should.have.property("fields");
                res.body[0].fields.should.be.a("array");
                res.body[0].fields[0].should.have.property("name");
                res.body[0].fields[0].name.should.equal("Server Name");
                res.body[0].fields[1].should.have.property("name");
                res.body[0].fields[1].name.should.equal("IP Address");
                res.body[0].fields[2].should.have.property("name");
                res.body[0].fields[2].name.should.equal("Subnet");
                res.body[0].should.have.property("script");
                res.body[0].script.should.equal("Test Script Goes Here");
                res.body[0].should.not.have.property("deleted");
                done();
            });
    });

    it("Should get a listing of all Template by Template Type", function(done) {
        chai.request(server)
            .get("/api/templates?type=" + addedTemplateType._id)
            .set("authorization", 'Bearer ' + authUser.token)
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a("array");
                res.body.length.should.equal(1);
                res.body[0].should.have.property("_id");
                res.body[0]._id.should.equal(addedTemplate._id.toString());
                res.body[0].should.have.property("name");
                res.body[0].name.should.equal("Server");
                res.body[0].should.have.property("description");
                res.body[0].description.should.equal("Base Server Template");
                res.body[0].should.have.property("templateType");
                res.body[0].templateType.should.be.a("object");
                res.body[0].templateType.should.have.property("_id");
                res.body[0].templateType._id.should.equal(addedTemplateType._id.toString());
                res.body[0].templateType.should.have.property("description");
                res.body[0].templateType.description.should.equal(addedTemplateType.description);
                res.body[0].should.have.property("fields");
                res.body[0].fields.should.be.a("array");
                res.body[0].fields[0].should.have.property("name");
                res.body[0].fields[0].name.should.equal("Server Name");
                res.body[0].fields[1].should.have.property("name");
                res.body[0].fields[1].name.should.equal("IP Address");
                res.body[0].fields[2].should.have.property("name");
                res.body[0].fields[2].name.should.equal("Subnet");
                res.body[0].should.have.property("script");
                res.body[0].script.should.equal("Test Script Goes Here");
                res.body[0].should.not.have.property("deleted");
                done();
            });
    });
    
    it("Should get a single Template", function(done) {
        chai.request(server)
            .get("/api/templates/" + addedTemplate._id)
            .set("authorization", 'Bearer ' + authUser.token)
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.json;
                res.should.be.a("object");
                res.body.should.have.property("_id");
                res.body._id.should.equal(addedTemplate._id.toString());
                res.body.should.have.property("name");
                res.body.name.should.equal("Server");
                res.body.should.have.property("description");
                res.body.description.should.equal("Base Server Template");
                res.body.should.have.property("templateType");
                res.body.templateType.should.be.a("object");
                res.body.templateType.should.have.property("_id");
                res.body.templateType._id.should.equal(addedTemplateType._id.toString());
                res.body.templateType.should.have.property("description");
                res.body.templateType.description.should.equal(addedTemplateType.description);
                res.body.should.have.property("fields");
                res.body.fields.should.be.a("array");
                res.body.fields[0].should.have.property("name");
                res.body.fields[0].name.should.equal("Server Name");
                res.body.fields[1].should.have.property("name");
                res.body.fields[1].name.should.equal("IP Address");
                res.body.fields[2].should.have.property("name");
                res.body.fields[2].name.should.equal("Subnet");
                res.body.should.have.property("script");
                res.body.script.should.equal("Test Script Goes Here");
                res.body.should.not.have.property("deleted");
                done();
            });
    });
    
    it("Should update a single Template", function(done) {
        chai.request(server)
            .put("/api/templates/" + addedTemplate._id)
            .set("authorization", 'Bearer ' + authUser.token)
            // INSERT FIELDS TO UPDATE {description: test, active: false}
            .send({
                name: "Server Template",
                description: "Version 2 of Server",
                templateType: null,
                fields: [{name: "Server Naming"}, {name: "Field Number 2"}],
                script: "Test Script Goes Here"
            })
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.json;
                res.should.be.a("object");
                res.body.should.have.property("_id");
                res.body._id.should.equal(addedTemplate._id.toString());
                res.body.should.have.property("name");
                res.body.name.should.equal("Server Template");
                res.body.should.have.property("description");
                res.body.description.should.equal("Version 2 of Server");
                res.body.should.have.property("templateType");
                chai.expect(res.body.templateType).to.not.exist; // Just because I can't seem to check for null with should.
                res.body.should.have.property("fields");
                res.body.fields.should.be.a("array");
                res.body.fields[0].should.have.property("name");
                res.body.fields[0].name.should.equal("Server Naming");
                res.body.fields[1].should.have.property("name");
                res.body.fields[1].name.should.equal("Field Number 2");
                res.body.should.have.property("script");
                res.body.script.should.equal("Test Script Goes Here");
                res.body.should.not.have.property("deleted");
                done();
            });
    });

    it("Should try to update a single template with an existing name", function(done) {
        Template.create({name: 'Server Test Asset'}, function(err, template){
            chai.request(server)
                .put("/api/templates/" + addedTemplate._id)
                .set('authorization', 'Bearer ' + authUser.token)
                .send({name: 'Server Test Asset'})
                .end(function(err, res) {
                    res.should.have.status(409);
                    res.should.json;
                    
                    done();
                });
        })
    });

    it("Should delete a Template", function(done) {
        chai.request(server)
            .delete("/api/templates/" + addedTemplate._id)
            .set("authorization", 'Bearer ' + authUser.token)
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.json;
                res.should.be.a("object");
                
                chai.request(server)
                    .get("/api/templates")
                    .set("authorization", 'Bearer ' + authUser.token)
                    .end(function(err, res) {
                        res.should.have.status(200);
                        res.should.json;
                        res.body.should.be.a("array");
                        res.body.length.should.equal(0);
                        done();        
                    })
            });
    });
    
});