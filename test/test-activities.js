process.env.NODE_ENV = "test";

var chai = require("chai");
var chaiHttp = require("chai-http");

// Start JWT Requirements
var bcrypt = require("bcryptjs");
var User = require("../app/models/user");
var authUser;
// End JWT Requirements

var Activity = require("../app/models/activity");

var server = require("../server");

var should = chai.should();

var addedActivity;

chai.use(chaiHttp);

describe("Activity", function() {
    
    User.collection.drop();
    Activity.collection.drop();
    
    beforeEach(function(done) {
        bcrypt.hash("password123", 10, function(err, hash) {
            User.create({name: "Jane Samson", email: "jsamson@hotmail.com", password: hash}, function(err, user){
                // Authenticate User, that way every test can use.
                chai.request(server)
                    .post("/api/authenticate")
                    .send({email: "jsamson@hotmail.com", password: "password123"})
                    .end(function(err, res) {
                        res.should.have.status(200);
                        res.should.be.a("object");
                        res.body.should.have.property("message");
                        res.body.message.should.equal("Authentication successful");
                        authUser = res.body;
                        
                        // ADD FIELDS FOR THE MODEL {FIELD_NAME: FIELD_VALUE}
                        Activity.create({details: "Comment # 1", type: "COMMENT"}, function(err, activity) {
                            addedActivity = activity;
                            done();
                        });
                    });
            });
        });
    });
    
    afterEach(function(done) {
        User.collection.drop();
        Activity.collection.drop();
        done();
    });
    
    it("Should add a new Activity", function(done) {
        // ADD FIELDS FOR THE MODEL TO ADD {FIELD_NAME: FIELD_VALUE}
        var newActivity = {details: "Comment # 2", type: "COMMENT"};

        chai.request(server)
            .post("/api/activities")
            .set("authorization", 'Bearer ' + authUser.token)
            .send(newActivity)
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.be.json;
                res.should.be.a("object");
                res.body.should.have.property("_id");
                res.body.should.have.property("details");
                res.body.details.should.equal("Comment # 2");
                res.body.should.have.property("type");
                res.body.type.should.equal("COMMENT");
                res.body.should.not.have.property("deleted");
                done();              
            });
    });
    
    it("Should get a listing of all Activities no type supplied", function(done) {
        chai.request(server)
            .get("/api/activities")
            .set("authorization", 'Bearer ' + authUser.token)
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a("array");
                res.body.length.should.equal(1);
                res.body[0].should.have.property("_id");
                res.body[0].should.have.property("details");
                res.body[0].details.should.equal("Comment # 1");
                res.body[0].should.have.property("type");
                res.body[0].type.should.equal("COMMENT");
                res.body[0].should.not.have.property("deleted");
                done();
            });
    });

    it("Should get a listing of all Activities by COMMENT type", function(done) {
        Activity.create({details: "Comment # 1", type: "LABEL"}, function(err, activity) {
            chai.request(server)
                .get("/api/activities?type=COMMENT")
                .set("authorization", 'Bearer ' + authUser.token)
                .end(function(err, res) {
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.be.a("array");
                    res.body.length.should.equal(1);
                    res.body[0].should.have.property("_id");
                    res.body[0].should.have.property("details");
                    res.body[0].details.should.equal("Comment # 1");
                    res.body[0].should.have.property("type");
                    res.body[0].type.should.equal("COMMENT");
                    res.body[0].should.not.have.property("deleted");
                    done();
                });
        });
    });
    
    it("Should get a single Activity", function(done) {
        chai.request(server)
            .get("/api/activities/" + addedActivity._id)
            .set("authorization", 'Bearer ' + authUser.token)
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.json;
                res.should.be.a("object");
                res.body.should.have.property("_id");
                res.body._id.should.equal(addedActivity._id.toString());
                res.body.should.have.property("details");
                res.body.details.should.equal("Comment # 1");
                res.body.should.have.property("type");
                res.body.type.should.equal("COMMENT");
                res.body.should.not.have.property("deleted");
                done();
            });
    });
    
    it("Should update a single Activity", function(done) {
        chai.request(server)
            .put("/api/activities/" + addedActivity._id)
            .send({details: "EDITED COMMENT", type: "COMMENT"})
            .set("authorization", 'Bearer ' + authUser.token)
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.json;
                res.should.be.a("object");
                res.body.should.have.property("details");
                res.body.details.should.equal("EDITED COMMENT");
                res.body.should.have.property("type");
                res.body.type.should.equal("COMMENT");
                res.body.should.not.have.property("deleted");
                done();
            });
    });

    it("Should delete a Activity", function(done) {
        chai.request(server)
            .delete("/api/activities/" + addedActivity._id)
            .set("authorization", 'Bearer ' + authUser.token)
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.json;
                res.should.be.a("object");
                
                chai.request(server)
                    .get("/api/activities")
                    .set("authorization", 'Bearer ' + authUser.token)
                    .end(function(err, res) {
                        res.should.have.status(200);
                        res.should.json;
                        res.body.should.be.a("array");
                        res.body.length.should.equal(0);
                        done();        
                    })
            });
    });
    
});