process.env.NODE_ENV = "test";

var chai = require("chai");
var chaiHttp = require("chai-http");

// Start JWT Requirements
var bcrypt = require("bcryptjs");
var User = require("../app/models/user");
var authUser;
// End JWT Requirements

var Organization = require("../app/models/organization");

var server = require("../server");

var should = chai.should();

var addedOrganization;

chai.use(chaiHttp);

describe("Organization", function() {
    
    User.collection.drop();
    Organization.collection.drop();
    
    beforeEach(function(done) {
         bcrypt.hash("password123", 10, function(err, hash) {
            User.create({name: "Jane Samson", email: "jsamson@hotmail.com", password: hash}, function(err, user){
                // Authenticate User, that way every test can use.
                chai.request(server)
                    .post("/api/authenticate")
                    .send({email: "jsamson@hotmail.com", password: "password123"})
                    .end(function(err, res) {
                        res.should.have.status(200);
                        res.should.be.a("object");
                        res.body.should.have.property("message");
                        res.body.message.should.equal("Authentication successful");
                        authUser = res.body;

                         // ADD FIELDS FOR THE MODEL {FIELD_NAME: FIELD_VALUE}
                        Organization.create({name: 'Organization 1', shortname: 'Org1', website: 'www.org.com', description: 'Description about the company here.', admin: user._id}, function(err, organization) {
                            addedOrganization = organization;
                            done();
                        });
                    });
            });
        });
    });
    
    afterEach(function(done) {
        User.collection.drop();
        Organization.collection.drop();
        done();
    });
    
    it("Should get a single Organization", function(done) {
        chai.request(server)
            .get("/api/organization")
            .set("authorization", 'Bearer ' + authUser.token)
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.json;
                res.should.be.a("object");
                res.body.should.have.property("_id");
                res.body._id.should.equal(addedOrganization._id.toString());
                res.body.should.have.property("name");
                res.body.name.should.equal("Organization 1")
                res.body.should.have.property("shortname");
                res.body.shortname.should.equal("Org1");
                res.body.should.have.property("website");
                res.body.website.should.equal("www.org.com");
                res.body.should.have.property("description");
                res.body.should.have.property("admin");
                res.body.admin.should.have.property("_id");
                res.body.admin._id.should.equal(authUser.user._id);
                res.body.admin.should.have.property("name");
                res.body.admin.name.should.equal(authUser.user.name);
                res.body.admin.should.have.property("email");
                res.body.admin.email.should.equal(authUser.user.email);
                res.body.description.should.equal("Description about the company here.");
                done();
            });
    });
    
    it("Should update a single Organization", function(done) {
        chai.request(server)
            .put("/api/organization")
            .set("authorization", 'Bearer ' + authUser.token)
            .send({name: 'Organization 2', shortname: 'Org2', website: 'www.org2.com', description: 'Description about the company here. More Blah Blah', admin: authUser.user._id})
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.json;
                res.should.be.a("object");
                res.body.should.have.property("_id");
                res.body._id.should.equal(addedOrganization._id.toString());
                res.body.should.have.property("name");
                res.body.name.should.equal("Organization 2")
                res.body.should.have.property("shortname");
                res.body.shortname.should.equal("Org2");
                res.body.should.have.property("website");
                res.body.website.should.equal("www.org2.com");
                res.body.should.have.property("description");
                res.body.should.have.property("admin");
                res.body.admin.should.equal(authUser.user._id);
                res.body.description.should.equal("Description about the company here. More Blah Blah");
                done();
            });
    });
    
    describe("Locations", function() {
        var addedLocation;

        beforeEach(function(done) {
            chai.request(server)
                .post("/api/organization/locations")
                .set('authorization', 'Bearer ' + authUser.token)
                .send({name: "Head Office", address1: "123 Google Ave", address2: "Building 52", city: "Mountain View", province: "California", country: "USA", postalcode: "90210", phonenumber: "123-123-1234", faxnumber: "987-654-3210", email: "office1@company.com", active: true})
                .end(function(err, res) {
                    res.should.have.status(200)
                    res.should.json;
                    res.body.should.be.a("object");
                    res.body.should.have.property("_id");
                    res.body.should.have.property("name");
                    res.body.name.should.equal("Head Office");
                    res.body.should.have.property("address1");
                    res.body.address1.should.equal("123 Google Ave");
                    res.body.should.have.property("address2");
                    res.body.address2.should.equal("Building 52");
                    res.body.should.have.property("city");
                    res.body.city.should.equal("Mountain View");
                    res.body.should.have.property("province");
                    res.body.province.should.equal("California");
                    res.body.should.have.property("country");
                    res.body.country.should.equal("USA");
                    res.body.should.have.property("postalcode");
                    res.body.postalcode.should.equal("90210");
                    res.body.should.have.property("phonenumber");
                    res.body.phonenumber.should.equal("123-123-1234");
                    res.body.should.have.property("faxnumber");
                    res.body.faxnumber.should.equal("987-654-3210");
                    res.body.should.have.property("email");
                    res.body.email.should.equal("office1@company.com");
                    res.body.should.have.property("active");
                    res.body.active.should.equal(true);
                    addedLocation = res.body;
                    done();                
                });
        });

        afterEach(function(done) {
            done();
        });

        it("Should add new location", function(done) {
            chai.request(server)
                .post("/api/organization/locations")
                .set('authorization', 'Bearer ' + authUser.token)
                .send({name: "Head Office 2", address1: "123 Google Ave", address2: "Building 52", city: "Mountain View", province: "California", country: "USA", postalcode: "90210", phonenumber: "123-123-1234", faxnumber: "987-654-3210", email: "office1@company.com", active: true})
                .end(function(err, res) {
                    res.should.have.status(200)
                    res.should.json;
                    res.body.should.be.a("object");
                    res.body.should.have.property("_id");
                    res.body.should.have.property("name");
                    res.body.name.should.equal("Head Office 2");
                    res.body.should.have.property("address1");
                    res.body.address1.should.equal("123 Google Ave");
                    res.body.should.have.property("address2");
                    res.body.address2.should.equal("Building 52");
                    res.body.should.have.property("city");
                    res.body.city.should.equal("Mountain View");
                    res.body.should.have.property("province");
                    res.body.province.should.equal("California");
                    res.body.should.have.property("country");
                    res.body.country.should.equal("USA");
                    res.body.should.have.property("postalcode");
                    res.body.postalcode.should.equal("90210");
                    res.body.should.have.property("phonenumber");
                    res.body.phonenumber.should.equal("123-123-1234");
                    res.body.should.have.property("faxnumber");
                    res.body.faxnumber.should.equal("987-654-3210");
                    res.body.should.have.property("email");
                    res.body.email.should.equal("office1@company.com");
                    res.body.should.have.property("active");
                    res.body.active.should.equal(true);
                    done();                
                });
        });

        it("Should try to add a new location with a duplicate name", function(done) {
            chai.request(server)
                .post("/api/organization/locations")
                .set('authorization', 'Bearer ' + authUser.token)
                .send({name: "Head Office"})
                .end(function(err, res) {
                    res.should.have.status(409);
                    
                    chai.request(server)
                        .get("/api/organization/locations")
                        .set('authorization', 'Bearer ' + authUser.token)
                        .end(function(err, res) {
                            res.should.have.status(200);
                            res.should.be.json;
                            res.body.should.be.a("array");
                            res.body.length.should.equal(1);
                            done();
                        })
                });
        });

        it("Should get all locations", function(done) {
            chai.request(server)
                .get("/api/organization/locations")
                .set('authorization', 'Bearer ' + authUser.token)
                .end(function(err, res) {
                    res.should.have.status(200);
                    res.should.json;
                    res.body.should.be.a("array");
                    res.body.length.should.equal(1);
                    res.body[0].should.have.property("_id");
                    res.body[0].should.have.property("name");
                    res.body[0].name.should.equal("Head Office");
                    res.body[0].should.have.property("address1");
                    res.body[0].address1.should.equal("123 Google Ave");
                    res.body[0].should.have.property("address2");
                    res.body[0].address2.should.equal("Building 52");
                    res.body[0].should.have.property("city");
                    res.body[0].city.should.equal("Mountain View");
                    res.body[0].should.have.property("province");
                    res.body[0].province.should.equal("California");
                    res.body[0].should.have.property("country");
                    res.body[0].country.should.equal("USA");
                    res.body[0].should.have.property("postalcode");
                    res.body[0].postalcode.should.equal("90210");
                    res.body[0].should.have.property("phonenumber");
                    res.body[0].phonenumber.should.equal("123-123-1234");
                    res.body[0].should.have.property("faxnumber");
                    res.body[0].faxnumber.should.equal("987-654-3210");
                    res.body[0].should.have.property("email");
                    res.body[0].email.should.equal("office1@company.com");
                    res.body[0].should.have.property("active");
                    res.body[0].active.should.equal(true);
                    done();
                });
        });

        it("Should get specified location", function(done) {
            chai.request(server)
                .get("/api/organization/locations/" + addedLocation._id)
                .set('authorization', 'Bearer ' + authUser.token)
                .end(function(err, res) {
                    res.should.have.status(200);
                    res.should.json;
                    res.body.should.be.a("object");
                    res.body.should.have.property("_id");
                    res.body.should.have.property("name");
                    res.body.name.should.equal("Head Office");
                    res.body.should.have.property("address1");
                    res.body.address1.should.equal("123 Google Ave");
                    res.body.should.have.property("address2");
                    res.body.address2.should.equal("Building 52");
                    res.body.should.have.property("city");
                    res.body.city.should.equal("Mountain View");
                    res.body.should.have.property("province");
                    res.body.province.should.equal("California");
                    res.body.should.have.property("country");
                    res.body.country.should.equal("USA");
                    res.body.should.have.property("postalcode");
                    res.body.postalcode.should.equal("90210");
                    res.body.should.have.property("phonenumber");
                    res.body.phonenumber.should.equal("123-123-1234");
                    res.body.should.have.property("faxnumber");
                    res.body.faxnumber.should.equal("987-654-3210");
                    res.body.should.have.property("email");
                    res.body.email.should.equal("office1@company.com");
                    res.body.should.have.property("active");
                    res.body.active.should.equal(true);
                    done();
                });
        });

        it("Should update a location", function(done) {
            chai.request(server)
                .put("/api/organization/locations/" + addedLocation._id)
                .set('authorization', 'Bearer ' + authUser.token)
                .send({name: "New Head Office", address1: "321 google ave.", address2: "Building 2", city: "Mountain View", province: "California", country: "USA", postalcode:"90210", phonenumber: "123-123-1234", faxnumber: "987-654-3210", email: "office1@company.com", active: false})
                .end(function(err, res) {
                    res.should.have.status(200);
                    res.should.json;
                    res.body.should.be.a("object");
                    res.body.should.have.property("_id");
                    res.body.should.have.property("name");
                    res.body.name.should.equal("New Head Office");
                    res.body.should.have.property("address1");
                    res.body.address1.should.equal("321 google ave.");
                    res.body.should.have.property("address2");
                    res.body.address2.should.equal("Building 2");
                    res.body.should.have.property("city");
                    res.body.city.should.equal("Mountain View");
                    res.body.should.have.property("province");
                    res.body.province.should.equal("California");
                    res.body.should.have.property("country");
                    res.body.country.should.equal("USA");
                    res.body.should.have.property("postalcode");
                    res.body.postalcode.should.equal("90210");
                    res.body.should.have.property("phonenumber");
                    res.body.phonenumber.should.equal("123-123-1234");
                    res.body.should.have.property("faxnumber");
                    res.body.faxnumber.should.equal("987-654-3210");
                    res.body.should.have.property("email");
                    res.body.email.should.equal("office1@company.com");
                    res.body.should.have.property("active");
                    res.body.active.should.equal(false);
                    done();
                });
        });

        it("Should try to update a new location with a duplicate name", function(done) {
            chai.request(server)
                .post("/api/organization/locations")
                .set('authorization', 'Bearer ' + authUser.token)
                .send({name: "Main Office"})
                .end(function(err, res) {
                    chai.request(server)
                    .put("/api/organization/locations/" + addedLocation._id)
                    .set('authorization', 'Bearer ' + authUser.token)
                    .send({name: 'Main Office'})
                    .end(function(err, res) {
                        res.should.have.status(409);
                        res.should.json;
                        done();
                    });
                });
        });

        it("Should delete a location", function(done) {
            chai.request(server)
                .delete("/api/organization/locations/" + addedLocation._id)
                .set('authorization', 'Bearer ' + authUser.token)
                .end(function(err, res) {
                    res.should.have.status(200);
                    res.should.json;
                    res.should.be.a("object");
                    
                    chai.request(server)
                        .get("/api/organization")
                        .set('authorization', 'Bearer ' + authUser.token)
                        .end(function(err, res) {
                            res.should.have.status(200);
                            res.should.json;
                            res.body.locations.should.be.a("array");
                            res.body.locations.length.should.equal(0);
                            done();        
                        })
                });
        });

        it("Should fail to delete a location if it is set as primary", function(done) {
            Organization.findById(addedOrganization._id, function(err, organization) {
                organization.primarylocation = addedLocation._id;
                organization.save(function(err) {
                    chai.request(server)
                        .delete("/api/organization/locations/" + addedLocation._id)
                        .set('authorization', 'Bearer ' + authUser.token)
                        .end(function(err, res) {
                            res.should.have.status(409);
                            res.should.json;
                            res.should.be.a("object");
                            chai.request(server)
                                .get("/api/organization")
                                .set('authorization', 'Bearer ' + authUser.token)
                                .end(function(err, res) {
                                    res.should.have.status(200);
                                    res.should.json;
                                    res.body.locations.should.be.a("array");
                                    res.body.locations.length.should.equal(1);
                                    done();        
                                })
                        });
                });
            });
        });
    });
    
});

