process.env.NODE_ENV = "test";

var chai = require("chai");
var chaiHttp = require("chai-http");

// Start JWT Requirements
var bcrypt = require("bcryptjs");
var User = require("../app/models/user");
var authUser;
// End JWT Requirements

var Client = require("../app/models/client");

var TemplateType = require('../app/models/templateType');
var Template = require('../app/models/template');
var Asset = require('../app/models/asset');

var server = require("../server");

var should = chai.should();

var addedClient;

chai.use(chaiHttp);

describe("Client", function() {
    
    User.collection.drop();
    Client.collection.drop();
    
    beforeEach(function(done) {
        bcrypt.hash("password123", 10, function(err, hash) {

            User.create({name: "Jane Samson", email: "jsamson@hotmail.com", password: hash}, function(err, user){
                
                // Authenticate User, that way every test can use.
                chai.request(server)
                    .post("/api/authenticate")
                    .send({email: "jsamson@hotmail.com", password: "password123"})
                    .end(function(err, res) {
                        res.should.have.status(200);
                        res.should.be.a("object");
                        res.body.should.have.property("message");
                        res.body.message.should.equal("Authentication successful");
                        authUser = res.body;

                        // Create atleast one client in the DB.
                        Client.create({name: "Lawyer's Office", website: "www.lawyer.com", businesshours: "M-F 9-5", description: "Best lawyer and isn't a rip off", primarylocation: "1", createdby: authUser.user._id, lastupdatedby: authUser.user._id}, function(err, client) {
                            addedClient = client;
                            done();    
                        });
                    });
            });
        });
    });
    
    afterEach(function(done) {
        User.collection.drop();
        Client.collection.drop();
        done();
    });
    
    it("Should add a new Client", function(done) {
        var newClient = {name: "Lawyer's Office #2", website: "www.lawyer2.com", businesshours: "M-F noon-5", description: "Crappy lawyer and cheap!", primarylocation: "32"};

        chai.request(server)
            .post("/api/clients")
            // Add next link only when JWT Used
            .set("authorization", 'Bearer ' + authUser.token)
            .send(newClient)
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.be.json;
                res.should.be.a("object");
                res.body.should.have.property("_id");
                res.body.should.have.property("name");
                res.body.name.should.equal("Lawyer's Office #2");
                res.body.should.have.property("website");
                res.body.website.should.equal("www.lawyer2.com");
                res.body.should.have.property("businesshours");
                res.body.businesshours.should.equal("M-F noon-5");
                res.body.should.have.property("description");
                res.body.description.should.equal("Crappy lawyer and cheap!");
                res.body.should.have.property("primarylocation");
                res.body.primarylocation.should.equal("32");
                res.body.should.have.property("active");
                res.body.active.should.equal(true);
                res.body.should.not.have.property("deleted");
                    
                res.body.should.have.property("createdat");
                res.body.createdat.should.exist;
                res.body.should.have.property("lastupdatedat");
                res.body.lastupdatedat.should.exist;

                res.body.should.have.property("lastupdatedby");
                res.body.lastupdatedby.should.have.property("_id");
                res.body.lastupdatedby._id.should.equal(authUser.user._id);
                res.body.lastupdatedby.should.have.property("name");
                res.body.lastupdatedby.name.should.equal("Jane Samson");

                done();              
            });
    });

    it("Should try to add a client that already exists", function(done) {        
        chai.request(server)
            .post("/api/clients")
            .set('authorization', 'Bearer ' + authUser.token)
            .send({name: "Lawyer's office"})
            .end(function(err, res) {
                res.should.have.status(409);
                
                chai.request(server)
                    .get("/api/clients")
                    .set('authorization', 'Bearer ' + authUser.token)
                    .end(function(err, res) {
                        res.should.have.status(200);
                        res.should.be.json;
                        res.body.should.be.a("array");
                        res.body.length.should.equal(1);
                        done();
                    })
            });
    });
    
    it("Should get a listing of all Client", function(done) {
        chai.request(server)
            .get("/api/clients")
            // Add next link only when JWT Used
            .set("authorization", 'Bearer ' + authUser.token)
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a("array");
                res.body.length.should.equal(1);
                res.body[0].should.have.property("_id");
                res.body[0].should.have.property("name");
                res.body[0].name.should.equal("Lawyer's Office");
                res.body[0].should.have.property("website");
                res.body[0].website.should.equal("www.lawyer.com");
                res.body[0].should.have.property("businesshours");
                res.body[0].businesshours.should.equal("M-F 9-5");
                res.body[0].should.have.property("description");
                res.body[0].description.should.equal("Best lawyer and isn't a rip off");
                res.body[0].should.have.property("primarylocation");
                res.body[0].primarylocation.should.equal("1");
                res.body[0].should.have.property("active");
                res.body[0].active.should.equal(true);
                res.body[0].should.not.have.property("deleted");

                res.body[0].should.have.property("createdat");
                res.body[0].createdat.should.exist;
                res.body[0].should.have.property("lastupdatedat");
                res.body[0].lastupdatedat.should.exist;

                res.body[0].should.have.property("lastupdatedby");
                res.body[0].lastupdatedby.should.have.property("_id");
                res.body[0].lastupdatedby._id.should.equal(authUser.user._id);
                res.body[0].lastupdatedby.should.have.property("name");
                res.body[0].lastupdatedby.name.should.equal("Jane Samson");

                done();
            });
    });
    
    it("Should get a single Client", function(done) {
        chai.request(server)
            .get("/api/clients/" + addedClient._id)
            .set("authorization", 'Bearer ' + authUser.token)
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.json;
                res.should.be.a("object");
                res.body.should.have.property("_id");
                res.body._id.should.equal(addedClient._id.toString());
                res.body.should.have.property("name");
                res.body.name.should.equal("Lawyer's Office");
                res.body.should.have.property("website");
                res.body.website.should.equal("www.lawyer.com");
                res.body.should.have.property("businesshours");
                res.body.businesshours.should.equal("M-F 9-5");
                res.body.should.have.property("description");
                res.body.description.should.equal("Best lawyer and isn't a rip off");
                res.body.should.have.property("primarylocation");
                res.body.primarylocation.should.equal("1");
                res.body.assets.should.be.a("array");
                res.body.should.not.have.property("deleted");

                res.body.should.have.property("createdat");
                res.body.createdat.should.exist;
                res.body.should.have.property("lastupdatedat");
                res.body.lastupdatedat.should.exist;

                res.body.should.have.property("lastupdatedby");
                res.body.lastupdatedby.should.have.property("_id");
                res.body.lastupdatedby._id.should.equal(authUser.user._id);
                res.body.lastupdatedby.should.have.property("name");
                res.body.lastupdatedby.name.should.equal("Jane Samson");

                done();
            });
    });
    
    it("Should update a single Client", function(done) {
        chai.request(server)
            .put("/api/clients/" + addedClient._id)
            .set("authorization", 'Bearer ' + authUser.token)
            .send({name: "JD Lawyer Office", website: "www.JDlawyer.com", businesshours: "M-W 1-5", description: "OK and isn't a rip off", primarylocation: "44", active: false})
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.json;
                res.should.be.a("object");
                res.body._id.should.equal(addedClient._id.toString());
                res.body.should.have.property("name");
                res.body.name.should.equal("JD Lawyer Office");
                res.body.should.have.property("website");
                res.body.website.should.equal("www.JDlawyer.com");
                res.body.should.have.property("businesshours");
                res.body.businesshours.should.equal("M-W 1-5");
                res.body.should.have.property("description");
                res.body.description.should.equal("OK and isn't a rip off");
                res.body.should.have.property("primarylocation");
                res.body.primarylocation.should.equal("44");
                res.body.should.have.property("active");
                res.body.active.should.equal(false);
                res.body.should.not.have.property("deleted");

                res.body.should.have.property("createdat");
                res.body.createdat.should.exist;
                res.body.should.have.property("lastupdatedat");
                res.body.lastupdatedat.should.exist;

                res.body.should.have.property("lastupdatedby");
                res.body.lastupdatedby.should.have.property("_id");
                res.body.lastupdatedby._id.should.equal(authUser.user._id);
                res.body.lastupdatedby.should.have.property("name");
                res.body.lastupdatedby.name.should.equal("Jane Samson");

                done();
            });
    });

    it("Should try to update a single client with an existing name", function(done) {
        Client.create({name: 'Client Name 1'}, function(err, client){
            chai.request(server)
                .put("/api/clients/" + addedClient._id)
                .set('authorization', 'Bearer ' + authUser.token)
                .send({name: 'Client name 1'})
                .end(function(err, res) {
                    res.should.have.status(409);
                    res.should.json;
                    
                    done();
                });
        })
    });

    it("Should delete a Client", function(done) {
        chai.request(server)
            .delete("/api/clients/" + addedClient._id)
            .set("authorization", 'Bearer ' + authUser.token)
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.json;
                res.should.be.a("object");
                
                chai.request(server)
                    .get("/api/clients")
                    .set("authorization", 'Bearer ' + authUser.token)
                    .end(function(err, res) {
                        res.should.have.status(200);
                        res.should.json;
                        res.body.should.be.a("array");
                        res.body.length.should.equal(0);
                        done();        
                    })
            });
    });

    describe("Locations", function() {
        var addedLocation;

        beforeEach(function(done) {
            chai.request(server)
                .post("/api/clients/" + addedClient._id + "/locations")
                .set('authorization', 'Bearer ' + authUser.token)
                .send({name: "Head Office", address1: "123 Google Ave", address2: "Building 52", city: "Mountain View", province: "California", country: "USA", postalcode: "90210", phonenumber: "123-123-1234", faxnumber: "987-654-3210", email: "office1@company.com", active: true})
                .end(function(err, res) {
                    res.should.have.status(200)
                    res.should.json;
                    res.body.should.be.a("object");
                    res.body.should.have.property("_id");
                    res.body.should.have.property("name");
                    res.body.name.should.equal("Head Office");
                    res.body.should.have.property("address1");
                    res.body.address1.should.equal("123 Google Ave");
                    res.body.should.have.property("address2");
                    res.body.address2.should.equal("Building 52");
                    res.body.should.have.property("city");
                    res.body.city.should.equal("Mountain View");
                    res.body.should.have.property("province");
                    res.body.province.should.equal("California");
                    res.body.should.have.property("country");
                    res.body.country.should.equal("USA");
                    res.body.should.have.property("postalcode");
                    res.body.postalcode.should.equal("90210");
                    res.body.should.have.property("phonenumber");
                    res.body.phonenumber.should.equal("123-123-1234");
                    res.body.should.have.property("faxnumber");
                    res.body.faxnumber.should.equal("987-654-3210");
                    res.body.should.have.property("email");
                    res.body.email.should.equal("office1@company.com");
                    res.body.should.have.property("active");
                    res.body.active.should.equal(true);
                    addedLocation = res.body;
                    done();                
                });
        });

        afterEach(function(done) {
            done();
        });

        it("Should add new location", function(done) {
            chai.request(server)
                .post("/api/clients/" + addedClient._id + "/locations")
                .set('authorization', 'Bearer ' + authUser.token)
                .send({name: "Main Office", address1: "123 Google Ave", address2: "Building 52", city: "Mountain View", province: "California", country: "USA", postalcode: "90210", phonenumber: "123-123-1234", faxnumber: "987-654-3210", email: "office1@company.com", active: true})
                .end(function(err, res) {
                    res.should.have.status(200)
                    res.should.json;
                    res.body.should.be.a("object");
                    res.body.should.have.property("_id");
                    res.body.should.have.property("name");
                    res.body.name.should.equal("Main Office");
                    res.body.should.have.property("address1");
                    res.body.address1.should.equal("123 Google Ave");
                    res.body.should.have.property("address2");
                    res.body.address2.should.equal("Building 52");
                    res.body.should.have.property("city");
                    res.body.city.should.equal("Mountain View");
                    res.body.should.have.property("province");
                    res.body.province.should.equal("California");
                    res.body.should.have.property("country");
                    res.body.country.should.equal("USA");
                    res.body.should.have.property("postalcode");
                    res.body.postalcode.should.equal("90210");
                    res.body.should.have.property("phonenumber");
                    res.body.phonenumber.should.equal("123-123-1234");
                    res.body.should.have.property("faxnumber");
                    res.body.faxnumber.should.equal("987-654-3210");
                    res.body.should.have.property("email");
                    res.body.email.should.equal("office1@company.com");
                    res.body.should.have.property("active");
                    res.body.active.should.equal(true);
                    done();                
                });
        });

        it("Should try to add a new location with a duplicate name", function(done) {
            chai.request(server)
                .post("/api/clients/" + addedClient._id + "/locations")
                .set('authorization', 'Bearer ' + authUser.token)
                .send({name: "Head Office"})
                .end(function(err, res) {
                    res.should.have.status(409);
                    
                    chai.request(server)
                        .get("/api/clients/" + addedClient._id + "/locations")
                        .set('authorization', 'Bearer ' + authUser.token)
                        .end(function(err, res) {
                            res.should.have.status(200);
                            res.should.be.json;
                            res.body.should.be.a("array");
                            res.body.length.should.equal(1);
                            done();
                        })
                });
        });

        it("Should get all locations", function(done) {
            chai.request(server)
                .get("/api/clients/" + addedClient._id + "/locations")
                .set('authorization', 'Bearer ' + authUser.token)
                .end(function(err, res) {
                    res.should.have.status(200);
                    res.should.json;
                    res.body.should.be.a("array");
                    res.body.length.should.equal(1);
                    res.body[0].should.have.property("_id");
                    res.body[0].should.have.property("name");
                    res.body[0].name.should.equal("Head Office");
                    res.body[0].should.have.property("address1");
                    res.body[0].address1.should.equal("123 Google Ave");
                    res.body[0].should.have.property("address2");
                    res.body[0].address2.should.equal("Building 52");
                    res.body[0].should.have.property("city");
                    res.body[0].city.should.equal("Mountain View");
                    res.body[0].should.have.property("province");
                    res.body[0].province.should.equal("California");
                    res.body[0].should.have.property("country");
                    res.body[0].country.should.equal("USA");
                    res.body[0].should.have.property("postalcode");
                    res.body[0].postalcode.should.equal("90210");
                    res.body[0].should.have.property("phonenumber");
                    res.body[0].phonenumber.should.equal("123-123-1234");
                    res.body[0].should.have.property("faxnumber");
                    res.body[0].faxnumber.should.equal("987-654-3210");
                    res.body[0].should.have.property("email");
                    res.body[0].email.should.equal("office1@company.com");
                    res.body[0].should.have.property("active");
                    res.body[0].active.should.equal(true);
                    done();
                });
        });

        it("Should get specified location", function(done) {
            chai.request(server)
                .get("/api/clients/" + addedClient._id + "/locations/" + addedLocation._id)
                .set('authorization', 'Bearer ' + authUser.token)
                .end(function(err, res) {
                    res.should.have.status(200);
                    res.should.json;
                    res.body.should.be.a("object");
                    res.body.should.have.property("_id");
                    res.body.should.have.property("name");
                    res.body.name.should.equal("Head Office");
                    res.body.should.have.property("address1");
                    res.body.address1.should.equal("123 Google Ave");
                    res.body.should.have.property("address2");
                    res.body.address2.should.equal("Building 52");
                    res.body.should.have.property("city");
                    res.body.city.should.equal("Mountain View");
                    res.body.should.have.property("province");
                    res.body.province.should.equal("California");
                    res.body.should.have.property("country");
                    res.body.country.should.equal("USA");
                    res.body.should.have.property("postalcode");
                    res.body.postalcode.should.equal("90210");
                    res.body.should.have.property("phonenumber");
                    res.body.phonenumber.should.equal("123-123-1234");
                    res.body.should.have.property("faxnumber");
                    res.body.faxnumber.should.equal("987-654-3210");
                    res.body.should.have.property("email");
                    res.body.email.should.equal("office1@company.com");
                    res.body.should.have.property("active");
                    res.body.active.should.equal(true);
                    done();
                });
        });

        it("Should update a location", function(done) {
            chai.request(server)
                .put("/api/clients/" + addedClient._id + "/locations/" + addedLocation._id)
                .set('authorization', 'Bearer ' + authUser.token)
                .send({name: "New Head Office", address1: "321 google ave.", address2: "Building 2", city: "Mountain View", province: "California", country: "USA", postalcode:"90210", phonenumber: "123-123-1234", faxnumber: "987-654-3210", email: "office1@company.com", active: false})
                .end(function(err, res) {
                    res.should.have.status(200);
                    res.should.json;
                    res.body.should.be.a("object");
                    res.body.should.have.property("_id");
                    res.body.should.have.property("name");
                    res.body.name.should.equal("New Head Office");
                    res.body.should.have.property("address1");
                    res.body.address1.should.equal("321 google ave.");
                    res.body.should.have.property("address2");
                    res.body.address2.should.equal("Building 2");
                    res.body.should.have.property("city");
                    res.body.city.should.equal("Mountain View");
                    res.body.should.have.property("province");
                    res.body.province.should.equal("California");
                    res.body.should.have.property("country");
                    res.body.country.should.equal("USA");
                    res.body.should.have.property("postalcode");
                    res.body.postalcode.should.equal("90210");
                    res.body.should.have.property("phonenumber");
                    res.body.phonenumber.should.equal("123-123-1234");
                    res.body.should.have.property("faxnumber");
                    res.body.faxnumber.should.equal("987-654-3210");
                    res.body.should.have.property("email");
                    res.body.email.should.equal("office1@company.com");
                    res.body.should.have.property("active");
                    res.body.active.should.equal(false);
                    done();
                });
        });

        it("Should try to update a new location with a duplicate name", function(done) {
            chai.request(server)
                .post("/api/clients/" + addedClient._id + "/locations")
                .set('authorization', 'Bearer ' + authUser.token)
                .send({name: "Main Office"})
                .end(function(err, res) {
                    chai.request(server)
                    .put("/api/clients/" + addedClient._id + "/locations/" + addedLocation._id)
                    .set('authorization', 'Bearer ' + authUser.token)
                    .send({name: 'Main Office'})
                    .end(function(err, res) {
                        res.should.have.status(409);
                        res.should.json;
                        done();
                    });
                });
        });

        it("Should delete a location", function(done) {
            chai.request(server)
                .delete("/api/clients/" + addedClient._id + "/locations/" + addedLocation._id)
                .set('authorization', 'Bearer ' + authUser.token)
                .end(function(err, res) {
                    res.should.have.status(200);
                    res.should.json;
                    res.should.be.a("object");
                    
                    chai.request(server)
                        .get("/api/clients/" + addedClient._id + "/locations")
                        .set('authorization', 'Bearer ' + authUser.token)
                        .end(function(err, res) {
                            res.should.have.status(200);
                            res.should.json;
                            res.body.should.be.a("array");
                            res.body.length.should.equal(0);
                            done();        
                        })
                });
        });

        it("Should fail to delete a location if it is set as primary", function(done) {
            Client.findById(addedClient._id, function(err, client) {
                client.primarylocation = addedLocation._id;
                client.save(function(err) {
                    chai.request(server)
                        .delete("/api/clients/" + addedClient._id + "/locations/" + addedLocation._id)
                        .set('authorization', 'Bearer ' + authUser.token)
                        .end(function(err, res) {
                            res.should.have.status(409);
                            res.should.json;
                            res.should.be.a("object");
                            chai.request(server)
                                .get("/api/clients/" + addedClient._id + "/locations")
                                .set('authorization', 'Bearer ' + authUser.token)
                                .end(function(err, res) {
                                    res.should.have.status(200);
                                    res.should.json;
                                    res.body.should.be.a("array");
                                    res.body.length.should.equal(1);
                                    done();        
                                })
                        });
                });
            });
        });
    });

    describe("Assets", function() {
        var addedTemplateType, addedTemplate, addedAsset; 

        beforeEach(function(done) {
            TemplateType.create({description: "Asset"}, function(err, templateType) {
                addedTemplateType = templateType;  

                Template.create({
                    name: "Server",
                    description: "Base Server Template",
                    templateType: addedTemplateType._id,
                    fields: [{name: "server_name"}, {name: "ip_address"}, {name: "Subnet"}],
                    script: "@ServerName = %server_name%, @IPAddress = %ip_address%, @subnet = %subnet% New server has been created %server_name%, with ip address %ip_address% and subnet %subnet%"
                }, function(err, template) {
                    addedTemplate = template;

                    Asset.create({
                        name: "File Server 1",
                        client: addedClient._id,
                        types: [
                            {
                                template: addedTemplate._id,
                                fields: [
                                    { name: "server_name", value: "fs1" },
                                    { name: "ip_address", value: "192.168.0.2"},
                                    { name: "subnet", value: "255.255.250.0" }
                                ],
                                script: addedTemplate.script
                            }
                        ],
                        createdby: authUser.user._id,
                        lastupdatedby: authUser.user._id
                    }, function(err, asset) {
                        addedAsset = asset;
                        done();    
                    });
                    
                });
            });
        });
        
        it("Should add a server asset to a client", function(done) {
            var newAsset = {
                name: "DC1",
                client: addedClient._id,
                types: [
                    {
                        template: addedTemplate._id,
                        fields: [
                            { name: "Server Name", value: "DC1" },
                            { name: "IP Address", value: "192.168.0.1" },
                            { name: "Subnet", value: "255.255.250.0" }
                        ]
                    }
                ]
            };

            chai.request(server)
                .post("/api/clients/" + addedClient._id + '/assets')
                .set("authorization", 'Bearer ' + authUser.token)
                .send(newAsset)
                .end(function(err, res) {
                    res.should.have.status(200);
                    res.should.json;
                    res.should.be.a("object");
                    res.body.should.have.property("_id");
                    res.body.should.have.property("name");
                    res.body.name.should.equal("DC1");
                    res.body.should.have.property("types");
                    res.body.should.have.property("client");
                    res.body.client.should.equal(addedClient._id.toString());
                    res.body.types.should.be.a("array");
                    res.body.types.length.should.equal(1);
                    res.body.types[0].should.have.property("template");
                    res.body.types[0].template.should.equal(addedTemplate._id.toString());
                    res.body.types[0].fields.should.be.a("array");
                    res.body.types[0].fields[0].should.have.property("_id");
                    res.body.types[0].fields[0].should.have.property("name");
                    res.body.types[0].fields[0].name.should.equal("Server Name");
                    res.body.types[0].fields[0].should.have.property("value");
                    res.body.types[0].fields[0].value.should.equal("DC1");

                    res.body.should.have.property("createdat");
                    res.body.createdat.should.exist;
                    res.body.should.have.property("lastupdatedat");
                    res.body.lastupdatedat.should.exist;

                    res.body.should.have.property("lastupdatedby");
                    res.body.lastupdatedby.should.have.property("_id");
                    res.body.lastupdatedby._id.should.equal(authUser.user._id);
                    res.body.lastupdatedby.should.have.property("name");
                    res.body.lastupdatedby.name.should.equal("Jane Samson");

                    done();
                });
        });

        it("Should get a listing of all client assets", function(done) {
            chai.request(server)
                .get("/api/clients/" + addedClient._id + '/assets')
                .set("authorization", 'Bearer ' + authUser.token)
                .end(function(err, res) {
                    res.should.have.status(200);
                    res.should.json;
                    res.body.should.have.property("total");
                    res.body.total.should.equal(1);
                    res.body.should.have.property("top");
                    res.body.top.should.equal(20);
                    res.body.should.have.property("page");
                    res.body.page.should.equal(1)
                    res.body.should.have.property("data");
                    res.body.data.should.be.a("array");
                    res.body.data.length.should.equal(1);
                    res.body.data[0].should.have.property("_id");
                    res.body.data[0].should.have.property("name");
                    res.body.data[0].name.should.equal("File Server 1");
                    res.body.data[0].should.have.property("client");
                    res.body.data[0].client.should.equal(addedClient._id.toString());
                    res.body.data[0].should.have.property("types");
                    res.body.data[0].types.should.be.a("array");
                    res.body.data[0].types.length.should.equal(1);
                    res.body.data[0].types[0].should.have.property("template");
                    res.body.data[0].types[0].template.should.have.property("_id");
                    res.body.data[0].types[0].template._id.should.equal(addedTemplate._id.toString());
                    res.body.data[0].types[0].template.should.have.property("name");
                    res.body.data[0].types[0].template.name.should.equal(addedTemplate.name);
                    res.body.data[0].types[0].template.should.have.property("description");
                    res.body.data[0].types[0].template.description.should.equal(addedTemplate.description);
                    res.body.data[0].types[0].template.should.have.property("templateType");
                    res.body.data[0].types[0].template.templateType.should.equal(addedTemplate.templateType.toString());
                    res.body.data[0].types[0].template.should.have.not.property("fields");
                    res.body.data[0].types[0].fields.should.be.a("array");
                    res.body.data[0].types[0].fields[0].should.have.property("_id");
                    res.body.data[0].types[0].fields[0].should.have.property("name");
                    res.body.data[0].types[0].fields[0].name.should.equal("server_name");
                    res.body.data[0].types[0].fields[0].should.have.property("value");
                    res.body.data[0].types[0].fields[0].value.should.equal("fs1");
                    res.body.data[0].types[0].fields[1].should.have.property("_id");
                    res.body.data[0].types[0].fields[1].should.have.property("name");
                    res.body.data[0].types[0].fields[1].name.should.equal("ip_address");
                    res.body.data[0].types[0].fields[1].should.have.property("value");
                    res.body.data[0].types[0].fields[1].value.should.equal("192.168.0.2");
                    res.body.data[0].types[0].fields[2].should.have.property("_id");
                    res.body.data[0].types[0].fields[2].should.have.property("name");
                    res.body.data[0].types[0].fields[2].name.should.equal("subnet");
                    res.body.data[0].types[0].fields[2].should.have.property("value");
                    res.body.data[0].types[0].fields[2].value.should.equal("255.255.250.0");

                    res.body.data[0].should.have.property("createdat");
                    res.body.data[0].createdat.should.exist;
                    res.body.data[0].should.have.property("lastupdatedat");
                    res.body.data[0].lastupdatedat.should.exist;

                    res.body.data[0].should.have.property("lastupdatedby");
                    res.body.data[0].lastupdatedby.should.have.property("_id");
                    res.body.data[0].lastupdatedby._id.should.equal(authUser.user._id);
                    res.body.data[0].lastupdatedby.should.have.property("name");
                    res.body.data[0].lastupdatedby.name.should.equal("Jane Samson");

                    done();
                });
        });

        it('should get an individual client asset', function(done) {
            chai.request(server)
                .get("/api/clients/" + addedClient._id + '/assets/' + addedAsset._id)
                .set("authorization", 'Bearer ' + authUser.token)
                .end(function(err, res) {
                    res.should.have.status(200);
                    res.should.json;
                    res.should.be.a("object");
                    res.body.should.have.property("_id");
                    res.body._id.should.equal(addedAsset._id.toString());
                    res.body.should.have.property("name");
                    res.body.name.should.equal(addedAsset.name);
                    res.body.should.have.property("client");
                    res.body.client.should.equal(addedClient._id.toString());
                    res.body.should.have.property("active");
                    res.body.active.should.equal(true);
                    res.body.should.have.property("types");
                    res.body.types.should.be.a("array");
                    res.body.types[0].template.should.have.property("_id");
                    res.body.types[0].template._id.should.equal(addedTemplate._id.toString());
                    res.body.types[0].template.should.have.property("name");
                    res.body.types[0].template.name.should.equal(addedTemplate.name);
                    res.body.types[0].template.should.have.property("description");
                    res.body.types[0].template.description.should.equal(addedTemplate.description);
                    res.body.types[0].template.should.have.property("templateType");
                    res.body.types[0].template.templateType.should.equal(addedTemplate.templateType.toString());
                    res.body.types[0].template.should.have.not.property("fields");
                    res.body.types[0].fields.should.be.a("array");
                    res.body.types[0].fields[0].should.have.property("_id");
                    res.body.types[0].fields[0].should.have.property("name");
                    res.body.types[0].fields[0].name.should.equal("server_name");
                    res.body.types[0].fields[0].should.have.property("value");
                    res.body.types[0].fields[0].value.should.equal("fs1");
                    res.body.types[0].fields[1].should.have.property("_id");
                    res.body.types[0].fields[1].should.have.property("name");
                    res.body.types[0].fields[1].name.should.equal("ip_address");
                    res.body.types[0].fields[1].should.have.property("value");
                    res.body.types[0].fields[1].value.should.equal("192.168.0.2");
                    res.body.types[0].fields[2].should.have.property("_id");
                    res.body.types[0].fields[2].should.have.property("name");
                    res.body.types[0].fields[2].name.should.equal("subnet");
                    res.body.types[0].fields[2].should.have.property("value");
                    res.body.types[0].fields[2].value.should.equal("255.255.250.0");

                    res.body.should.have.property("createdat");
                    res.body.createdat.should.exist;
                    res.body.should.have.property("lastupdatedat");
                    res.body.lastupdatedat.should.exist;

                    res.body.should.have.property("lastupdatedby");
                    res.body.lastupdatedby.should.have.property("_id");
                    res.body.lastupdatedby._id.should.equal(authUser.user._id);
                    res.body.lastupdatedby.should.have.property("name");
                    res.body.lastupdatedby.name.should.equal("Jane Samson");

                    done();
                });
        });

        it("Should update a single Asset", function(done) {
            var editedAsset = addedAsset;

            editedAsset.name = "FS01";
            editedAsset.types[0].fields[0].value = "FS01";
            editedAsset.types[0].fields[1].value = "192.168.1.1";
            editedAsset.types[0].fields[2].value = "255.255.0.0";

            chai.request(server)
                .put("/api/clients/" + addedClient._id + '/assets/' + addedAsset._id)
                .set("authorization", 'Bearer ' + authUser.token)
                .send(editedAsset)
                .end(function(err, res) {
                    res.should.have.status(200);
                    res.should.json;
                    res.should.be.a("object");
                    res.body._id.should.equal(addedAsset._id.toString());
                    res.body.should.have.property("name");
                    res.body.name.should.equal("FS01");
                    res.body.should.have.property("client");
                    res.body.client.should.equal(addedClient._id.toString());
                    res.body.should.have.property("active");
                    res.body.active.should.equal(true);
                    res.body.should.have.property("types");
                    res.body.types.should.be.a("array");
                    res.body.types[0].template.should.have.property("_id");
                    res.body.types[0].template._id.should.equal(addedTemplate._id.toString());
                    res.body.types[0].template.should.have.property("name");
                    res.body.types[0].template.name.should.equal(addedTemplate.name);
                    res.body.types[0].template.should.have.property("description");
                    res.body.types[0].template.description.should.equal(addedTemplate.description);
                    res.body.types[0].template.should.have.property("templateType");
                    res.body.types[0].template.templateType.should.equal(addedTemplate.templateType.toString());
                    res.body.types[0].template.should.have.not.property("fields");
                    res.body.types[0].fields.should.be.a("array");
                    res.body.types[0].fields[0].should.have.property("_id");
                    res.body.types[0].fields[0].should.have.property("name");
                    res.body.types[0].fields[0].name.should.equal("server_name");
                    res.body.types[0].fields[0].should.have.property("value");
                    res.body.types[0].fields[0].value.should.equal("FS01");
                    res.body.types[0].fields[1].should.have.property("_id");
                    res.body.types[0].fields[1].should.have.property("name");
                    res.body.types[0].fields[1].name.should.equal("ip_address");
                    res.body.types[0].fields[1].should.have.property("value");
                    res.body.types[0].fields[1].value.should.equal("192.168.1.1");
                    res.body.types[0].fields[2].should.have.property("_id");
                    res.body.types[0].fields[2].should.have.property("name");
                    res.body.types[0].fields[2].name.should.equal("subnet");
                    res.body.types[0].fields[2].should.have.property("value");
                    res.body.types[0].fields[2].value.should.equal("255.255.0.0");

                    res.body.should.have.property("createdat");
                    res.body.createdat.should.exist;
                    res.body.should.have.property("lastupdatedat");
                    res.body.lastupdatedat.should.exist;

                    res.body.should.have.property("lastupdatedby");
                    res.body.lastupdatedby.should.have.property("_id");
                    res.body.lastupdatedby._id.should.equal(authUser.user._id);
                    res.body.lastupdatedby.should.have.property("name");
                    res.body.lastupdatedby.name.should.equal("Jane Samson");

                    done();
            });
        });

        it("Should delete an Asset", function(done) {
            chai.request(server)
                .delete("/api/clients/" + addedClient._id + "/assets/" + addedAsset._id)
                .set("authorization", 'Bearer ' + authUser.token)
                .end(function(err, res) {
                    res.should.have.status(200);
                    res.should.json;
                    res.should.be.a("object");
                    
                    chai.request(server)
                        .get("/api/clients/" + addedClient._id + '/assets')
                        .set("authorization", 'Bearer ' + authUser.token)
                        .end(function(err, res) {
                            res.should.have.status(200);
                            res.should.json;
                            res.body.should.have.property("data");
                            res.body.data.should.be.a("array");
                            res.body.data.length.should.equal(0);
                            res.body.should.have.property("total");
                            res.body.total.should.equal(0);
                            res.body.should.have.property("top");
                            res.body.top.should.equal(20);
                            res.body.should.have.property("page");
                            res.body.page.should.equal(1)
                            done();        
                        })
                });
        });

        it("Should return a script with supplied fields and values", function(done) {
            chai.request(server)
                .get("/api/clients/" + addedClient._id + "/assets/" + addedAsset._id + "/script/" + addedAsset.types[0]._id)
                .set("authorization", 'Bearer ' + authUser.token)
                .end(function(err, res) {
                    res.should.have.status(200);
                    res.should.json;
                    res.should.be.a("object");
                    res.body.should.have.property("script");
                    res.body.script.should.equal("@ServerName = fs1, @IPAddress = 192.168.0.2, @subnet = 255.255.250.0 New server has been created fs1, with ip address 192.168.0.2 and subnet 255.255.250.0");
                    done();
                });
        });

        it("Should add a custom field to the asset", function(done) {
            var editedAsset = addedAsset;
            
            editedAsset.custom = { fields: 
                [{ name: 'Description', value: 'Test description for custom field' },
                { name: 'Custom Mgmt IP', value: '10.2.177.43' }]
            };

            chai.request(server)
                .put("/api/clients/" + editedAsset._id + '/assets/' + editedAsset._id)
                .set("authorization", 'Bearer ' + authUser.token)
                .send(editedAsset)
                .end(function(err, res) {
                    res.should.have.status(200);
                    res.should.json;
                    res.should.be.a("object");
                    res.body._id.should.equal(addedAsset._id.toString());
                    res.body.should.have.property("name");
                    res.body.name.should.equal("File Server 1");
                    res.body.should.have.property("custom");
                    res.body.custom.should.have.property("fields");
                    res.body.custom.fields.should.be.a("array");
                    res.body.custom.fields.length.should.equal(2);
                    res.body.custom.fields[0].should.have.property("name");
                    res.body.custom.fields[0].name.should.equal("Description");
                    res.body.custom.fields[0].should.have.property("value");
                    res.body.custom.fields[0].value.should.equal("Test description for custom field");
                    res.body.custom.fields[1].should.have.property("name");
                    res.body.custom.fields[1].name.should.equal("Custom Mgmt IP");
                    res.body.custom.fields[1].should.have.property("value");
                    res.body.custom.fields[1].value.should.equal("10.2.177.43");
                    done();
            });
        });

        it("Should duplicate an asset", function(done) {
            chai.request(server)
                .post("/api/clients/" + addedAsset.client + '/assets/' + addedAsset._id + '/copy')
                .set("authorization", 'Bearer ' + authUser.token)
                .end(function(err, res) {
                    res.should.have.status(200);
                    res.should.json;
                    res.should.be.a("object");
                    res.body._id.should.not.equal(addedAsset._id.toString());
                    res.body.should.have.property("name");
                    res.body.name.should.equal("copy of File Server 1");
                    res.body.should.have.property("types");
                    res.body.types.should.be.a("array")
                    res.body.types[0].template.should.equal(addedAsset.types[0].template.toString());
                    res.body.types[0].fields[0].name.should.equal(addedAsset.types[0].fields[0].name);
                    res.body.types[0].script.should.equal(addedAsset.types[0].script);
                    res.body.client.should.equal(addedAsset.client.toString());
                    res.body.createdat.should.not.equal(addedAsset.createdat);
                    res.body.lastupdatedat.should.not.equal(addedAsset.lastupdatedat);
                    done();
            });
        });

        describe("Paging", function() {

            it("should return defaults when no params supplied.", function(done) {

                chai.request(server)
                    .get("/api/clients/" + addedAsset.client + '/assets')
                    .set("authorization", 'Bearer ' + authUser.token)
                    .end(function(err, res) {
                        res.should.have.status(200);
                        res.should.json;
                        res.should.be.a("object");
                        res.body.should.have.property("total");
                        res.body.total.should.equal(1);
                        res.body.should.have.property("data");
                        res.body.data.should.be.a("array");
                        res.body.should.have.property("top");
                        res.body.top.should.equal(20);
                        res.body.should.have.property("page");
                        res.body.page.should.equal(1);
                        done();
                });
            });

            it("should return page 2 when page param is supplied", function(done) {
                for (i = 1; i < 25; i++) {
                    Asset.create({name: 'Asset ' + i, client: addedClient._id}, function(err, asset) {

                    });
                };

                chai.request(server)
                    .get("/api/clients/" + addedClient._id + '/assets?page=2')
                    .set("authorization", 'Bearer ' + authUser.token)
                    .end(function(err, res) {
                        res.should.have.status(200);
                        res.should.json;
                        res.should.be.a("object");
                        res.body.should.have.property("total");
                        res.body.total.should.equal(25);
                        res.body.should.have.property("data");
                        res.body.data.length.should.equal(5);
                        res.body.data.should.be.a("array");
                        res.body.should.have.property("top");
                        res.body.top.should.equal(20);
                        res.body.should.have.property("page");
                        res.body.page.should.equal(2);
                        done();
                });
            });

            it("should return 3 items only when top param is supplied", function(done) {
                for (i = 1; i < 25; i++) {
                    Asset.create({name: 'Asset ' + i, client: addedClient._id}, function(err, asset) {

                    });
                };

                chai.request(server)
                    .get("/api/clients/" + addedClient._id + '/assets?top=3')
                    .set("authorization", 'Bearer ' + authUser.token)
                    .end(function(err, res) {
                        res.should.have.status(200);
                        res.should.json;
                        res.should.be.a("object");
                        res.body.should.have.property("total");
                        res.body.total.should.equal(25);
                        res.body.should.have.property("data");
                        res.body.data.length.should.equal(3);
                        res.body.data.should.be.a("array");
                        res.body.should.have.property("top");
                        res.body.top.should.equal(3);
                        res.body.should.have.property("page");
                        res.body.page.should.equal(1);
                        done();
                });
            });


        });
    });
});