process.env.NODE_ENV = "test";

var chai = require("chai");
var chaiHttp = require("chai-http");

// Start JWT Requirements
var bcrypt = require("bcryptjs");
var User = require("../app/models/user");
var authUser;
// End JWT Requirements

var Task = require("../app/models/task");
var Client = require("../app/models/client");
var Asset = require("../app/models/asset");
var Activity = require("../app/models/activity");

var server = require("../server");

var should = chai.should();

var addedTask, addedClient, addedAsset;

chai.use(chaiHttp);

describe("Task", function() {
    
    User.collection.drop();
    Task.collection.drop();
    
    beforeEach(function(done) {
        bcrypt.hash("password123", 10, function(err, hash) {
            User.create({name: "Jane Samson", email: "jsamson@hotmail.com", password: hash}, function(err, user){
                // Authenticate User, that way every test can use.
                chai.request(server)
                    .post("/api/authenticate")
                    .send({email: "jsamson@hotmail.com", password: "password123"})
                    .end(function(err, res) {
                        res.should.have.status(200);
                        res.should.be.a("object");
                        res.body.should.have.property("message");
                        res.body.message.should.equal("Authentication successful");
                        authUser = res.body;

                        Client.create({name: 'Client 1'}, function(err, client) {
                            addedClient = client;
                            Asset.create({name: 'Asset 1', client: addedClient._id}, function(err, asset) {
                                addedAsset = asset;

                                chai.request(server)
                                    .post("/api/tasks")
                                    .set("authorization", 'Bearer ' + authUser.token)
                                    .send({
                                        title: 'Do Stuff Here.', 
                                        description: 'Test Task', 
                                        assignedto: authUser.user._id, 
                                        client: addedClient._id,
                                        asset: addedAsset._id,
                                        checklists: [{
                                            name: 'Checklist 1',
                                            items: [{
                                                description: 'Install New Server',
                                                completed: false
                                            },{
                                                description: 'Create User Account',
                                                completed: true
                                            }]
                                        }]
                                    })
                                    .end(function(err, res) {
                                        addedTask = res.body;
                                        done();          
                                    });
                            });
                        });
                    });
            });
        });
    });
    
    afterEach(function(done) {
        User.collection.drop();
        Task.collection.drop();
        done();
    });
    
    it("Should add a new Task", function(done) {
        var newTask = {
            title: 'Do Stuff Here.', 
            description: 'Install Server', 
            assignedto: authUser.user._id, 
            client: addedClient._id,
            asset: addedAsset._id,
            checklists: [{
                name: 'Checklist 1',
                items: [{
                    description: 'Install New Server',
                    completed: false
                },{
                    description: 'Create User Account',
                    completed: true
                }]
            }]};

        chai.request(server)
            .post("/api/tasks")
            .set("authorization", 'Bearer ' + authUser.token)
            .send(newTask)
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.be.json;
                res.should.be.a("object");
                res.body.should.have.property("_id");
                res.body.should.have.property("title");
                res.body.title.should.equal("Do Stuff Here.");
                res.body.should.have.property("description");
                res.body.description.should.equal("Install Server");
                res.body.should.have.property("completed");
                res.body.completed.should.equal(false);
                chai.expect(res.body.completedat).to.not.exist;
                
                res.body.should.have.property("createdby");
                res.body.createdby.should.have.property("_id");
                res.body.createdby._id.should.equal(authUser.user._id);
                res.body.createdby.should.have.property("name");
                res.body.createdby.name.should.equal("Jane Samson");

                res.body.should.have.property("assignedto");
                res.body.assignedto.should.have.property("_id");
                res.body.assignedto._id.should.equal(authUser.user._id);
                res.body.assignedto.should.have.property("name");
                res.body.assignedto.name.should.equal("Jane Samson");
                
                res.body.should.have.property("client");
                res.body.client.should.have.property("_id");
                res.body.client._id.should.equal(addedClient._id.toString());
                res.body.client.should.have.property("name");
                res.body.client.name.should.equal("Client 1");

                res.body.should.have.property("asset");
                res.body.asset.should.have.property("_id");
                res.body.asset._id.should.equal(addedAsset._id.toString());
                res.body.asset.should.have.property("name");
                res.body.asset.name.should.equal("Asset 1");

                res.body.should.have.property("checklists");
                res.body.checklists.should.be.a("array");
                res.body.checklists.length.should.equal(1);
                res.body.checklists[0].should.have.property("name");
                res.body.checklists[0].name.should.equal("Checklist 1");
                res.body.checklists[0].should.have.property("items");
                res.body.checklists[0].items.should.be.a("array");
                res.body.checklists[0].items.length.should.equal(2);
                res.body.checklists[0].items[0].should.have.property("description");
                res.body.checklists[0].items[0].description.should.equal("Install New Server");
                res.body.checklists[0].items[0].should.have.property("completed");
                res.body.checklists[0].items[0].completed.should.equal(false);
                res.body.checklists[0].items[1].should.have.property("description");
                res.body.checklists[0].items[1].description.should.equal("Create User Account");
                res.body.checklists[0].items[1].should.have.property("completed");
                res.body.checklists[0].items[1].completed.should.equal(true);

                res.body.should.have.property("createdat");
                res.body.createdat.should.exist;
                res.body.should.have.property("lastupdatedat");
                res.body.lastupdatedat.should.exist;

                res.body.should.have.property("lastupdatedby");
                res.body.lastupdatedby.should.have.property("_id");
                res.body.lastupdatedby._id.should.equal(authUser.user._id);
                res.body.lastupdatedby.should.have.property("name");
                res.body.lastupdatedby.name.should.equal("Jane Samson");

                res.body.should.have.property("active");
                res.body.active.should.equal(true);
                res.body.should.not.have.property("deleted");
                done();              
            });
    });
    
    it("Should get a listing of all open tasks", function(done) {
        chai.request(server)
            .get("/api/tasks")
            .set("authorization", 'Bearer ' + authUser.token)
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a("array");
                res.body.length.should.equal(1);
                res.body[0].should.have.property("_id");
                res.body[0].should.have.property("title");
                res.body[0].title.should.equal("Do Stuff Here.");
                res.body[0].should.have.property("description");
                res.body[0].description.should.equal("Test Task");
                res.body[0].should.have.property("completed");
                res.body[0].completed.should.equal(false);
                
                res.body[0].should.have.property("createdby");
                res.body[0].createdby.should.have.property("_id");
                res.body[0].createdby._id.should.equal(authUser.user._id);
                res.body[0].createdby.should.have.property("name");
                res.body[0].createdby.name.should.equal("Jane Samson");

                res.body[0].should.have.property("assignedto");
                res.body[0].assignedto.should.have.property("_id");
                res.body[0].assignedto._id.should.equal(authUser.user._id);
                res.body[0].assignedto.should.have.property("name");
                res.body[0].assignedto.name.should.equal("Jane Samson");

                res.body[0].should.have.property("client");
                res.body[0].client.should.have.property("_id");
                res.body[0].client._id.should.equal(addedClient._id.toString());
                res.body[0].client.should.have.property("name");
                res.body[0].client.name.should.equal("Client 1");

                res.body[0].should.have.property("asset");
                res.body[0].asset.should.have.property("_id");
                res.body[0].asset._id.should.equal(addedAsset._id.toString());
                res.body[0].asset.should.have.property("name");
                res.body[0].asset.name.should.equal("Asset 1");

                res.body[0].should.have.property("checklists");
                res.body[0].checklists.should.be.a("array");
                res.body[0].checklists.length.should.equal(1);
                res.body[0].checklists[0].should.have.property("name");
                res.body[0].checklists[0].name.should.equal("Checklist 1");
                res.body[0].checklists[0].should.have.property("items");
                res.body[0].checklists[0].items.should.be.a("array");
                res.body[0].checklists[0].items.length.should.equal(2);
                res.body[0].checklists[0].items[0].should.have.property("description");
                res.body[0].checklists[0].items[0].description.should.equal("Install New Server");
                res.body[0].checklists[0].items[0].should.have.property("completed");
                res.body[0].checklists[0].items[0].completed.should.equal(false);
                res.body[0].checklists[0].items[1].should.have.property("description");
                res.body[0].checklists[0].items[1].description.should.equal("Create User Account");
                res.body[0].checklists[0].items[1].should.have.property("completed");
                res.body[0].checklists[0].items[1].completed.should.equal(true);

                res.body[0].should.have.property("totalItems");
                res.body[0].totalItems.should.equal(2);
                res.body[0].should.have.property("completedItems");
                res.body[0].completedItems.should.equal(1);

                res.body[0].should.have.property("createdat");
                res.body[0].createdat.should.exist;

                res.body[0].should.have.property("lastupdatedat");
                res.body[0].lastupdatedat.should.exist;

                res.body[0].should.have.property("lastupdatedby");
                res.body[0].lastupdatedby.should.have.property("_id");
                res.body[0].lastupdatedby._id.should.equal(authUser.user._id);
                res.body[0].lastupdatedby.should.have.property("name");
                res.body[0].lastupdatedby.name.should.equal("Jane Samson");

                res.body[0].should.have.property("active");
                res.body[0].active.should.equal(true);
                res.body[0].should.not.have.property("deleted");
                done();
            });
    });

    it("Should get all completed tasks", function(done) {
        Task.create({title: 'Test Completed Task', completed: true, createdby: authUser.user._id}, function(err, task) {
            chai.request(server)
                .get("/api/tasks?q=completed")
                .set("authorization", 'Bearer ' + authUser.token)
                .end(function(err, res) {
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.be.a("array");
                    res.body.length.should.equal(1);
                    res.body[0].should.have.property("_id");
                    res.body[0].should.have.property("title");
                    res.body[0].title.should.equal("Test Completed Task");
                    res.body[0].should.have.property("completed");
                    res.body[0].completed.should.equal(true);
                    done();
                });
        });
    });

    it('Should get all open tasks created to Jane Samson', function(done) {
        Task.create({title: 'Test Completed Task', completed: true, createdby: authUser.user._id, assignedto: authUser.user._id}, function(err, task) {
            chai.request(server)
            .get("/api/tasks?created='Jane Samson'")
            .set("authorization", 'Bearer ' + authUser.token)
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a("array");
                res.body.length.should.equal(1);
                res.body[0].should.have.property("_id");
                res.body[0].should.have.property("title");
                res.body[0].title.should.equal("Do Stuff Here.");
                res.body[0].should.have.property("description");
                res.body[0].description.should.equal("Test Task");
                res.body[0].should.have.property("completed");
                res.body[0].completed.should.equal(false);
                
                res.body[0].should.have.property("createdby");
                res.body[0].createdby.should.have.property("_id");
                res.body[0].createdby._id.should.equal(authUser.user._id);
                res.body[0].createdby.should.have.property("name");
                res.body[0].createdby.name.should.equal("Jane Samson");

                res.body[0].should.have.property("assignedto");
                res.body[0].assignedto.should.have.property("_id");
                res.body[0].assignedto._id.should.equal(authUser.user._id);
                res.body[0].assignedto.should.have.property("name");
                res.body[0].assignedto.name.should.equal("Jane Samson");

                res.body[0].should.have.property("client");
                res.body[0].client.should.have.property("_id");
                res.body[0].client._id.should.equal(addedClient._id.toString());
                res.body[0].client.should.have.property("name");
                res.body[0].client.name.should.equal("Client 1");

                res.body[0].should.have.property("asset");
                res.body[0].asset.should.have.property("_id");
                res.body[0].asset._id.should.equal(addedAsset._id.toString());
                res.body[0].asset.should.have.property("name");
                res.body[0].asset.name.should.equal("Asset 1");

                res.body[0].should.have.property("checklists");
                res.body[0].checklists.should.be.a("array");
                res.body[0].checklists.length.should.equal(1);
                res.body[0].checklists[0].should.have.property("name");
                res.body[0].checklists[0].name.should.equal("Checklist 1");
                res.body[0].checklists[0].should.have.property("items");
                res.body[0].checklists[0].items.should.be.a("array");
                res.body[0].checklists[0].items.length.should.equal(2);
                res.body[0].checklists[0].items[0].should.have.property("description");
                res.body[0].checklists[0].items[0].description.should.equal("Install New Server");
                res.body[0].checklists[0].items[0].should.have.property("completed");
                res.body[0].checklists[0].items[0].completed.should.equal(false);
                res.body[0].checklists[0].items[1].should.have.property("description");
                res.body[0].checklists[0].items[1].description.should.equal("Create User Account");
                res.body[0].checklists[0].items[1].should.have.property("completed");
                res.body[0].checklists[0].items[1].completed.should.equal(true);

                res.body[0].should.have.property("createdat");
                res.body[0].createdat.should.exist;

                res.body[0].should.have.property("lastupdatedat");
                res.body[0].lastupdatedat.should.exist;

                res.body[0].should.have.property("lastupdatedby");
                res.body[0].lastupdatedby.should.have.property("_id");
                res.body[0].lastupdatedby._id.should.equal(authUser.user._id);
                res.body[0].lastupdatedby.should.have.property("name");
                res.body[0].lastupdatedby.name.should.equal("Jane Samson");

                res.body[0].should.have.property("active");
                res.body[0].active.should.equal(true);
                res.body[0].should.not.have.property("deleted");
                done();
            });
        });
    });

    it('Should get all completed tasks created by Jane Samson', function(done) {
        Task.create({title: 'Test Completed Task', completed: true, createdby: authUser.user._id, assignedto: authUser.user._id}, function(err, task) {
            chai.request(server)
            .get("/api/tasks?q=completed&created='Jane Samson'")
            .set("authorization", 'Bearer ' + authUser.token)
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a("array");
                res.body.length.should.equal(1);
                res.body[0].should.have.property("_id");
                res.body[0].should.have.property("title");
                res.body[0].title.should.equal("Test Completed Task");
                res.body[0].should.have.property("completed");
                res.body[0].completed.should.equal(true);
                
                res.body[0].should.have.property("createdby");
                res.body[0].createdby.should.have.property("_id");
                res.body[0].createdby._id.should.equal(authUser.user._id);
                res.body[0].createdby.should.have.property("name");
                res.body[0].createdby.name.should.equal("Jane Samson");

                res.body[0].should.have.property("assignedto");
                res.body[0].assignedto.should.have.property("_id");
                res.body[0].assignedto._id.should.equal(authUser.user._id);
                res.body[0].assignedto.should.have.property("name");
                res.body[0].assignedto.name.should.equal("Jane Samson");

                res.body[0].should.have.property("active");
                res.body[0].active.should.equal(true);
                res.body[0].should.not.have.property("deleted");
                done();
            });
        });
    });

    it("Should get all open tasks assigned to a specified user", function(done) {
        chai.request(server)
            .get("/api/tasks?assignedto='Jane Samson'")
            .set("authorization", 'Bearer ' + authUser.token)
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a("array");
                res.body.length.should.equal(1);
                res.body[0].should.have.property("_id");
                res.body[0].should.have.property("title");
                res.body[0].title.should.equal("Do Stuff Here.");
                res.body[0].should.have.property("completed");
                res.body[0].completed.should.equal(false);
                
                res.body[0].should.have.property("createdby");
                res.body[0].createdby.should.have.property("_id");
                res.body[0].createdby._id.should.equal(authUser.user._id);
                res.body[0].createdby.should.have.property("name");
                res.body[0].createdby.name.should.equal("Jane Samson");

                res.body[0].should.have.property("assignedto");
                res.body[0].assignedto.should.have.property("_id");
                res.body[0].assignedto._id.should.equal(authUser.user._id);
                res.body[0].assignedto.should.have.property("name");
                res.body[0].assignedto.name.should.equal("Jane Samson");

                res.body[0].should.have.property("active");
                res.body[0].active.should.equal(true);
                res.body[0].should.not.have.property("deleted");
                done();
            });
    });

    it("Should get all open tasks for the specified client", function(done) {
        chai.request(server)
            .get("/api/tasks?client='" + addedClient.name + "'")
            .set("authorization", 'Bearer ' + authUser.token)
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a("array");
                res.body.length.should.equal(1);
                res.body[0].should.have.property("_id");
                res.body[0].should.have.property("title");
                res.body[0].title.should.equal("Do Stuff Here.");
                res.body[0].should.have.property("completed");
                res.body[0].completed.should.equal(false);
                
                res.body[0].should.have.property("createdby");
                res.body[0].createdby.should.have.property("_id");
                res.body[0].createdby._id.should.equal(authUser.user._id);
                res.body[0].createdby.should.have.property("name");
                res.body[0].createdby.name.should.equal("Jane Samson");

                res.body[0].should.have.property("assignedto");
                res.body[0].assignedto.should.have.property("_id");
                res.body[0].assignedto._id.should.equal(authUser.user._id);
                res.body[0].assignedto.should.have.property("name");
                res.body[0].assignedto.name.should.equal("Jane Samson");

                res.body[0].should.have.property("active");
                res.body[0].active.should.equal(true);
                res.body[0].should.not.have.property("deleted");
                done();
            });
    });

    it("Should get all open tasks for the specified asset", function(done) {
        chai.request(server)
            .get("/api/tasks?asset='" + addedAsset.name + "'")
            .set("authorization", 'Bearer ' + authUser.token)
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a("array");
                res.body.length.should.equal(1);
                res.body[0].should.have.property("_id");
                res.body[0].should.have.property("title");
                res.body[0].title.should.equal("Do Stuff Here.");
                res.body[0].should.have.property("completed");
                res.body[0].completed.should.equal(false);
                
                res.body[0].should.have.property("createdby");
                res.body[0].createdby.should.have.property("_id");
                res.body[0].createdby._id.should.equal(authUser.user._id);
                res.body[0].createdby.should.have.property("name");
                res.body[0].createdby.name.should.equal("Jane Samson");

                res.body[0].should.have.property("assignedto");
                res.body[0].assignedto.should.have.property("_id");
                res.body[0].assignedto._id.should.equal(authUser.user._id);
                res.body[0].assignedto.should.have.property("name");
                res.body[0].assignedto.name.should.equal("Jane Samson");

                res.body[0].should.have.property("active");
                res.body[0].active.should.equal(true);
                res.body[0].should.not.have.property("deleted");
                done();
            });
    });

    it("Should sort the list newest to oldest", function(done) {
        Task.create({title: 'A later task', completed: false, createdby: authUser.user._id, createdat: new Date(), assignedto: authUser.user._id}, function(err, task) {
            chai.request(server)
            .get("/api/tasks?sort=desc")
            .set("authorization", 'Bearer ' + authUser.token)
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a("array");
                res.body.length.should.equal(2);
                res.body[0].should.have.property("_id");
                res.body[0].should.have.property("title");
                res.body[0].title.should.equal("A later task");
                res.body[0].should.have.property("completed");
                res.body[0].completed.should.equal(false);
                done();
            });
        });
    });

    it("Should sort the list oldest to newest", function(done) {
        Task.create({title: 'A later task', completed: false, createdby: authUser.user._id, createdat: new Date(), assignedto: authUser.user._id}, function(err, task) {
            chai.request(server)
            .get("/api/tasks?sort=asc")
            .set("authorization", 'Bearer ' + authUser.token)
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a("array");
                res.body.length.should.equal(2);
                res.body[0].should.have.property("_id");
                res.body[0].should.have.property("title");
                res.body[0].title.should.equal("Do Stuff Here.");
                res.body[0].should.have.property("completed");
                res.body[0].completed.should.equal(false);
                done();
            });
        });
    });
    
    it("Should get a single Task", function(done) {
        chai.request(server)
            .get("/api/tasks/" + addedTask._id)
            .set("authorization", 'Bearer ' + authUser.token)
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.json;
                res.should.be.a("object");
                res.body.should.have.property("_id");
                res.body._id.should.equal(addedTask._id.toString());
                res.body.should.have.property("title");
                res.body.title.should.equal("Do Stuff Here.");
                res.body.should.have.property("description");
                res.body.description.should.equal("Test Task");
                res.body.should.have.property("completed");
                res.body.completed.should.equal(false);
                
                res.body.should.have.property("createdby");
                res.body.createdby.should.have.property("_id");
                res.body.createdby._id.should.equal(authUser.user._id);
                res.body.createdby.should.have.property("name");
                res.body.createdby.name.should.equal("Jane Samson");

                res.body.should.have.property("assignedto");
                res.body.assignedto.should.have.property("_id");
                res.body.assignedto._id.should.equal(authUser.user._id);
                res.body.assignedto.should.have.property("name");
                res.body.assignedto.name.should.equal("Jane Samson");

                res.body.should.have.property("client");
                res.body.client.should.have.property("_id");
                res.body.client._id.should.equal(addedClient._id.toString());
                res.body.client.should.have.property("name");
                res.body.client.name.should.equal("Client 1");

                res.body.should.have.property("asset");
                res.body.asset.should.have.property("_id");
                res.body.asset._id.should.equal(addedAsset._id.toString());
                res.body.asset.should.have.property("name");
                res.body.asset.name.should.equal("Asset 1");

                res.body.should.have.property("checklists");
                res.body.checklists.should.be.a("array");
                res.body.checklists.length.should.equal(1);
                res.body.checklists[0].should.have.property("name");
                res.body.checklists[0].name.should.equal("Checklist 1");
                res.body.checklists[0].should.have.property("items");
                res.body.checklists[0].items.should.be.a("array");
                res.body.checklists[0].items.length.should.equal(2);
                res.body.checklists[0].items[0].should.have.property("description");
                res.body.checklists[0].items[0].description.should.equal("Install New Server");
                res.body.checklists[0].items[0].should.have.property("completed");
                res.body.checklists[0].items[0].completed.should.equal(false);
                res.body.checklists[0].items[1].should.have.property("description");
                res.body.checklists[0].items[1].description.should.equal("Create User Account");
                res.body.checklists[0].items[1].should.have.property("completed");
                res.body.checklists[0].items[1].completed.should.equal(true);

                res.body.should.have.property("createdat");
                res.body.createdat.should.exist;

                res.body.should.have.property("lastupdatedat");
                res.body.lastupdatedat.should.exist;

                res.body.should.have.property("lastupdatedby");
                res.body.lastupdatedby.should.have.property("_id");
                res.body.lastupdatedby._id.should.equal(authUser.user._id);
                res.body.lastupdatedby.should.have.property("name");
                res.body.lastupdatedby.name.should.equal("Jane Samson");

                res.body.should.have.property("active");
                res.body.active.should.equal(true);
                res.body.should.not.have.property("deleted");
                done();
            });
    });
    
    it("Should update a single Task", function(done) {
        chai.request(server)
            .put("/api/tasks/" + addedTask._id)
            .set("authorization", 'Bearer ' + authUser.token)
            .send({
                title: 'More Stuff',
                description: 'Test Task 2',
                completed: true,
                assignedto: authUser.user._id,
                active: false,
                client: addedClient._id,
                asset: addedAsset._id,
                checklists: [{
                    name: 'Checklist 1',
                    items: [{
                        description: 'Install New Server',
                        completed: false
                    },{
                        description: 'Create User Account',
                        completed: true
                    }]
                }]
            })
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.json;
                res.should.be.a("object");
                res.body.should.have.property("_id");
                res.body._id.should.equal(addedTask._id.toString());
                res.body.should.have.property("title");
                res.body.title.should.equal("More Stuff");
                res.body.should.have.property("description");
                res.body.description.should.equal("Test Task 2");
                res.body.should.have.property("completed");
                res.body.completed.should.equal(true);
                res.body.should.have.property("completedat");
                res.body.completedat.should.exist;

                res.body.should.have.property("createdby");
                res.body.createdby.should.have.property("_id");
                res.body.createdby._id.should.equal(authUser.user._id);
                res.body.createdby.should.have.property("name");
                res.body.createdby.name.should.equal("Jane Samson");
                
                res.body.should.have.property("assignedto");
                res.body.assignedto.should.have.property("_id");
                res.body.assignedto._id.should.equal(authUser.user._id);
                res.body.assignedto.should.have.property("name");
                res.body.assignedto.name.should.equal("Jane Samson");

                res.body.should.have.property("client");
                res.body.client.should.have.property("_id");
                res.body.client._id.should.equal(addedClient._id.toString());
                res.body.client.should.have.property("name");
                res.body.client.name.should.equal("Client 1");

                res.body.should.have.property("asset");
                res.body.asset.should.have.property("_id");
                res.body.asset._id.should.equal(addedAsset._id.toString());
                res.body.asset.should.have.property("name");
                res.body.asset.name.should.equal("Asset 1");

                res.body.should.have.property("checklists");
                res.body.checklists.should.be.a("array");
                res.body.checklists.length.should.equal(1);
                res.body.checklists[0].should.have.property("name");
                res.body.checklists[0].name.should.equal("Checklist 1");
                res.body.checklists[0].should.have.property("items");
                res.body.checklists[0].items.should.be.a("array");
                res.body.checklists[0].items.length.should.equal(2);
                res.body.checklists[0].items[0].should.have.property("description");
                res.body.checklists[0].items[0].description.should.equal("Install New Server");
                res.body.checklists[0].items[0].should.have.property("completed");
                res.body.checklists[0].items[0].completed.should.equal(false);
                chai.expect(res.body.checklists[0].items[0].completedat).to.not.exist;
                chai.expect(res.body.checklists[0].items[0].completedby).to.not.exist;
                res.body.checklists[0].items[1].should.have.property("description");
                res.body.checklists[0].items[1].description.should.equal("Create User Account");
                res.body.checklists[0].items[1].should.have.property("completed");
                res.body.checklists[0].items[1].completed.should.equal(true);
                res.body.checklists[0].items[1].should.have.property("completedat");
                res.body.checklists[0].items[1].completedat.should.exist;
                res.body.checklists[0].items[1].should.have.property("completedby");
                res.body.checklists[0].items[1].completedby.should.have.property("_id");
                res.body.checklists[0].items[1].completedby._id.should.equal(authUser.user._id);
                res.body.checklists[0].items[1].completedby.should.have.property("name");
                res.body.checklists[0].items[1].completedby.name.should.equal("Jane Samson");

                res.body.should.have.property("createdat");
                res.body.createdat.should.exist;

                res.body.should.have.property("lastupdatedat");
                res.body.lastupdatedat.should.exist;

                res.body.should.have.property("lastupdatedby");
                res.body.lastupdatedby.should.have.property("_id");
                res.body.lastupdatedby._id.should.equal(authUser.user._id);
                res.body.lastupdatedby.should.have.property("name");
                res.body.lastupdatedby.name.should.equal("Jane Samson");

                res.body.should.have.property("active");
                res.body.active.should.equal(false);
                res.body.should.not.have.property("deleted");
                done();
            });
    });

    it("Should update a completed task to not completed", function(done) {
        chai.request(server)
            .put("/api/tasks/" + addedTask._id)
            .set("authorization", 'Bearer ' + authUser.token)
            .send({
                title: 'Do Stuff Here.',
                description: 'Test Task 2',
                completed: false,
                assignedto: authUser.user._id,
                active: false,
                client: addedClient._id,
                asset: addedAsset._id,
                checklists: [{
                    name: 'Checklist 1',
                    items: [{
                        description: 'Install New Server',
                        completed: false
                    },{
                        description: 'Create User Account',
                        completed: true
                    }]
                }]
            })
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.json;
                res.should.be.a("object");
                res.body.should.have.property("_id");
                res.body._id.should.equal(addedTask._id.toString());
                res.body.should.have.property("title");
                res.body.title.should.equal("Do Stuff Here.");
                res.body.should.have.property("description");
                res.body.description.should.equal("Test Task 2");
                res.body.should.have.property("completed");
                res.body.completed.should.equal(false);
                chai.expect(res.body.completedat).to.not.exist; // Just because I can't seem to check for null with should.
                
                res.body.should.have.property("createdby");
                res.body.createdby.should.have.property("_id");
                res.body.createdby._id.should.equal(authUser.user._id);
                res.body.createdby.should.have.property("name");
                res.body.createdby.name.should.equal("Jane Samson");
                
                res.body.should.have.property("assignedto");
                res.body.assignedto.should.have.property("_id");
                res.body.assignedto._id.should.equal(authUser.user._id);
                res.body.assignedto.should.have.property("name");
                res.body.assignedto.name.should.equal("Jane Samson");

                res.body.should.have.property("client");
                res.body.client.should.have.property("_id");
                res.body.client._id.should.equal(addedClient._id.toString());
                res.body.client.should.have.property("name");
                res.body.client.name.should.equal("Client 1");

                res.body.should.have.property("asset");
                res.body.asset.should.have.property("_id");
                res.body.asset._id.should.equal(addedAsset._id.toString());
                res.body.asset.should.have.property("name");
                res.body.asset.name.should.equal("Asset 1");

                res.body.should.have.property("checklists");
                res.body.checklists.should.be.a("array");
                res.body.checklists.length.should.equal(1);
                res.body.checklists[0].should.have.property("name");
                res.body.checklists[0].name.should.equal("Checklist 1");
                res.body.checklists[0].should.have.property("items");
                res.body.checklists[0].items.should.be.a("array");
                res.body.checklists[0].items.length.should.equal(2);
                res.body.checklists[0].items[0].should.have.property("description");
                res.body.checklists[0].items[0].description.should.equal("Install New Server");
                res.body.checklists[0].items[0].should.have.property("completed");
                res.body.checklists[0].items[0].completed.should.equal(false);
                res.body.checklists[0].items[1].should.have.property("description");
                res.body.checklists[0].items[1].description.should.equal("Create User Account");
                res.body.checklists[0].items[1].should.have.property("completed");
                res.body.checklists[0].items[1].completed.should.equal(true);
                
                res.body.should.have.property("createdat");
                res.body.createdat.should.exist;

                res.body.should.have.property("lastupdatedat");
                res.body.lastupdatedat.should.exist;

                res.body.should.have.property("lastupdatedby");
                res.body.lastupdatedby.should.have.property("_id");
                res.body.lastupdatedby._id.should.equal(authUser.user._id);
                res.body.lastupdatedby.should.have.property("name");
                res.body.lastupdatedby.name.should.equal("Jane Samson");

                res.body.should.have.property("active");
                res.body.active.should.equal(false);
                res.body.should.not.have.property("deleted");
                done();
            });
    });

    it("Should not update completedat if task was already completed", function(done) {
        chai.request(server)
            .post("/api/tasks")
            .set("authorization", 'Bearer ' + authUser.token)
            .send({name: 'Test Completed', completed: true, checklists: []})
            .end(function(err, res) {        
                var newTask = res.body;
                res.body.completedat.should.exist;
                
                chai.request(server)
                    .put("/api/tasks/" + newTask._id)
                    .set("authorization", 'Bearer ' + authUser.token)
                    .send(newTask)
                    .end(function(err, res) {
                        res.should.have.status(200);
                        res.should.json;
                        res.should.be.a("object");
                        res.body.completed.should.equal(true);
                        res.body.completedat.should.exist;
                        res.body.completedat.should.equal(newTask.completedat);
                        done();
                    });
        });
    });

    it("Should delete a Task", function(done) {
        chai.request(server)
            .delete("/api/tasks/" + addedTask._id)
            .set("authorization", 'Bearer ' + authUser.token)
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.json;
                res.should.be.a("object");
                
                chai.request(server)
                    .get("/api/tasks")
                    .set("authorization", 'Bearer ' + authUser.token)
                    .end(function(err, res) {
                        res.should.have.status(200);
                        res.should.json;
                        res.body.should.be.a("array");
                        res.body.length.should.equal(0);
                        done();        
                    })
            });
    });
    
    describe("Count", function() {
        it("Should get a count of all open tasks", function(done) {
            chai.request(server)
                .get("/api/tasks/count")
                .set("authorization", 'Bearer ' + authUser.token)
                .end(function(err, res) {
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.be.a("object");
                    res.body.should.have.property("count");
                    res.body.count.should.equal(1);
                    done();
            });
        });

        it("Should get a count of all completed tasks", function(done) {
            Task.create({title: 'Test Completed Task', completed: true}, function(err, task) {
                chai.request(server)
                    .get("/api/tasks/count?q=completed")
                    .set("authorization", 'Bearer ' + authUser.token)
                    .end(function(err, res) {
                        res.should.have.status(200);
                        res.should.be.json;
                        res.body.should.be.a("object");
                        res.body.should.have.property("count");
                        res.body.count.should.equal(1);
                        done();
                    });
            });
        });

        it("should get a count off all not completed tasks with client filter applied", function(done) {
            Task.create({title: 'Test Completed Task', completed: true, client: addedClient._id}, function(err, task) {
                chai.request(server)
                    .get("/api/tasks/count?client=" + addedClient.name)
                    .set("authorization", 'Bearer ' + authUser.token)
                    .end(function(err, res) {
                        res.should.have.status(200);
                        res.should.be.json;
                        res.body.should.be.a("object");
                        res.body.should.have.property("count");
                        res.body.count.should.equal(1);
                        done();
                    });
            });
        });

        it("should get a count of all completed tasks with a asset filter applied", function(done) {
            Task.create({title: 'Test Completed Task', completed: true, asset: addedAsset._id}, function(err, task) {
                chai.request(server)
                    .get("/api/tasks/count?q=completed&asset=" + addedAsset.name)
                    .set("authorization", 'Bearer ' + authUser.token)
                    .end(function(err, res) {
                        res.should.have.status(200);
                        res.should.be.json;
                        res.body.should.be.a("object");
                        res.body.should.have.property("count");
                        res.body.count.should.equal(1);

                        done();
                    });
            });
        });

        it("should get a count of all not completed tasks with created by and assignedto filters applied", function(done) {
            Task.create({title: 'Test Completed Task', completed: false, assignedto: addedTask.assignedto._id}, function(err, task) {
                chai.request(server)
                    .get("/api/tasks/count?createdby=" + addedTask.createdby.name + "&assignedto=" + addedTask.assignedto.name)
                    .set("authorization", 'Bearer ' + authUser.token)
                    .end(function(err, res) {
                        res.should.have.status(200);
                        res.should.be.json;
                        res.body.should.be.a("object");
                        res.body.should.have.property("count");
                        res.body.count.should.equal(2);
                        done();
                    });
            });
        });
    });

    describe("Paging - Default 20 items per page", function() {
        it("should return 1 page of not completed items", function(done) {
            for (i = 1; i < 25; i++) {
                Task.create({title: 'Task Item ' + i}, function(err, task) {

                });
            };

            chai.request(server)
                .get("/api/tasks")
                .set("authorization", 'Bearer ' + authUser.token)
                .end(function(err, res) {
                    // console.log(res.body);
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.be.a("array");
                    res.body.length.should.equal(20);

                    // res.body[0].should.have.property("title");
                    // res.body[0].title.should.equal("Task Item 24");

                    // res.body[19].should.have.property("title");
                    // res.body[19].title.should.equal("Task Item 5");
                    done();
                });
        });

        it("should return page 2 of not completed items", function(done) {
            for (i = 1; i < 25; i++) {
                Task.create({title: 'Task Item ' + i}, function(err, task) {

                });
            };

            chai.request(server)
                .get("/api/tasks?page=2")
                .set("authorization", 'Bearer ' + authUser.token)
                .end(function(err, res) {
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.be.a("array");
                    res.body.length.should.equal(5);
                    done();
                });
        });

        it("should return 1 page of completed items", function(done) {
            for (i = 1; i < 15; i++) {
                Task.create({title: 'Task Item ' + i, completed: false}, function(err, task) {

                });
            };

            for (i = 1; i < 10; i++) {
                Task.create({title: 'Task Item ' + i, completed: true}, function(err, task) {

                });
            };

            chai.request(server)
                .get("/api/tasks?q=completed")
                .set("authorization", 'Bearer ' + authUser.token)
                .end(function(err, res) {
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.be.a("array");
                    res.body.length.should.equal(9);
                    done();
                });
        });

        it("should return page 2 of not completed items", function(done) {
            for (i = 1; i < 25; i++) {
                Task.create({title: 'Task Item ' + i, completed: false}, function(err, task) {

                });
            };

            for (i = 1; i < 10; i++) {
                Task.create({title: 'Task Item ' + i, completed: true}, function(err, task) {

                });
            };

            chai.request(server)
                .get("/api/tasks?page=2")
                .set("authorization", 'Bearer ' + authUser.token)
                .end(function(err, res) {
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.be.a("array");
                    res.body.length.should.equal(5);
                    done();
                });
        });
    });

    describe("checklist", function() {
        it("should set completedby and completedat when completed", function(done) {
            var currentTask = addedTask;
            currentTask.checklists[0].items[0].completed = true;

            chai.request(server)
                .put("/api/tasks/" + addedTask._id)
                .set("authorization", 'Bearer ' + authUser.token)
                .send(currentTask)
                .end(function(err, res) {
                    res.should.have.status(200);
                    res.should.json;
                    res.should.be.a("object");
                    res.body.checklists[0].items[0].completed.should.equal(true);
                    res.body.checklists[0].items[0].completedat.should.exist;
                    res.body.checklists[0].items[0].completedby._id.should.equal(authUser.user._id);
                    done();
                });
        });

        it("should not update completedat at if completed was already set.", function(done) {
            var currentTask = addedTask;

            chai.request(server)
                .put("/api/tasks/" + addedTask._id)
                .set("authorization", 'Bearer ' + authUser.token)
                .send(currentTask)
                .end(function(err, res) {
                    res.should.have.status(200);
                    res.should.json;
                    res.should.be.a("object");
                    res.body.checklists[0].items[1].completed.should.equal(true);
                    res.body.checklists[0].items[1].completedat.should.exist;
                    res.body.checklists[0].items[1].completedat.should.equal(addedTask.checklists[0].items[1].completedat);
                    res.body.checklists[0].items[1].completedby._id.should.equal(addedTask.checklists[0].items[1].completedby._id);
                    done();
                });
        });

        it("should set completedby and completedat when completed", function(done) {
            var currentTask = addedTask;
            currentTask.checklists[0].items[1].completed = false;

            chai.request(server)
                .put("/api/tasks/" + addedTask._id)
                .set("authorization", 'Bearer ' + authUser.token)
                .send(currentTask)
                .end(function(err, res) {
                    res.should.have.status(200);
                    res.should.json;
                    res.should.be.a("object");
                    res.body.checklists[0].items[1].completed.should.equal(false);
                    chai.expect(res.body.checklists[0].items[1].completedat).to.not.exist;
                    chai.expect(res.body.checklists[0].items[1].completedby).to.not.exist;
                    done();
                });
        });
    });

    // Should make this Activities later, when I start to record other things.
    describe("Comments", function() {
        var addedActivities = []
        var addedTaskWithActivity;
        
        Activity.collection.drop();

        beforeEach(function(done) {
            addedActivities = [];

            Activity.create({details: 'Comment #1', type: 'COMMENT'}, function(err, activity) {
                addedActivities.push(activity);

                Activity.create({details: 'Set Label', type: 'LABEL'}, function(err, labelActivity) {
                    addedActivities.push(labelActivity);

                    Task.create({title: 'New Task', description: 'One Task here.', activities: addedActivities}, function(err, task) {
                        addedTaskWithActivity = task;
                        done();
                    });
                });
            });
        });

        afterEach(function(done) {
            Activity.collection.drop();
            done();
        });

        it('should get all comments for a task', function(done) {
            chai.request(server)
                .get("/api/tasks/" + addedTaskWithActivity._id + '/comments')
                .set("authorization", 'Bearer ' + authUser.token)
                .end(function(err, res) {
                    res.should.have.status(200);
                    res.should.json;
                    res.body.should.be.a("array");
                    res.body.length.should.equal(1);
                    res.body[0].should.have.property("details");
                    res.body[0].details.should.equal("Comment #1");
                    res.body[0].should.have.property("type");
                    res.body[0].type.should.equal("COMMENT");
                    done();
                });
        });

        it('should add a new comment', function(done) {
            chai.request(server)
                .post("/api/tasks/" + addedTaskWithActivity._id + '/comments')
                .send({details: 'New Comment', type: 'COMMENT'})
                .set("authorization", 'Bearer ' + authUser.token)
                .end(function(err, res) {
                    res.should.have.status(200);
                    res.should.json;
                    res.body.should.be.a("object");
                    res.body.should.have.property("_id");
                    res.body.should.have.property("details");
                    res.body.details.should.equal("New Comment");
                    res.body.should.have.property("type");
                    res.body.type.should.equal("COMMENT");

                    res.body.should.have.property("createdat");
                    res.body.createdat.should.exist;
                    
                    res.body.should.have.property("createdby");
                    res.body.createdby.should.equal(authUser.user._id)

                    res.body.should.have.property("lastupdatedby");
                    res.body.lastupdatedby.should.equal(authUser.user._id);

                    res.body.should.not.have.property("deleted");

                    chai.request(server)
                        .get("/api/tasks/" + addedTaskWithActivity._id + '/comments')
                        .set("authorization", 'Bearer ' + authUser.token)
                        .end(function(err, res) {
                            res.should.have.status(200);
                            res.should.json;
                            res.body.should.be.a("array");
                            res.body.length.should.equal(2);
                            res.body[1].should.have.property("details");
                            res.body[1].details.should.equal("New Comment");
                            res.body[1].should.have.property("type");
                            res.body[1].type.should.equal("COMMENT");
                            done();
                        });
                });
        });

        it('should update a comment', function(done) {
            chai.request(server)
                .put("/api/tasks/" + addedTaskWithActivity._id + '/comments/' + addedActivities[0]._id)
                .send({details: 'Updated Comment'})
                .set("authorization", 'Bearer ' + authUser.token)
                .end(function(err, res) {
                    res.should.have.status(200);
                    res.should.json;
                    res.body.should.be.a("object");
                    res.body.should.have.property("_id");
                    res.body.should.have.property("details");
                    res.body.details.should.equal("Updated Comment");
                    res.body.should.have.property("type");
                    res.body.type.should.equal("COMMENT");
                    res.body.should.not.have.property("deleted");
                    done();
                });
        });

        it('should delete a comment', function(done) {
            chai.request(server)
                .delete("/api/tasks/" + addedTaskWithActivity._id + '/comments/' + addedActivities[0]._id)
                .set("authorization", 'Bearer ' + authUser.token)
                .end(function(err, res) {
                    res.should.have.status(200);
                    res.should.json;
                    res.body.should.be.a("object");
                    res.body.should.have.property("message");
                    res.body.message.should.equal("Comment Deleted");
                    
                    chai.request(server)
                        .get("/api/tasks/" + addedTaskWithActivity._id + '/comments')
                        .set("authorization", 'Bearer ' + authUser.token)
                        .end(function(err, res) {
                            res.should.have.status(200);
                            res.should.json;
                            res.body.should.be.a("array");
                            res.body.length.should.equal(0);
                            done();
                        });
                });
        });

        it('should get a task with activities, ensure only non deleted ones show', function(done) {
            // addedActivities = [];

            Activity.create({details: 'Comment #2', type: 'COMMENT', deleted: true}, function(err, activity) {
                addedActivities.push(activity);

                Activity.create({details: 'Set Label 2', type: 'LABEL'}, function(err, labelActivity) {
                    addedActivities.push(labelActivity);

                    Task.create({title: 'New Task', description: 'One Task here.', activities: addedActivities}, function(err, task) {
                        addedTaskWithActivity = task;

                        chai.request(server)
                            .get("/api/tasks/" + addedTaskWithActivity._id)
                            .set("authorization", 'Bearer ' + authUser.token)
                            .end(function(err, res) {
                                res.should.have.status(200);
                                res.should.json;
                                res.body.should.be.a("object");
                                res.body.should.have.property("activities");
                                res.body.activities.should.be.a("array");
                                res.body.activities.length.should.equal(3);
                                done();
                            });
                    });
                });
            });



        });
    });
});