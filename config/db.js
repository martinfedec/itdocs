var env = process.env.NODE_ENV || "development";

if (env === "production" || env === "development")
	url = process.env.ITDOCS_URI || "mongodb://localhost:27017/itdocs";
else if (env ==="test")
	url = process.env.ITDOCS_TEST_URI || "mongodb://localhost:27017/itdocs_test";

module.exports = url